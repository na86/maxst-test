﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.WWW::.ctor(System.String)
extern void WWW__ctor_mE77AD6C372CC76F48A893C5E2F91A81193A9F8C5 (void);
// 0x00000002 System.Byte[] UnityEngine.WWW::get_bytes()
extern void WWW_get_bytes_m378FCCD8E91FB7FE7FA22E05BA3FE528CD7EAF1A (void);
// 0x00000003 System.String UnityEngine.WWW::get_error()
extern void WWW_get_error_mB278F5EC90EF99FEF70D80112940CFB49E79C9BC (void);
// 0x00000004 UnityEngine.Texture2D UnityEngine.WWW::CreateTextureFromDownloadedData(System.Boolean)
extern void WWW_CreateTextureFromDownloadedData_m978F4A746B24EEA580E7AA8F1F05203196A299F9 (void);
// 0x00000005 UnityEngine.Texture2D UnityEngine.WWW::get_texture()
extern void WWW_get_texture_mC23FF88883698F3E6F7BED2733A2DB3B18F788E4 (void);
// 0x00000006 System.String UnityEngine.WWW::get_url()
extern void WWW_get_url_m1D75D492D78A7AA8F607C5D7700497B8FE5E9526 (void);
// 0x00000007 System.Boolean UnityEngine.WWW::get_keepWaiting()
extern void WWW_get_keepWaiting_m231A6A7A835610182D78FC414665CC75195ABD70 (void);
// 0x00000008 System.Void UnityEngine.WWW::Dispose()
extern void WWW_Dispose_mF5A8B944281564903043545BC1E7F1CAD941519F (void);
// 0x00000009 System.Boolean UnityEngine.WWW::WaitUntilDoneIfPossible()
extern void WWW_WaitUntilDoneIfPossible_m8D6B638F661CBD13B442F392BF42F5C9BDF0E84D (void);
static Il2CppMethodPointer s_methodPointers[9] = 
{
	WWW__ctor_mE77AD6C372CC76F48A893C5E2F91A81193A9F8C5,
	WWW_get_bytes_m378FCCD8E91FB7FE7FA22E05BA3FE528CD7EAF1A,
	WWW_get_error_mB278F5EC90EF99FEF70D80112940CFB49E79C9BC,
	WWW_CreateTextureFromDownloadedData_m978F4A746B24EEA580E7AA8F1F05203196A299F9,
	WWW_get_texture_mC23FF88883698F3E6F7BED2733A2DB3B18F788E4,
	WWW_get_url_m1D75D492D78A7AA8F607C5D7700497B8FE5E9526,
	WWW_get_keepWaiting_m231A6A7A835610182D78FC414665CC75195ABD70,
	WWW_Dispose_mF5A8B944281564903043545BC1E7F1CAD941519F,
	WWW_WaitUntilDoneIfPossible_m8D6B638F661CBD13B442F392BF42F5C9BDF0E84D,
};
static const int32_t s_InvokerIndices[9] = 
{
	26,
	14,
	14,
	313,
	14,
	14,
	102,
	23,
	102,
};
extern const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule = 
{
	"UnityEngine.UnityWebRequestWWWModule.dll",
	9,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
