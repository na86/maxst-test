﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String JsonFx.Json.JsonSpecifiedPropertyAttribute::get_SpecifiedProperty()
extern void JsonSpecifiedPropertyAttribute_get_SpecifiedProperty_m4C4091A988D27D92F00ACA63616BA9492D3E575E (void);
// 0x00000002 System.String JsonFx.Json.JsonSpecifiedPropertyAttribute::GetJsonSpecifiedProperty(System.Reflection.MemberInfo)
extern void JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m6AE9FEE2B4586FAE782C1D81965839928E695756 (void);
// 0x00000003 System.Void JsonFx.Json.JsonReader::.ctor(System.String)
extern void JsonReader__ctor_m11F3DFB3890D5D40A0CEE08302D02FDB42B5D758 (void);
// 0x00000004 System.Void JsonFx.Json.JsonReader::.ctor(System.String,JsonFx.Json.JsonReaderSettings)
extern void JsonReader__ctor_mC101684B8A4A69A26E6CEF5F5CA5270B8E66B181 (void);
// 0x00000005 T JsonFx.Json.JsonReader::Deserialize()
// 0x00000006 System.Object JsonFx.Json.JsonReader::Deserialize(System.Int32,System.Type)
extern void JsonReader_Deserialize_m951BF5F01E20FFF5779CB9C3C328E7D0DCB87C46 (void);
// 0x00000007 System.Object JsonFx.Json.JsonReader::Read(System.Type,System.Boolean)
extern void JsonReader_Read_m8BB18D8A185FBCAA88FD6993135FAB71E0A9AF61 (void);
// 0x00000008 System.Object JsonFx.Json.JsonReader::ReadObject(System.Type)
extern void JsonReader_ReadObject_mADAFA35185E386962DE35CAFC58C259EC7C8C150 (void);
// 0x00000009 System.Collections.IEnumerable JsonFx.Json.JsonReader::ReadArray(System.Type)
extern void JsonReader_ReadArray_mD5354FFD05A1CEB40E65E320C37A02EDEC348C19 (void);
// 0x0000000A System.String JsonFx.Json.JsonReader::ReadUnquotedKey()
extern void JsonReader_ReadUnquotedKey_mFC30C4C9AE2203D86DB3730DC1797AA529665307 (void);
// 0x0000000B System.Object JsonFx.Json.JsonReader::ReadString(System.Type)
extern void JsonReader_ReadString_m3538B71E0A53407042260CDF9E9F1DC31DDC94C9 (void);
// 0x0000000C System.Object JsonFx.Json.JsonReader::ReadNumber(System.Type)
extern void JsonReader_ReadNumber_m5E395844F25FFA83F085FCE42462835A604F53AD (void);
// 0x0000000D T JsonFx.Json.JsonReader::Deserialize(System.String)
// 0x0000000E System.Object JsonFx.Json.JsonReader::Deserialize(System.String,System.Int32,System.Type)
extern void JsonReader_Deserialize_mE079731EA6443417C7E7E86EFA558F7BE87B8D40 (void);
// 0x0000000F JsonFx.Json.JsonToken JsonFx.Json.JsonReader::Tokenize()
extern void JsonReader_Tokenize_mA8B3C1734C68055C9176FC191F27BB40EED242AA (void);
// 0x00000010 JsonFx.Json.JsonToken JsonFx.Json.JsonReader::Tokenize(System.Boolean)
extern void JsonReader_Tokenize_mDE3053B97710E04BA6B0EF2EC794EE244F5EE783 (void);
// 0x00000011 System.Boolean JsonFx.Json.JsonReader::MatchLiteral(System.String)
extern void JsonReader_MatchLiteral_mC93AD2F868E3FD779F2D7BC1B40703551FAD7015 (void);
// 0x00000012 System.Void JsonFx.Json.JsonWriter::.ctor(System.Text.StringBuilder)
extern void JsonWriter__ctor_mA9484BE39BC4B9F034CCE73F1BA0A46FD9784119 (void);
// 0x00000013 System.Void JsonFx.Json.JsonWriter::.ctor(System.Text.StringBuilder,JsonFx.Json.JsonWriterSettings)
extern void JsonWriter__ctor_mD9979EE32F117B703CBB2211AFB8A281503C8B0E (void);
// 0x00000014 System.String JsonFx.Json.JsonWriter::Serialize(System.Object)
extern void JsonWriter_Serialize_mBD7B71600ADED7452CDE4B2E501911D7BDB30021 (void);
// 0x00000015 System.Void JsonFx.Json.JsonWriter::Write(System.Object)
extern void JsonWriter_Write_mD6334093778A0EB61BCBA96D488BC935297561FF (void);
// 0x00000016 System.Void JsonFx.Json.JsonWriter::Write(System.Object,System.Boolean)
extern void JsonWriter_Write_m1B98CA48D2EA650727ADBFC7683C4F3E73ED02F2 (void);
// 0x00000017 System.Void JsonFx.Json.JsonWriter::Write(System.DateTime)
extern void JsonWriter_Write_mBBF77CF44438A59B467943416F2AE12EE9CBC51E (void);
// 0x00000018 System.Void JsonFx.Json.JsonWriter::Write(System.Guid)
extern void JsonWriter_Write_m83E2A3ABF03DC4FEE3E95A73ACE09134CF14C9ED (void);
// 0x00000019 System.Void JsonFx.Json.JsonWriter::Write(System.Enum)
extern void JsonWriter_Write_mD1DC9FE5BA27F1C6D79EBAF2D275BA7C40A2BC99 (void);
// 0x0000001A System.Void JsonFx.Json.JsonWriter::Write(System.String)
extern void JsonWriter_Write_m0E7B6DB381914B60420201FAC8F891AB8B963325 (void);
// 0x0000001B System.Void JsonFx.Json.JsonWriter::Write(System.Boolean)
extern void JsonWriter_Write_m63EE97E7B43CEAB20709511FE96358E36FFCA3F2 (void);
// 0x0000001C System.Void JsonFx.Json.JsonWriter::Write(System.Byte)
extern void JsonWriter_Write_mF283D06B8A27A5F9D896907CF889529FE36D5C48 (void);
// 0x0000001D System.Void JsonFx.Json.JsonWriter::Write(System.SByte)
extern void JsonWriter_Write_mBE71BA37CFBBDB39E02E1FFBD4D3B71D99B1ADF3 (void);
// 0x0000001E System.Void JsonFx.Json.JsonWriter::Write(System.Int16)
extern void JsonWriter_Write_m2ACAE47CC418C51CC60EBAEDB4931D0795ACA05B (void);
// 0x0000001F System.Void JsonFx.Json.JsonWriter::Write(System.UInt16)
extern void JsonWriter_Write_m462A4DDC01E1862ED4164320581D5D740324FF51 (void);
// 0x00000020 System.Void JsonFx.Json.JsonWriter::Write(System.Int32)
extern void JsonWriter_Write_m373FAEE608CF68542EAA1FDE2984C8C23646B4B4 (void);
// 0x00000021 System.Void JsonFx.Json.JsonWriter::Write(System.UInt32)
extern void JsonWriter_Write_mC84C1111575984D8E22AAA991C2F451BBAD12F1A (void);
// 0x00000022 System.Void JsonFx.Json.JsonWriter::Write(System.Int64)
extern void JsonWriter_Write_m71D382EEE581904994ADA24EC129EA19EBC70AF6 (void);
// 0x00000023 System.Void JsonFx.Json.JsonWriter::Write(System.UInt64)
extern void JsonWriter_Write_m6A78236363B690A4EEAAC38AEE64BE3FE7923EC2 (void);
// 0x00000024 System.Void JsonFx.Json.JsonWriter::Write(System.Single)
extern void JsonWriter_Write_m761042176C1159637C317760C8B9242A6202384E (void);
// 0x00000025 System.Void JsonFx.Json.JsonWriter::Write(System.Double)
extern void JsonWriter_Write_mD63305F659DC2ABBB5F3BBAE462F766A75DDE5B5 (void);
// 0x00000026 System.Void JsonFx.Json.JsonWriter::Write(System.Decimal)
extern void JsonWriter_Write_mAA99A5533DCC6ABB8B90B891249113015B3A95B5 (void);
// 0x00000027 System.Void JsonFx.Json.JsonWriter::Write(System.Char)
extern void JsonWriter_Write_mD5FC344964FE5ED2A019F57EE162F8BD91E9E905 (void);
// 0x00000028 System.Void JsonFx.Json.JsonWriter::Write(System.TimeSpan)
extern void JsonWriter_Write_mC181B79F63D467870101B93B8793308F8674894A (void);
// 0x00000029 System.Void JsonFx.Json.JsonWriter::Write(System.Uri)
extern void JsonWriter_Write_m7A1C6FD689C33181EADF22A416F81C21FA6D773A (void);
// 0x0000002A System.Void JsonFx.Json.JsonWriter::Write(System.Version)
extern void JsonWriter_Write_m2830986E1601F2FEFD8E11B7615E8909F130F919 (void);
// 0x0000002B System.Void JsonFx.Json.JsonWriter::Write(System.Xml.XmlNode)
extern void JsonWriter_Write_m2997D0CA33DADBA871444510CA656883E9B331BC (void);
// 0x0000002C System.Void JsonFx.Json.JsonWriter::WriteArray(System.Collections.IEnumerable)
extern void JsonWriter_WriteArray_mAD564BE45956FA926169D9CC79C50290CDEDC955 (void);
// 0x0000002D System.Void JsonFx.Json.JsonWriter::WriteArrayItem(System.Object)
extern void JsonWriter_WriteArrayItem_m0C0D42ABD171D7EF774378488680BE94C046DD39 (void);
// 0x0000002E System.Void JsonFx.Json.JsonWriter::WriteObject(System.Collections.IDictionary)
extern void JsonWriter_WriteObject_m4DAFEE37C35E52E507D47A1E32F187D9F6A279BF (void);
// 0x0000002F System.Void JsonFx.Json.JsonWriter::WriteDictionary(System.Collections.IEnumerable)
extern void JsonWriter_WriteDictionary_mC916150EFE7F63F909E6EA08F23FDF326AE29A3F (void);
// 0x00000030 System.Void JsonFx.Json.JsonWriter::WriteObjectProperty(System.String,System.Object)
extern void JsonWriter_WriteObjectProperty_m61C6BEDC4523325AA4E499A474E0982AB31EB90A (void);
// 0x00000031 System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyName(System.String)
extern void JsonWriter_WriteObjectPropertyName_mEE5E2CBA09FC078A1A40B569EB133B67BAEEA746 (void);
// 0x00000032 System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyValue(System.Object)
extern void JsonWriter_WriteObjectPropertyValue_m50EAA6EB7B71C2CA95946E5021C1C71BDF14F9EB (void);
// 0x00000033 System.Void JsonFx.Json.JsonWriter::WriteObject(System.Object,System.Type)
extern void JsonWriter_WriteObject_mF51392442FAAABE2FC4BB90475D350B3AF86C317 (void);
// 0x00000034 System.Void JsonFx.Json.JsonWriter::WriteArrayItemDelim()
extern void JsonWriter_WriteArrayItemDelim_m0AD0B96CDD475AE9FDA7B56A6107232F0DF7093D (void);
// 0x00000035 System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyDelim()
extern void JsonWriter_WriteObjectPropertyDelim_mD96ECEF28D3DE490A08E103C021FF37E1A56F8A6 (void);
// 0x00000036 System.Void JsonFx.Json.JsonWriter::WriteLine()
extern void JsonWriter_WriteLine_m13D006E42C1879825FC33FE6C6B6D9BB45039C22 (void);
// 0x00000037 System.Boolean JsonFx.Json.JsonWriter::IsIgnored(System.Type,System.Reflection.MemberInfo,System.Object)
extern void JsonWriter_IsIgnored_m496E680C8948DEFA840D81F147F842B66CD9C352 (void);
// 0x00000038 System.Boolean JsonFx.Json.JsonWriter::IsDefaultValue(System.Reflection.MemberInfo,System.Object)
extern void JsonWriter_IsDefaultValue_mD64F653FADA9DBF85859A06610719BB29398B247 (void);
// 0x00000039 System.Enum[] JsonFx.Json.JsonWriter::GetFlagList(System.Type,System.Object)
extern void JsonWriter_GetFlagList_mBC92700748D9BB20950D607E2F794A3DF9F96FED (void);
// 0x0000003A System.Boolean JsonFx.Json.JsonWriter::InvalidIeee754(System.Decimal)
extern void JsonWriter_InvalidIeee754_m2A0397B3053EF2E80C2EA45135A450F261246133 (void);
// 0x0000003B System.Void JsonFx.Json.JsonWriter::System.IDisposable.Dispose()
extern void JsonWriter_System_IDisposable_Dispose_mE700D80165F6E78191DCF5C9E22CC4612FB610A5 (void);
// 0x0000003C System.Void JsonFx.Json.JsonSerializationException::.ctor(System.String)
extern void JsonSerializationException__ctor_m924DCFA746CAD7D13669E21FD002578D2C964D8F (void);
// 0x0000003D System.Void JsonFx.Json.JsonDeserializationException::.ctor(System.String,System.Int32)
extern void JsonDeserializationException__ctor_m6CB6CE760A45B6F8A575226C636EF88494DFCFD9 (void);
// 0x0000003E System.Void JsonFx.Json.JsonTypeCoercionException::.ctor(System.String)
extern void JsonTypeCoercionException__ctor_m8462986BCBD49CB81C5FD14BA9914424970143FF (void);
// 0x0000003F System.Void JsonFx.Json.JsonTypeCoercionException::.ctor(System.String,System.Exception)
extern void JsonTypeCoercionException__ctor_m13F7AE52747D12D5DF4860CC206584EB94C0D10F (void);
// 0x00000040 System.Boolean JsonFx.Json.JsonReaderSettings::get_AllowUnquotedObjectKeys()
extern void JsonReaderSettings_get_AllowUnquotedObjectKeys_mD92DED494E026C0384A6A2F526D72CE073904856 (void);
// 0x00000041 System.Boolean JsonFx.Json.JsonReaderSettings::IsTypeHintName(System.String)
extern void JsonReaderSettings_IsTypeHintName_m9D2AB984D32E684E49E994B1B24A57A5DD4CCDCF (void);
// 0x00000042 System.Void JsonFx.Json.JsonReaderSettings::.ctor()
extern void JsonReaderSettings__ctor_m457A121B1DE3B7BB99A7FFF3C09C905FAE413D22 (void);
// 0x00000043 System.Void JsonFx.Json.WriteDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x00000044 System.Void JsonFx.Json.WriteDelegate`1::Invoke(JsonFx.Json.JsonWriter,T)
// 0x00000045 System.IAsyncResult JsonFx.Json.WriteDelegate`1::BeginInvoke(JsonFx.Json.JsonWriter,T,System.AsyncCallback,System.Object)
// 0x00000046 System.Void JsonFx.Json.WriteDelegate`1::EndInvoke(System.IAsyncResult)
// 0x00000047 System.String JsonFx.Json.JsonWriterSettings::get_TypeHintName()
extern void JsonWriterSettings_get_TypeHintName_m293F7A950D16C8D78E9767F1EC023234652DB9E3 (void);
// 0x00000048 System.Boolean JsonFx.Json.JsonWriterSettings::get_PrettyPrint()
extern void JsonWriterSettings_get_PrettyPrint_m0923AE2C76EFE8D4BD1EDCFF64C690FA437A5B1C (void);
// 0x00000049 System.String JsonFx.Json.JsonWriterSettings::get_Tab()
extern void JsonWriterSettings_get_Tab_m3D7EF63500FB39E41D1E3AFD1DE8E06BEF636AB2 (void);
// 0x0000004A System.String JsonFx.Json.JsonWriterSettings::get_NewLine()
extern void JsonWriterSettings_get_NewLine_mE88CF4F43FF1662A840FB60AF1F5E7B1545B59D6 (void);
// 0x0000004B System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth()
extern void JsonWriterSettings_get_MaxDepth_m0119E0BB77B06F20E38994C97D8056FA63340D28 (void);
// 0x0000004C System.Boolean JsonFx.Json.JsonWriterSettings::get_UseXmlSerializationAttributes()
extern void JsonWriterSettings_get_UseXmlSerializationAttributes_m9360224AEBF9D17CB0E8F3A55B7272047F10E57F (void);
// 0x0000004D JsonFx.Json.WriteDelegate`1<System.DateTime> JsonFx.Json.JsonWriterSettings::get_DateTimeSerializer()
extern void JsonWriterSettings_get_DateTimeSerializer_mF4948BC3020F71A92AE845FA46FD2044790CEF42 (void);
// 0x0000004E System.Void JsonFx.Json.JsonWriterSettings::.ctor()
extern void JsonWriterSettings__ctor_mF25D8130149490723BE9DBB218805F92FB8F15C2 (void);
// 0x0000004F System.String JsonFx.Json.JsonNameAttribute::get_Name()
extern void JsonNameAttribute_get_Name_m1236C8BDC86133590DBC71E5EFE56A670934F8BF (void);
// 0x00000050 System.String JsonFx.Json.JsonNameAttribute::GetJsonName(System.Object)
extern void JsonNameAttribute_GetJsonName_m0DC39167EFDB4DBC418672EDEDF61EE699991816 (void);
// 0x00000051 System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>> JsonFx.Json.TypeCoercionUtility::get_MemberMapCache()
extern void TypeCoercionUtility_get_MemberMapCache_mFADEEED7078BD11E7D14581A9E0D4B60137B9DCD (void);
// 0x00000052 System.Object JsonFx.Json.TypeCoercionUtility::ProcessTypeHint(System.Collections.IDictionary,System.String,System.Type&,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern void TypeCoercionUtility_ProcessTypeHint_mBA8E900A04169F8038592676F4D330238931D430 (void);
// 0x00000053 System.Object JsonFx.Json.TypeCoercionUtility::InstantiateObject(System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern void TypeCoercionUtility_InstantiateObject_mE427F4669535431D4E89837E263ABFAE53A81232 (void);
// 0x00000054 System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo> JsonFx.Json.TypeCoercionUtility::CreateMemberMap(System.Type)
extern void TypeCoercionUtility_CreateMemberMap_m15ACB425946B5C3FB88D1C890EE7DAAB02E190F0 (void);
// 0x00000055 System.Type JsonFx.Json.TypeCoercionUtility::GetMemberInfo(System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>,System.String,System.Reflection.MemberInfo&)
extern void TypeCoercionUtility_GetMemberInfo_m7FB91C12B11E1BBE35F81A1DE7AE671900543085 (void);
// 0x00000056 System.Void JsonFx.Json.TypeCoercionUtility::SetMemberValue(System.Object,System.Type,System.Reflection.MemberInfo,System.Object)
extern void TypeCoercionUtility_SetMemberValue_m08E6FBEB98501CAA8CAE46CBEF13C16200D81A3E (void);
// 0x00000057 System.Object JsonFx.Json.TypeCoercionUtility::CoerceType(System.Type,System.Object)
extern void TypeCoercionUtility_CoerceType_m9A999904581022FE82F15AE90B24E71B88D04C95 (void);
// 0x00000058 System.Object JsonFx.Json.TypeCoercionUtility::CoerceType(System.Type,System.Collections.IDictionary,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern void TypeCoercionUtility_CoerceType_m6F37453AF3A2D3C809F9C8D912DB658449FB384D (void);
// 0x00000059 System.Object JsonFx.Json.TypeCoercionUtility::CoerceList(System.Type,System.Type,System.Collections.IEnumerable)
extern void TypeCoercionUtility_CoerceList_m7B98F0DACCC3A634317D7B6E77A19E43ECDCC232 (void);
// 0x0000005A System.Array JsonFx.Json.TypeCoercionUtility::CoerceArray(System.Type,System.Collections.IEnumerable)
extern void TypeCoercionUtility_CoerceArray_m8D8D97F3B13BDD85AFD56BAAE4C57948230944AE (void);
// 0x0000005B System.Boolean JsonFx.Json.TypeCoercionUtility::IsNullable(System.Type)
extern void TypeCoercionUtility_IsNullable_m60FC45054D716CD78C92D3263468852329A94E9D (void);
// 0x0000005C System.Void JsonFx.Json.TypeCoercionUtility::.ctor()
extern void TypeCoercionUtility__ctor_mF228344278BBDC05F1E76B5641A6BA8FF3D714E9 (void);
// 0x0000005D System.Void JsonFx.Json.IJsonSerializable::WriteJson(JsonFx.Json.JsonWriter)
// 0x0000005E System.Boolean JsonFx.Json.JsonIgnoreAttribute::IsJsonIgnore(System.Object)
extern void JsonIgnoreAttribute_IsJsonIgnore_m6BC8749926601C94054AD637DE846CC88A95665E (void);
// 0x0000005F System.Boolean JsonFx.Json.JsonIgnoreAttribute::IsXmlIgnore(System.Object)
extern void JsonIgnoreAttribute_IsXmlIgnore_m925805A1EC6E7D75C96DD18AA762BFA83FE0AB53 (void);
static Il2CppMethodPointer s_methodPointers[95] = 
{
	JsonSpecifiedPropertyAttribute_get_SpecifiedProperty_m4C4091A988D27D92F00ACA63616BA9492D3E575E,
	JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m6AE9FEE2B4586FAE782C1D81965839928E695756,
	JsonReader__ctor_m11F3DFB3890D5D40A0CEE08302D02FDB42B5D758,
	JsonReader__ctor_mC101684B8A4A69A26E6CEF5F5CA5270B8E66B181,
	NULL,
	JsonReader_Deserialize_m951BF5F01E20FFF5779CB9C3C328E7D0DCB87C46,
	JsonReader_Read_m8BB18D8A185FBCAA88FD6993135FAB71E0A9AF61,
	JsonReader_ReadObject_mADAFA35185E386962DE35CAFC58C259EC7C8C150,
	JsonReader_ReadArray_mD5354FFD05A1CEB40E65E320C37A02EDEC348C19,
	JsonReader_ReadUnquotedKey_mFC30C4C9AE2203D86DB3730DC1797AA529665307,
	JsonReader_ReadString_m3538B71E0A53407042260CDF9E9F1DC31DDC94C9,
	JsonReader_ReadNumber_m5E395844F25FFA83F085FCE42462835A604F53AD,
	NULL,
	JsonReader_Deserialize_mE079731EA6443417C7E7E86EFA558F7BE87B8D40,
	JsonReader_Tokenize_mA8B3C1734C68055C9176FC191F27BB40EED242AA,
	JsonReader_Tokenize_mDE3053B97710E04BA6B0EF2EC794EE244F5EE783,
	JsonReader_MatchLiteral_mC93AD2F868E3FD779F2D7BC1B40703551FAD7015,
	JsonWriter__ctor_mA9484BE39BC4B9F034CCE73F1BA0A46FD9784119,
	JsonWriter__ctor_mD9979EE32F117B703CBB2211AFB8A281503C8B0E,
	JsonWriter_Serialize_mBD7B71600ADED7452CDE4B2E501911D7BDB30021,
	JsonWriter_Write_mD6334093778A0EB61BCBA96D488BC935297561FF,
	JsonWriter_Write_m1B98CA48D2EA650727ADBFC7683C4F3E73ED02F2,
	JsonWriter_Write_mBBF77CF44438A59B467943416F2AE12EE9CBC51E,
	JsonWriter_Write_m83E2A3ABF03DC4FEE3E95A73ACE09134CF14C9ED,
	JsonWriter_Write_mD1DC9FE5BA27F1C6D79EBAF2D275BA7C40A2BC99,
	JsonWriter_Write_m0E7B6DB381914B60420201FAC8F891AB8B963325,
	JsonWriter_Write_m63EE97E7B43CEAB20709511FE96358E36FFCA3F2,
	JsonWriter_Write_mF283D06B8A27A5F9D896907CF889529FE36D5C48,
	JsonWriter_Write_mBE71BA37CFBBDB39E02E1FFBD4D3B71D99B1ADF3,
	JsonWriter_Write_m2ACAE47CC418C51CC60EBAEDB4931D0795ACA05B,
	JsonWriter_Write_m462A4DDC01E1862ED4164320581D5D740324FF51,
	JsonWriter_Write_m373FAEE608CF68542EAA1FDE2984C8C23646B4B4,
	JsonWriter_Write_mC84C1111575984D8E22AAA991C2F451BBAD12F1A,
	JsonWriter_Write_m71D382EEE581904994ADA24EC129EA19EBC70AF6,
	JsonWriter_Write_m6A78236363B690A4EEAAC38AEE64BE3FE7923EC2,
	JsonWriter_Write_m761042176C1159637C317760C8B9242A6202384E,
	JsonWriter_Write_mD63305F659DC2ABBB5F3BBAE462F766A75DDE5B5,
	JsonWriter_Write_mAA99A5533DCC6ABB8B90B891249113015B3A95B5,
	JsonWriter_Write_mD5FC344964FE5ED2A019F57EE162F8BD91E9E905,
	JsonWriter_Write_mC181B79F63D467870101B93B8793308F8674894A,
	JsonWriter_Write_m7A1C6FD689C33181EADF22A416F81C21FA6D773A,
	JsonWriter_Write_m2830986E1601F2FEFD8E11B7615E8909F130F919,
	JsonWriter_Write_m2997D0CA33DADBA871444510CA656883E9B331BC,
	JsonWriter_WriteArray_mAD564BE45956FA926169D9CC79C50290CDEDC955,
	JsonWriter_WriteArrayItem_m0C0D42ABD171D7EF774378488680BE94C046DD39,
	JsonWriter_WriteObject_m4DAFEE37C35E52E507D47A1E32F187D9F6A279BF,
	JsonWriter_WriteDictionary_mC916150EFE7F63F909E6EA08F23FDF326AE29A3F,
	JsonWriter_WriteObjectProperty_m61C6BEDC4523325AA4E499A474E0982AB31EB90A,
	JsonWriter_WriteObjectPropertyName_mEE5E2CBA09FC078A1A40B569EB133B67BAEEA746,
	JsonWriter_WriteObjectPropertyValue_m50EAA6EB7B71C2CA95946E5021C1C71BDF14F9EB,
	JsonWriter_WriteObject_mF51392442FAAABE2FC4BB90475D350B3AF86C317,
	JsonWriter_WriteArrayItemDelim_m0AD0B96CDD475AE9FDA7B56A6107232F0DF7093D,
	JsonWriter_WriteObjectPropertyDelim_mD96ECEF28D3DE490A08E103C021FF37E1A56F8A6,
	JsonWriter_WriteLine_m13D006E42C1879825FC33FE6C6B6D9BB45039C22,
	JsonWriter_IsIgnored_m496E680C8948DEFA840D81F147F842B66CD9C352,
	JsonWriter_IsDefaultValue_mD64F653FADA9DBF85859A06610719BB29398B247,
	JsonWriter_GetFlagList_mBC92700748D9BB20950D607E2F794A3DF9F96FED,
	JsonWriter_InvalidIeee754_m2A0397B3053EF2E80C2EA45135A450F261246133,
	JsonWriter_System_IDisposable_Dispose_mE700D80165F6E78191DCF5C9E22CC4612FB610A5,
	JsonSerializationException__ctor_m924DCFA746CAD7D13669E21FD002578D2C964D8F,
	JsonDeserializationException__ctor_m6CB6CE760A45B6F8A575226C636EF88494DFCFD9,
	JsonTypeCoercionException__ctor_m8462986BCBD49CB81C5FD14BA9914424970143FF,
	JsonTypeCoercionException__ctor_m13F7AE52747D12D5DF4860CC206584EB94C0D10F,
	JsonReaderSettings_get_AllowUnquotedObjectKeys_mD92DED494E026C0384A6A2F526D72CE073904856,
	JsonReaderSettings_IsTypeHintName_m9D2AB984D32E684E49E994B1B24A57A5DD4CCDCF,
	JsonReaderSettings__ctor_m457A121B1DE3B7BB99A7FFF3C09C905FAE413D22,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonWriterSettings_get_TypeHintName_m293F7A950D16C8D78E9767F1EC023234652DB9E3,
	JsonWriterSettings_get_PrettyPrint_m0923AE2C76EFE8D4BD1EDCFF64C690FA437A5B1C,
	JsonWriterSettings_get_Tab_m3D7EF63500FB39E41D1E3AFD1DE8E06BEF636AB2,
	JsonWriterSettings_get_NewLine_mE88CF4F43FF1662A840FB60AF1F5E7B1545B59D6,
	JsonWriterSettings_get_MaxDepth_m0119E0BB77B06F20E38994C97D8056FA63340D28,
	JsonWriterSettings_get_UseXmlSerializationAttributes_m9360224AEBF9D17CB0E8F3A55B7272047F10E57F,
	JsonWriterSettings_get_DateTimeSerializer_mF4948BC3020F71A92AE845FA46FD2044790CEF42,
	JsonWriterSettings__ctor_mF25D8130149490723BE9DBB218805F92FB8F15C2,
	JsonNameAttribute_get_Name_m1236C8BDC86133590DBC71E5EFE56A670934F8BF,
	JsonNameAttribute_GetJsonName_m0DC39167EFDB4DBC418672EDEDF61EE699991816,
	TypeCoercionUtility_get_MemberMapCache_mFADEEED7078BD11E7D14581A9E0D4B60137B9DCD,
	TypeCoercionUtility_ProcessTypeHint_mBA8E900A04169F8038592676F4D330238931D430,
	TypeCoercionUtility_InstantiateObject_mE427F4669535431D4E89837E263ABFAE53A81232,
	TypeCoercionUtility_CreateMemberMap_m15ACB425946B5C3FB88D1C890EE7DAAB02E190F0,
	TypeCoercionUtility_GetMemberInfo_m7FB91C12B11E1BBE35F81A1DE7AE671900543085,
	TypeCoercionUtility_SetMemberValue_m08E6FBEB98501CAA8CAE46CBEF13C16200D81A3E,
	TypeCoercionUtility_CoerceType_m9A999904581022FE82F15AE90B24E71B88D04C95,
	TypeCoercionUtility_CoerceType_m6F37453AF3A2D3C809F9C8D912DB658449FB384D,
	TypeCoercionUtility_CoerceList_m7B98F0DACCC3A634317D7B6E77A19E43ECDCC232,
	TypeCoercionUtility_CoerceArray_m8D8D97F3B13BDD85AFD56BAAE4C57948230944AE,
	TypeCoercionUtility_IsNullable_m60FC45054D716CD78C92D3263468852329A94E9D,
	TypeCoercionUtility__ctor_mF228344278BBDC05F1E76B5641A6BA8FF3D714E9,
	NULL,
	JsonIgnoreAttribute_IsJsonIgnore_m6BC8749926601C94054AD637DE846CC88A95665E,
	JsonIgnoreAttribute_IsXmlIgnore_m925805A1EC6E7D75C96DD18AA762BFA83FE0AB53,
};
static const int32_t s_InvokerIndices[95] = 
{
	14,
	0,
	26,
	27,
	-1,
	419,
	100,
	28,
	28,
	14,
	28,
	28,
	-1,
	205,
	10,
	177,
	9,
	26,
	27,
	0,
	26,
	391,
	863,
	1162,
	26,
	26,
	31,
	31,
	31,
	548,
	548,
	32,
	32,
	160,
	160,
	282,
	283,
	861,
	548,
	862,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	27,
	26,
	26,
	27,
	23,
	23,
	23,
	837,
	103,
	1,
	289,
	23,
	26,
	124,
	26,
	27,
	102,
	9,
	23,
	-1,
	-1,
	-1,
	-1,
	14,
	102,
	14,
	14,
	10,
	102,
	14,
	23,
	14,
	0,
	14,
	1346,
	416,
	28,
	653,
	378,
	101,
	658,
	166,
	101,
	109,
	23,
	26,
	109,
	109,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000005, { 0, 2 } },
	{ 0x0600000D, { 2, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)1, 7310 },
	{ (Il2CppRGCTXDataType)2, 7310 },
	{ (Il2CppRGCTXDataType)1, 7311 },
	{ (Il2CppRGCTXDataType)2, 7311 },
};
extern const Il2CppCodeGenModule g_JsonFx_JsonCodeGenModule;
const Il2CppCodeGenModule g_JsonFx_JsonCodeGenModule = 
{
	"JsonFx.Json.dll",
	95,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
};
