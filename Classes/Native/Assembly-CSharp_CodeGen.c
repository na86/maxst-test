﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void FeaturePointBehaviour::.ctor()
extern void FeaturePointBehaviour__ctor_mF4DEFC6BDD0D97988A7B9ECFA8D2E520D5F6027A (void);
// 0x00000002 System.Void PinchZoom::.ctor()
extern void PinchZoom__ctor_m560990797F8867DD7A1826E88736FD35CBA31C0B (void);
// 0x00000003 System.Void PinchZoom::Update()
extern void PinchZoom_Update_mFE471B49017E569F93F4F5DD4DB490D2CC91AAB7 (void);
// 0x00000004 T[] JsonHelper::FromJson(System.String)
// 0x00000005 System.String JsonHelper::ToJson(T[])
// 0x00000006 System.String JsonHelper::ToJson(T[],System.Boolean)
// 0x00000007 System.Void JsonHelper_Wrapper`1::.ctor()
// 0x00000008 T[] JsonHelperForAnchor::FromJson(System.String)
// 0x00000009 System.String JsonHelperForAnchor::ToJson(T[])
// 0x0000000A System.String JsonHelperForAnchor::ToJson(T[],System.Boolean)
// 0x0000000B System.Void JsonHelperForAnchor_Wrapper`1::.ctor()
// 0x0000000C System.Void ARBehaviour::Init()
extern void ARBehaviour_Init_m59940284D5097F666E063D2AD15E92F2A86FCC4D (void);
// 0x0000000D System.Void ARBehaviour::OnClickBackButton()
extern void ARBehaviour_OnClickBackButton_m753A1D5DF44EC0114830C393CF1BB4800CD20429 (void);
// 0x0000000E System.Void ARBehaviour::StartCamera()
extern void ARBehaviour_StartCamera_mE2D6E7580B9EBA5DCF53EA0D09C1CD7C91D57804 (void);
// 0x0000000F System.Void ARBehaviour::StopCamera()
extern void ARBehaviour_StopCamera_m97DF8F7F762241F4EAD7FBFE7E1568DB9610FFD3 (void);
// 0x00000010 System.Void ARBehaviour::.ctor()
extern void ARBehaviour__ctor_mB1AA74585FB1EF7992288C1B07767CEBC8F9AE75 (void);
// 0x00000011 System.Void BackKeyHandler::Update()
extern void BackKeyHandler_Update_m49BEC634EBF49F516EF5F833B0C4F058AF5EBC42 (void);
// 0x00000012 System.Void BackKeyHandler::.ctor()
extern void BackKeyHandler__ctor_m8AEA5C94A250AEDF6DEA2AC25E848B9A1A0D63D7 (void);
// 0x00000013 System.Void CameraConfigurationSample::Awake()
extern void CameraConfigurationSample_Awake_m983DC81D22C846E30F29EAD45C57891EBA1AFEDB (void);
// 0x00000014 System.Void CameraConfigurationSample::Start()
extern void CameraConfigurationSample_Start_m924E517D8ADE190EA08F779C515117A8227C244E (void);
// 0x00000015 System.Void CameraConfigurationSample::AddTrackerData()
extern void CameraConfigurationSample_AddTrackerData_m62AA9B7AA9E7302288A4380709965684481B03F3 (void);
// 0x00000016 System.Void CameraConfigurationSample::DisableAllTrackables()
extern void CameraConfigurationSample_DisableAllTrackables_mF953BDDDD1B0017BEB4724D4095D79EEB9920FD6 (void);
// 0x00000017 System.Void CameraConfigurationSample::Update()
extern void CameraConfigurationSample_Update_m06986F2050AB58C8B999F149A3ABFBA50F7821F8 (void);
// 0x00000018 System.Void CameraConfigurationSample::SetFlashLightMode()
extern void CameraConfigurationSample_SetFlashLightMode_m238DB73A33530D6BCC3C02278CE35D4B6821DCE1 (void);
// 0x00000019 System.Void CameraConfigurationSample::FlipHorizontal()
extern void CameraConfigurationSample_FlipHorizontal_m73D5D5460103A1A0B1977AE01F2DF418A410DF8A (void);
// 0x0000001A System.Void CameraConfigurationSample::FlipVertical()
extern void CameraConfigurationSample_FlipVertical_m76F1C099EDB240548F9D43C73A5E3E3C54C5C4D9 (void);
// 0x0000001B System.Void CameraConfigurationSample::WhiteBalanceLock()
extern void CameraConfigurationSample_WhiteBalanceLock_m9FA3383F069BCD8E7F30DB766830256F4351FBAA (void);
// 0x0000001C System.Void CameraConfigurationSample::SetContinuousFocus()
extern void CameraConfigurationSample_SetContinuousFocus_m43F1824FD527C30ECD4DE18290F40C5D20C4C1DF (void);
// 0x0000001D System.Void CameraConfigurationSample::ChangeCameraPosition()
extern void CameraConfigurationSample_ChangeCameraPosition_m92153A77FBCFF8F24F128867EBA6D2A0C49A4C53 (void);
// 0x0000001E System.Void CameraConfigurationSample::OnApplicationPause(System.Boolean)
extern void CameraConfigurationSample_OnApplicationPause_mD2B7777AB37584B54855F8E5F4A2D7102D3D6FE2 (void);
// 0x0000001F System.Void CameraConfigurationSample::OnDestroy()
extern void CameraConfigurationSample_OnDestroy_m26F85D1106EA4F634642126DEA56C16886A1EFEF (void);
// 0x00000020 System.Void CameraConfigurationSample::.ctor()
extern void CameraConfigurationSample__ctor_mC4104FC04B4A61B7D693B64EC6EC09AF7CE26800 (void);
// 0x00000021 System.Void CloudTrackerSample::Awake()
extern void CloudTrackerSample_Awake_mA7887F25E1264C15A9F382E8E23CF82633CFEE81 (void);
// 0x00000022 System.Void CloudTrackerSample::Start()
extern void CloudTrackerSample_Start_m3E6A2D751B3F8871762F936E62EFFC3ABD92156A (void);
// 0x00000023 System.Void CloudTrackerSample::DisableAllTrackables()
extern void CloudTrackerSample_DisableAllTrackables_m53CF0A8711A1E2F35714A3291BB462E770879F42 (void);
// 0x00000024 System.Void CloudTrackerSample::Update()
extern void CloudTrackerSample_Update_mD050B82420073AC3C3F53513886CC6614C546532 (void);
// 0x00000025 System.Void CloudTrackerSample::OnApplicationPause(System.Boolean)
extern void CloudTrackerSample_OnApplicationPause_m0A24CA4E2D48182E340A753C8EC2D1BFE362DC9A (void);
// 0x00000026 System.Void CloudTrackerSample::OnDestroy()
extern void CloudTrackerSample_OnDestroy_m7F355AE1EBA839F87822C68E343BCE6ABE2C1C29 (void);
// 0x00000027 System.Void CloudTrackerSample::.ctor()
extern void CloudTrackerSample__ctor_mA46D0E93246D89985FF7FF31FED5F899E31CDB12 (void);
// 0x00000028 System.Void CodeScanSample::Awake()
extern void CodeScanSample_Awake_mA7FB64C8FF2BAA2EA4BBAA85A20D056A3565F929 (void);
// 0x00000029 System.Void CodeScanSample::Start()
extern void CodeScanSample_Start_m8DC64D3C0541C2A523E4E6F9FF31CF88A5BCD57B (void);
// 0x0000002A System.Void CodeScanSample::Update()
extern void CodeScanSample_Update_mFE796E45473A13D2B66D4BB62D14E46DB8EAC6B9 (void);
// 0x0000002B System.Void CodeScanSample::OnApplicationPause(System.Boolean)
extern void CodeScanSample_OnApplicationPause_m6174EE29A34707649C5EFC4ABC5E1E88102C95F9 (void);
// 0x0000002C System.Void CodeScanSample::OnDestroy()
extern void CodeScanSample_OnDestroy_m4E254134C3D4A7F7B4DAFCDF9D3559DCD12EFB92 (void);
// 0x0000002D System.Void CodeScanSample::StartCodeScan()
extern void CodeScanSample_StartCodeScan_mD31668C8BD3BD40202B14D825CE0285E5961F2C8 (void);
// 0x0000002E System.Void CodeScanSample::StartCameraInternal()
extern void CodeScanSample_StartCameraInternal_m3EE5524B90A7C7B27893A885EC80401A99C5DE86 (void);
// 0x0000002F System.Void CodeScanSample::StopCameraInternal()
extern void CodeScanSample_StopCameraInternal_mD47603BCF1D13AE17F26F9AFC460EE3635F272EA (void);
// 0x00000030 System.Collections.IEnumerator CodeScanSample::AutoFocusCoroutine()
extern void CodeScanSample_AutoFocusCoroutine_m02623B014BAF8C6E74055DEFE1D154CFB82A8EF8 (void);
// 0x00000031 System.Void CodeScanSample::.ctor()
extern void CodeScanSample__ctor_mAA9301C15734C1592275EAAB7B4BDC5FFF4CD5FB (void);
// 0x00000032 System.Void CodeScanSample_<AutoFocusCoroutine>d__11::.ctor(System.Int32)
extern void U3CAutoFocusCoroutineU3Ed__11__ctor_m27A8FFE5992D2B26E22046765FA2E90276E065D2 (void);
// 0x00000033 System.Void CodeScanSample_<AutoFocusCoroutine>d__11::System.IDisposable.Dispose()
extern void U3CAutoFocusCoroutineU3Ed__11_System_IDisposable_Dispose_mBDB6D9855ACF13C86E2FC8E6FB71244342875F06 (void);
// 0x00000034 System.Boolean CodeScanSample_<AutoFocusCoroutine>d__11::MoveNext()
extern void U3CAutoFocusCoroutineU3Ed__11_MoveNext_m1BEE42812370B0C112CE3AAD02DE91DF6BE88310 (void);
// 0x00000035 System.Object CodeScanSample_<AutoFocusCoroutine>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAutoFocusCoroutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A48D6BAE9ED87DA7AF4E61DA6378968B7DF2F7B (void);
// 0x00000036 System.Void CodeScanSample_<AutoFocusCoroutine>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAutoFocusCoroutineU3Ed__11_System_Collections_IEnumerator_Reset_m061B634F0C1303432F0BDACE36C292092BB469DB (void);
// 0x00000037 System.Object CodeScanSample_<AutoFocusCoroutine>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAutoFocusCoroutineU3Ed__11_System_Collections_IEnumerator_get_Current_m6468B35ED480B25CB536A46942683BD015CA4F96 (void);
// 0x00000038 System.Void HomeSceneManager::Update()
extern void HomeSceneManager_Update_m8F9D0687155755E375EB3E7CEE9237B3F1B3ABB9 (void);
// 0x00000039 System.Void HomeSceneManager::OnImageTargetClick()
extern void HomeSceneManager_OnImageTargetClick_mF932792FB7941328AE29EC5B6C61D1BB0E05E085 (void);
// 0x0000003A System.Void HomeSceneManager::OnMarkerImageClick()
extern void HomeSceneManager_OnMarkerImageClick_m02BF39B0A49E6EEC6085B236DA089801F3F71AA7 (void);
// 0x0000003B System.Void HomeSceneManager::OnInstantImageClick()
extern void HomeSceneManager_OnInstantImageClick_m33109F30DAEC7CCEB8C2EDB46FE2E7839546F7FF (void);
// 0x0000003C System.Void HomeSceneManager::OnObjectTargetClick()
extern void HomeSceneManager_OnObjectTargetClick_m1B98A9C9E1FAE6C5F75246FC1C6F4F8CFE88E2B9 (void);
// 0x0000003D System.Void HomeSceneManager::OnQR_BarcodeClick()
extern void HomeSceneManager_OnQR_BarcodeClick_m426952CFD6CA2204DBE13F0AC2F8ECA504B8EDA7 (void);
// 0x0000003E System.Void HomeSceneManager::OnCameraConfigClick()
extern void HomeSceneManager_OnCameraConfigClick_m4CF72079EC4647144B2EFF987F4709EA74B13693 (void);
// 0x0000003F System.Void HomeSceneManager::OnCloudRecognizerClick()
extern void HomeSceneManager_OnCloudRecognizerClick_m3D7CA8A9A758AA542D99C7371B90FFEC7E31618D (void);
// 0x00000040 System.Void HomeSceneManager::OnQRTrackerClick()
extern void HomeSceneManager_OnQRTrackerClick_m25490F1C35A3AD007CB7C86A5D4165D77B351871 (void);
// 0x00000041 System.Void HomeSceneManager::OnVideoTrackerClick()
extern void HomeSceneManager_OnVideoTrackerClick_mA98B80B9C999CE3921F737A9F2F0567BC4DCBB7E (void);
// 0x00000042 System.Void HomeSceneManager::OnImageFusionTrackerClick()
extern void HomeSceneManager_OnImageFusionTrackerClick_m0CADFB88F0D9634E0A11613C882F4A9CECDE5E16 (void);
// 0x00000043 System.Void HomeSceneManager::OnInstantFusionTrackerClick()
extern void HomeSceneManager_OnInstantFusionTrackerClick_m57A2602BDB7A9B16366102FBDE8AA0F88B7EA595 (void);
// 0x00000044 System.Void HomeSceneManager::OnMarkerFusionTrackerClick()
extern void HomeSceneManager_OnMarkerFusionTrackerClick_m9205AD93B22837E8AD1088ACD9476F6065152D38 (void);
// 0x00000045 System.Void HomeSceneManager::OnQRCodeFusionTrackerClick()
extern void HomeSceneManager_OnQRCodeFusionTrackerClick_mEACE167E17F9DC7DD7E0E5746620868C31E5EFF7 (void);
// 0x00000046 System.Void HomeSceneManager::OnObjectFusionTrackerClick()
extern void HomeSceneManager_OnObjectFusionTrackerClick_m6B77E0E79A34291A7A6CD0AE63E24684EE83E92F (void);
// 0x00000047 System.Void HomeSceneManager::.ctor()
extern void HomeSceneManager__ctor_m393EFBA77334F8C7D72F0C93D96A743BE6CB3F78 (void);
// 0x00000048 System.Void ImageFusionTrackerSample::Awake()
extern void ImageFusionTrackerSample_Awake_m4620A5274A15F6FE2EEA9901B8B6A80CD725C2FB (void);
// 0x00000049 System.Void ImageFusionTrackerSample::Start()
extern void ImageFusionTrackerSample_Start_m05B380597367021256ED1292195C1D9A23AFF0EC (void);
// 0x0000004A System.Void ImageFusionTrackerSample::AddTrackerData()
extern void ImageFusionTrackerSample_AddTrackerData_m276C291F787D520F0F5BA6725348D0CE2D19C45C (void);
// 0x0000004B System.Void ImageFusionTrackerSample::DisableAllTrackables()
extern void ImageFusionTrackerSample_DisableAllTrackables_m38E1097FBFD7B7C699955C4BA61EF452D59052B2 (void);
// 0x0000004C System.Void ImageFusionTrackerSample::Update()
extern void ImageFusionTrackerSample_Update_mCFDCFBDFA2920DB707A3A67CD593525FBCB7A7F6 (void);
// 0x0000004D System.Void ImageFusionTrackerSample::OnApplicationPause(System.Boolean)
extern void ImageFusionTrackerSample_OnApplicationPause_m6DC031532EBA3E96DC6B43E2BE1E8E7FE238937E (void);
// 0x0000004E System.Void ImageFusionTrackerSample::OnDestroy()
extern void ImageFusionTrackerSample_OnDestroy_m148440F1DE1180424E068B9997C2E88947F64B3C (void);
// 0x0000004F System.Void ImageFusionTrackerSample::.ctor()
extern void ImageFusionTrackerSample__ctor_m3AD669C8E2ABC5A4CAAB90BD5884BCC37EEEFB07 (void);
// 0x00000050 System.Void ImageFusionTrackerSample_<>c::.cctor()
extern void U3CU3Ec__cctor_m824457FF95DB3FD327618E99EA8634BF254FA53F (void);
// 0x00000051 System.Void ImageFusionTrackerSample_<>c::.ctor()
extern void U3CU3Ec__ctor_m68FA269E2858F02327B825B0C35022EA5B04E295 (void);
// 0x00000052 System.Void ImageFusionTrackerSample_<>c::<AddTrackerData>b__5_0(System.String)
extern void U3CU3Ec_U3CAddTrackerDataU3Eb__5_0_mEBB230E0678B0C798D544BE73C301710D74D07D9 (void);
// 0x00000053 System.Void ImageTrackerSample::Awake()
extern void ImageTrackerSample_Awake_m7412A77B26CDDEF794D9F65F57F927371BD52A07 (void);
// 0x00000054 System.Void ImageTrackerSample::Start()
extern void ImageTrackerSample_Start_m28FF829559576024D5C3EB6E8040E69554A60195 (void);
// 0x00000055 System.Void ImageTrackerSample::AddTrackerData()
extern void ImageTrackerSample_AddTrackerData_m42DB63C042AFCF1E98F73CC7EB4863EE52189AC4 (void);
// 0x00000056 System.Void ImageTrackerSample::DisableAllTrackables()
extern void ImageTrackerSample_DisableAllTrackables_mB6AF4F7221A7E26A68074CF234565B6756CAC5B7 (void);
// 0x00000057 System.Void ImageTrackerSample::Update()
extern void ImageTrackerSample_Update_m512A2DA0D422D8626A2C2C20276801F616C90169 (void);
// 0x00000058 System.Void ImageTrackerSample::SetNormalMode()
extern void ImageTrackerSample_SetNormalMode_m333C0750019CBFA3075C4B1FE6737E696AC2F74A (void);
// 0x00000059 System.Void ImageTrackerSample::SetExtendedMode()
extern void ImageTrackerSample_SetExtendedMode_m68FA42C668C849E45763232EE4E8FFFCC2E3BEFD (void);
// 0x0000005A System.Void ImageTrackerSample::SetMultiMode()
extern void ImageTrackerSample_SetMultiMode_m8A862CA8E063A4B7165F426468C7096CFAFE2468 (void);
// 0x0000005B System.Void ImageTrackerSample::OnApplicationPause(System.Boolean)
extern void ImageTrackerSample_OnApplicationPause_mF36A2A4822008DD953BA53A187ABF112CF42A3A2 (void);
// 0x0000005C System.Void ImageTrackerSample::OnDestroy()
extern void ImageTrackerSample_OnDestroy_mF32151617A84F3CD5EA18146CC60D74F6364218E (void);
// 0x0000005D System.Void ImageTrackerSample::.ctor()
extern void ImageTrackerSample__ctor_m110C04AA45CB5F3900A517CF76864F70C8F5E99F (void);
// 0x0000005E System.Void ImageTrackerSample_<>c::.cctor()
extern void U3CU3Ec__cctor_m853E6C5D1335C175622C6AA047FA2ED273C342A8 (void);
// 0x0000005F System.Void ImageTrackerSample_<>c::.ctor()
extern void U3CU3Ec__ctor_m3784AFA6A99F3FCD3EE597A9D59C3456E1A5FC23 (void);
// 0x00000060 System.Void ImageTrackerSample_<>c::<AddTrackerData>b__4_0(System.String)
extern void U3CU3Ec_U3CAddTrackerDataU3Eb__4_0_m566EDD9A1F170C6FBA63240AB9275DD0009FE0AC (void);
// 0x00000061 System.Void InstantFusionTrackerSample::Awake()
extern void InstantFusionTrackerSample_Awake_m8C3D5AF0E35448BD4F26507DF2742EF9318EB839 (void);
// 0x00000062 System.Void InstantFusionTrackerSample::Start()
extern void InstantFusionTrackerSample_Start_mD878AB0500549E5F73C26FF62B71E618AE832A74 (void);
// 0x00000063 System.Void InstantFusionTrackerSample::Update()
extern void InstantFusionTrackerSample_Update_mCE525F027F0DDD419C61F1C2D585F9BE0DD7C898 (void);
// 0x00000064 System.Void InstantFusionTrackerSample::UpdateTouchDelta(UnityEngine.Vector2)
extern void InstantFusionTrackerSample_UpdateTouchDelta_m6307721F0F9E7D0C1A8BB638972166E52161AF7D (void);
// 0x00000065 System.Void InstantFusionTrackerSample::OnApplicationPause(System.Boolean)
extern void InstantFusionTrackerSample_OnApplicationPause_mCFCB3C5523F09D844A3F368071A8C7A8C2771618 (void);
// 0x00000066 System.Void InstantFusionTrackerSample::OnDestroy()
extern void InstantFusionTrackerSample_OnDestroy_mB287B3E5F17FB3BD306A1ED533C07D68FD3F0D15 (void);
// 0x00000067 System.Void InstantFusionTrackerSample::OnClickStart()
extern void InstantFusionTrackerSample_OnClickStart_mA0CD02E490BC31058907163A958C18322D7C2AF3 (void);
// 0x00000068 System.Void InstantFusionTrackerSample::.ctor()
extern void InstantFusionTrackerSample__ctor_m4BE3E812988F5C62B2589141D8610C5B52CD2686 (void);
// 0x00000069 System.Void InstantTrackerSample::Awake()
extern void InstantTrackerSample_Awake_m25EA3C21DF2D934660A40AE643A8F23415DCA3E1 (void);
// 0x0000006A System.Void InstantTrackerSample::Start()
extern void InstantTrackerSample_Start_m32B641226A1FC02713D7631AF248F9BF8C0FD9AA (void);
// 0x0000006B System.Void InstantTrackerSample::Update()
extern void InstantTrackerSample_Update_mE3244B7548EC5349ECAE315AEC50B468A3BD8C43 (void);
// 0x0000006C System.Void InstantTrackerSample::UpdateTouchDelta(UnityEngine.Vector2)
extern void InstantTrackerSample_UpdateTouchDelta_mF64ADBB188366286CF76D05F5C69083155B9BF18 (void);
// 0x0000006D System.Void InstantTrackerSample::OnApplicationPause(System.Boolean)
extern void InstantTrackerSample_OnApplicationPause_m0B539B914C201FEEDD18AD5F96238D103ACE678C (void);
// 0x0000006E System.Void InstantTrackerSample::OnDestroy()
extern void InstantTrackerSample_OnDestroy_mAF0DE672525871E8209D910C26CEFF28C578714D (void);
// 0x0000006F System.Void InstantTrackerSample::OnClickStart()
extern void InstantTrackerSample_OnClickStart_mA750F20B27816D1A8F7882D39A2632174075F449 (void);
// 0x00000070 System.Void InstantTrackerSample::.ctor()
extern void InstantTrackerSample__ctor_m5B1D25AD966F8A3CCF487ECD2FDB0D088B3A7A69 (void);
// 0x00000071 System.Void MarkerFusionTrackerSample::Awake()
extern void MarkerFusionTrackerSample_Awake_mAEC7CCA69CD294420898251E6B68AEB4C2105AB8 (void);
// 0x00000072 System.Void MarkerFusionTrackerSample::Start()
extern void MarkerFusionTrackerSample_Start_m875280AD497340AFEE8413D24E9FD0F2A6925DB8 (void);
// 0x00000073 System.Void MarkerFusionTrackerSample::AddTrackerData()
extern void MarkerFusionTrackerSample_AddTrackerData_m9A89FE1F16E0AC782F78C66746B6478F74553130 (void);
// 0x00000074 System.Void MarkerFusionTrackerSample::DisableAllTrackables()
extern void MarkerFusionTrackerSample_DisableAllTrackables_m2C5B74EDBB8798C8C84D1755B0507E70753CF9CD (void);
// 0x00000075 System.Void MarkerFusionTrackerSample::Update()
extern void MarkerFusionTrackerSample_Update_m55BE4A484266F2023FD18BA108AC9CE5CBE333CD (void);
// 0x00000076 System.Void MarkerFusionTrackerSample::OnApplicationPause(System.Boolean)
extern void MarkerFusionTrackerSample_OnApplicationPause_m9DBD4AA29E45B5216628D124F0EA26F7B95EF32A (void);
// 0x00000077 System.Void MarkerFusionTrackerSample::OnDestroy()
extern void MarkerFusionTrackerSample_OnDestroy_mBC8C7AD433AF1315C9AA0FE13914D387F334CF78 (void);
// 0x00000078 System.Void MarkerFusionTrackerSample::.ctor()
extern void MarkerFusionTrackerSample__ctor_m217070E9C8B014A4C05C021C0061405F718ECBFA (void);
// 0x00000079 System.Void MarkerTrackerSample::Awake()
extern void MarkerTrackerSample_Awake_mE3BA80848C019452FA9A5EA231BD6B9E228AF7F0 (void);
// 0x0000007A System.Void MarkerTrackerSample::Start()
extern void MarkerTrackerSample_Start_mC44C48E70323B9ACEA1283F5097965EE3B4D053D (void);
// 0x0000007B System.Void MarkerTrackerSample::AddTrackerData()
extern void MarkerTrackerSample_AddTrackerData_m09519334B2CF59DF15D50989553B27C4E9D49303 (void);
// 0x0000007C System.Void MarkerTrackerSample::DisableAllTrackables()
extern void MarkerTrackerSample_DisableAllTrackables_m7D0155874CF500A7BED0AB75CA6A241B5A03B610 (void);
// 0x0000007D System.Void MarkerTrackerSample::Update()
extern void MarkerTrackerSample_Update_m496E72624A47915A4BAA9919C407F2C3FD567DC9 (void);
// 0x0000007E System.Void MarkerTrackerSample::OnClickedNormal()
extern void MarkerTrackerSample_OnClickedNormal_m4E8C38B62B77619225AD7E436EE3B03BC7C39E3C (void);
// 0x0000007F System.Void MarkerTrackerSample::OnClickedEnhanced()
extern void MarkerTrackerSample_OnClickedEnhanced_m2F2C3D7A9E5C435128939A8690DC3E160B360327 (void);
// 0x00000080 System.Void MarkerTrackerSample::OnApplicationPause(System.Boolean)
extern void MarkerTrackerSample_OnApplicationPause_mFC7FD479A29B3CFDF58DF32ED2E684CD18316ACF (void);
// 0x00000081 System.Void MarkerTrackerSample::OnDestroy()
extern void MarkerTrackerSample_OnDestroy_mB5D5010339171337CF94F7719A926B5C8EB64D7C (void);
// 0x00000082 System.Void MarkerTrackerSample::.ctor()
extern void MarkerTrackerSample__ctor_m44F8DCE19F97281A9A5D59873C118ACDE3A9D745 (void);
// 0x00000083 System.Void ObjectFusionTrackerSample::Awake()
extern void ObjectFusionTrackerSample_Awake_m870336D3C133A6CE634888A146C15889C292298E (void);
// 0x00000084 System.Void ObjectFusionTrackerSample::Start()
extern void ObjectFusionTrackerSample_Start_mC3AA66CC1458E9976552130EC697516AAA4C2FC7 (void);
// 0x00000085 System.Void ObjectFusionTrackerSample::AddTrackerData()
extern void ObjectFusionTrackerSample_AddTrackerData_mE2B51398EEA3014573A5F9DC2072AF5E56ED1CB5 (void);
// 0x00000086 System.Void ObjectFusionTrackerSample::DisableAllTrackables()
extern void ObjectFusionTrackerSample_DisableAllTrackables_mE98997D72EF55F33B67BEAEEBDA108CDF7B710E7 (void);
// 0x00000087 System.Void ObjectFusionTrackerSample::Update()
extern void ObjectFusionTrackerSample_Update_mD6805EC8849FE599545855E628503C086D7260EE (void);
// 0x00000088 System.Void ObjectFusionTrackerSample::OnApplicationPause(System.Boolean)
extern void ObjectFusionTrackerSample_OnApplicationPause_m97CC6076EB75B64155FF208B3224CE1AB5E6049A (void);
// 0x00000089 System.Void ObjectFusionTrackerSample::OnDestroy()
extern void ObjectFusionTrackerSample_OnDestroy_m534E5F7DEDE4A182A504ABE6698E433F0E9A2935 (void);
// 0x0000008A System.Void ObjectFusionTrackerSample::.ctor()
extern void ObjectFusionTrackerSample__ctor_m19F0A18830120C007242EB41F4CEB09C660D149C (void);
// 0x0000008B System.Void ObjectFusionTrackerSample_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m7A07932697362B9D337BEC77213D0DE02D4BE291 (void);
// 0x0000008C System.Void ObjectFusionTrackerSample_<>c__DisplayClass5_0::<AddTrackerData>b__0(System.String)
extern void U3CU3Ec__DisplayClass5_0_U3CAddTrackerDataU3Eb__0_mACC7F7951D007AEC9EFF7DBE8D64EDDF24863635 (void);
// 0x0000008D System.Void ObjectTrackerSample::Awake()
extern void ObjectTrackerSample_Awake_m298EEDD2023DCE4F6770FF1609481C81F30769ED (void);
// 0x0000008E System.Void ObjectTrackerSample::Start()
extern void ObjectTrackerSample_Start_m10B30AC068431794E9D470A81E23A0687300D510 (void);
// 0x0000008F System.Void ObjectTrackerSample::AddTrackerData()
extern void ObjectTrackerSample_AddTrackerData_m960AAF6DAADE8E912BF1B3AF5977CEB85EE0A4D8 (void);
// 0x00000090 System.Void ObjectTrackerSample::DisableAllTrackables()
extern void ObjectTrackerSample_DisableAllTrackables_m586190A03C5787A0905EC399D58B9AB31F6C3F5B (void);
// 0x00000091 System.Void ObjectTrackerSample::Update()
extern void ObjectTrackerSample_Update_m8A372AF78AA4115FBA11450CD9B48B2CE7AB67D7 (void);
// 0x00000092 System.Void ObjectTrackerSample::OnApplicationPause(System.Boolean)
extern void ObjectTrackerSample_OnApplicationPause_m39D8B8EA0D56C612A7EB5126F1B1B5039033FA36 (void);
// 0x00000093 System.Void ObjectTrackerSample::OnDestroy()
extern void ObjectTrackerSample_OnDestroy_m4C3F3646FC5E9C24912A3507FB5C2DE0A24BAC33 (void);
// 0x00000094 System.Void ObjectTrackerSample::.ctor()
extern void ObjectTrackerSample__ctor_m8453D054DC9141B4939CE430CF44F4DB2799F235 (void);
// 0x00000095 System.Void ObjectTrackerSample_<>c::.cctor()
extern void U3CU3Ec__cctor_m247E98A5837BA8A199A0AB75B612C978D772B944 (void);
// 0x00000096 System.Void ObjectTrackerSample_<>c::.ctor()
extern void U3CU3Ec__ctor_mF3F159D350C11A67DD6599E6DAC44DC3FADAA2B8 (void);
// 0x00000097 System.Void ObjectTrackerSample_<>c::<AddTrackerData>b__4_0(System.String)
extern void U3CU3Ec_U3CAddTrackerDataU3Eb__4_0_m8CFE18688869A184282957884D870CD8B3ED046A (void);
// 0x00000098 System.Void QrCodeFusionTrackerSample::Awake()
extern void QrCodeFusionTrackerSample_Awake_m14B0A4ADF61476B33D56E4565E4D8444461C066F (void);
// 0x00000099 System.Void QrCodeFusionTrackerSample::Start()
extern void QrCodeFusionTrackerSample_Start_m78ACB970BB5AEA9207245724FDC9B0B29766FD1F (void);
// 0x0000009A System.Void QrCodeFusionTrackerSample::AddTrackerData()
extern void QrCodeFusionTrackerSample_AddTrackerData_m25FABAB2B9EF9C19DDBF1762B93B2A1C9AA1388A (void);
// 0x0000009B System.Void QrCodeFusionTrackerSample::DisableAllTrackables()
extern void QrCodeFusionTrackerSample_DisableAllTrackables_m8E38F00ACB80CDC538B430432015BB41E36210B9 (void);
// 0x0000009C System.Void QrCodeFusionTrackerSample::Update()
extern void QrCodeFusionTrackerSample_Update_m76CFA34E61DA9E282AFC602748DC0FB0D1FDB260 (void);
// 0x0000009D System.Void QrCodeFusionTrackerSample::OnApplicationPause(System.Boolean)
extern void QrCodeFusionTrackerSample_OnApplicationPause_mB9E94FFB04F3BE7C6F5719C6200D87CBCCA792DA (void);
// 0x0000009E System.Void QrCodeFusionTrackerSample::OnDestroy()
extern void QrCodeFusionTrackerSample_OnDestroy_mE38D45DFB07B53C7A44D2095FBC6907CD854A96B (void);
// 0x0000009F System.Void QrCodeFusionTrackerSample::.ctor()
extern void QrCodeFusionTrackerSample__ctor_mEF34F247EDEDFA90796B04226FF3AF81D97AB4CA (void);
// 0x000000A0 System.Void QrCodeTrackerSample::Awake()
extern void QrCodeTrackerSample_Awake_mAA41745A70BA5FD8A60A6C02FDB3CDFF245CF317 (void);
// 0x000000A1 System.Void QrCodeTrackerSample::Start()
extern void QrCodeTrackerSample_Start_mDE4CCEDF47204F0FE0EF5CBA0A9989C459B86345 (void);
// 0x000000A2 System.Void QrCodeTrackerSample::AddTrackerData()
extern void QrCodeTrackerSample_AddTrackerData_mDC33F1FF8193B9CD29FF8296CB73728A4C257F6D (void);
// 0x000000A3 System.Void QrCodeTrackerSample::DisableAllTrackables()
extern void QrCodeTrackerSample_DisableAllTrackables_m491B04E051D33DD2DB6B50CE109C2351AB5A75F3 (void);
// 0x000000A4 System.Void QrCodeTrackerSample::Update()
extern void QrCodeTrackerSample_Update_mC20196AF08C9060BB2751209D570FDF31EF4E203 (void);
// 0x000000A5 System.Void QrCodeTrackerSample::OnApplicationPause(System.Boolean)
extern void QrCodeTrackerSample_OnApplicationPause_m348E181EC2B73C66E3E89D9CCECC1784FC9DAD60 (void);
// 0x000000A6 System.Void QrCodeTrackerSample::OnDestroy()
extern void QrCodeTrackerSample_OnDestroy_m262CE396B1FC20A581B5A201AB6E9D1B4C48021A (void);
// 0x000000A7 System.Void QrCodeTrackerSample::.ctor()
extern void QrCodeTrackerSample__ctor_m1169322B46F39732D48829D65087345DAB245B14 (void);
// 0x000000A8 System.Void SceneStackManager::.ctor()
extern void SceneStackManager__ctor_m784AC2CAA424BD6A7D6ADB2AD8F6C8378C92EE41 (void);
// 0x000000A9 SceneStackManager SceneStackManager::get_Instance()
extern void SceneStackManager_get_Instance_m7AF77B540713F96AC0409F80A3205057A7DCDAFD (void);
// 0x000000AA System.Void SceneStackManager::LoadScene(System.String,System.String)
extern void SceneStackManager_LoadScene_m9AEDA45F3F9B6EB9A1A3E96E93E4DE985C29C982 (void);
// 0x000000AB System.Void SceneStackManager::LoadPrevious()
extern void SceneStackManager_LoadPrevious_m042A541247219EFABAF0B48FDA1577F29711C89C (void);
// 0x000000AC System.Void SceneStackManager::.cctor()
extern void SceneStackManager__cctor_m140FAEDEB77C67A773D2687DC13ABCDE869E0679 (void);
// 0x000000AD T Singleton`1::get_Instance()
// 0x000000AE System.Void Singleton`1::OnDestroy()
// 0x000000AF System.Void Singleton`1::.ctor()
// 0x000000B0 System.Void Singleton`1::.cctor()
// 0x000000B1 System.Void VideoTrackerSample::Awake()
extern void VideoTrackerSample_Awake_m51F42B6D07A53389E9EF905EE53EF24453B43429 (void);
// 0x000000B2 System.Void VideoTrackerSample::Start()
extern void VideoTrackerSample_Start_mBD85251CFCB8EDACD74A7C2115E5DDD001493BD2 (void);
// 0x000000B3 System.Void VideoTrackerSample::AddTrackerData()
extern void VideoTrackerSample_AddTrackerData_m557B9C58EE0AA4F48B68297C09B051800CA290F3 (void);
// 0x000000B4 System.Void VideoTrackerSample::DisableAllTrackables()
extern void VideoTrackerSample_DisableAllTrackables_m9B8DE6B0E38A96F51E7267415B54461B1D528C3B (void);
// 0x000000B5 System.Void VideoTrackerSample::OnNewFrame(UnityEngine.Video.VideoPlayer,System.Int64)
extern void VideoTrackerSample_OnNewFrame_mF8157121E4F36B90839665F47ECDAC2BCB2DEABF (void);
// 0x000000B6 System.Void VideoTrackerSample::SwitchCameraToVideo(System.Boolean)
extern void VideoTrackerSample_SwitchCameraToVideo_m8E4593CB18692B0224A366BE85EF331F9992EAEC (void);
// 0x000000B7 System.Void VideoTrackerSample::PauseAndPlayVideo()
extern void VideoTrackerSample_PauseAndPlayVideo_mCED96EC7E26DDD5A5B505374B49F8F1C339068C9 (void);
// 0x000000B8 System.Void VideoTrackerSample::Update()
extern void VideoTrackerSample_Update_m26196264359E413E2CCB35461F78C1B8F5020977 (void);
// 0x000000B9 System.Void VideoTrackerSample::SetNormalMode()
extern void VideoTrackerSample_SetNormalMode_m10F42BDBAE21D87613CD7B7B93316D078A2CEE0D (void);
// 0x000000BA System.Void VideoTrackerSample::SetExtendedMode()
extern void VideoTrackerSample_SetExtendedMode_m2796BAA1DFFB591F1BF1BBC355EBE59AD53D9D29 (void);
// 0x000000BB System.Void VideoTrackerSample::SetMultiMode()
extern void VideoTrackerSample_SetMultiMode_mA70630576408EB2BD606D4F75CA95A09226DC085 (void);
// 0x000000BC System.Void VideoTrackerSample::OnApplicationPause(System.Boolean)
extern void VideoTrackerSample_OnApplicationPause_m74FFC0962BC903D1C94C6C1500A3A3202CB6552C (void);
// 0x000000BD System.Void VideoTrackerSample::OnDestroy()
extern void VideoTrackerSample_OnDestroy_m8163C7AE53D346DABAB5E102C10D5FE6B313B893 (void);
// 0x000000BE System.Void VideoTrackerSample::.ctor()
extern void VideoTrackerSample__ctor_m8F86F048B0EB6AA27D27BFC37FF8A30B55CB51C6 (void);
// 0x000000BF System.Void VideoTrackerSample::<Start>b__5_0(System.String)
extern void VideoTrackerSample_U3CStartU3Eb__5_0_mC461FB1679DC02B29B2D7C57B62DAE3B812EAEBC (void);
// 0x000000C0 System.Void VideoTrackerSample_<>c::.cctor()
extern void U3CU3Ec__cctor_m37DD0D3F11AD8487D374DCE500539FEE904E6228 (void);
// 0x000000C1 System.Void VideoTrackerSample_<>c::.ctor()
extern void U3CU3Ec__ctor_m8A566A34008BBD0B6DE4E56098B49E6860E6C408 (void);
// 0x000000C2 System.Void VideoTrackerSample_<>c::<AddTrackerData>b__6_0(System.String)
extern void U3CU3Ec_U3CAddTrackerDataU3Eb__6_0_m6AEB6709D3BEA3DE5574C350E04FC8ABDF1923BD (void);
// 0x000000C3 System.Void Supyrb.AdaptingEventSystemDragThreshold::Awake()
extern void AdaptingEventSystemDragThreshold_Awake_m29753D9DC51FBB3E299A2512299EBE68EA2C2DA1 (void);
// 0x000000C4 System.Void Supyrb.AdaptingEventSystemDragThreshold::UpdatePixelDrag(System.Single)
extern void AdaptingEventSystemDragThreshold_UpdatePixelDrag_mF5C694C6B97ADD2CF2F31C6C36E8AA541F016A95 (void);
// 0x000000C5 System.Void Supyrb.AdaptingEventSystemDragThreshold::Update()
extern void AdaptingEventSystemDragThreshold_Update_mB83423313AD3456296C402376402C6D166FBA6B3 (void);
// 0x000000C6 System.Void Supyrb.AdaptingEventSystemDragThreshold::OnLevelWasLoaded(System.Int32)
extern void AdaptingEventSystemDragThreshold_OnLevelWasLoaded_m71DE0DD26F111417A9690585102606178B17EBBE (void);
// 0x000000C7 System.Void Supyrb.AdaptingEventSystemDragThreshold::DisableBtnControll()
extern void AdaptingEventSystemDragThreshold_DisableBtnControll_m86624F9FEF27085ECE430E607543C55337E41722 (void);
// 0x000000C8 System.Void Supyrb.AdaptingEventSystemDragThreshold::Reset()
extern void AdaptingEventSystemDragThreshold_Reset_mF423CBDBC24770C0C5ABD8CD17C48B070B12BB6E (void);
// 0x000000C9 System.Void Supyrb.AdaptingEventSystemDragThreshold::.ctor()
extern void AdaptingEventSystemDragThreshold__ctor_mDC8BC16A02948AD4876A41CF90B7F0B4D83AA92C (void);
// 0x000000CA System.Void maxstAR.ARManager::Awake()
extern void ARManager_Awake_mF9A1FD43A812AB7C715521333BEF99A5CF2F164A (void);
// 0x000000CB System.Void maxstAR.ARManager::OnDestroy()
extern void ARManager_OnDestroy_m698B1926664753D21A14D81779BB0EC0A0D4DFC5 (void);
// 0x000000CC System.Void maxstAR.ARManager::.ctor()
extern void ARManager__ctor_m3379ADAD23DAC4FBF810F1E74A16982A3C1087B7 (void);
// 0x000000CD System.Void maxstAR.AndroidEngine::.ctor()
extern void AndroidEngine__ctor_mEA71A3C5541F092C8C4B25AAC2E9871F6E62D4ED (void);
// 0x000000CE System.Void maxstAR.AndroidEngine::Dispose()
extern void AndroidEngine_Dispose_m206483E69FD8990C2101B547DF9AD1CAB776E02F (void);
// 0x000000CF System.Void maxstAR.CameraBackgroundBehaviour::.ctor()
extern void CameraBackgroundBehaviour__ctor_m210FC5F301B95FE4C9BBD94B66A8FA67B43D93CE (void);
// 0x000000D0 System.Void maxstAR.CloudTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void CloudTrackableBehaviour_OnTrackSuccess_mB57C64761E644555D8D3827178C0DF15D8D67566 (void);
// 0x000000D1 System.Void maxstAR.CloudTrackableBehaviour::OnTrackFail()
extern void CloudTrackableBehaviour_OnTrackFail_m87C58458F999AD1AAB243A17F226FDFCA0C1CD5B (void);
// 0x000000D2 System.Void maxstAR.CloudTrackableBehaviour::.ctor()
extern void CloudTrackableBehaviour__ctor_m1D9745929F77C0BD7A2E386D51FC98DDECAE1F6F (void);
// 0x000000D3 System.Void maxstAR.ConfigurationScriptableObject::.ctor()
extern void ConfigurationScriptableObject__ctor_m1421B6E421DAAAD15E64B21FB0DA4A5D14982E5E (void);
// 0x000000D4 System.Void maxstAR.ImageTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void ImageTrackableBehaviour_OnTrackSuccess_mDD81968CECBCBCEA3675832224F1B8A7D0C2EE43 (void);
// 0x000000D5 System.Void maxstAR.ImageTrackableBehaviour::OnTrackFail()
extern void ImageTrackableBehaviour_OnTrackFail_mA6FCD1D47D437ACFEA74ADF0FA880331C374D010 (void);
// 0x000000D6 System.Void maxstAR.ImageTrackableBehaviour::.ctor()
extern void ImageTrackableBehaviour__ctor_mEBC53EAEFDC23383B74939B0E0748F3A6606A88D (void);
// 0x000000D7 System.Void maxstAR.InstantTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void InstantTrackableBehaviour_OnTrackSuccess_m6C47F3A2FEE692379446C4830138DD6F5EC9E91A (void);
// 0x000000D8 System.Void maxstAR.InstantTrackableBehaviour::OnTrackFail()
extern void InstantTrackableBehaviour_OnTrackFail_mFD5FE6AB7754F19410696DE6596C99A0EC31F019 (void);
// 0x000000D9 System.Void maxstAR.InstantTrackableBehaviour::.ctor()
extern void InstantTrackableBehaviour__ctor_m86CA1BD6B698D0B0626E77B4DD557C352FA8847E (void);
// 0x000000DA System.Void maxstAR.MapViewerBehaviour::.ctor()
extern void MapViewerBehaviour__ctor_m6ACDBADD2E87A35EC634693341A4186BD25DED05 (void);
// 0x000000DB System.Single maxstAR.MarkerGroupBehaviour::get_MarkerGroupSize()
extern void MarkerGroupBehaviour_get_MarkerGroupSize_m0E17A479CCC20475FD99A0BAEDB948B135102846 (void);
// 0x000000DC System.Void maxstAR.MarkerGroupBehaviour::set_MarkerGroupSize(System.Single)
extern void MarkerGroupBehaviour_set_MarkerGroupSize_m0477AC2E7A525EC3E904AF422C00C0D3E0337A08 (void);
// 0x000000DD System.Boolean maxstAR.MarkerGroupBehaviour::get_ApplyAll()
extern void MarkerGroupBehaviour_get_ApplyAll_m36679E577FC0BE93B2A866266ECF1CB54742F60C (void);
// 0x000000DE System.Void maxstAR.MarkerGroupBehaviour::set_ApplyAll(System.Boolean)
extern void MarkerGroupBehaviour_set_ApplyAll_m237D7F1F4C57DCF7EA2D03E7B0EAE6F7889A863F (void);
// 0x000000DF System.Void maxstAR.MarkerGroupBehaviour::.ctor()
extern void MarkerGroupBehaviour__ctor_m7470690E8B5EF58FB1143D412F5350FAA63FBA29 (void);
// 0x000000E0 System.Int32 maxstAR.MarkerTrackerBehaviour::get_MarkerID()
extern void MarkerTrackerBehaviour_get_MarkerID_m9A804752BAC9B87CCCF3EC2D12BECC8C9C9EE847 (void);
// 0x000000E1 System.Void maxstAR.MarkerTrackerBehaviour::set_MarkerID(System.Int32)
extern void MarkerTrackerBehaviour_set_MarkerID_mDB4A6573ACF10F82723ED51187134379B6CA7CAA (void);
// 0x000000E2 System.Single maxstAR.MarkerTrackerBehaviour::get_MarkerSize()
extern void MarkerTrackerBehaviour_get_MarkerSize_m6A5AAB5F412DB98F4A65A016E3DDB9F1A46382F4 (void);
// 0x000000E3 System.Void maxstAR.MarkerTrackerBehaviour::set_MarkerSize(System.Single)
extern void MarkerTrackerBehaviour_set_MarkerSize_m46F276AB4DCC5E037149E78A1B5007D05A4D2E3B (void);
// 0x000000E4 System.Void maxstAR.MarkerTrackerBehaviour::SetMarkerTrackerFileName(System.Int32,System.Single)
extern void MarkerTrackerBehaviour_SetMarkerTrackerFileName_m7EFF42C7CD781519F9A5658721CD1B8EED216F94 (void);
// 0x000000E5 System.Void maxstAR.MarkerTrackerBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void MarkerTrackerBehaviour_OnTrackSuccess_m9E5064A77DA0C46D550824B5E02916FDB95AE1E1 (void);
// 0x000000E6 System.Void maxstAR.MarkerTrackerBehaviour::OnTrackFail()
extern void MarkerTrackerBehaviour_OnTrackFail_mDF5AEC5E3A136D8611CF41F17DEC627565B75D5E (void);
// 0x000000E7 System.Void maxstAR.MarkerTrackerBehaviour::.ctor()
extern void MarkerTrackerBehaviour__ctor_mB058BA1868E865B648CF3D46822A68CF54465358 (void);
// 0x000000E8 System.Single maxstAR.ObjectTrackableBehaviour::get_RealSize()
extern void ObjectTrackableBehaviour_get_RealSize_mB82689C405CCCB712345D9BA3A8FDBC94F538951 (void);
// 0x000000E9 System.Void maxstAR.ObjectTrackableBehaviour::set_RealSize(System.Single)
extern void ObjectTrackableBehaviour_set_RealSize_mB2A8A1945E1F9EF9AF5CEEA579B5D82E3585C134 (void);
// 0x000000EA System.Void maxstAR.ObjectTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void ObjectTrackableBehaviour_OnTrackSuccess_m5ECDE2064A040A5AB868DF46B6D82DB8BCF86283 (void);
// 0x000000EB System.Void maxstAR.ObjectTrackableBehaviour::OnTrackFail()
extern void ObjectTrackableBehaviour_OnTrackFail_m432B93F75751D3B52BB7529501CA3800409C36CE (void);
// 0x000000EC System.Void maxstAR.ObjectTrackableBehaviour::.ctor()
extern void ObjectTrackableBehaviour__ctor_m419BDDA62E2A63D502B323BE412040035AAD140A (void);
// 0x000000ED System.String maxstAR.QrCodeTrackableBehaviour::get_QrCodeSearchingWords()
extern void QrCodeTrackableBehaviour_get_QrCodeSearchingWords_mCCD936FD29BBB09DECD1167B61396A0CDD4A8231 (void);
// 0x000000EE System.Void maxstAR.QrCodeTrackableBehaviour::set_QrCodeSearchingWords(System.String)
extern void QrCodeTrackableBehaviour_set_QrCodeSearchingWords_m7B162BC3E46197B873EAF4CE06DEC99EB945C939 (void);
// 0x000000EF System.Single maxstAR.QrCodeTrackableBehaviour::get_QrCodeRealSize()
extern void QrCodeTrackableBehaviour_get_QrCodeRealSize_m52FDB2D6AF88E2DBB14868D1F46EB7CE0E210A02 (void);
// 0x000000F0 System.Void maxstAR.QrCodeTrackableBehaviour::set_QrCodeRealSize(System.Single)
extern void QrCodeTrackableBehaviour_set_QrCodeRealSize_m9F14343CE85DBC4D38051371AD6C46D089C3052C (void);
// 0x000000F1 System.Void maxstAR.QrCodeTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void QrCodeTrackableBehaviour_OnTrackSuccess_mFAF923FA53E5027112036C0D243C8C1E14C050CC (void);
// 0x000000F2 System.Void maxstAR.QrCodeTrackableBehaviour::OnTrackFail()
extern void QrCodeTrackableBehaviour_OnTrackFail_m0B4A536013B6C2E073A0FDCD2B5763C4A2C9A951 (void);
// 0x000000F3 System.Void maxstAR.QrCodeTrackableBehaviour::.ctor()
extern void QrCodeTrackableBehaviour__ctor_m85B708AC69700D21B2DAA0C0C7F37D55DB25175A (void);
// 0x000000F4 System.Void maxstAR.WearableDeviceController::.ctor()
extern void WearableDeviceController__ctor_mFE6F47BBC5E06B99ACBCBE2395FF99B6CDE0B2C6 (void);
// 0x000000F5 System.Void maxstAR.WearableDeviceController::Init()
extern void WearableDeviceController_Init_m0B59A4DC7D85027CC2850B74B57B5201D2BB24C7 (void);
// 0x000000F6 System.Void maxstAR.WearableDeviceController::DeInit()
extern void WearableDeviceController_DeInit_mC965799ACE5666A10125A1F5B5E1E6C168E784B0 (void);
// 0x000000F7 System.Boolean maxstAR.WearableDeviceController::IsSupportedWearableDevice()
extern void WearableDeviceController_IsSupportedWearableDevice_mC9ECA396D4DB2EE4DA1604BCAC3E769BD0DCCFFD (void);
// 0x000000F8 System.String maxstAR.WearableDeviceController::GetModelName()
extern void WearableDeviceController_GetModelName_mB6F25268D3AE1B12807051444342456AA9BE925B (void);
// 0x000000F9 System.Void maxstAR.WearableDeviceController::SetStereoMode(System.Boolean)
extern void WearableDeviceController_SetStereoMode_m0158E69150CD2D38C1D271719B2070CA3048B166 (void);
// 0x000000FA System.Boolean maxstAR.WearableDeviceController::IsStereoEnabled()
extern void WearableDeviceController_IsStereoEnabled_m1D80A827662EE897D5317E223A89D95CA3A77E6F (void);
// 0x000000FB System.Boolean maxstAR.WearableDeviceController::IsSideBySideType()
extern void WearableDeviceController_IsSideBySideType_m4CD2B554F0DF724176301B9E1592B0062AB0C39E (void);
// 0x000000FC maxstAR.WearableManager maxstAR.WearableManager::GetInstance()
extern void WearableManager_GetInstance_m637FBD4CFE59BC8F00CEB69521DF2ABC42E216C4 (void);
// 0x000000FD System.Void maxstAR.WearableManager::.ctor()
extern void WearableManager__ctor_m364677DEBB9FB83CAE4DEF61D224C076D6311C7A (void);
// 0x000000FE maxstAR.WearableDeviceController maxstAR.WearableManager::GetDeviceController()
extern void WearableManager_GetDeviceController_m868C6C777B5F73C1DB5D2866DBDF25F66D2A0DED (void);
// 0x000000FF maxstAR.WearableCalibration maxstAR.WearableManager::GetCalibration()
extern void WearableManager_GetCalibration_m917859FD20B08F1DC7FF7874BB8B6E105A53B612 (void);
// 0x00000100 System.Void maxstAR.WearableManager::.cctor()
extern void WearableManager__cctor_mBDAE8A851F024D29AA5B9FC7A35ECF17A90E8970 (void);
// 0x00000101 System.Collections.IEnumerator maxstAR.APIController::POST(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int32,System.Action`1<System.String>)
extern void APIController_POST_m4B7832E65770F83646887987045053C7A7FBC9B8 (void);
// 0x00000102 System.String maxstAR.APIController::Escape(System.String)
extern void APIController_Escape_mA6E28E07F24C7EA35412EB6889B08C40F861BDF9 (void);
// 0x00000103 System.Collections.IEnumerator maxstAR.APIController::DownloadFile(System.String,System.String,System.Action`1<System.String>)
extern void APIController_DownloadFile_m5A9F6604CC9CAD6E028299EF94FA2D534D886F8D (void);
// 0x00000104 System.Void maxstAR.APIController::.ctor()
extern void APIController__ctor_m644D9B0DD6B34894765BCC2A9DCBCDECAED60922 (void);
// 0x00000105 System.Void maxstAR.APIController_<POST>d__0::.ctor(System.Int32)
extern void U3CPOSTU3Ed__0__ctor_m8B18AC9A9D0A18E1B0E00DB57FCE2C2A75F9E716 (void);
// 0x00000106 System.Void maxstAR.APIController_<POST>d__0::System.IDisposable.Dispose()
extern void U3CPOSTU3Ed__0_System_IDisposable_Dispose_mC53245B55296C9131843C47CEFCA965EB26BB6BC (void);
// 0x00000107 System.Boolean maxstAR.APIController_<POST>d__0::MoveNext()
extern void U3CPOSTU3Ed__0_MoveNext_mBA14A302009F2C66D93E7D50B5C8B95F4406FDF2 (void);
// 0x00000108 System.Object maxstAR.APIController_<POST>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPOSTU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF91203E6CB04F82AAA7D9B82A5B5034D7E654B3E (void);
// 0x00000109 System.Void maxstAR.APIController_<POST>d__0::System.Collections.IEnumerator.Reset()
extern void U3CPOSTU3Ed__0_System_Collections_IEnumerator_Reset_m5BDC27162CCB2EDEB272F624C20B1135A1D749AC (void);
// 0x0000010A System.Object maxstAR.APIController_<POST>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CPOSTU3Ed__0_System_Collections_IEnumerator_get_Current_m1788A56AB4715A6C693FC0082DE70BA50512E16D (void);
// 0x0000010B System.Void maxstAR.APIController_<DownloadFile>d__2::.ctor(System.Int32)
extern void U3CDownloadFileU3Ed__2__ctor_m8758FF5B05FE6C6A8FAD599401468A4451B37278 (void);
// 0x0000010C System.Void maxstAR.APIController_<DownloadFile>d__2::System.IDisposable.Dispose()
extern void U3CDownloadFileU3Ed__2_System_IDisposable_Dispose_mAE5289B45A32E7C0A907C14FCF64E8C7DCE9A856 (void);
// 0x0000010D System.Boolean maxstAR.APIController_<DownloadFile>d__2::MoveNext()
extern void U3CDownloadFileU3Ed__2_MoveNext_mA584706B1DAFAA4EBB79DB261774119DC9CF0DF6 (void);
// 0x0000010E System.Object maxstAR.APIController_<DownloadFile>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadFileU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m084D68C77F65F9FB194C3FD394B4598D89DF0E05 (void);
// 0x0000010F System.Void maxstAR.APIController_<DownloadFile>d__2::System.Collections.IEnumerator.Reset()
extern void U3CDownloadFileU3Ed__2_System_Collections_IEnumerator_Reset_m51A960BA3A23B5E4D861177EFF637A2A4B7888A6 (void);
// 0x00000110 System.Object maxstAR.APIController_<DownloadFile>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadFileU3Ed__2_System_Collections_IEnumerator_get_Current_mD7AA2A53E3356452C26D1E8E2334DA8705D9096F (void);
// 0x00000111 maxstAR.AbstractARManager maxstAR.AbstractARManager::get_Instance()
extern void AbstractARManager_get_Instance_m7D40524BF14232C5E6A6D4AF4A04A36EDE18FB66 (void);
// 0x00000112 System.Void maxstAR.AbstractARManager::Init()
extern void AbstractARManager_Init_m8A07903031B43348359E1B55A410A9268BDEE5A2 (void);
// 0x00000113 System.Void maxstAR.AbstractARManager::InitInternal()
extern void AbstractARManager_InitInternal_m526724444A3DA88E8A0744DFD1DD3A8C1BDD76D9 (void);
// 0x00000114 System.Void maxstAR.AbstractARManager::Update()
extern void AbstractARManager_Update_m984CB9F472D1292989B923B5D99375E54CD4A8DE (void);
// 0x00000115 System.Void maxstAR.AbstractARManager::Deinit()
extern void AbstractARManager_Deinit_m9B9B4DD1DDA8990CA0D1B432501697D6F7F7C0D3 (void);
// 0x00000116 maxstAR.AbstractARManager_WorldCenterMode maxstAR.AbstractARManager::get_WorldCenterModeSetting()
extern void AbstractARManager_get_WorldCenterModeSetting_mEAD7B7E7CE53508427BCE3C48AE8969A14C073BC (void);
// 0x00000117 UnityEngine.Camera maxstAR.AbstractARManager::GetARCamera()
extern void AbstractARManager_GetARCamera_m8A25D4F65F37B9D5FFC17868215702193A6DA66B (void);
// 0x00000118 System.Void maxstAR.AbstractARManager::SetWorldCenterMode(maxstAR.AbstractARManager_WorldCenterMode)
extern void AbstractARManager_SetWorldCenterMode_m4654AB9FAEFE58F085938ED85CB3863AB8347889 (void);
// 0x00000119 System.Void maxstAR.AbstractARManager::OnPreRender()
extern void AbstractARManager_OnPreRender_m1A558418BA6D775A98A4C9EBECA8C36FFD1330DC (void);
// 0x0000011A System.Void maxstAR.AbstractARManager::.ctor()
extern void AbstractARManager__ctor_m33A8FF7D5356B9D3018B80EFFCB62FBB7DEB4639 (void);
// 0x0000011B System.Void maxstAR.AbstractARManager::.cctor()
extern void AbstractARManager__cctor_m5CF88D25442016DEB5B5258310F406CCA93B632A (void);
// 0x0000011C maxstAR.AbstractCameraBackgroundBehaviour maxstAR.AbstractCameraBackgroundBehaviour::get_Instance()
extern void AbstractCameraBackgroundBehaviour_get_Instance_mDD4BE85534265958FAEF13922D0CD9F1BB26459F (void);
// 0x0000011D System.Void maxstAR.AbstractCameraBackgroundBehaviour::Awake()
extern void AbstractCameraBackgroundBehaviour_Awake_mDD13452A743BF9A4FDABF1B55C57ABF6B1E7F6BA (void);
// 0x0000011E System.Void maxstAR.AbstractCameraBackgroundBehaviour::OnApplicationPause(System.Boolean)
extern void AbstractCameraBackgroundBehaviour_OnApplicationPause_m54B6F2856374F3DE4F33A48AADCB25D31DC35447 (void);
// 0x0000011F System.Void maxstAR.AbstractCameraBackgroundBehaviour::OnEnable()
extern void AbstractCameraBackgroundBehaviour_OnEnable_mC987AC7F22C089D75CC1C53F65F289B9F48E245B (void);
// 0x00000120 System.Void maxstAR.AbstractCameraBackgroundBehaviour::OnDisable()
extern void AbstractCameraBackgroundBehaviour_OnDisable_mBD4B3C733EE36DC20F8BD3EE6E0A7228EDB111A2 (void);
// 0x00000121 System.Void maxstAR.AbstractCameraBackgroundBehaviour::OnDestroy()
extern void AbstractCameraBackgroundBehaviour_OnDestroy_mFCFCB63BE12BADC717F59FAE6C2EC5F349600F3C (void);
// 0x00000122 System.Void maxstAR.AbstractCameraBackgroundBehaviour::StartRendering()
extern void AbstractCameraBackgroundBehaviour_StartRendering_m2EFC0F3A181C163E4B4CDD91B23D008205887483 (void);
// 0x00000123 System.Void maxstAR.AbstractCameraBackgroundBehaviour::StopRendering()
extern void AbstractCameraBackgroundBehaviour_StopRendering_m8121621802CF60485CB87782995C7EC26E5036B9 (void);
// 0x00000124 System.Void maxstAR.AbstractCameraBackgroundBehaviour::CreateCameraTexture(System.Int32,System.Int32,maxstAR.ColorFormat)
extern void AbstractCameraBackgroundBehaviour_CreateCameraTexture_m45EC199D1329EB24F36708C0D018F4309E5F28BB (void);
// 0x00000125 System.Void maxstAR.AbstractCameraBackgroundBehaviour::UpdateCameraBackgroundImage(maxstAR.TrackingState)
extern void AbstractCameraBackgroundBehaviour_UpdateCameraBackgroundImage_m7F7AFBED65DE836D04DDB7DA4021657CDA63A850 (void);
// 0x00000126 System.Void maxstAR.AbstractCameraBackgroundBehaviour::UpdateCameraTexture(maxstAR.TrackedImage)
extern void AbstractCameraBackgroundBehaviour_UpdateCameraTexture_m85ACA56AB1B5DE83214A883379A83A613F6DF708 (void);
// 0x00000127 System.Void maxstAR.AbstractCameraBackgroundBehaviour::TransformBackgroundPlane()
extern void AbstractCameraBackgroundBehaviour_TransformBackgroundPlane_m816D05BC0A05282B2A16BD1A947EA6A4079FEDE8 (void);
// 0x00000128 System.Boolean maxstAR.AbstractCameraBackgroundBehaviour::RenderingEnabled()
extern void AbstractCameraBackgroundBehaviour_RenderingEnabled_mD5924B962C71C7B43EFA646979E23A7F7E2798E6 (void);
// 0x00000129 System.Void maxstAR.AbstractCameraBackgroundBehaviour::.ctor()
extern void AbstractCameraBackgroundBehaviour__ctor_m5E482D5ADF3EB9E57F31B224B225DDACEDD579C8 (void);
// 0x0000012A System.Void maxstAR.AbstractCameraBackgroundBehaviour::.cctor()
extern void AbstractCameraBackgroundBehaviour__cctor_mEF4042EFAFF4C788623CD2C9DC9172653DAE464D (void);
// 0x0000012B System.Void maxstAR.AbstractCloudTrackableBehaviour::Start()
extern void AbstractCloudTrackableBehaviour_Start_m012BB5701DCED1874A666D7F140DE3F144F9EB25 (void);
// 0x0000012C System.Void maxstAR.AbstractCloudTrackableBehaviour::OnTrackerCloudName(System.String)
extern void AbstractCloudTrackableBehaviour_OnTrackerCloudName_mDE96D9A63DE9179F3EEDD951389F09AC6B418D75 (void);
// 0x0000012D System.Void maxstAR.AbstractCloudTrackableBehaviour::.ctor()
extern void AbstractCloudTrackableBehaviour__ctor_m65DC0C23151A3B0C2D297F516E04AD646344811A (void);
// 0x0000012E maxstAR.AbstractConfigurationScriptableObject maxstAR.AbstractConfigurationScriptableObject::GetInstance()
extern void AbstractConfigurationScriptableObject_GetInstance_m817B4F645D8D4D438CB6CA55CB4F369947346DB0 (void);
// 0x0000012F System.Void maxstAR.AbstractConfigurationScriptableObject::.ctor()
extern void AbstractConfigurationScriptableObject__ctor_m5D4C4CAD1228274A85A46B71AB32219CE3D29CEE (void);
// 0x00000130 System.Void maxstAR.AbstractConfigurationScriptableObject::.cctor()
extern void AbstractConfigurationScriptableObject__cctor_m6CA54FE2D4930269A2306841C8EE1CF9E87996B3 (void);
// 0x00000131 System.Void maxstAR.AbstractFeaturePointBehaviour::OnApplicationPause(System.Boolean)
extern void AbstractFeaturePointBehaviour_OnApplicationPause_m267C33415C614AA97CD717A473733068ACDFA2C7 (void);
// 0x00000132 System.Void maxstAR.AbstractFeaturePointBehaviour::Generate(UnityEngine.Vector3[])
extern void AbstractFeaturePointBehaviour_Generate_mB4D1BCEEEFAD26FAA8126884F8AB16FC1F03A3FE (void);
// 0x00000133 UnityEngine.Vector3[] maxstAR.AbstractFeaturePointBehaviour::convertFloatToVertex3(System.Single[],System.Int32)
extern void AbstractFeaturePointBehaviour_convertFloatToVertex3_m5A115A7D6FCCD3AAB7F9F051670DC041A396870C (void);
// 0x00000134 System.Void maxstAR.AbstractFeaturePointBehaviour::Start()
extern void AbstractFeaturePointBehaviour_Start_mE75A7B77873331EECDEBCB1AB057FB8DC4F35A67 (void);
// 0x00000135 System.Void maxstAR.AbstractFeaturePointBehaviour::Update()
extern void AbstractFeaturePointBehaviour_Update_m279837D60FE2DD0A18C575BAAF06708B6C754885 (void);
// 0x00000136 System.Void maxstAR.AbstractFeaturePointBehaviour::.ctor()
extern void AbstractFeaturePointBehaviour__ctor_mBB7E11573E39269BFF27225612149E96DE46D2CF (void);
// 0x00000137 System.Single maxstAR.AbstractImageTrackableBehaviour::get_TargetWidth()
extern void AbstractImageTrackableBehaviour_get_TargetWidth_m931E767D38410ED7B6CCC96FA103BA275C0E029F (void);
// 0x00000138 System.Single maxstAR.AbstractImageTrackableBehaviour::get_TargetHeight()
extern void AbstractImageTrackableBehaviour_get_TargetHeight_m490445BAA2225BFEA0DBF5C1ED82DC0A31CBBEBC (void);
// 0x00000139 System.Void maxstAR.AbstractImageTrackableBehaviour::Start()
extern void AbstractImageTrackableBehaviour_Start_m8DDAD3BADD1D3267A19689D4DC27C50418F6EEED (void);
// 0x0000013A System.Void maxstAR.AbstractImageTrackableBehaviour::OnTrackerDataFileChanged(System.String)
extern void AbstractImageTrackableBehaviour_OnTrackerDataFileChanged_m578DE17C773310CEE6EB1C43AE808B47C0D2A911 (void);
// 0x0000013B System.Void maxstAR.AbstractImageTrackableBehaviour::ChangeObjectProperty(System.String,System.Single)
extern void AbstractImageTrackableBehaviour_ChangeObjectProperty_mA2A71792D1024F49523A8BFF08686E3AD88504AA (void);
// 0x0000013C System.Void maxstAR.AbstractImageTrackableBehaviour::.ctor()
extern void AbstractImageTrackableBehaviour__ctor_m26F32A7990629E3445BB2F45C4324B93CB1B1618 (void);
// 0x0000013D System.Void maxstAR.AbstractImageTrackableBehaviour_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m0D480A6A701277BB949634E5A19E560096D3EA13 (void);
// 0x0000013E System.Void maxstAR.AbstractImageTrackableBehaviour_<>c__DisplayClass9_0::<ChangeObjectProperty>b__0(System.Int32,System.Int32,UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass9_0_U3CChangeObjectPropertyU3Eb__0_mAEAD3D5C272E3EB08601CEF0696161BD13D4A46E (void);
// 0x0000013F System.Void maxstAR.AbstractInstantTrackableBehaviour::.ctor()
extern void AbstractInstantTrackableBehaviour__ctor_m5B9CD7A85A5E6DD6557D5E5FCD69BD5E455D5838 (void);
// 0x00000140 System.Int32 maxstAR.AbstractMapViewerBehaviour::get_KeyframeIndex()
extern void AbstractMapViewerBehaviour_get_KeyframeIndex_m1CA0B1DE92B49A8CCC34C49A7B0C622A95A4DDBC (void);
// 0x00000141 System.Void maxstAR.AbstractMapViewerBehaviour::set_KeyframeIndex(System.Int32)
extern void AbstractMapViewerBehaviour_set_KeyframeIndex_m9FFE3D6917C0781FB92E240A42BF67F4CE04C847 (void);
// 0x00000142 System.Boolean maxstAR.AbstractMapViewerBehaviour::get_ShowMesh()
extern void AbstractMapViewerBehaviour_get_ShowMesh_mF02248B8DACE3DA0ACD2C1056F9D85917194BD52 (void);
// 0x00000143 System.Void maxstAR.AbstractMapViewerBehaviour::set_ShowMesh(System.Boolean)
extern void AbstractMapViewerBehaviour_set_ShowMesh_m89B334BCD3B4B77353473B3FCC925DDF265F397F (void);
// 0x00000144 System.Int32 maxstAR.AbstractMapViewerBehaviour::get_MaxKeyframeCount()
extern void AbstractMapViewerBehaviour_get_MaxKeyframeCount_mDFF547CA0AD5AF84B5AB0696110F73B1E1782F2E (void);
// 0x00000145 System.Void maxstAR.AbstractMapViewerBehaviour::set_MaxKeyframeCount(System.Int32)
extern void AbstractMapViewerBehaviour_set_MaxKeyframeCount_mC15F5F82DAA55D42CC015C02547C68CB5AC2EC19 (void);
// 0x00000146 System.Boolean maxstAR.AbstractMapViewerBehaviour::get_Transparent()
extern void AbstractMapViewerBehaviour_get_Transparent_mC459A8E663D3C750DA6F93E2549089F6F22D2034 (void);
// 0x00000147 System.Void maxstAR.AbstractMapViewerBehaviour::set_Transparent(System.Boolean)
extern void AbstractMapViewerBehaviour_set_Transparent_m885293F63D5C495C266D8589C8DC2E20C0FC73AC (void);
// 0x00000148 System.Boolean maxstAR.AbstractMapViewerBehaviour::Load(System.String,System.Single)
extern void AbstractMapViewerBehaviour_Load_m190CEFBD02C46C05BF9F98B8AD5F97052E8B4D92 (void);
// 0x00000149 System.Boolean maxstAR.AbstractMapViewerBehaviour::ReadMap(System.String,System.Single)
extern void AbstractMapViewerBehaviour_ReadMap_m43B02859AD5A6E692671483A44A77F7EFD824843 (void);
// 0x0000014A System.Void maxstAR.AbstractMapViewerBehaviour::SetTransparent(System.Boolean)
extern void AbstractMapViewerBehaviour_SetTransparent_mA52B79F6D0D04CB94733D553A3765B5D228ACDDA (void);
// 0x0000014B UnityEngine.Texture2D maxstAR.AbstractMapViewerBehaviour::GetCameraTexture(System.Int32,System.Int32,System.Int32)
extern void AbstractMapViewerBehaviour_GetCameraTexture_mF18BD7ABF5F8B802FD1755D29F70299B2D0477D8 (void);
// 0x0000014C System.Void maxstAR.AbstractMapViewerBehaviour::UpdateMapViewer()
extern void AbstractMapViewerBehaviour_UpdateMapViewer_m10359D97D7E8DA229E836EF87AFF099F2CC3F7E6 (void);
// 0x0000014D System.Void maxstAR.AbstractMapViewerBehaviour::ApplyViewCamera(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void AbstractMapViewerBehaviour_ApplyViewCamera_m4330E1C882DBA08622F03E98AD0F331BA6F29256 (void);
// 0x0000014E System.Void maxstAR.AbstractMapViewerBehaviour::.ctor()
extern void AbstractMapViewerBehaviour__ctor_m2AD5E019B3BE3C93B8F0B065E377BDE95CD69720 (void);
// 0x0000014F System.Void maxstAR.AbstractMarkerTrackableBehaviour::Start()
extern void AbstractMarkerTrackableBehaviour_Start_m5D7CF7B02474BFDCD924D8434A1C9FC78A5F877E (void);
// 0x00000150 System.Void maxstAR.AbstractMarkerTrackableBehaviour::OnTrackerDataFileChanged(System.String)
extern void AbstractMarkerTrackableBehaviour_OnTrackerDataFileChanged_m623D76CF5EE697FED673474BE5B64C5D1657D386 (void);
// 0x00000151 System.Void maxstAR.AbstractMarkerTrackableBehaviour::SetTargetTexture(System.String)
extern void AbstractMarkerTrackableBehaviour_SetTargetTexture_mDCE5E0427B3383C2E26B543CA8F0C1680624965A (void);
// 0x00000152 System.Void maxstAR.AbstractMarkerTrackableBehaviour::.ctor()
extern void AbstractMarkerTrackableBehaviour__ctor_m2F63E8D20F9D29CF2584B3C16D33639E49FF5A4B (void);
// 0x00000153 System.Void maxstAR.AbstractMarkerTrackableBehaviour::<SetTargetTexture>b__3_0(System.Int32,System.Int32,UnityEngine.Texture2D)
extern void AbstractMarkerTrackableBehaviour_U3CSetTargetTextureU3Eb__3_0_mE25F046AC60C97C1D3C4FF60AA2C71F388CE7BF4 (void);
// 0x00000154 System.Void maxstAR.AbstractObjectTrackableBehaviour::Start()
extern void AbstractObjectTrackableBehaviour_Start_m52DC647B315DDE9CE59AE3657851EA9B3CBD1F28 (void);
// 0x00000155 System.Void maxstAR.AbstractObjectTrackableBehaviour::OnTrackerDataFileChanged(System.String)
extern void AbstractObjectTrackableBehaviour_OnTrackerDataFileChanged_m80F1B60A137E19A2B16F4DB9AE35A75CAA3F5E26 (void);
// 0x00000156 System.Void maxstAR.AbstractObjectTrackableBehaviour::.ctor()
extern void AbstractObjectTrackableBehaviour__ctor_m361D0F1CBD29977E796122835EEC4F5F008789F6 (void);
// 0x00000157 System.Single maxstAR.AbstractQrCodeTrackableBehaviour::get_TargetWidth()
extern void AbstractQrCodeTrackableBehaviour_get_TargetWidth_mB6F07A9620F9CC47CADB670D39F4602903FC24CC (void);
// 0x00000158 System.Single maxstAR.AbstractQrCodeTrackableBehaviour::get_TargetHeight()
extern void AbstractQrCodeTrackableBehaviour_get_TargetHeight_m373AD6AB97163E03646708D5C910E85D60295217 (void);
// 0x00000159 System.Void maxstAR.AbstractQrCodeTrackableBehaviour::Start()
extern void AbstractQrCodeTrackableBehaviour_Start_mCD00ED9B28F51A5864A56855E9109328B1E3D48D (void);
// 0x0000015A System.Void maxstAR.AbstractQrCodeTrackableBehaviour::OnTrackerDataFileChanged(System.String)
extern void AbstractQrCodeTrackableBehaviour_OnTrackerDataFileChanged_m8095D85685E404810BF43A4F4537013C00FBF9A7 (void);
// 0x0000015B System.Void maxstAR.AbstractQrCodeTrackableBehaviour::ChangeObjectProperty(System.String,System.Single)
extern void AbstractQrCodeTrackableBehaviour_ChangeObjectProperty_m3F3ED3047AD6A634AF2A57FFC5B39BC3687EFED7 (void);
// 0x0000015C System.Void maxstAR.AbstractQrCodeTrackableBehaviour::.ctor()
extern void AbstractQrCodeTrackableBehaviour__ctor_mF316FE8B163F8D67B793FD9900627F9C0D93D950 (void);
// 0x0000015D System.Void maxstAR.AbstractQrCodeTrackableBehaviour_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mA0EC57F299C6A1D988F399F458CBFE24F0610FC1 (void);
// 0x0000015E System.Void maxstAR.AbstractQrCodeTrackableBehaviour_<>c__DisplayClass9_0::<ChangeObjectProperty>b__0(System.Int32,System.Int32,UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass9_0_U3CChangeObjectPropertyU3Eb__0_m94984962456FCF23E8AE4419AF71239ABED40F9E (void);
// 0x0000015F maxstAR.StorageType maxstAR.AbstractTrackableBehaviour::get_StorageType()
extern void AbstractTrackableBehaviour_get_StorageType_m2BBB71A55CB5FEF9CDD89D4ABDD939F30B08ED9A (void);
// 0x00000160 System.Void maxstAR.AbstractTrackableBehaviour::set_StorageType(maxstAR.StorageType)
extern void AbstractTrackableBehaviour_set_StorageType_m45AB0B80FFCAB7225CD2B520234A581B6D0AC934 (void);
// 0x00000161 UnityEngine.Object maxstAR.AbstractTrackableBehaviour::get_TrackerDataFileObject()
extern void AbstractTrackableBehaviour_get_TrackerDataFileObject_mE565071FE69543A330D7BBC718FB2B2FD7CB775C (void);
// 0x00000162 System.Void maxstAR.AbstractTrackableBehaviour::set_TrackerDataFileObject(UnityEngine.Object)
extern void AbstractTrackableBehaviour_set_TrackerDataFileObject_mB2F4E0421376DF1C669B76E1D376961EDFEEDEEC (void);
// 0x00000163 System.String maxstAR.AbstractTrackableBehaviour::get_TrackerDataFileName()
extern void AbstractTrackableBehaviour_get_TrackerDataFileName_m0DE6B48A9BF3F2CA36CA7D3DFDF05052539C0D51 (void);
// 0x00000164 System.Void maxstAR.AbstractTrackableBehaviour::set_TrackerDataFileName(System.String)
extern void AbstractTrackableBehaviour_set_TrackerDataFileName_m724B0E37AFA68C112FB89F3B66FE4F2A7988CA41 (void);
// 0x00000165 System.String maxstAR.AbstractTrackableBehaviour::get_TrackableId()
extern void AbstractTrackableBehaviour_get_TrackableId_m12B931DD4EE524250CE00F4D8445AE19A4DD8D58 (void);
// 0x00000166 System.Void maxstAR.AbstractTrackableBehaviour::set_TrackableId(System.String)
extern void AbstractTrackableBehaviour_set_TrackableId_m442B3876129827A23A2EC79A74D12A0C020833C2 (void);
// 0x00000167 System.String maxstAR.AbstractTrackableBehaviour::get_TrackableName()
extern void AbstractTrackableBehaviour_get_TrackableName_m4E2D41E96FDBE9F6311123F474A53518C9959B90 (void);
// 0x00000168 System.Void maxstAR.AbstractTrackableBehaviour::set_TrackableName(System.String)
extern void AbstractTrackableBehaviour_set_TrackableName_m02E87D73DAAA3E30CEE3BD0CEC1181044E5F3222 (void);
// 0x00000169 System.Void maxstAR.AbstractTrackableBehaviour::OnTrackerDataFileChanged(System.String)
extern void AbstractTrackableBehaviour_OnTrackerDataFileChanged_m2CE30B54D67B011F59067B4547E37CEAA4CB97DE (void);
// 0x0000016A System.Void maxstAR.AbstractTrackableBehaviour::OnTrackFail()
extern void AbstractTrackableBehaviour_OnTrackFail_m74271C81ABB8AD11949918B851268A7885B6872A (void);
// 0x0000016B System.Void maxstAR.AbstractTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void AbstractTrackableBehaviour_OnTrackSuccess_m7EC7E3A05A38D7E83E0C9C7BA122F70D2FDAA7AD (void);
// 0x0000016C System.Void maxstAR.AbstractTrackableBehaviour::.ctor()
extern void AbstractTrackableBehaviour__ctor_m87ECDD54EF4F5EF18CB24AB0CB758F8372639BE6 (void);
// 0x0000016D maxstAR.CameraDevice maxstAR.CameraDevice::GetInstance()
extern void CameraDevice_GetInstance_m7B4D41A3F7D4DBE7D23EA6B9DD7E8CC47D9C4849 (void);
// 0x0000016E System.Void maxstAR.CameraDevice::.ctor()
extern void CameraDevice__ctor_mF5F0FA1C514129160D121CEF08AE7FD50A35CCC5 (void);
// 0x0000016F maxstAR.ResultCode maxstAR.CameraDevice::Start()
extern void CameraDevice_Start_m0EF2AE94ED109805AAECDBBFD84849BB77072F22 (void);
// 0x00000170 System.Boolean maxstAR.CameraDevice::SetNewFrame(System.Byte[],System.Int32,System.Int32,System.Int32,maxstAR.ColorFormat)
extern void CameraDevice_SetNewFrame_m99BC5C4CF010EA4DD9A9900F1766B93EC72312EA (void);
// 0x00000171 System.Boolean maxstAR.CameraDevice::SetNewFrame(System.UInt64,System.Int32,System.Int32,System.Int32,maxstAR.ColorFormat)
extern void CameraDevice_SetNewFrame_mBC9C8AC5D4A1A0CF40AA5658BCC936EA007A20F2 (void);
// 0x00000172 System.Boolean maxstAR.CameraDevice::SetNewFrameAndTimestamp(System.Byte[],System.Int32,System.Int32,System.Int32,maxstAR.ColorFormat,System.UInt64)
extern void CameraDevice_SetNewFrameAndTimestamp_m70A04DC093F5DF1FB9B6856A8B5330AA6629084E (void);
// 0x00000173 System.Boolean maxstAR.CameraDevice::SetNewFrameAndTimestamp(System.UInt64,System.Int32,System.Int32,System.Int32,maxstAR.ColorFormat,System.UInt64)
extern void CameraDevice_SetNewFrameAndTimestamp_mB68F5AED6AB59BFFA302F67E2F36F2B9A30CE144 (void);
// 0x00000174 System.Boolean maxstAR.CameraDevice::SetFocusMode(maxstAR.CameraDevice_FocusMode)
extern void CameraDevice_SetFocusMode_mE967450239E66416FD9BE18839932850A0EE56B7 (void);
// 0x00000175 System.Boolean maxstAR.CameraDevice::SetFlashLightMode(System.Boolean)
extern void CameraDevice_SetFlashLightMode_mD750C1983547711E0C28029BAE5E23EB0552B329 (void);
// 0x00000176 System.Boolean maxstAR.CameraDevice::SetAutoWhiteBalanceLock(System.Boolean)
extern void CameraDevice_SetAutoWhiteBalanceLock_m5000BF2E7E8DF8F80D4CC90D26265348D985DFA5 (void);
// 0x00000177 System.Void maxstAR.CameraDevice::FlipVideo(maxstAR.CameraDevice_FlipDirection,System.Boolean)
extern void CameraDevice_FlipVideo_m39F2804F6C7B6E45DD9BA1E91D52066693C5BA99 (void);
// 0x00000178 System.Boolean maxstAR.CameraDevice::IsVideoFlipped(maxstAR.CameraDevice_FlipDirection)
extern void CameraDevice_IsVideoFlipped_m4121AD281EBCE4BC25A22A2FBD1FCE7150AF83F5 (void);
// 0x00000179 System.Boolean maxstAR.CameraDevice::SetZoom(System.Single)
extern void CameraDevice_SetZoom_m8F455796C6AA7A0DF54B060516923723161428EF (void);
// 0x0000017A System.Single maxstAR.CameraDevice::getMaxZoomValue()
extern void CameraDevice_getMaxZoomValue_m16B2FF622B9E3FCAA0E8FCECC9B188D7D188D641 (void);
// 0x0000017B System.Collections.Generic.List`1<System.String> maxstAR.CameraDevice::GetParamList()
extern void CameraDevice_GetParamList_mB4A3C3F0CD66F0810FE7E644B7420E92623EE8CF (void);
// 0x0000017C System.Boolean maxstAR.CameraDevice::SetParam(System.String,System.Boolean)
extern void CameraDevice_SetParam_m2779AF750BDFE7734CFC85FE38556A71393B2A89 (void);
// 0x0000017D System.Boolean maxstAR.CameraDevice::SetParam(System.String,System.Int32)
extern void CameraDevice_SetParam_mCB589383D69DE6E20AA21E0AE6FCE461BE81904C (void);
// 0x0000017E System.Boolean maxstAR.CameraDevice::SetParam(System.String,System.Int32,System.Int32)
extern void CameraDevice_SetParam_m1F021E6329BB58E534E1D0E62EBC203698810EF5 (void);
// 0x0000017F System.Boolean maxstAR.CameraDevice::SetParam(System.String,System.String)
extern void CameraDevice_SetParam_mD3B26FF46801948833C6FD25E1BBF76DC467A903 (void);
// 0x00000180 maxstAR.ResultCode maxstAR.CameraDevice::Stop()
extern void CameraDevice_Stop_m9C379AE847093944B5814516045576677915F1C1 (void);
// 0x00000181 System.Int32 maxstAR.CameraDevice::GetWidth()
extern void CameraDevice_GetWidth_mBDC8FFBF8A3883FC4F8AEA4A188B7E9A8E2E03E9 (void);
// 0x00000182 System.Int32 maxstAR.CameraDevice::GetHeight()
extern void CameraDevice_GetHeight_mB6E0B66A7B14EEA2B64E59838A6C0E7669D19016 (void);
// 0x00000183 UnityEngine.Matrix4x4 maxstAR.CameraDevice::GetProjectionMatrix()
extern void CameraDevice_GetProjectionMatrix_m8BFDF8998C90D524AB3D32E5B1473C382CFA7F8F (void);
// 0x00000184 System.Single[] maxstAR.CameraDevice::GetBackgroundPlaneInfo()
extern void CameraDevice_GetBackgroundPlaneInfo_m729FECA5316B236DFC59245BECC414B471A507AC (void);
// 0x00000185 System.Boolean maxstAR.CameraDevice::IsFlipHorizontal()
extern void CameraDevice_IsFlipHorizontal_m863E8A65FC2747908D596605F0871D0E7B1EC034 (void);
// 0x00000186 System.Boolean maxstAR.CameraDevice::IsFlipVertical()
extern void CameraDevice_IsFlipVertical_m9ADE0CC01822BE740564256778390715601119F3 (void);
// 0x00000187 System.Boolean maxstAR.CameraDevice::CheckCameraMove(maxstAR.TrackedImage)
extern void CameraDevice_CheckCameraMove_m52F20E396766013B043435B6BFD5CACE1B8D1600 (void);
// 0x00000188 System.Void maxstAR.CameraDevice::SetFusionEnable()
extern void CameraDevice_SetFusionEnable_mCCC1EB2C8953A4730BFA87C8309B4AC48427FD86 (void);
// 0x00000189 System.Void maxstAR.CameraDevice::SetARCoreTexture()
extern void CameraDevice_SetARCoreTexture_m38D00985DD03345959DB7A15BADE71F77A37C472 (void);
// 0x0000018A System.Void maxstAR.CameraDevice::.cctor()
extern void CameraDevice__cctor_m12EC8EF441A1F68493373E31F0933273C18F9976 (void);
// 0x0000018B System.Void maxstAR.CloudRecognitionAPIController::Recognize(System.String,System.String,System.String,System.Action`1<System.String>)
extern void CloudRecognitionAPIController_Recognize_m432A1484A56094ABFF8D59ADFA147177383480D9 (void);
// 0x0000018C System.Void maxstAR.CloudRecognitionAPIController::DownloadCloudDataAndSave(System.String,System.String,System.Action`1<System.String>)
extern void CloudRecognitionAPIController_DownloadCloudDataAndSave_mEE40E349F44D619C41B2964482BA90927C154D9D (void);
// 0x0000018D System.String maxstAR.CloudRecognitionAPIController::JWTEncode(System.String,System.String)
extern void CloudRecognitionAPIController_JWTEncode_mF2B569C90D25C079E601920B727ED9695D7523F0 (void);
// 0x0000018E System.Void maxstAR.CloudRecognitionAPIController::destroyApi()
extern void CloudRecognitionAPIController_destroyApi_mBCD89AB176C222114107FA1D1B7130647DC063E4 (void);
// 0x0000018F System.Void maxstAR.CloudRecognitionAPIController::.ctor()
extern void CloudRecognitionAPIController__ctor_m316933BF81F638B30D81683B09E5537D3BF6FAF6 (void);
// 0x00000190 System.Void maxstAR.CloudRecognitionAPIController_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mF7F58D1D6B0F86C36E7B13F6867B1D04B586CA4A (void);
// 0x00000191 System.Void maxstAR.CloudRecognitionAPIController_<>c__DisplayClass1_0::<Recognize>b__0(System.String)
extern void U3CU3Ec__DisplayClass1_0_U3CRecognizeU3Eb__0_mA57F8FFAD6650DE1895D17CB5E99FD6F48ADF242 (void);
// 0x00000192 System.Void maxstAR.CloudRecognitionAPIController_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m3D11DDFA7D3DEDF9B8D3623BA4E9A10391DFF4D9 (void);
// 0x00000193 System.Void maxstAR.CloudRecognitionAPIController_<>c__DisplayClass2_0::<DownloadCloudDataAndSave>b__0(System.String)
extern void U3CU3Ec__DisplayClass2_0_U3CDownloadCloudDataAndSaveU3Eb__0_m6E96A6526E4EDABF505EF5382EAE3840C8CF85A3 (void);
// 0x00000194 System.Void maxstAR.CloudRecognitionController::SetCloudRecognitionSecretIdAndSecretKey(System.String,System.String)
extern void CloudRecognitionController_SetCloudRecognitionSecretIdAndSecretKey_mC94E6E91003122DA32C20D230D4BB26620A696C2 (void);
// 0x00000195 maxstAR.CloudRecognitionController_CloudState maxstAR.CloudRecognitionController::GetCloudStatus()
extern void CloudRecognitionController_GetCloudStatus_m917E33C7A16CFD59C79B72F893AAFECB17321516 (void);
// 0x00000196 System.Void maxstAR.CloudRecognitionController::StartTracker()
extern void CloudRecognitionController_StartTracker_m599AB716748B8719527BC5AE715BD1DABF9E502A (void);
// 0x00000197 System.Void maxstAR.CloudRecognitionController::StopTracker()
extern void CloudRecognitionController_StopTracker_m0F415836231846E3ADD0C095BF9658BF9801D653 (void);
// 0x00000198 System.Void maxstAR.CloudRecognitionController::DestroyTracker()
extern void CloudRecognitionController_DestroyTracker_mC6C84ED7F76E5D2677DA283A94D52C0630B040AD (void);
// 0x00000199 System.Void maxstAR.CloudRecognitionController::FindImageOfCloudRecognition()
extern void CloudRecognitionController_FindImageOfCloudRecognition_mF0E2390A1E5AD23671EC0B45B4CFECE269E79A04 (void);
// 0x0000019A System.Void maxstAR.CloudRecognitionController::SetAutoEnable(System.Boolean)
extern void CloudRecognitionController_SetAutoEnable_mB327DE2DC13502831DBBFCA35F0B0F07FC8C255E (void);
// 0x0000019B System.Void maxstAR.CloudRecognitionController::Update()
extern void CloudRecognitionController_Update_m85ADB3DF79456DF8D034F9E14B3FFEF392E4AFFC (void);
// 0x0000019C System.Void maxstAR.CloudRecognitionController::StartCloud()
extern void CloudRecognitionController_StartCloud_mF97C95FD6E30A36AC304066AD4735BF14BDC7C44 (void);
// 0x0000019D System.Boolean maxstAR.CloudRecognitionController::GetFeatureClient(maxstAR.TrackedImage,System.Byte[],System.Int32[])
extern void CloudRecognitionController_GetFeatureClient_m8EB55CD3F15957FE9BE89067A1E126AA3FE680D4 (void);
// 0x0000019E System.Void maxstAR.CloudRecognitionController::GetCloudRecognition(maxstAR.TrackedImage,System.Action`2<System.Boolean,System.String>)
extern void CloudRecognitionController_GetCloudRecognition_m0F8938E560BBCB741596AF5505A9F6679493CE8F (void);
// 0x0000019F System.Void maxstAR.CloudRecognitionController::.ctor()
extern void CloudRecognitionController__ctor_mCFA809A8341EE88F74C1D2A82575602E794B3CB8 (void);
// 0x000001A0 System.Void maxstAR.CloudRecognitionController::<FindImageOfCloudRecognition>b__18_0()
extern void CloudRecognitionController_U3CFindImageOfCloudRecognitionU3Eb__18_0_m23C114CABB583CC9E10029220F013752060C0611 (void);
// 0x000001A1 System.Void maxstAR.CloudRecognitionController::<FindImageOfCloudRecognition>b__18_1(System.Boolean,System.String)
extern void CloudRecognitionController_U3CFindImageOfCloudRecognitionU3Eb__18_1_m6AED8A10BC77B0B9E2DF0C289BB3E7F553C13AC5 (void);
// 0x000001A2 System.Void maxstAR.CloudRecognitionController::<Update>b__20_0(System.String)
extern void CloudRecognitionController_U3CUpdateU3Eb__20_0_mF2B268F00305D2BE26AE3F3F94B181AB257DF1D3 (void);
// 0x000001A3 System.Void maxstAR.CloudRecognitionController::<StartCloud>b__21_0(System.Boolean,System.String)
extern void CloudRecognitionController_U3CStartCloudU3Eb__21_0_mCCD75AC229691F2B2A17F14A1A73DA4103A5F2FF (void);
// 0x000001A4 System.Void maxstAR.CloudRecognitionController_<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m9C6A0C0486AACED086721E4D1B6C322E3F5A0059 (void);
// 0x000001A5 System.Void maxstAR.CloudRecognitionController_<>c__DisplayClass20_1::.ctor()
extern void U3CU3Ec__DisplayClass20_1__ctor_mEC57BE075DF90D86EE44DB5E0AE42DB2EE7D098D (void);
// 0x000001A6 System.Void maxstAR.CloudRecognitionController_<>c__DisplayClass20_1::<Update>b__1(System.String)
extern void U3CU3Ec__DisplayClass20_1_U3CUpdateU3Eb__1_m18631C370200AEE3E66247605F4C28F22757D514 (void);
// 0x000001A7 System.String maxstAR.CloudRecognitionData::get_ImgId()
extern void CloudRecognitionData_get_ImgId_mCA032244632C578194150EC4FBBB952453005F5F (void);
// 0x000001A8 System.Void maxstAR.CloudRecognitionData::set_ImgId(System.String)
extern void CloudRecognitionData_set_ImgId_mFF7235537DCBDDDC9C0896DB42F0938BFA01DA56 (void);
// 0x000001A9 System.String maxstAR.CloudRecognitionData::get_Custom()
extern void CloudRecognitionData_get_Custom_m8C9B2490C5C61345DC912E1D00D8E1187EDC4243 (void);
// 0x000001AA System.Void maxstAR.CloudRecognitionData::set_Custom(System.String)
extern void CloudRecognitionData_set_Custom_m661AFD5D37038F4D57F8695B57892B0AB3351D34 (void);
// 0x000001AB System.String maxstAR.CloudRecognitionData::get_Track2dMapUrl()
extern void CloudRecognitionData_get_Track2dMapUrl_mFACBC40BD0575A942177731CE2F35F87537CB97C (void);
// 0x000001AC System.Void maxstAR.CloudRecognitionData::set_Track2dMapUrl(System.String)
extern void CloudRecognitionData_set_Track2dMapUrl_mCD55B656D52E84F0F4A225EB4C92716C627268F6 (void);
// 0x000001AD System.String maxstAR.CloudRecognitionData::get_Name()
extern void CloudRecognitionData_get_Name_m34A6CC8B54DACF7333920827E03AD0DF548B3D2A (void);
// 0x000001AE System.Void maxstAR.CloudRecognitionData::set_Name(System.String)
extern void CloudRecognitionData_set_Name_m85E503DEB98326367DA4C223A7E96A9664FF6EAA (void);
// 0x000001AF System.String maxstAR.CloudRecognitionData::get_ImgGSUrl()
extern void CloudRecognitionData_get_ImgGSUrl_mB933724ABFBBF603C62041EA585F8BB64497403A (void);
// 0x000001B0 System.Void maxstAR.CloudRecognitionData::set_ImgGSUrl(System.String)
extern void CloudRecognitionData_set_ImgGSUrl_m704D674C3A44E9C2396B3ADD2A6F905211454D49 (void);
// 0x000001B1 System.Single maxstAR.CloudRecognitionData::get_RealWidth()
extern void CloudRecognitionData_get_RealWidth_m5C8676A9D5671EADCB4060C028AFA943E8FA0E68 (void);
// 0x000001B2 System.Void maxstAR.CloudRecognitionData::set_RealWidth(System.Single)
extern void CloudRecognitionData_set_RealWidth_m31103FE4023713DAE684C6C5A7AB0CFC52BBAD14 (void);
// 0x000001B3 System.Void maxstAR.CloudRecognitionData::.ctor()
extern void CloudRecognitionData__ctor_mDEBA3778583730D24A615AED89C22299D5D9EB06 (void);
// 0x000001B4 maxstAR.CloudRecognizerCache maxstAR.CloudRecognizerCache::GetInstance()
extern void CloudRecognizerCache_GetInstance_m355775F74C13974437E0775232FA4727727BB954 (void);
// 0x000001B5 System.Void maxstAR.CloudRecognizerCache::ADD(System.String,System.String)
extern void CloudRecognizerCache_ADD_m30A9064B05F3679554AE8448CD4DCFFFCF762FDE (void);
// 0x000001B6 System.Void maxstAR.CloudRecognizerCache::LOAD()
extern void CloudRecognizerCache_LOAD_m16A8CA14A5290FB0C5BE9995AEC9FDC355A10ED5 (void);
// 0x000001B7 System.Void maxstAR.CloudRecognizerCache::.ctor()
extern void CloudRecognizerCache__ctor_mA182F8FB9EEB770EE1953FDECDE039338A5EDC71 (void);
// 0x000001B8 System.Void maxstAR.CloudRecognizerCache::.cctor()
extern void CloudRecognizerCache__cctor_mEE11AD8113EFC1AE374B00A8DF89ACC96A10FFD4 (void);
// 0x000001B9 System.String maxstAR.CloudRecognitionLocalData::get_cloud()
extern void CloudRecognitionLocalData_get_cloud_m11430FD5CAC6ACFBD404FDA6C0EE23A8513EE0D8 (void);
// 0x000001BA System.Void maxstAR.CloudRecognitionLocalData::set_cloud(System.String)
extern void CloudRecognitionLocalData_set_cloud_mDA904ADA325E76221C236CA0CF6745AC277F12DD (void);
// 0x000001BB System.String maxstAR.CloudRecognitionLocalData::get_cloud_2dmap_path()
extern void CloudRecognitionLocalData_get_cloud_2dmap_path_mC6B8BBEA7C292E32DEA3D456FB60FC80927BCD2E (void);
// 0x000001BC System.Void maxstAR.CloudRecognitionLocalData::set_cloud_2dmap_path(System.String)
extern void CloudRecognitionLocalData_set_cloud_2dmap_path_mD265F7FD5BAE0332C21B862F9B5B9D41A5D22D05 (void);
// 0x000001BD System.String maxstAR.CloudRecognitionLocalData::get_output_path()
extern void CloudRecognitionLocalData_get_output_path_m6074C3E508C32E84C683237F96D3519295EA7A4A (void);
// 0x000001BE System.Void maxstAR.CloudRecognitionLocalData::set_output_path(System.String)
extern void CloudRecognitionLocalData_set_output_path_mE47CC4AFC907F59B0D6C54820BECCF88DAB5B872 (void);
// 0x000001BF System.String maxstAR.CloudRecognitionLocalData::get_cloud_image_path()
extern void CloudRecognitionLocalData_get_cloud_image_path_mB7BF5453DF217A11208263EC723C6E006215D6E5 (void);
// 0x000001C0 System.Void maxstAR.CloudRecognitionLocalData::set_cloud_image_path(System.String)
extern void CloudRecognitionLocalData_set_cloud_image_path_m7723EAD911957C9C994CEC332713DEEC24A6C249 (void);
// 0x000001C1 System.Single maxstAR.CloudRecognitionLocalData::get_image_width()
extern void CloudRecognitionLocalData_get_image_width_m5D290176001A24243CF27305C4D3F3E04ABCBB78 (void);
// 0x000001C2 System.Void maxstAR.CloudRecognitionLocalData::set_image_width(System.Single)
extern void CloudRecognitionLocalData_set_image_width_m2F0AAF622FA6A97316D6287FF3CD3DE6F3AA891D (void);
// 0x000001C3 System.String maxstAR.CloudRecognitionLocalData::get_cloud_name()
extern void CloudRecognitionLocalData_get_cloud_name_m5DA3B2595FD96D426685635DC505A24B9572A30E (void);
// 0x000001C4 System.Void maxstAR.CloudRecognitionLocalData::set_cloud_name(System.String)
extern void CloudRecognitionLocalData_set_cloud_name_mEEB6D28709B3FA2DC53E93247DE0BFD96EB7E620 (void);
// 0x000001C5 System.String maxstAR.CloudRecognitionLocalData::get_cloud_meta()
extern void CloudRecognitionLocalData_get_cloud_meta_mA45A750F34748797F3CC92290EEA889D40F24EFC (void);
// 0x000001C6 System.Void maxstAR.CloudRecognitionLocalData::set_cloud_meta(System.String)
extern void CloudRecognitionLocalData_set_cloud_meta_m839B32112B787F3CE571475CBCC13A5B5B522D84 (void);
// 0x000001C7 System.Void maxstAR.CloudRecognitionLocalData::.ctor()
extern void CloudRecognitionLocalData__ctor_mB491CD6644D8F3B7DF15F65FE82A37D5F3F2B555 (void);
// 0x000001C8 System.Void maxstAR.Dimensions::.ctor(System.Int32,System.Int32)
extern void Dimensions__ctor_m47F28DC631B1C3E6287AFFEBDB5DF13786785D9E (void);
// 0x000001C9 System.Int32 maxstAR.Dimensions::get_Width()
extern void Dimensions_get_Width_m0D5022D79CF463DA9FFD663258FF2E5D931DB5A3 (void);
// 0x000001CA System.Int32 maxstAR.Dimensions::get_Height()
extern void Dimensions_get_Height_m1BE5D8685B7944D3C76C7707003952EB4F4EB8C2 (void);
// 0x000001CB System.String maxstAR.Dimensions::ToString()
extern void Dimensions_ToString_m8E94E585F488AD8ED0BCE7A237512ADFD8AC2FE9 (void);
// 0x000001CC System.Void maxstAR.GuideInfo::UpdateGuideInfo()
extern void GuideInfo_UpdateGuideInfo_mDC7A662E86640A0382D138886F24EC8E37558645 (void);
// 0x000001CD System.Single maxstAR.GuideInfo::GetInitializingProgress()
extern void GuideInfo_GetInitializingProgress_m7EE3F3D2111DAA15DAF62FC4F4A75A6198C90222 (void);
// 0x000001CE System.Int32 maxstAR.GuideInfo::GetFeatureCount()
extern void GuideInfo_GetFeatureCount_m0D5F2D0F3384AAE528CF60B3EA5580E2BB95E82F (void);
// 0x000001CF System.Int32 maxstAR.GuideInfo::GetKeyframeCount()
extern void GuideInfo_GetKeyframeCount_m6DDE9F5F0DC82BE67C1C245D225EAB7BD7C0F1C3 (void);
// 0x000001D0 System.Single[] maxstAR.GuideInfo::GetFeatureBuffer()
extern void GuideInfo_GetFeatureBuffer_m77E6751DE7244817A7D8DA10DEC8AC570792AB88 (void);
// 0x000001D1 maxstAR.TagAnchor[] maxstAR.GuideInfo::GetTagAnchors()
extern void GuideInfo_GetTagAnchors_mE8966A2F34B55DCB95C6511B36E8664A923D7C48 (void);
// 0x000001D2 System.Void maxstAR.GuideInfo::.ctor()
extern void GuideInfo__ctor_m5B0C6BAC596BCA0837582B2E6E62243EBA1EC8E2 (void);
// 0x000001D3 System.Void maxstAR.GuideInfo::.cctor()
extern void GuideInfo__cctor_m1E0DD7BF25EDF6A8650F8D4CDDC4FDB7CC217DFD (void);
// 0x000001D4 maxstAR.Dimensions maxstAR.JpegUtils::GetJpegDimensions(System.Byte[])
extern void JpegUtils_GetJpegDimensions_m82F6AFCF809C0A9304CFF05D61A5A002BA049E9C (void);
// 0x000001D5 maxstAR.Dimensions maxstAR.JpegUtils::GetJpegDimensions(System.String)
extern void JpegUtils_GetJpegDimensions_m4A811C429785EAC86D15C84CD900708BE7711560 (void);
// 0x000001D6 maxstAR.Dimensions maxstAR.JpegUtils::GetJpegDimensions(System.IO.Stream)
extern void JpegUtils_GetJpegDimensions_mA24E1653396B67026BF24161B07124AFABFBF216 (void);
// 0x000001D7 System.Void maxstAR.JpegUtils::.ctor()
extern void JpegUtils__ctor_m57D4AF9468E9EA20B1700801C0B6A60B2C37A040 (void);
// 0x000001D8 System.Void maxstAR.Map3D::.ctor()
extern void Map3D__ctor_m2EDD0B9A684F67E856306C50EA02626BB34E48F8 (void);
// 0x000001D9 System.Void maxstAR.MapRendererBehaviour::Create(UnityEngine.Vector3[],UnityEngine.Matrix4x4[],UnityEngine.Material[])
extern void MapRendererBehaviour_Create_m324ED7489A940B64D8DD89CA401FBF993FFF76CA (void);
// 0x000001DA System.Void maxstAR.MapRendererBehaviour::CreateAnchors(maxstAR.TagAnchor[])
extern void MapRendererBehaviour_CreateAnchors_m755E309BE8D580180A29F6C941E08ADD06A43806 (void);
// 0x000001DB UnityEngine.Mesh maxstAR.MapRendererBehaviour::CreateMapViewerMesh(System.Int32,UnityEngine.Vector3[])
extern void MapRendererBehaviour_CreateMapViewerMesh_m44B7C67233D5FB8AEFD378225A9E2CB81CE2A6CA (void);
// 0x000001DC System.Void maxstAR.MapRendererBehaviour::Clear()
extern void MapRendererBehaviour_Clear_m7D18B3E6CC4E30B81C599226BDF64C0F1114DD9E (void);
// 0x000001DD System.Void maxstAR.MapRendererBehaviour::SetActiveImageObject(System.Int32)
extern void MapRendererBehaviour_SetActiveImageObject_mC9648203108305B53843DFC1F56F90B9800695BF (void);
// 0x000001DE System.Void maxstAR.MapRendererBehaviour::SetActiveMeshObject(System.Int32)
extern void MapRendererBehaviour_SetActiveMeshObject_mD2617B8AC194962AE7D72D10D1C1FCF24FED67C0 (void);
// 0x000001DF System.Void maxstAR.MapRendererBehaviour::SetDeactiveImageObjects()
extern void MapRendererBehaviour_SetDeactiveImageObjects_m68E5601664A3DCD39BA78EDB6A39A71E409FF6BD (void);
// 0x000001E0 System.Void maxstAR.MapRendererBehaviour::SetDeactiveMeshObjects()
extern void MapRendererBehaviour_SetDeactiveMeshObjects_mE547D9E0FFF02DCDD64A8ADD6277A6ACAC1931F9 (void);
// 0x000001E1 System.Void maxstAR.MapRendererBehaviour::OnRenderObject()
extern void MapRendererBehaviour_OnRenderObject_m8F1C250626CCC074FA68605E4A4B5B0EF80A235A (void);
// 0x000001E2 UnityEngine.Vector2 maxstAR.MapRendererBehaviour::GetGameViewSize()
extern void MapRendererBehaviour_GetGameViewSize_mF541D50839B6BA7F067610720534B2F0E4178BF3 (void);
// 0x000001E3 System.Void maxstAR.MapRendererBehaviour::.ctor()
extern void MapRendererBehaviour__ctor_m0A7EE1F41C9B7BB3D496C7279A383AB931BBBFC9 (void);
// 0x000001E4 maxstAR.MapViewer maxstAR.MapViewer::GetInstance()
extern void MapViewer_GetInstance_mFA97A42492F240612089D27D0F3AFD7FB9854681 (void);
// 0x000001E5 System.Void maxstAR.MapViewer::.ctor()
extern void MapViewer__ctor_m7824783612FD7F5FFE23B7E37C185A88FECE1A04 (void);
// 0x000001E6 System.Int32 maxstAR.MapViewer::Initialize(System.String,System.Single)
extern void MapViewer_Initialize_m282862E10923D4E45DC3967015AFF2F58C5E44E9 (void);
// 0x000001E7 System.Void maxstAR.MapViewer::Deinitialize()
extern void MapViewer_Deinitialize_mAF0443059205DC5A0B863329240B047E643DE3AE (void);
// 0x000001E8 System.Void maxstAR.MapViewer::GetJson(System.Byte[],System.Int32)
extern void MapViewer_GetJson_m7B7E0343F68516B5B229B7697E53312E5C5CDFB1 (void);
// 0x000001E9 System.Int32 maxstAR.MapViewer::Create(System.Int32)
extern void MapViewer_Create_m87FC5F3FDC13363C6EA312ED61E79674BB358117 (void);
// 0x000001EA System.Void maxstAR.MapViewer::GetIndices(System.Int32&)
extern void MapViewer_GetIndices_mDC9C269E6E69D9C1A1B30ACE6CCC3B77C47A24ED (void);
// 0x000001EB System.Void maxstAR.MapViewer::GetTexCoords(System.Single&)
extern void MapViewer_GetTexCoords_mF0B2A186B37750FD8C3A32774C9E046F84031250 (void);
// 0x000001EC System.Int32 maxstAR.MapViewer::GetImageSize(System.Int32)
extern void MapViewer_GetImageSize_mA4C9285C6594BC911E5373DECC7EAF1895015ABB (void);
// 0x000001ED System.Void maxstAR.MapViewer::GetImage(System.Int32,System.Byte&)
extern void MapViewer_GetImage_mA6E41486407336824ED12780D0D73175CEB5CDDC (void);
// 0x000001EE System.Void maxstAR.MapViewer::.cctor()
extern void MapViewer__cctor_mDE29F81F1AC0BC7D130B158BB226195AC1506ABC (void);
// 0x000001EF UnityEngine.Matrix4x4 maxstAR.MatrixUtils::ConvertGLMatrixToUnityMatrix4x4(System.Single[])
extern void MatrixUtils_ConvertGLMatrixToUnityMatrix4x4_mE408249A02F624E4C773C33C4F8438046B9AA421 (void);
// 0x000001F0 UnityEngine.Matrix4x4 maxstAR.MatrixUtils::ConvertGLProjectionToUnityProjection(System.Single[])
extern void MatrixUtils_ConvertGLProjectionToUnityProjection_m951F3E0B613BB5E9F92409666A8EF3ABA47863EC (void);
// 0x000001F1 UnityEngine.Matrix4x4 maxstAR.MatrixUtils::GetUnityPoseMatrix(System.Single[])
extern void MatrixUtils_GetUnityPoseMatrix_mF526E85AD182AFCE3FE8D2161450AAA18642AB86 (void);
// 0x000001F2 UnityEngine.Matrix4x4 maxstAR.MatrixUtils::GetUnityPoseMatrix(UnityEngine.Matrix4x4)
extern void MatrixUtils_GetUnityPoseMatrix_m2B22301A12F00FAB404F0AED88998D0EF00496F8 (void);
// 0x000001F3 System.Void maxstAR.MatrixUtils::ApplyLocalTransformFromMatrix(UnityEngine.Transform,UnityEngine.Matrix4x4)
extern void MatrixUtils_ApplyLocalTransformFromMatrix_m49F32084AB7540B13843E5BC6418669DE156E030 (void);
// 0x000001F4 UnityEngine.Quaternion maxstAR.MatrixUtils::InvQuaternionFromMatrix(UnityEngine.Matrix4x4)
extern void MatrixUtils_InvQuaternionFromMatrix_mADE4AFB532ABDAFF594BA7866636195B8FAFB489 (void);
// 0x000001F5 UnityEngine.Vector3 maxstAR.MatrixUtils::PositionFromMatrix(UnityEngine.Matrix4x4)
extern void MatrixUtils_PositionFromMatrix_mD02D238E5913ED5BA2204310CE4B9FC907F22816 (void);
// 0x000001F6 UnityEngine.Vector3 maxstAR.MatrixUtils::ScaleFromMatrix(UnityEngine.Matrix4x4)
extern void MatrixUtils_ScaleFromMatrix_m6A00E24E5963E7CFE55971A60FE8D7820A912049 (void);
// 0x000001F7 UnityEngine.Matrix4x4 maxstAR.MatrixUtils::MatrixFromQuaternionAndTranslate(UnityEngine.Quaternion,UnityEngine.Vector3)
extern void MatrixUtils_MatrixFromQuaternionAndTranslate_mCAF7247A4C8C929EFA9096F6A46EE7AEFC122627 (void);
// 0x000001F8 UnityEngine.Matrix4x4 maxstAR.MatrixUtils::MatrixFromQuaternion(UnityEngine.Quaternion)
extern void MatrixUtils_MatrixFromQuaternion_m3ED94B4E318E230174869ABE94A11EA7021CD862 (void);
// 0x000001F9 UnityEngine.Quaternion maxstAR.MatrixUtils::QuaternionFromMatrix(UnityEngine.Matrix4x4)
extern void MatrixUtils_QuaternionFromMatrix_mFE8CFF1F046595B8A85491CA08F7BBE8A20CFE34 (void);
// 0x000001FA System.Void maxstAR.MatrixUtils::.ctor()
extern void MatrixUtils__ctor_mF5218D00D82B921750DAC6C6D2BA0CC94E1E2455 (void);
// 0x000001FB System.String maxstAR.MaxstAR::GetVersion()
extern void MaxstAR_GetVersion_m3C9089CB18A45D0BA176A06401EE52834D69D4D6 (void);
// 0x000001FC System.Void maxstAR.MaxstAR::OnSurfaceChanged(System.Int32,System.Int32)
extern void MaxstAR_OnSurfaceChanged_mB35F33B14AC5F1AF9526C49099185B38B45D2060 (void);
// 0x000001FD System.Void maxstAR.MaxstAR::SetScreenOrientation(System.Int32)
extern void MaxstAR_SetScreenOrientation_m62E76A343A3BEC0B3EC28993E2755A3104EE1CC9 (void);
// 0x000001FE System.Void maxstAR.MaxstAR::.ctor()
extern void MaxstAR__ctor_m15B73A265494FE7FAC321F01F7F46E0B47202E7A (void);
// 0x000001FF System.Boolean maxstAR.MaxstARUtil::IsDirectXAPI()
extern void MaxstARUtil_IsDirectXAPI_m6BC01430AA25B8E26DEF1BBBBB3F28434FF69696 (void);
// 0x00000200 System.Collections.IEnumerator maxstAR.MaxstARUtil::LoadImageFromFileWithSizeAndTexture(System.String,System.Action`3<System.Int32,System.Int32,UnityEngine.Texture2D>)
extern void MaxstARUtil_LoadImageFromFileWithSizeAndTexture_mEDD961DEAE5C8F3C39D0FB9ABB0F3007760EF57A (void);
// 0x00000201 System.String maxstAR.MaxstARUtil::ChangeNewLine(System.String)
extern void MaxstARUtil_ChangeNewLine_m2E9EBBF5147CBABBACFFEBCAE5CB44AEC68DDE8A (void);
// 0x00000202 System.String maxstAR.MaxstARUtil::DeleteNewLine(System.String)
extern void MaxstARUtil_DeleteNewLine_mCB3DC0F8DE9479A0B137657C2D7E74816EC20821 (void);
// 0x00000203 System.Single maxstAR.MaxstARUtil::GetPixelFromInch(System.Int32,System.Single)
extern void MaxstARUtil_GetPixelFromInch_mEB11F6D02CC7381F47D532672561E0039029A75D (void);
// 0x00000204 System.Single maxstAR.MaxstARUtil::DeviceDiagonalSizeInInches()
extern void MaxstARUtil_DeviceDiagonalSizeInInches_m52E5BDBD8C90AEF3F43B66261054452401A741DE (void);
// 0x00000205 System.Collections.IEnumerator maxstAR.MaxstARUtil::ExtractAssets(System.String,System.Action`1<System.String>)
extern void MaxstARUtil_ExtractAssets_m066CE8C8AB84B844C492F6C7E6DDAC05F0EE5375 (void);
// 0x00000206 System.Void maxstAR.MaxstARUtil::.ctor()
extern void MaxstARUtil__ctor_m2440EF4C745F79D22AF2AB9EBC7E05EFE209070B (void);
// 0x00000207 System.Void maxstAR.MaxstARUtil_<LoadImageFromFileWithSizeAndTexture>d__7::.ctor(System.Int32)
extern void U3CLoadImageFromFileWithSizeAndTextureU3Ed__7__ctor_m71F4F81B5212E2BFB6B46F54F4BD99B07DE43BAD (void);
// 0x00000208 System.Void maxstAR.MaxstARUtil_<LoadImageFromFileWithSizeAndTexture>d__7::System.IDisposable.Dispose()
extern void U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_IDisposable_Dispose_mDE2B51E68F1388B563AB1E81D32CC7E6F19F9B49 (void);
// 0x00000209 System.Boolean maxstAR.MaxstARUtil_<LoadImageFromFileWithSizeAndTexture>d__7::MoveNext()
extern void U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_MoveNext_m31944946B7FC2348F3DF09E7DD610FB1E5B39342 (void);
// 0x0000020A System.Object maxstAR.MaxstARUtil_<LoadImageFromFileWithSizeAndTexture>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF3FB7AAB4BFDA41BE7451833A4138636D4B910D2 (void);
// 0x0000020B System.Void maxstAR.MaxstARUtil_<LoadImageFromFileWithSizeAndTexture>d__7::System.Collections.IEnumerator.Reset()
extern void U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_Collections_IEnumerator_Reset_m480B2F004F05610C7FDA4BE61427E2F6E75551C6 (void);
// 0x0000020C System.Object maxstAR.MaxstARUtil_<LoadImageFromFileWithSizeAndTexture>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_Collections_IEnumerator_get_Current_m6AFD1398A09E1F103DF0432920077887D5395378 (void);
// 0x0000020D System.Void maxstAR.MaxstARUtil_<ExtractAssets>d__14::.ctor(System.Int32)
extern void U3CExtractAssetsU3Ed__14__ctor_mD1ECC726D892EF7DF1B924D09260F70A71D20BD1 (void);
// 0x0000020E System.Void maxstAR.MaxstARUtil_<ExtractAssets>d__14::System.IDisposable.Dispose()
extern void U3CExtractAssetsU3Ed__14_System_IDisposable_Dispose_m41C90A325F0E55692BE8D1A05E5CF520CD069407 (void);
// 0x0000020F System.Boolean maxstAR.MaxstARUtil_<ExtractAssets>d__14::MoveNext()
extern void U3CExtractAssetsU3Ed__14_MoveNext_m6CF1D91E037D70FB14FDFDC341413E822FEA6929 (void);
// 0x00000210 System.Object maxstAR.MaxstARUtil_<ExtractAssets>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExtractAssetsU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4DF288844ED548412F34B635BEBBD1901AA0F315 (void);
// 0x00000211 System.Void maxstAR.MaxstARUtil_<ExtractAssets>d__14::System.Collections.IEnumerator.Reset()
extern void U3CExtractAssetsU3Ed__14_System_Collections_IEnumerator_Reset_m387CAEF9FB92CD551B267592DE88103740E737BB (void);
// 0x00000212 System.Object maxstAR.MaxstARUtil_<ExtractAssets>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CExtractAssetsU3Ed__14_System_Collections_IEnumerator_get_Current_m4FF8B46E1729307920440B2E159D43B2FFB3F23C (void);
// 0x00000213 T maxstAR.MaxstSingleton`1::get_Instance()
// 0x00000214 System.Void maxstAR.MaxstSingleton`1::DestroySingleTon()
// 0x00000215 System.Void maxstAR.MaxstSingleton`1::.ctor()
// 0x00000216 System.Void maxstAR.MaxstSingleton`1::.cctor()
// 0x00000217 System.Void maxstAR.NativeAPI::maxst_init(System.String)
extern void NativeAPI_maxst_init_mC421B3568E7E354EE758992F9FEE91AACB754906 (void);
// 0x00000218 System.Void maxstAR.NativeAPI::maxst_getVersion(System.Byte[],System.Int32)
extern void NativeAPI_maxst_getVersion_mEA9CEF894DE3F5CCE4C82ED36FDA06D9A7127EFE (void);
// 0x00000219 System.Void maxstAR.NativeAPI::maxst_onSurfaceChanged(System.Int32,System.Int32)
extern void NativeAPI_maxst_onSurfaceChanged_m618861493F1D215C9987FC494E9C9DFC8DB56341 (void);
// 0x0000021A System.Void maxstAR.NativeAPI::maxst_setScreenOrientation(System.Int32)
extern void NativeAPI_maxst_setScreenOrientation_m4201B34755027F4B2F189C225F834443FB230239 (void);
// 0x0000021B System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_start(System.Int32,System.Int32,System.Int32)
extern void NativeAPI_maxst_CameraDevice_start_mA148CCBEB395140FB888403518FA19126519FD6D (void);
// 0x0000021C System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_stop()
extern void NativeAPI_maxst_CameraDevice_stop_mC6C881EB1195C2402F90C27D233A78C16894C1E8 (void);
// 0x0000021D System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setNewFrame(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32)
extern void NativeAPI_maxst_CameraDevice_setNewFrame_mC40388178C53F040C9691DFBDB31724FB2C5D99B (void);
// 0x0000021E System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setNewFramePtr(System.UInt64,System.Int32,System.Int32,System.Int32,System.Int32)
extern void NativeAPI_maxst_CameraDevice_setNewFramePtr_mF45F0F18F951F3DF9E70EA73ACAD5B80299297A4 (void);
// 0x0000021F System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setNewFrameAndTimestamp(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32,System.UInt64)
extern void NativeAPI_maxst_CameraDevice_setNewFrameAndTimestamp_mC57FA1496ACAB11EEA5AC70B78AB10057FF977F7 (void);
// 0x00000220 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setNewFramePtrAndTimestamp(System.UInt64,System.Int32,System.Int32,System.Int32,System.Int32,System.UInt64)
extern void NativeAPI_maxst_CameraDevice_setNewFramePtrAndTimestamp_mC637F15F7A426343F2C21A8F7CB84F52CA6ADF80 (void);
// 0x00000221 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setFocusMode(System.Int32)
extern void NativeAPI_maxst_CameraDevice_setFocusMode_mAC5D359262EE800885242A7B1020301F7BF97CBB (void);
// 0x00000222 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setFlashLightMode(System.Boolean)
extern void NativeAPI_maxst_CameraDevice_setFlashLightMode_m84432C67B669BE560BE5F0A3431D0FC655433B66 (void);
// 0x00000223 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setAutoWhiteBalanceLock(System.Boolean)
extern void NativeAPI_maxst_CameraDevice_setAutoWhiteBalanceLock_mFF5C02AC8765AFCF4CCC48E3396E4F5D8B4B7575 (void);
// 0x00000224 System.Void maxstAR.NativeAPI::maxst_CameraDevice_flipVideo(System.Int32,System.Boolean)
extern void NativeAPI_maxst_CameraDevice_flipVideo_m5A8B109AA322E7738DB53A4763D326B5067FB32E (void);
// 0x00000225 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_isVideoFlipped(System.Int32)
extern void NativeAPI_maxst_CameraDevice_isVideoFlipped_m5D17BD93BBC9ED8FD4881949FBFA00B7FA50061C (void);
// 0x00000226 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setZoom(System.Single)
extern void NativeAPI_maxst_CameraDevice_setZoom_mACA296819A0A90194F6310A9BA8BE8EB10381013 (void);
// 0x00000227 System.Single maxstAR.NativeAPI::maxst_CameraDevice_getMaxZoomValue()
extern void NativeAPI_maxst_CameraDevice_getMaxZoomValue_m489F96B138941686BF901351134E0131D437B32E (void);
// 0x00000228 System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_getParamList()
extern void NativeAPI_maxst_CameraDevice_getParamList_mCE8E8049EECD925C889CA399DE1C212609DC3DAE (void);
// 0x00000229 System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_Param_getKeyLength(System.Int32)
extern void NativeAPI_maxst_CameraDevice_Param_getKeyLength_m9971AEBDE465E45C799ABFEA1C0BF4CDA4389691 (void);
// 0x0000022A System.Void maxstAR.NativeAPI::maxst_CameraDevice_Param_getKey(System.Int32,System.Byte[])
extern void NativeAPI_maxst_CameraDevice_Param_getKey_m88F9F08965A5C01EED7C9C19953F3EDCDBD580E2 (void);
// 0x0000022B System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setBoolTypeParameter(System.String,System.Boolean)
extern void NativeAPI_maxst_CameraDevice_setBoolTypeParameter_mDFEAD348009DC9DE8640BF5F953F10641CF3EC01 (void);
// 0x0000022C System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setIntTypeParameter(System.String,System.Int32)
extern void NativeAPI_maxst_CameraDevice_setIntTypeParameter_m08299249A808D02BDE5CE094151FCCE750A1496D (void);
// 0x0000022D System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setRangeTypeParameter(System.String,System.Int32,System.Int32)
extern void NativeAPI_maxst_CameraDevice_setRangeTypeParameter_m41CB577D38C32B1E4F9B7756F4C936E59DFF36D8 (void);
// 0x0000022E System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setStringTypeParameter(System.String,System.String)
extern void NativeAPI_maxst_CameraDevice_setStringTypeParameter_mB003CB420D2E5668214C3A2FF1656E89DA99E947 (void);
// 0x0000022F System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_getWidth()
extern void NativeAPI_maxst_CameraDevice_getWidth_mFE2F4F92B10D643E5A650ED2C6083172EC323467 (void);
// 0x00000230 System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_getHeight()
extern void NativeAPI_maxst_CameraDevice_getHeight_m0A24FB0D69B25D4A374051C8695B06FB759A17EC (void);
// 0x00000231 System.Void maxstAR.NativeAPI::maxst_CameraDevice_getProjectionMatrix(System.Single[])
extern void NativeAPI_maxst_CameraDevice_getProjectionMatrix_m2013DA48718C146FB1285F59BB8F2A533501D70F (void);
// 0x00000232 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_checkCameraMove(System.UInt64)
extern void NativeAPI_maxst_CameraDevice_checkCameraMove_mC19A106F9726A5B5F05F94F2B37CB89D3F4B6970 (void);
// 0x00000233 System.Void maxstAR.NativeAPI::maxst_CameraDevice_getBackgroundPlaneInfo(System.Single[])
extern void NativeAPI_maxst_CameraDevice_getBackgroundPlaneInfo_mC54F5739ABBAC1352D4AD82B277FF0389368C06D (void);
// 0x00000234 System.Void maxstAR.NativeAPI::maxst_CameraDevice_setARCoreTexture()
extern void NativeAPI_maxst_CameraDevice_setARCoreTexture_mD0FFD802A4E0BF605514A495A1CF197AA3AB196A (void);
// 0x00000235 System.Void maxstAR.NativeAPI::maxst_CameraDevice_setFusionEnable()
extern void NativeAPI_maxst_CameraDevice_setFusionEnable_mBF357029D75084A338A142AC172AF646342B92A7 (void);
// 0x00000236 System.Void maxstAR.NativeAPI::maxst_TrackerManager_startTracker(System.Int32)
extern void NativeAPI_maxst_TrackerManager_startTracker_m971E03A88CA6EC5B30A3C9E7A68D2A17954FB26F (void);
// 0x00000237 System.Void maxstAR.NativeAPI::maxst_TrackerManager_stopTracker()
extern void NativeAPI_maxst_TrackerManager_stopTracker_m5A8EE1D410208C74BC6AF5D87A7510220AFC05BC (void);
// 0x00000238 System.Void maxstAR.NativeAPI::maxst_TrackerManager_destroyTracker()
extern void NativeAPI_maxst_TrackerManager_destroyTracker_mB74A5810BD25D0CB1A57CF383031E2921F9B8BF0 (void);
// 0x00000239 System.Void maxstAR.NativeAPI::maxst_TrackerManager_addTrackerData(System.String,System.Boolean)
extern void NativeAPI_maxst_TrackerManager_addTrackerData_m50AD9A060A5D3FA3E8E38C88D2F29DC74483363A (void);
// 0x0000023A System.Void maxstAR.NativeAPI::maxst_TrackerManager_removeTrackerData(System.String)
extern void NativeAPI_maxst_TrackerManager_removeTrackerData_m7D27EE780662ECF8FC8DEAE4D25594E934D449A8 (void);
// 0x0000023B System.Void maxstAR.NativeAPI::maxst_TrackerManager_loadTrackerData()
extern void NativeAPI_maxst_TrackerManager_loadTrackerData_mC42564153C5EF09C5D56219D1D740C12AAF4F526 (void);
// 0x0000023C System.Void maxstAR.NativeAPI::maxst_TrackerManager_setTrackingOption(System.Int32)
extern void NativeAPI_maxst_TrackerManager_setTrackingOption_m4668D35FEEC1D35AD57C731C2426F61CFEFBE435 (void);
// 0x0000023D System.Boolean maxstAR.NativeAPI::maxst_TrackerManager_isTrackerDataLoadCompleted()
extern void NativeAPI_maxst_TrackerManager_isTrackerDataLoadCompleted_m92769B2A5E0FF8B6E89582550A976F8F6D1B0875 (void);
// 0x0000023E System.Boolean maxstAR.NativeAPI::maxst_TrackerManager_isFusionSupported()
extern void NativeAPI_maxst_TrackerManager_isFusionSupported_mA00F64266B3B24FE23AEF14578A26F4E77C31A2E (void);
// 0x0000023F System.Int32 maxstAR.NativeAPI::maxst_TrackerManager_getFusionTrackingState()
extern void NativeAPI_maxst_TrackerManager_getFusionTrackingState_m5DCA51D41DABD24A99FAC65CED3B45391546F89E (void);
// 0x00000240 System.Void maxstAR.NativeAPI::maxst_TrackerManager_setVocabulary(System.String,System.Boolean)
extern void NativeAPI_maxst_TrackerManager_setVocabulary_mB9507D1CD48277302CD4B5B38E75C2BC128F469D (void);
// 0x00000241 System.UInt64 maxstAR.NativeAPI::maxst_TrackerManager_updateTrackingState()
extern void NativeAPI_maxst_TrackerManager_updateTrackingState_m4E4B3B8E8A69E0512D69CE8D67737656F60BBD84 (void);
// 0x00000242 System.Void maxstAR.NativeAPI::maxst_TrackerManager_findSurface()
extern void NativeAPI_maxst_TrackerManager_findSurface_mA37ED878FEE4E865EED55B91955C4075CDF95775 (void);
// 0x00000243 System.Void maxstAR.NativeAPI::maxst_TrackerManager_quitFindingSurface()
extern void NativeAPI_maxst_TrackerManager_quitFindingSurface_m0C148526FB8A1C7BBC5C347E7F2801A9D52CD147 (void);
// 0x00000244 System.UInt64 maxstAR.NativeAPI::maxst_TrackerManager_getGuideInfo()
extern void NativeAPI_maxst_TrackerManager_getGuideInfo_mEC1297F219F10A73B816CF8AE70C485DBFD3E727 (void);
// 0x00000245 System.UInt64 maxstAR.NativeAPI::maxst_TrackerManager_saveSurfaceData(System.String)
extern void NativeAPI_maxst_TrackerManager_saveSurfaceData_m2B54C617A28B345052F49017C2F8E1F82F4A4C3D (void);
// 0x00000246 System.Void maxstAR.NativeAPI::maxst_TrackerManager_getWorldPositionFromScreenCoordinate(System.Single[],System.Single[])
extern void NativeAPI_maxst_TrackerManager_getWorldPositionFromScreenCoordinate_mD0EE7454B9E0D8CBF5A1984B85E753273E41D499 (void);
// 0x00000247 System.Boolean maxstAR.NativeAPI::maxst_CloudManager_GetFeatureClient(System.UInt64,System.Byte[],System.Int32[])
extern void NativeAPI_maxst_CloudManager_GetFeatureClient_mC1035CF7E3C3AD0D69ABFEA7922023487CDC0D71 (void);
// 0x00000248 System.Void maxstAR.NativeAPI::maxst_CloudManager_JWTEncode(System.String,System.String,System.Byte[])
extern void NativeAPI_maxst_CloudManager_JWTEncode_mD48952482C7A8C3933F246A5E50ACB77C7F33B60 (void);
// 0x00000249 System.Int32 maxstAR.NativeAPI::maxst_TrackingResult_getCount(System.UInt64)
extern void NativeAPI_maxst_TrackingResult_getCount_m7597D810E7436A469DCC58B3758D535A89658002 (void);
// 0x0000024A System.UInt64 maxstAR.NativeAPI::maxst_TrackingResult_getTrackable(System.UInt64,System.Int32)
extern void NativeAPI_maxst_TrackingResult_getTrackable_m240DB6DB0F18CC92D23440D4B9A6494F059F1245 (void);
// 0x0000024B System.Void maxstAR.NativeAPI::maxst_Trackable_getId(System.UInt64,System.Byte[])
extern void NativeAPI_maxst_Trackable_getId_m2DEE48E439EA8406F377D1746094036622E3C9C2 (void);
// 0x0000024C System.Void maxstAR.NativeAPI::maxst_Trackable_getName(System.UInt64,System.Byte[])
extern void NativeAPI_maxst_Trackable_getName_mCEE4B36D3A74D2BCE330455736A0468DCA14479B (void);
// 0x0000024D System.Void maxstAR.NativeAPI::maxst_Trackable_getCloudName(System.UInt64,System.Byte[])
extern void NativeAPI_maxst_Trackable_getCloudName_mA1FE53DF31A037A17D9D94F6A5E9F298CAB5767D (void);
// 0x0000024E System.Void maxstAR.NativeAPI::maxst_Trackable_getCloudMeta(System.UInt64,System.Byte[],System.Int32[])
extern void NativeAPI_maxst_Trackable_getCloudMeta_mE07FED8AB309A43528FC473CC669D222D0A9A918 (void);
// 0x0000024F System.Void maxstAR.NativeAPI::maxst_Trackable_getPose(System.UInt64,System.Single[])
extern void NativeAPI_maxst_Trackable_getPose_m12E3676E586A813CE1D77D932060E3411075B7AC (void);
// 0x00000250 System.Single maxstAR.NativeAPI::maxst_Trackable_getWidth(System.UInt64)
extern void NativeAPI_maxst_Trackable_getWidth_mC57D8EC900A2FA32B1FDD374EABC921F7C9083AE (void);
// 0x00000251 System.Single maxstAR.NativeAPI::maxst_Trackable_getHeight(System.UInt64)
extern void NativeAPI_maxst_Trackable_getHeight_m82AE200A875BCA11B9A6ED2BF14AB915EDA8CF73 (void);
// 0x00000252 System.UInt64 maxstAR.NativeAPI::maxst_TrackingState_getTrackingResult(System.UInt64)
extern void NativeAPI_maxst_TrackingState_getTrackingResult_m8E636950DB4BE895F91FA8097BE3531DD1781B33 (void);
// 0x00000253 System.UInt64 maxstAR.NativeAPI::maxst_TrackingState_getImage(System.UInt64)
extern void NativeAPI_maxst_TrackingState_getImage_m9F5F305D12B2329745B96FEE05BD2337EBB2309E (void);
// 0x00000254 System.Int32 maxstAR.NativeAPI::maxst_TrackingState_getCodeScanResultLength(System.UInt64)
extern void NativeAPI_maxst_TrackingState_getCodeScanResultLength_m3041564879082248114B8FA0B49E3988C482C130 (void);
// 0x00000255 System.Void maxstAR.NativeAPI::maxst_TrackingState_getCodeScanResult(System.UInt64,System.Byte[],System.Int32)
extern void NativeAPI_maxst_TrackingState_getCodeScanResult_m5283C45EB7A99AD04C407CDE298EB587FFB6C17D (void);
// 0x00000256 System.Single maxstAR.NativeAPI::maxst_GuideInfo_getInitializingProgress(System.UInt64)
extern void NativeAPI_maxst_GuideInfo_getInitializingProgress_m583D5D97F10BE1DA780F74D00E381B638B648028 (void);
// 0x00000257 System.Int32 maxstAR.NativeAPI::maxst_GuideInfo_getKeyframeCount(System.UInt64)
extern void NativeAPI_maxst_GuideInfo_getKeyframeCount_m8B7BACC6890DDEB91B4024CB1625B7B8823EBCC6 (void);
// 0x00000258 System.Int32 maxstAR.NativeAPI::maxst_GuideInfo_getFeatureCount(System.UInt64)
extern void NativeAPI_maxst_GuideInfo_getFeatureCount_m3FFA629C4DDA65E0888C063FFE169DF4EAE8E0E7 (void);
// 0x00000259 System.Void maxstAR.NativeAPI::maxst_GuideInfo_getFeatureBuffer(System.UInt64,System.Single[],System.Int32)
extern void NativeAPI_maxst_GuideInfo_getFeatureBuffer_m08D7498058158CC78F7DA4F952D6336578D552B4 (void);
// 0x0000025A System.Int32 maxstAR.NativeAPI::maxst_GuideInfo_getTagAnchorsLength(System.UInt64)
extern void NativeAPI_maxst_GuideInfo_getTagAnchorsLength_mCB5C3823895D5EC0D38A5013C1409636B3228814 (void);
// 0x0000025B System.Void maxstAR.NativeAPI::maxst_GuideInfo_getTagAnchors(System.UInt64,System.Byte[],System.Int32)
extern void NativeAPI_maxst_GuideInfo_getTagAnchors_m5002310C42B614D7D85D6852702651716BD3E2A6 (void);
// 0x0000025C System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getWidth(System.UInt64)
extern void NativeAPI_maxst_SurfaceThumbnail_getWidth_m520F114443145A6A7812774331FC953D16675ABE (void);
// 0x0000025D System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getHeight(System.UInt64)
extern void NativeAPI_maxst_SurfaceThumbnail_getHeight_m2F72481A1AA4833B541A0CBA02E13B5988859076 (void);
// 0x0000025E System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getLength(System.UInt64)
extern void NativeAPI_maxst_SurfaceThumbnail_getLength_m6DF005AFD8001FAAC41BC32753E9E726BB3F0AC0 (void);
// 0x0000025F System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getBpp(System.UInt64)
extern void NativeAPI_maxst_SurfaceThumbnail_getBpp_m9DD5C31DDF7A5C898B461FD129C6ABB6A3BB6484 (void);
// 0x00000260 System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getData(System.UInt64,System.Byte[],System.Int32)
extern void NativeAPI_maxst_SurfaceThumbnail_getData_m981A55C316D8B42D85C3F3C02B2A0A8DE7BA5714 (void);
// 0x00000261 System.Void maxstAR.NativeAPI::maxst_SensorDevice_startSensor()
extern void NativeAPI_maxst_SensorDevice_startSensor_m26113D4C45739B50831518A758334F067ACE54AB (void);
// 0x00000262 System.Void maxstAR.NativeAPI::maxst_SensorDevice_stopSensor()
extern void NativeAPI_maxst_SensorDevice_stopSensor_mA201C5474D4484EC064AD8CE0EEB0F8F896A085D (void);
// 0x00000263 System.Int32 maxstAR.NativeAPI::maxst_MapViewer_initialize(System.String,System.Single)
extern void NativeAPI_maxst_MapViewer_initialize_mB7A5CB51A6DDF56DCE263DE5884770C5986CBE07 (void);
// 0x00000264 System.Void maxstAR.NativeAPI::maxst_MapViewer_deInitialize()
extern void NativeAPI_maxst_MapViewer_deInitialize_m81AABB7C65F04A0025CB948AA2450FB3E7F4A0CC (void);
// 0x00000265 System.Void maxstAR.NativeAPI::maxst_MapViewer_getJson(System.Byte[],System.Int32)
extern void NativeAPI_maxst_MapViewer_getJson_m8CB6EC65BB89E26FAB81B8EFE12FFAC0E9BB4E74 (void);
// 0x00000266 System.Int32 maxstAR.NativeAPI::maxst_MapViewer_create(System.Int32)
extern void NativeAPI_maxst_MapViewer_create_m86BC2E8591107064528F9ECA2E53857EA2EEBE8B (void);
// 0x00000267 System.Void maxstAR.NativeAPI::maxst_MapViewer_getIndices(System.Int32&)
extern void NativeAPI_maxst_MapViewer_getIndices_m89CCDE5BFC5609F1A8362D142A3DDA6596FB70F4 (void);
// 0x00000268 System.Void maxstAR.NativeAPI::maxst_MapViewer_getTexCoords(System.Single&)
extern void NativeAPI_maxst_MapViewer_getTexCoords_m9B09717D34C0CA260F65E6014373736B4AA5C3B9 (void);
// 0x00000269 System.Int32 maxstAR.NativeAPI::maxst_MapViewer_getImageSize(System.Int32)
extern void NativeAPI_maxst_MapViewer_getImageSize_m3D9234DF0B7412AB69D81B903D40B78D8510AE8A (void);
// 0x0000026A System.Void maxstAR.NativeAPI::maxst_MapViewer_getImage(System.Int32,System.Byte&)
extern void NativeAPI_maxst_MapViewer_getImage_m955FEB2A74476DB677754032B09242C629FA42B1 (void);
// 0x0000026B System.Boolean maxstAR.NativeAPI::maxst_WearableCalibration_isActivated()
extern void NativeAPI_maxst_WearableCalibration_isActivated_mBE3366086158CA44C2E084347A74061DCCCA5D6C (void);
// 0x0000026C System.Boolean maxstAR.NativeAPI::maxst_WearableCalibration_init(System.String)
extern void NativeAPI_maxst_WearableCalibration_init_m40FC939132D15219F92CB2A27D6F2D3FA4AFEA71 (void);
// 0x0000026D System.Void maxstAR.NativeAPI::maxst_WearableCalibration_deinit()
extern void NativeAPI_maxst_WearableCalibration_deinit_mEAD2920ADBB6C0626B1E9E21648F40573E7347DE (void);
// 0x0000026E System.Void maxstAR.NativeAPI::maxst_WearableCalibration_setSurfaceSize(System.Int32,System.Int32)
extern void NativeAPI_maxst_WearableCalibration_setSurfaceSize_m0948C95339C702B187AC3A34DCE87A8FE7FA97C3 (void);
// 0x0000026F System.Void maxstAR.NativeAPI::maxst_WearableCalibration_getProjectionMatrix(System.Single[],System.Int32)
extern void NativeAPI_maxst_WearableCalibration_getProjectionMatrix_mDB6B7E83E5C85AF8C31CAAFAD159E78FA7CFF1FA (void);
// 0x00000270 System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getIndex(System.UInt64)
extern void NativeAPI_maxst_TrackedImage_getIndex_m2AE2581A69F31117A9C62A8CDDC8815DF965EADC (void);
// 0x00000271 System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getWidth(System.UInt64)
extern void NativeAPI_maxst_TrackedImage_getWidth_m434D019740805BFD6E820FE098984858C1FC98E1 (void);
// 0x00000272 System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getHeight(System.UInt64)
extern void NativeAPI_maxst_TrackedImage_getHeight_m8E1519A4CFD45053ABAA05EAADC14FFA0706ADE7 (void);
// 0x00000273 System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getLength(System.UInt64)
extern void NativeAPI_maxst_TrackedImage_getLength_m15AF0402D86BE722F3AFD7CFBC3A96A21B95499E (void);
// 0x00000274 System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getFormat(System.UInt64)
extern void NativeAPI_maxst_TrackedImage_getFormat_mFE9CE69AE9A34CFB087E12BE80692B0DCF676A33 (void);
// 0x00000275 System.Void maxstAR.NativeAPI::maxst_TrackedImage_getData(System.UInt64,System.Byte[],System.Int32)
extern void NativeAPI_maxst_TrackedImage_getData_mD958C322ADD5331759FB0E8E60B291403660FCAE (void);
// 0x00000276 System.UInt64 maxstAR.NativeAPI::maxst_TrackedImage_getDataPtr(System.UInt64)
extern void NativeAPI_maxst_TrackedImage_getDataPtr_mFAE0D226B7687B01B636E949F187124AC7EF27BA (void);
// 0x00000277 System.Void maxstAR.NativeAPI::maxst_TrackedImage_getYuv420spY_UVPtr(System.UInt64,System.IntPtr&,System.IntPtr&)
extern void NativeAPI_maxst_TrackedImage_getYuv420spY_UVPtr_m1C80CFB077016B47D0C748689BBA7A9D1B286E23 (void);
// 0x00000278 System.Void maxstAR.NativeAPI::maxst_TrackedImage_getYuv420spY_U_VPtr(System.UInt64,System.IntPtr&,System.IntPtr&,System.IntPtr&)
extern void NativeAPI_maxst_TrackedImage_getYuv420spY_U_VPtr_m09595CC3C2493A2484B109FB5B0E6F701C7AB7D0 (void);
// 0x00000279 System.Void maxstAR.NativeAPI::maxst_TrackedImage_getYuv420_888YUVPtr(System.UInt64,System.IntPtr&,System.IntPtr&,System.IntPtr&,System.Boolean)
extern void NativeAPI_maxst_TrackedImage_getYuv420_888YUVPtr_m335FEC1E34B3B0BC0D779D9EBEC8F09AB277EBEB (void);
// 0x0000027A System.Void maxstAR.NativeAPI::.ctor()
extern void NativeAPI__ctor_m30EB75B4916AFA765378C21EF8803E3CF8BEBD30 (void);
// 0x0000027B System.Void maxstAR.Point3Df::.ctor()
extern void Point3Df__ctor_mCCBF4D230FC3AB6E910A94328554DA89F5CB46F5 (void);
// 0x0000027C maxstAR.SensorDevice maxstAR.SensorDevice::GetInstance()
extern void SensorDevice_GetInstance_m3864BED3197F8B1C00021265FAFA6CFBB452F8E4 (void);
// 0x0000027D System.Void maxstAR.SensorDevice::.ctor()
extern void SensorDevice__ctor_m715604CA802B26F81CDCC0CEB0FA9EC72890456E (void);
// 0x0000027E System.Void maxstAR.SensorDevice::Start()
extern void SensorDevice_Start_m1A70CC1D74E860B4716AA4D3AD699248A055C301 (void);
// 0x0000027F System.Void maxstAR.SensorDevice::Stop()
extern void SensorDevice_Stop_mA7C93D511F50C2C21E64FF50AB9645C34169EB9D (void);
// 0x00000280 System.Void maxstAR.SensorDevice::.cctor()
extern void SensorDevice__cctor_mC6C505263AD1BE96216C837AC5D6829A47242353 (void);
// 0x00000281 System.Void maxstAR.SurfaceThumbnail::.ctor(System.UInt64)
extern void SurfaceThumbnail__ctor_m39DC5B565F76B4C634FE72BF6921AA502E344FC2 (void);
// 0x00000282 System.Int32 maxstAR.SurfaceThumbnail::GetWidth()
extern void SurfaceThumbnail_GetWidth_m91FCFDC9A7507322F424251472896E61791C067E (void);
// 0x00000283 System.Int32 maxstAR.SurfaceThumbnail::GetHeight()
extern void SurfaceThumbnail_GetHeight_m404377B70D92E8C395E6CA33CE526E6CD5C31C2E (void);
// 0x00000284 System.Int32 maxstAR.SurfaceThumbnail::GetLength()
extern void SurfaceThumbnail_GetLength_mF101214587C7027721688FEAA1AD9B1949567DAC (void);
// 0x00000285 System.Byte[] maxstAR.SurfaceThumbnail::GetData()
extern void SurfaceThumbnail_GetData_m51F47401919F24C71810D68DFC75DFC5EF61177B (void);
// 0x00000286 System.String maxstAR.TagAnchor::get_name()
extern void TagAnchor_get_name_mF8DAC64068A1C6CB707EE63A32DE4D6687D4BAD2 (void);
// 0x00000287 System.Void maxstAR.TagAnchor::set_name(System.String)
extern void TagAnchor_set_name_m2D36CCCCC28CA3944617F35624A911586DD7ECEE (void);
// 0x00000288 System.Single maxstAR.TagAnchor::get_positionX()
extern void TagAnchor_get_positionX_m80D2855FF8B8E937EB6E997D78068DB759B04895 (void);
// 0x00000289 System.Void maxstAR.TagAnchor::set_positionX(System.Single)
extern void TagAnchor_set_positionX_mF86791611A360323B8EFE691600481952FDEFFF8 (void);
// 0x0000028A System.Single maxstAR.TagAnchor::get_positionY()
extern void TagAnchor_get_positionY_mD7C3C0B2CCCDE677466B713224E3910FE67D2EE3 (void);
// 0x0000028B System.Void maxstAR.TagAnchor::set_positionY(System.Single)
extern void TagAnchor_set_positionY_mD099D343BDCA591701DA50EE5172CED04762B436 (void);
// 0x0000028C System.Single maxstAR.TagAnchor::get_positionZ()
extern void TagAnchor_get_positionZ_m630455D354909F12E49EA9E24ACE06FE4CC7D6CC (void);
// 0x0000028D System.Void maxstAR.TagAnchor::set_positionZ(System.Single)
extern void TagAnchor_set_positionZ_m02122FF5AEB088DAF87647A7F9210AD239C5F897 (void);
// 0x0000028E System.Single maxstAR.TagAnchor::get_rotationX()
extern void TagAnchor_get_rotationX_mA8B0C1AF862854107D20294F538DBD9385BE9EB0 (void);
// 0x0000028F System.Void maxstAR.TagAnchor::set_rotationX(System.Single)
extern void TagAnchor_set_rotationX_mEA9BCCDB93EF9F4A0619800283492CF5FCC23C13 (void);
// 0x00000290 System.Single maxstAR.TagAnchor::get_rotationY()
extern void TagAnchor_get_rotationY_m0957DFA6D62933D3FDC0840B47DAD08FA3F64B99 (void);
// 0x00000291 System.Void maxstAR.TagAnchor::set_rotationY(System.Single)
extern void TagAnchor_set_rotationY_m0CAB790BC87941F59A5A975296CC09D68616C58B (void);
// 0x00000292 System.Single maxstAR.TagAnchor::get_rotationZ()
extern void TagAnchor_get_rotationZ_mA64C688D0891B95238B67B377203E8F7BEFF3394 (void);
// 0x00000293 System.Void maxstAR.TagAnchor::set_rotationZ(System.Single)
extern void TagAnchor_set_rotationZ_mF3BCD157BADE9A13762BD1CEC108BBD583FE71F2 (void);
// 0x00000294 System.Single maxstAR.TagAnchor::get_scaleX()
extern void TagAnchor_get_scaleX_mE20980B9775C17E1591EEED3A74E48D2AE8DDC38 (void);
// 0x00000295 System.Void maxstAR.TagAnchor::set_scaleX(System.Single)
extern void TagAnchor_set_scaleX_m0EBC557D8C49175793433D5E04856ECBBD61E44B (void);
// 0x00000296 System.Single maxstAR.TagAnchor::get_scaleY()
extern void TagAnchor_get_scaleY_m687E77670814936BD84A5B23F199546120F19F6F (void);
// 0x00000297 System.Void maxstAR.TagAnchor::set_scaleY(System.Single)
extern void TagAnchor_set_scaleY_m1434272B19574017E5D07E210BD079BE8584663F (void);
// 0x00000298 System.Single maxstAR.TagAnchor::get_scaleZ()
extern void TagAnchor_get_scaleZ_m6BBD3E696700A729D851EA6DC8E2DD477C96734B (void);
// 0x00000299 System.Void maxstAR.TagAnchor::set_scaleZ(System.Single)
extern void TagAnchor_set_scaleZ_mE50D43807B83716A656C8D48AC881106BE16FAB5 (void);
// 0x0000029A System.Void maxstAR.TagAnchor::.ctor()
extern void TagAnchor__ctor_m41985530F083845058B51B231DAB0806515F926C (void);
// 0x0000029B System.Void maxstAR.TagAnchors::.ctor()
extern void TagAnchors__ctor_m2A45730268C7EA861EA6DC62346C08BADCBA9209 (void);
// 0x0000029C System.Void maxstAR.Trackable::.ctor(System.UInt64)
extern void Trackable__ctor_mE378B54439913AE700448034BDD1B8929D11E7E6 (void);
// 0x0000029D System.String maxstAR.Trackable::GetId()
extern void Trackable_GetId_mC16CAC03F5C923AC6DFABA4012C687399EBE85A8 (void);
// 0x0000029E System.String maxstAR.Trackable::GetName()
extern void Trackable_GetName_mBCE42F83977C3A8E2646641F2499B67B7367FAC3 (void);
// 0x0000029F System.String maxstAR.Trackable::GetCloudName()
extern void Trackable_GetCloudName_m4962B6B1393E277C7554B544E3F2D414AC22488E (void);
// 0x000002A0 System.String maxstAR.Trackable::GetCloudMeta()
extern void Trackable_GetCloudMeta_mFD46ED3A54372C6B79F5099562863035634186A4 (void);
// 0x000002A1 UnityEngine.Matrix4x4 maxstAR.Trackable::GetPose()
extern void Trackable_GetPose_m9C90B668ACE195F18153E91D0FC5FFE1B85B730F (void);
// 0x000002A2 UnityEngine.Matrix4x4 maxstAR.Trackable::GetTargetPose()
extern void Trackable_GetTargetPose_m32310454DC65F14D0A910FB8E68B4E88496DB0FA (void);
// 0x000002A3 System.Single maxstAR.Trackable::GetWidth()
extern void Trackable_GetWidth_mAD29588E187B1F996E8D02531F6B807A700025CD (void);
// 0x000002A4 System.Single maxstAR.Trackable::GetHeight()
extern void Trackable_GetHeight_m3F71DA86EB862B9D55ED83E3C828AC732A50EDCD (void);
// 0x000002A5 System.Void maxstAR.TrackedImage::.ctor(System.UInt64)
extern void TrackedImage__ctor_mB0D9283E25637CC0E8AC3418D6238B65F19C512E (void);
// 0x000002A6 System.Void maxstAR.TrackedImage::.ctor(System.UInt64,System.Boolean)
extern void TrackedImage__ctor_mEFAD2FD9910EF432CFF1A8395E9D5311DE5AE8E6 (void);
// 0x000002A7 System.Int32 maxstAR.TrackedImage::GetIndex()
extern void TrackedImage_GetIndex_mCC6DD7205D7D7E5EB34D5E3C4860BDFBCFC253B6 (void);
// 0x000002A8 System.Int32 maxstAR.TrackedImage::GetWidth()
extern void TrackedImage_GetWidth_m075E52B337A907A7183A8318962977311A806E73 (void);
// 0x000002A9 System.Int32 maxstAR.TrackedImage::GetHeight()
extern void TrackedImage_GetHeight_mAC3B5E8990EF2B35FF1EE2814780514BD6B36772 (void);
// 0x000002AA System.Int32 maxstAR.TrackedImage::GetLength()
extern void TrackedImage_GetLength_m240696E86D89E8B0ECA2E280E72A45AA8837152A (void);
// 0x000002AB maxstAR.ColorFormat maxstAR.TrackedImage::GetFormat()
extern void TrackedImage_GetFormat_mC940B47A6D2566D7300BA385947A43B0B3A45B90 (void);
// 0x000002AC System.UInt64 maxstAR.TrackedImage::GetImageCptr()
extern void TrackedImage_GetImageCptr_m73FCD1EEC8D7A60F1D4CF0F0AE956933B3A0C26C (void);
// 0x000002AD System.Byte[] maxstAR.TrackedImage::GetData()
extern void TrackedImage_GetData_mC30F5B00DD8ABB3787EFCA3D833BDE9A6EE1486F (void);
// 0x000002AE System.IntPtr maxstAR.TrackedImage::GetDataPtr()
extern void TrackedImage_GetDataPtr_m5C5B85ABCCE32F0F7A6870D1CA07F09AB36A8D05 (void);
// 0x000002AF System.Void maxstAR.TrackedImage::GetYuv420spYUVPtr(System.IntPtr&,System.IntPtr&)
extern void TrackedImage_GetYuv420spYUVPtr_mC6CBC88A7016B31227FD773713C4DF903BF2070E (void);
// 0x000002B0 System.Void maxstAR.TrackedImage::GetYuv420spYUVPtr(System.IntPtr&,System.IntPtr&,System.IntPtr&)
extern void TrackedImage_GetYuv420spYUVPtr_m9AFA2C8316B80654791007B7B2FF7FB2D6390A9C (void);
// 0x000002B1 System.Void maxstAR.TrackedImage::GetYuv420_888YUVPtr(System.IntPtr&,System.IntPtr&,System.IntPtr&,System.Boolean)
extern void TrackedImage_GetYuv420_888YUVPtr_m0622CC0F943104D7052231D064EC0E4071C82021 (void);
// 0x000002B2 System.Void maxstAR.TrackedImage::.cctor()
extern void TrackedImage__cctor_m83DC66AAEE0694B201752C7AE5F7573697106070 (void);
// 0x000002B3 maxstAR.TrackerManager maxstAR.TrackerManager::GetInstance()
extern void TrackerManager_GetInstance_m4370B1922E66F0889289DE97AD2A583766BF80CF (void);
// 0x000002B4 System.Void maxstAR.TrackerManager::.ctor()
extern void TrackerManager__ctor_m92FEAF7C4362ABCF09D6E0A903DEE33F107306D3 (void);
// 0x000002B5 System.Void maxstAR.TrackerManager::InitializeCloud()
extern void TrackerManager_InitializeCloud_mFC5508DF154B8C38C0C393A2AF4CC32001F41FE3 (void);
// 0x000002B6 System.Void maxstAR.TrackerManager::SetCloudRecognitionSecretIdAndSecretKey(System.String,System.String)
extern void TrackerManager_SetCloudRecognitionSecretIdAndSecretKey_m9FED663DE1E5DD85805060FBC4DBC8B85972882E (void);
// 0x000002B7 System.Void maxstAR.TrackerManager::StartTracker(System.Int32)
extern void TrackerManager_StartTracker_mD673FA4D553E5CC23BCCC6651FDA84691976D78A (void);
// 0x000002B8 System.Void maxstAR.TrackerManager::StopTracker()
extern void TrackerManager_StopTracker_m8AFFA6DF22E4087D29DF9292833B027076FF5A2F (void);
// 0x000002B9 System.Void maxstAR.TrackerManager::DestroyTracker()
extern void TrackerManager_DestroyTracker_m01E17C416B2C6D072492BFF9548C5200CA7BCBE0 (void);
// 0x000002BA System.Void maxstAR.TrackerManager::AddTrackerData(System.String,System.Boolean)
extern void TrackerManager_AddTrackerData_m2D6DFB2FD9766369E0239AA40BA720E14A5F572F (void);
// 0x000002BB System.Void maxstAR.TrackerManager::RemoveTrackerData(System.String)
extern void TrackerManager_RemoveTrackerData_m47F953F5F6A389B4275844EDE2F5A08407E7AF1E (void);
// 0x000002BC System.Void maxstAR.TrackerManager::LoadTrackerData()
extern void TrackerManager_LoadTrackerData_m7D9DF2A711A8E2F4127495DE865E73480093061E (void);
// 0x000002BD System.Void maxstAR.TrackerManager::SetTrackingOption(maxstAR.TrackerManager_TrackingOption)
extern void TrackerManager_SetTrackingOption_m033C0A6041FBB6C8922F7B2BB04BE6046F73B0CC (void);
// 0x000002BE System.Boolean maxstAR.TrackerManager::IsTrackerDataLoadCompleted()
extern void TrackerManager_IsTrackerDataLoadCompleted_m2DF8F22596F686AD7AAC118E3540877240B26714 (void);
// 0x000002BF System.Boolean maxstAR.TrackerManager::IsFusionSupported()
extern void TrackerManager_IsFusionSupported_mD168C27B508507E0B9E29D6E9F72C2AF06E08E9F (void);
// 0x000002C0 System.Void maxstAR.TrackerManager::RequestARCoreApk()
extern void TrackerManager_RequestARCoreApk_mA4D8EE0FA42785952F16EFA5471895CCBB4D56B4 (void);
// 0x000002C1 System.Int32 maxstAR.TrackerManager::GetFusionTrackingState()
extern void TrackerManager_GetFusionTrackingState_mF5B84C2E086E9D5ED76BD55FCA7ED770D804325A (void);
// 0x000002C2 System.Void maxstAR.TrackerManager::SetVocabulary(System.String,System.Boolean)
extern void TrackerManager_SetVocabulary_m00B62BF388C9179D1BA48371700E888BECB55653 (void);
// 0x000002C3 maxstAR.TrackingState maxstAR.TrackerManager::UpdateTrackingState()
extern void TrackerManager_UpdateTrackingState_m81C68EA3F99AEBED9C39EA43B890E328A9DC30E0 (void);
// 0x000002C4 maxstAR.TrackingState maxstAR.TrackerManager::GetTrackingState()
extern void TrackerManager_GetTrackingState_mBD05174B3AA55F3DAB45ECA4926CD644C896FC51 (void);
// 0x000002C5 UnityEngine.Vector3 maxstAR.TrackerManager::GetWorldPositionFromScreenCoordinate(UnityEngine.Vector2)
extern void TrackerManager_GetWorldPositionFromScreenCoordinate_m1613E29BD114D0DA567C1554970270E26EA2FEBD (void);
// 0x000002C6 System.Void maxstAR.TrackerManager::FindSurface()
extern void TrackerManager_FindSurface_m17263FB27620D8BA791E5F042FA20EE1E6004A00 (void);
// 0x000002C7 System.Void maxstAR.TrackerManager::QuitFindingSurface()
extern void TrackerManager_QuitFindingSurface_m9D5C57E58B025A72332E8DD72A7037EBD8AB21A6 (void);
// 0x000002C8 System.Void maxstAR.TrackerManager::FindImageOfCloudRecognition()
extern void TrackerManager_FindImageOfCloudRecognition_m8830506EE66B002666FBF62AD29864464912CC18 (void);
// 0x000002C9 maxstAR.GuideInfo maxstAR.TrackerManager::GetGuideInfo()
extern void TrackerManager_GetGuideInfo_m06E47154464FF9B66701008F383C670562FD7D48 (void);
// 0x000002CA maxstAR.SurfaceThumbnail maxstAR.TrackerManager::SaveSurfaceData(System.String)
extern void TrackerManager_SaveSurfaceData_m03C7860065C3D90FEEDD70635C410197C2B54693 (void);
// 0x000002CB System.Void maxstAR.TrackerManager::.cctor()
extern void TrackerManager__cctor_m8003E7294D3DC5F60860833E0833A5ED910976FF (void);
// 0x000002CC System.Void maxstAR.TrackingResult::.ctor(System.UInt64)
extern void TrackingResult__ctor_mD4299DA6C9FFB8F2AC472DD1A985818E821A1271 (void);
// 0x000002CD System.Int32 maxstAR.TrackingResult::GetCount()
extern void TrackingResult_GetCount_m3BA7A53048D257BA95637374486B78C353A74467 (void);
// 0x000002CE maxstAR.Trackable maxstAR.TrackingResult::GetTrackable(System.Int32)
extern void TrackingResult_GetTrackable_m2603532A1C9F802444000EA0B4E5F321DEBD560A (void);
// 0x000002CF System.Void maxstAR.TrackingState::.ctor(System.UInt64)
extern void TrackingState__ctor_mC65B93BA50E1601F3C9B5A0F9FC5234D2DD5DD93 (void);
// 0x000002D0 System.UInt64 maxstAR.TrackingState::GetTrackingStateCPtr()
extern void TrackingState_GetTrackingStateCPtr_m3324CFF0A90AD6CC15115B77F67D30122FA4000C (void);
// 0x000002D1 maxstAR.TrackingResult maxstAR.TrackingState::GetTrackingResult()
extern void TrackingState_GetTrackingResult_mC57CB67BA5FB282CF3C089C4B8B34B8C71D21B00 (void);
// 0x000002D2 System.String maxstAR.TrackingState::GetCodeScanResult()
extern void TrackingState_GetCodeScanResult_m17FB2618B6A9DA32B303ACA451A0F81449D4850B (void);
// 0x000002D3 maxstAR.TrackedImage maxstAR.TrackingState::GetImage()
extern void TrackingState_GetImage_m30337F216DD6DDE2A09D2629E91F6B0F64F404BC (void);
// 0x000002D4 maxstAR.TrackedImage maxstAR.TrackingState::GetImage(System.Boolean)
extern void TrackingState_GetImage_m33419ABA243952364A82E954EA9C9E99E459C916 (void);
// 0x000002D5 maxstAR.TrackingState_TrackingStatus maxstAR.TrackingState::GetTrackingStatus()
extern void TrackingState_GetTrackingStatus_mE8920D35D3ACEB3240A2E965F63EB01E70AA6D20 (void);
// 0x000002D6 System.Void maxstAR.WearableCalibration::.ctor()
extern void WearableCalibration__ctor_mBCFCF9C00ADD75726964A0E979344841D5BBB157 (void);
// 0x000002D7 System.String maxstAR.WearableCalibration::get_activeProfile()
extern void WearableCalibration_get_activeProfile_mAA579C71EB2F37E8F11F2220E9ECE186AB1A45D3 (void);
// 0x000002D8 System.Void maxstAR.WearableCalibration::set_activeProfile(System.String)
extern void WearableCalibration_set_activeProfile_m9833F5729E0AD9A09C30337524CE5240C8C52337 (void);
// 0x000002D9 System.Boolean maxstAR.WearableCalibration::IsActivated()
extern void WearableCalibration_IsActivated_m1C6680A0484B8605464DA9836F0FFE413B487852 (void);
// 0x000002DA System.Boolean maxstAR.WearableCalibration::Init(System.String,System.Int32,System.Int32)
extern void WearableCalibration_Init_m770F0F6E86C5FEE8C68393B10FD5116B2D8AB1E6 (void);
// 0x000002DB System.Void maxstAR.WearableCalibration::Deinit()
extern void WearableCalibration_Deinit_m8D0AF65A6857BAE8FDB1F7B14D82D52A39EF180C (void);
// 0x000002DC System.Single[] maxstAR.WearableCalibration::GetViewport(maxstAR.WearableCalibration_EyeType)
extern void WearableCalibration_GetViewport_mC83735DD3C180CF40F9864D0DA0907BF79206070 (void);
// 0x000002DD System.Single[] maxstAR.WearableCalibration::GetProjectionMatrix(maxstAR.WearableCalibration_EyeType)
extern void WearableCalibration_GetProjectionMatrix_mE286BE21D8BE85207C942307371BB6FEC1828BC8 (void);
// 0x000002DE System.Void maxstAR.WearableCalibration::CreateWearableEye(UnityEngine.Transform)
extern void WearableCalibration_CreateWearableEye_m15F3FD228A0378382B7198E02097715AA10EC3EA (void);
static Il2CppMethodPointer s_methodPointers[734] = 
{
	FeaturePointBehaviour__ctor_mF4DEFC6BDD0D97988A7B9ECFA8D2E520D5F6027A,
	PinchZoom__ctor_m560990797F8867DD7A1826E88736FD35CBA31C0B,
	PinchZoom_Update_mFE471B49017E569F93F4F5DD4DB490D2CC91AAB7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ARBehaviour_Init_m59940284D5097F666E063D2AD15E92F2A86FCC4D,
	ARBehaviour_OnClickBackButton_m753A1D5DF44EC0114830C393CF1BB4800CD20429,
	ARBehaviour_StartCamera_mE2D6E7580B9EBA5DCF53EA0D09C1CD7C91D57804,
	ARBehaviour_StopCamera_m97DF8F7F762241F4EAD7FBFE7E1568DB9610FFD3,
	ARBehaviour__ctor_mB1AA74585FB1EF7992288C1B07767CEBC8F9AE75,
	BackKeyHandler_Update_m49BEC634EBF49F516EF5F833B0C4F058AF5EBC42,
	BackKeyHandler__ctor_m8AEA5C94A250AEDF6DEA2AC25E848B9A1A0D63D7,
	CameraConfigurationSample_Awake_m983DC81D22C846E30F29EAD45C57891EBA1AFEDB,
	CameraConfigurationSample_Start_m924E517D8ADE190EA08F779C515117A8227C244E,
	CameraConfigurationSample_AddTrackerData_m62AA9B7AA9E7302288A4380709965684481B03F3,
	CameraConfigurationSample_DisableAllTrackables_mF953BDDDD1B0017BEB4724D4095D79EEB9920FD6,
	CameraConfigurationSample_Update_m06986F2050AB58C8B999F149A3ABFBA50F7821F8,
	CameraConfigurationSample_SetFlashLightMode_m238DB73A33530D6BCC3C02278CE35D4B6821DCE1,
	CameraConfigurationSample_FlipHorizontal_m73D5D5460103A1A0B1977AE01F2DF418A410DF8A,
	CameraConfigurationSample_FlipVertical_m76F1C099EDB240548F9D43C73A5E3E3C54C5C4D9,
	CameraConfigurationSample_WhiteBalanceLock_m9FA3383F069BCD8E7F30DB766830256F4351FBAA,
	CameraConfigurationSample_SetContinuousFocus_m43F1824FD527C30ECD4DE18290F40C5D20C4C1DF,
	CameraConfigurationSample_ChangeCameraPosition_m92153A77FBCFF8F24F128867EBA6D2A0C49A4C53,
	CameraConfigurationSample_OnApplicationPause_mD2B7777AB37584B54855F8E5F4A2D7102D3D6FE2,
	CameraConfigurationSample_OnDestroy_m26F85D1106EA4F634642126DEA56C16886A1EFEF,
	CameraConfigurationSample__ctor_mC4104FC04B4A61B7D693B64EC6EC09AF7CE26800,
	CloudTrackerSample_Awake_mA7887F25E1264C15A9F382E8E23CF82633CFEE81,
	CloudTrackerSample_Start_m3E6A2D751B3F8871762F936E62EFFC3ABD92156A,
	CloudTrackerSample_DisableAllTrackables_m53CF0A8711A1E2F35714A3291BB462E770879F42,
	CloudTrackerSample_Update_mD050B82420073AC3C3F53513886CC6614C546532,
	CloudTrackerSample_OnApplicationPause_m0A24CA4E2D48182E340A753C8EC2D1BFE362DC9A,
	CloudTrackerSample_OnDestroy_m7F355AE1EBA839F87822C68E343BCE6ABE2C1C29,
	CloudTrackerSample__ctor_mA46D0E93246D89985FF7FF31FED5F899E31CDB12,
	CodeScanSample_Awake_mA7FB64C8FF2BAA2EA4BBAA85A20D056A3565F929,
	CodeScanSample_Start_m8DC64D3C0541C2A523E4E6F9FF31CF88A5BCD57B,
	CodeScanSample_Update_mFE796E45473A13D2B66D4BB62D14E46DB8EAC6B9,
	CodeScanSample_OnApplicationPause_m6174EE29A34707649C5EFC4ABC5E1E88102C95F9,
	CodeScanSample_OnDestroy_m4E254134C3D4A7F7B4DAFCDF9D3559DCD12EFB92,
	CodeScanSample_StartCodeScan_mD31668C8BD3BD40202B14D825CE0285E5961F2C8,
	CodeScanSample_StartCameraInternal_m3EE5524B90A7C7B27893A885EC80401A99C5DE86,
	CodeScanSample_StopCameraInternal_mD47603BCF1D13AE17F26F9AFC460EE3635F272EA,
	CodeScanSample_AutoFocusCoroutine_m02623B014BAF8C6E74055DEFE1D154CFB82A8EF8,
	CodeScanSample__ctor_mAA9301C15734C1592275EAAB7B4BDC5FFF4CD5FB,
	U3CAutoFocusCoroutineU3Ed__11__ctor_m27A8FFE5992D2B26E22046765FA2E90276E065D2,
	U3CAutoFocusCoroutineU3Ed__11_System_IDisposable_Dispose_mBDB6D9855ACF13C86E2FC8E6FB71244342875F06,
	U3CAutoFocusCoroutineU3Ed__11_MoveNext_m1BEE42812370B0C112CE3AAD02DE91DF6BE88310,
	U3CAutoFocusCoroutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A48D6BAE9ED87DA7AF4E61DA6378968B7DF2F7B,
	U3CAutoFocusCoroutineU3Ed__11_System_Collections_IEnumerator_Reset_m061B634F0C1303432F0BDACE36C292092BB469DB,
	U3CAutoFocusCoroutineU3Ed__11_System_Collections_IEnumerator_get_Current_m6468B35ED480B25CB536A46942683BD015CA4F96,
	HomeSceneManager_Update_m8F9D0687155755E375EB3E7CEE9237B3F1B3ABB9,
	HomeSceneManager_OnImageTargetClick_mF932792FB7941328AE29EC5B6C61D1BB0E05E085,
	HomeSceneManager_OnMarkerImageClick_m02BF39B0A49E6EEC6085B236DA089801F3F71AA7,
	HomeSceneManager_OnInstantImageClick_m33109F30DAEC7CCEB8C2EDB46FE2E7839546F7FF,
	HomeSceneManager_OnObjectTargetClick_m1B98A9C9E1FAE6C5F75246FC1C6F4F8CFE88E2B9,
	HomeSceneManager_OnQR_BarcodeClick_m426952CFD6CA2204DBE13F0AC2F8ECA504B8EDA7,
	HomeSceneManager_OnCameraConfigClick_m4CF72079EC4647144B2EFF987F4709EA74B13693,
	HomeSceneManager_OnCloudRecognizerClick_m3D7CA8A9A758AA542D99C7371B90FFEC7E31618D,
	HomeSceneManager_OnQRTrackerClick_m25490F1C35A3AD007CB7C86A5D4165D77B351871,
	HomeSceneManager_OnVideoTrackerClick_mA98B80B9C999CE3921F737A9F2F0567BC4DCBB7E,
	HomeSceneManager_OnImageFusionTrackerClick_m0CADFB88F0D9634E0A11613C882F4A9CECDE5E16,
	HomeSceneManager_OnInstantFusionTrackerClick_m57A2602BDB7A9B16366102FBDE8AA0F88B7EA595,
	HomeSceneManager_OnMarkerFusionTrackerClick_m9205AD93B22837E8AD1088ACD9476F6065152D38,
	HomeSceneManager_OnQRCodeFusionTrackerClick_mEACE167E17F9DC7DD7E0E5746620868C31E5EFF7,
	HomeSceneManager_OnObjectFusionTrackerClick_m6B77E0E79A34291A7A6CD0AE63E24684EE83E92F,
	HomeSceneManager__ctor_m393EFBA77334F8C7D72F0C93D96A743BE6CB3F78,
	ImageFusionTrackerSample_Awake_m4620A5274A15F6FE2EEA9901B8B6A80CD725C2FB,
	ImageFusionTrackerSample_Start_m05B380597367021256ED1292195C1D9A23AFF0EC,
	ImageFusionTrackerSample_AddTrackerData_m276C291F787D520F0F5BA6725348D0CE2D19C45C,
	ImageFusionTrackerSample_DisableAllTrackables_m38E1097FBFD7B7C699955C4BA61EF452D59052B2,
	ImageFusionTrackerSample_Update_mCFDCFBDFA2920DB707A3A67CD593525FBCB7A7F6,
	ImageFusionTrackerSample_OnApplicationPause_m6DC031532EBA3E96DC6B43E2BE1E8E7FE238937E,
	ImageFusionTrackerSample_OnDestroy_m148440F1DE1180424E068B9997C2E88947F64B3C,
	ImageFusionTrackerSample__ctor_m3AD669C8E2ABC5A4CAAB90BD5884BCC37EEEFB07,
	U3CU3Ec__cctor_m824457FF95DB3FD327618E99EA8634BF254FA53F,
	U3CU3Ec__ctor_m68FA269E2858F02327B825B0C35022EA5B04E295,
	U3CU3Ec_U3CAddTrackerDataU3Eb__5_0_mEBB230E0678B0C798D544BE73C301710D74D07D9,
	ImageTrackerSample_Awake_m7412A77B26CDDEF794D9F65F57F927371BD52A07,
	ImageTrackerSample_Start_m28FF829559576024D5C3EB6E8040E69554A60195,
	ImageTrackerSample_AddTrackerData_m42DB63C042AFCF1E98F73CC7EB4863EE52189AC4,
	ImageTrackerSample_DisableAllTrackables_mB6AF4F7221A7E26A68074CF234565B6756CAC5B7,
	ImageTrackerSample_Update_m512A2DA0D422D8626A2C2C20276801F616C90169,
	ImageTrackerSample_SetNormalMode_m333C0750019CBFA3075C4B1FE6737E696AC2F74A,
	ImageTrackerSample_SetExtendedMode_m68FA42C668C849E45763232EE4E8FFFCC2E3BEFD,
	ImageTrackerSample_SetMultiMode_m8A862CA8E063A4B7165F426468C7096CFAFE2468,
	ImageTrackerSample_OnApplicationPause_mF36A2A4822008DD953BA53A187ABF112CF42A3A2,
	ImageTrackerSample_OnDestroy_mF32151617A84F3CD5EA18146CC60D74F6364218E,
	ImageTrackerSample__ctor_m110C04AA45CB5F3900A517CF76864F70C8F5E99F,
	U3CU3Ec__cctor_m853E6C5D1335C175622C6AA047FA2ED273C342A8,
	U3CU3Ec__ctor_m3784AFA6A99F3FCD3EE597A9D59C3456E1A5FC23,
	U3CU3Ec_U3CAddTrackerDataU3Eb__4_0_m566EDD9A1F170C6FBA63240AB9275DD0009FE0AC,
	InstantFusionTrackerSample_Awake_m8C3D5AF0E35448BD4F26507DF2742EF9318EB839,
	InstantFusionTrackerSample_Start_mD878AB0500549E5F73C26FF62B71E618AE832A74,
	InstantFusionTrackerSample_Update_mCE525F027F0DDD419C61F1C2D585F9BE0DD7C898,
	InstantFusionTrackerSample_UpdateTouchDelta_m6307721F0F9E7D0C1A8BB638972166E52161AF7D,
	InstantFusionTrackerSample_OnApplicationPause_mCFCB3C5523F09D844A3F368071A8C7A8C2771618,
	InstantFusionTrackerSample_OnDestroy_mB287B3E5F17FB3BD306A1ED533C07D68FD3F0D15,
	InstantFusionTrackerSample_OnClickStart_mA0CD02E490BC31058907163A958C18322D7C2AF3,
	InstantFusionTrackerSample__ctor_m4BE3E812988F5C62B2589141D8610C5B52CD2686,
	InstantTrackerSample_Awake_m25EA3C21DF2D934660A40AE643A8F23415DCA3E1,
	InstantTrackerSample_Start_m32B641226A1FC02713D7631AF248F9BF8C0FD9AA,
	InstantTrackerSample_Update_mE3244B7548EC5349ECAE315AEC50B468A3BD8C43,
	InstantTrackerSample_UpdateTouchDelta_mF64ADBB188366286CF76D05F5C69083155B9BF18,
	InstantTrackerSample_OnApplicationPause_m0B539B914C201FEEDD18AD5F96238D103ACE678C,
	InstantTrackerSample_OnDestroy_mAF0DE672525871E8209D910C26CEFF28C578714D,
	InstantTrackerSample_OnClickStart_mA750F20B27816D1A8F7882D39A2632174075F449,
	InstantTrackerSample__ctor_m5B1D25AD966F8A3CCF487ECD2FDB0D088B3A7A69,
	MarkerFusionTrackerSample_Awake_mAEC7CCA69CD294420898251E6B68AEB4C2105AB8,
	MarkerFusionTrackerSample_Start_m875280AD497340AFEE8413D24E9FD0F2A6925DB8,
	MarkerFusionTrackerSample_AddTrackerData_m9A89FE1F16E0AC782F78C66746B6478F74553130,
	MarkerFusionTrackerSample_DisableAllTrackables_m2C5B74EDBB8798C8C84D1755B0507E70753CF9CD,
	MarkerFusionTrackerSample_Update_m55BE4A484266F2023FD18BA108AC9CE5CBE333CD,
	MarkerFusionTrackerSample_OnApplicationPause_m9DBD4AA29E45B5216628D124F0EA26F7B95EF32A,
	MarkerFusionTrackerSample_OnDestroy_mBC8C7AD433AF1315C9AA0FE13914D387F334CF78,
	MarkerFusionTrackerSample__ctor_m217070E9C8B014A4C05C021C0061405F718ECBFA,
	MarkerTrackerSample_Awake_mE3BA80848C019452FA9A5EA231BD6B9E228AF7F0,
	MarkerTrackerSample_Start_mC44C48E70323B9ACEA1283F5097965EE3B4D053D,
	MarkerTrackerSample_AddTrackerData_m09519334B2CF59DF15D50989553B27C4E9D49303,
	MarkerTrackerSample_DisableAllTrackables_m7D0155874CF500A7BED0AB75CA6A241B5A03B610,
	MarkerTrackerSample_Update_m496E72624A47915A4BAA9919C407F2C3FD567DC9,
	MarkerTrackerSample_OnClickedNormal_m4E8C38B62B77619225AD7E436EE3B03BC7C39E3C,
	MarkerTrackerSample_OnClickedEnhanced_m2F2C3D7A9E5C435128939A8690DC3E160B360327,
	MarkerTrackerSample_OnApplicationPause_mFC7FD479A29B3CFDF58DF32ED2E684CD18316ACF,
	MarkerTrackerSample_OnDestroy_mB5D5010339171337CF94F7719A926B5C8EB64D7C,
	MarkerTrackerSample__ctor_m44F8DCE19F97281A9A5D59873C118ACDE3A9D745,
	ObjectFusionTrackerSample_Awake_m870336D3C133A6CE634888A146C15889C292298E,
	ObjectFusionTrackerSample_Start_mC3AA66CC1458E9976552130EC697516AAA4C2FC7,
	ObjectFusionTrackerSample_AddTrackerData_mE2B51398EEA3014573A5F9DC2072AF5E56ED1CB5,
	ObjectFusionTrackerSample_DisableAllTrackables_mE98997D72EF55F33B67BEAEEBDA108CDF7B710E7,
	ObjectFusionTrackerSample_Update_mD6805EC8849FE599545855E628503C086D7260EE,
	ObjectFusionTrackerSample_OnApplicationPause_m97CC6076EB75B64155FF208B3224CE1AB5E6049A,
	ObjectFusionTrackerSample_OnDestroy_m534E5F7DEDE4A182A504ABE6698E433F0E9A2935,
	ObjectFusionTrackerSample__ctor_m19F0A18830120C007242EB41F4CEB09C660D149C,
	U3CU3Ec__DisplayClass5_0__ctor_m7A07932697362B9D337BEC77213D0DE02D4BE291,
	U3CU3Ec__DisplayClass5_0_U3CAddTrackerDataU3Eb__0_mACC7F7951D007AEC9EFF7DBE8D64EDDF24863635,
	ObjectTrackerSample_Awake_m298EEDD2023DCE4F6770FF1609481C81F30769ED,
	ObjectTrackerSample_Start_m10B30AC068431794E9D470A81E23A0687300D510,
	ObjectTrackerSample_AddTrackerData_m960AAF6DAADE8E912BF1B3AF5977CEB85EE0A4D8,
	ObjectTrackerSample_DisableAllTrackables_m586190A03C5787A0905EC399D58B9AB31F6C3F5B,
	ObjectTrackerSample_Update_m8A372AF78AA4115FBA11450CD9B48B2CE7AB67D7,
	ObjectTrackerSample_OnApplicationPause_m39D8B8EA0D56C612A7EB5126F1B1B5039033FA36,
	ObjectTrackerSample_OnDestroy_m4C3F3646FC5E9C24912A3507FB5C2DE0A24BAC33,
	ObjectTrackerSample__ctor_m8453D054DC9141B4939CE430CF44F4DB2799F235,
	U3CU3Ec__cctor_m247E98A5837BA8A199A0AB75B612C978D772B944,
	U3CU3Ec__ctor_mF3F159D350C11A67DD6599E6DAC44DC3FADAA2B8,
	U3CU3Ec_U3CAddTrackerDataU3Eb__4_0_m8CFE18688869A184282957884D870CD8B3ED046A,
	QrCodeFusionTrackerSample_Awake_m14B0A4ADF61476B33D56E4565E4D8444461C066F,
	QrCodeFusionTrackerSample_Start_m78ACB970BB5AEA9207245724FDC9B0B29766FD1F,
	QrCodeFusionTrackerSample_AddTrackerData_m25FABAB2B9EF9C19DDBF1762B93B2A1C9AA1388A,
	QrCodeFusionTrackerSample_DisableAllTrackables_m8E38F00ACB80CDC538B430432015BB41E36210B9,
	QrCodeFusionTrackerSample_Update_m76CFA34E61DA9E282AFC602748DC0FB0D1FDB260,
	QrCodeFusionTrackerSample_OnApplicationPause_mB9E94FFB04F3BE7C6F5719C6200D87CBCCA792DA,
	QrCodeFusionTrackerSample_OnDestroy_mE38D45DFB07B53C7A44D2095FBC6907CD854A96B,
	QrCodeFusionTrackerSample__ctor_mEF34F247EDEDFA90796B04226FF3AF81D97AB4CA,
	QrCodeTrackerSample_Awake_mAA41745A70BA5FD8A60A6C02FDB3CDFF245CF317,
	QrCodeTrackerSample_Start_mDE4CCEDF47204F0FE0EF5CBA0A9989C459B86345,
	QrCodeTrackerSample_AddTrackerData_mDC33F1FF8193B9CD29FF8296CB73728A4C257F6D,
	QrCodeTrackerSample_DisableAllTrackables_m491B04E051D33DD2DB6B50CE109C2351AB5A75F3,
	QrCodeTrackerSample_Update_mC20196AF08C9060BB2751209D570FDF31EF4E203,
	QrCodeTrackerSample_OnApplicationPause_m348E181EC2B73C66E3E89D9CCECC1784FC9DAD60,
	QrCodeTrackerSample_OnDestroy_m262CE396B1FC20A581B5A201AB6E9D1B4C48021A,
	QrCodeTrackerSample__ctor_m1169322B46F39732D48829D65087345DAB245B14,
	SceneStackManager__ctor_m784AC2CAA424BD6A7D6ADB2AD8F6C8378C92EE41,
	SceneStackManager_get_Instance_m7AF77B540713F96AC0409F80A3205057A7DCDAFD,
	SceneStackManager_LoadScene_m9AEDA45F3F9B6EB9A1A3E96E93E4DE985C29C982,
	SceneStackManager_LoadPrevious_m042A541247219EFABAF0B48FDA1577F29711C89C,
	SceneStackManager__cctor_m140FAEDEB77C67A773D2687DC13ABCDE869E0679,
	NULL,
	NULL,
	NULL,
	NULL,
	VideoTrackerSample_Awake_m51F42B6D07A53389E9EF905EE53EF24453B43429,
	VideoTrackerSample_Start_mBD85251CFCB8EDACD74A7C2115E5DDD001493BD2,
	VideoTrackerSample_AddTrackerData_m557B9C58EE0AA4F48B68297C09B051800CA290F3,
	VideoTrackerSample_DisableAllTrackables_m9B8DE6B0E38A96F51E7267415B54461B1D528C3B,
	VideoTrackerSample_OnNewFrame_mF8157121E4F36B90839665F47ECDAC2BCB2DEABF,
	VideoTrackerSample_SwitchCameraToVideo_m8E4593CB18692B0224A366BE85EF331F9992EAEC,
	VideoTrackerSample_PauseAndPlayVideo_mCED96EC7E26DDD5A5B505374B49F8F1C339068C9,
	VideoTrackerSample_Update_m26196264359E413E2CCB35461F78C1B8F5020977,
	VideoTrackerSample_SetNormalMode_m10F42BDBAE21D87613CD7B7B93316D078A2CEE0D,
	VideoTrackerSample_SetExtendedMode_m2796BAA1DFFB591F1BF1BBC355EBE59AD53D9D29,
	VideoTrackerSample_SetMultiMode_mA70630576408EB2BD606D4F75CA95A09226DC085,
	VideoTrackerSample_OnApplicationPause_m74FFC0962BC903D1C94C6C1500A3A3202CB6552C,
	VideoTrackerSample_OnDestroy_m8163C7AE53D346DABAB5E102C10D5FE6B313B893,
	VideoTrackerSample__ctor_m8F86F048B0EB6AA27D27BFC37FF8A30B55CB51C6,
	VideoTrackerSample_U3CStartU3Eb__5_0_mC461FB1679DC02B29B2D7C57B62DAE3B812EAEBC,
	U3CU3Ec__cctor_m37DD0D3F11AD8487D374DCE500539FEE904E6228,
	U3CU3Ec__ctor_m8A566A34008BBD0B6DE4E56098B49E6860E6C408,
	U3CU3Ec_U3CAddTrackerDataU3Eb__6_0_m6AEB6709D3BEA3DE5574C350E04FC8ABDF1923BD,
	AdaptingEventSystemDragThreshold_Awake_m29753D9DC51FBB3E299A2512299EBE68EA2C2DA1,
	AdaptingEventSystemDragThreshold_UpdatePixelDrag_mF5C694C6B97ADD2CF2F31C6C36E8AA541F016A95,
	AdaptingEventSystemDragThreshold_Update_mB83423313AD3456296C402376402C6D166FBA6B3,
	AdaptingEventSystemDragThreshold_OnLevelWasLoaded_m71DE0DD26F111417A9690585102606178B17EBBE,
	AdaptingEventSystemDragThreshold_DisableBtnControll_m86624F9FEF27085ECE430E607543C55337E41722,
	AdaptingEventSystemDragThreshold_Reset_mF423CBDBC24770C0C5ABD8CD17C48B070B12BB6E,
	AdaptingEventSystemDragThreshold__ctor_mDC8BC16A02948AD4876A41CF90B7F0B4D83AA92C,
	ARManager_Awake_mF9A1FD43A812AB7C715521333BEF99A5CF2F164A,
	ARManager_OnDestroy_m698B1926664753D21A14D81779BB0EC0A0D4DFC5,
	ARManager__ctor_m3379ADAD23DAC4FBF810F1E74A16982A3C1087B7,
	AndroidEngine__ctor_mEA71A3C5541F092C8C4B25AAC2E9871F6E62D4ED,
	AndroidEngine_Dispose_m206483E69FD8990C2101B547DF9AD1CAB776E02F,
	CameraBackgroundBehaviour__ctor_m210FC5F301B95FE4C9BBD94B66A8FA67B43D93CE,
	CloudTrackableBehaviour_OnTrackSuccess_mB57C64761E644555D8D3827178C0DF15D8D67566,
	CloudTrackableBehaviour_OnTrackFail_m87C58458F999AD1AAB243A17F226FDFCA0C1CD5B,
	CloudTrackableBehaviour__ctor_m1D9745929F77C0BD7A2E386D51FC98DDECAE1F6F,
	ConfigurationScriptableObject__ctor_m1421B6E421DAAAD15E64B21FB0DA4A5D14982E5E,
	ImageTrackableBehaviour_OnTrackSuccess_mDD81968CECBCBCEA3675832224F1B8A7D0C2EE43,
	ImageTrackableBehaviour_OnTrackFail_mA6FCD1D47D437ACFEA74ADF0FA880331C374D010,
	ImageTrackableBehaviour__ctor_mEBC53EAEFDC23383B74939B0E0748F3A6606A88D,
	InstantTrackableBehaviour_OnTrackSuccess_m6C47F3A2FEE692379446C4830138DD6F5EC9E91A,
	InstantTrackableBehaviour_OnTrackFail_mFD5FE6AB7754F19410696DE6596C99A0EC31F019,
	InstantTrackableBehaviour__ctor_m86CA1BD6B698D0B0626E77B4DD557C352FA8847E,
	MapViewerBehaviour__ctor_m6ACDBADD2E87A35EC634693341A4186BD25DED05,
	MarkerGroupBehaviour_get_MarkerGroupSize_m0E17A479CCC20475FD99A0BAEDB948B135102846,
	MarkerGroupBehaviour_set_MarkerGroupSize_m0477AC2E7A525EC3E904AF422C00C0D3E0337A08,
	MarkerGroupBehaviour_get_ApplyAll_m36679E577FC0BE93B2A866266ECF1CB54742F60C,
	MarkerGroupBehaviour_set_ApplyAll_m237D7F1F4C57DCF7EA2D03E7B0EAE6F7889A863F,
	MarkerGroupBehaviour__ctor_m7470690E8B5EF58FB1143D412F5350FAA63FBA29,
	MarkerTrackerBehaviour_get_MarkerID_m9A804752BAC9B87CCCF3EC2D12BECC8C9C9EE847,
	MarkerTrackerBehaviour_set_MarkerID_mDB4A6573ACF10F82723ED51187134379B6CA7CAA,
	MarkerTrackerBehaviour_get_MarkerSize_m6A5AAB5F412DB98F4A65A016E3DDB9F1A46382F4,
	MarkerTrackerBehaviour_set_MarkerSize_m46F276AB4DCC5E037149E78A1B5007D05A4D2E3B,
	MarkerTrackerBehaviour_SetMarkerTrackerFileName_m7EFF42C7CD781519F9A5658721CD1B8EED216F94,
	MarkerTrackerBehaviour_OnTrackSuccess_m9E5064A77DA0C46D550824B5E02916FDB95AE1E1,
	MarkerTrackerBehaviour_OnTrackFail_mDF5AEC5E3A136D8611CF41F17DEC627565B75D5E,
	MarkerTrackerBehaviour__ctor_mB058BA1868E865B648CF3D46822A68CF54465358,
	ObjectTrackableBehaviour_get_RealSize_mB82689C405CCCB712345D9BA3A8FDBC94F538951,
	ObjectTrackableBehaviour_set_RealSize_mB2A8A1945E1F9EF9AF5CEEA579B5D82E3585C134,
	ObjectTrackableBehaviour_OnTrackSuccess_m5ECDE2064A040A5AB868DF46B6D82DB8BCF86283,
	ObjectTrackableBehaviour_OnTrackFail_m432B93F75751D3B52BB7529501CA3800409C36CE,
	ObjectTrackableBehaviour__ctor_m419BDDA62E2A63D502B323BE412040035AAD140A,
	QrCodeTrackableBehaviour_get_QrCodeSearchingWords_mCCD936FD29BBB09DECD1167B61396A0CDD4A8231,
	QrCodeTrackableBehaviour_set_QrCodeSearchingWords_m7B162BC3E46197B873EAF4CE06DEC99EB945C939,
	QrCodeTrackableBehaviour_get_QrCodeRealSize_m52FDB2D6AF88E2DBB14868D1F46EB7CE0E210A02,
	QrCodeTrackableBehaviour_set_QrCodeRealSize_m9F14343CE85DBC4D38051371AD6C46D089C3052C,
	QrCodeTrackableBehaviour_OnTrackSuccess_mFAF923FA53E5027112036C0D243C8C1E14C050CC,
	QrCodeTrackableBehaviour_OnTrackFail_m0B4A536013B6C2E073A0FDCD2B5763C4A2C9A951,
	QrCodeTrackableBehaviour__ctor_m85B708AC69700D21B2DAA0C0C7F37D55DB25175A,
	WearableDeviceController__ctor_mFE6F47BBC5E06B99ACBCBE2395FF99B6CDE0B2C6,
	WearableDeviceController_Init_m0B59A4DC7D85027CC2850B74B57B5201D2BB24C7,
	WearableDeviceController_DeInit_mC965799ACE5666A10125A1F5B5E1E6C168E784B0,
	WearableDeviceController_IsSupportedWearableDevice_mC9ECA396D4DB2EE4DA1604BCAC3E769BD0DCCFFD,
	WearableDeviceController_GetModelName_mB6F25268D3AE1B12807051444342456AA9BE925B,
	WearableDeviceController_SetStereoMode_m0158E69150CD2D38C1D271719B2070CA3048B166,
	WearableDeviceController_IsStereoEnabled_m1D80A827662EE897D5317E223A89D95CA3A77E6F,
	WearableDeviceController_IsSideBySideType_m4CD2B554F0DF724176301B9E1592B0062AB0C39E,
	WearableManager_GetInstance_m637FBD4CFE59BC8F00CEB69521DF2ABC42E216C4,
	WearableManager__ctor_m364677DEBB9FB83CAE4DEF61D224C076D6311C7A,
	WearableManager_GetDeviceController_m868C6C777B5F73C1DB5D2866DBDF25F66D2A0DED,
	WearableManager_GetCalibration_m917859FD20B08F1DC7FF7874BB8B6E105A53B612,
	WearableManager__cctor_mBDAE8A851F024D29AA5B9FC7A35ECF17A90E8970,
	APIController_POST_m4B7832E65770F83646887987045053C7A7FBC9B8,
	APIController_Escape_mA6E28E07F24C7EA35412EB6889B08C40F861BDF9,
	APIController_DownloadFile_m5A9F6604CC9CAD6E028299EF94FA2D534D886F8D,
	APIController__ctor_m644D9B0DD6B34894765BCC2A9DCBCDECAED60922,
	U3CPOSTU3Ed__0__ctor_m8B18AC9A9D0A18E1B0E00DB57FCE2C2A75F9E716,
	U3CPOSTU3Ed__0_System_IDisposable_Dispose_mC53245B55296C9131843C47CEFCA965EB26BB6BC,
	U3CPOSTU3Ed__0_MoveNext_mBA14A302009F2C66D93E7D50B5C8B95F4406FDF2,
	U3CPOSTU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF91203E6CB04F82AAA7D9B82A5B5034D7E654B3E,
	U3CPOSTU3Ed__0_System_Collections_IEnumerator_Reset_m5BDC27162CCB2EDEB272F624C20B1135A1D749AC,
	U3CPOSTU3Ed__0_System_Collections_IEnumerator_get_Current_m1788A56AB4715A6C693FC0082DE70BA50512E16D,
	U3CDownloadFileU3Ed__2__ctor_m8758FF5B05FE6C6A8FAD599401468A4451B37278,
	U3CDownloadFileU3Ed__2_System_IDisposable_Dispose_mAE5289B45A32E7C0A907C14FCF64E8C7DCE9A856,
	U3CDownloadFileU3Ed__2_MoveNext_mA584706B1DAFAA4EBB79DB261774119DC9CF0DF6,
	U3CDownloadFileU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m084D68C77F65F9FB194C3FD394B4598D89DF0E05,
	U3CDownloadFileU3Ed__2_System_Collections_IEnumerator_Reset_m51A960BA3A23B5E4D861177EFF637A2A4B7888A6,
	U3CDownloadFileU3Ed__2_System_Collections_IEnumerator_get_Current_mD7AA2A53E3356452C26D1E8E2334DA8705D9096F,
	AbstractARManager_get_Instance_m7D40524BF14232C5E6A6D4AF4A04A36EDE18FB66,
	AbstractARManager_Init_m8A07903031B43348359E1B55A410A9268BDEE5A2,
	AbstractARManager_InitInternal_m526724444A3DA88E8A0744DFD1DD3A8C1BDD76D9,
	AbstractARManager_Update_m984CB9F472D1292989B923B5D99375E54CD4A8DE,
	AbstractARManager_Deinit_m9B9B4DD1DDA8990CA0D1B432501697D6F7F7C0D3,
	AbstractARManager_get_WorldCenterModeSetting_mEAD7B7E7CE53508427BCE3C48AE8969A14C073BC,
	AbstractARManager_GetARCamera_m8A25D4F65F37B9D5FFC17868215702193A6DA66B,
	AbstractARManager_SetWorldCenterMode_m4654AB9FAEFE58F085938ED85CB3863AB8347889,
	AbstractARManager_OnPreRender_m1A558418BA6D775A98A4C9EBECA8C36FFD1330DC,
	AbstractARManager__ctor_m33A8FF7D5356B9D3018B80EFFCB62FBB7DEB4639,
	AbstractARManager__cctor_m5CF88D25442016DEB5B5258310F406CCA93B632A,
	AbstractCameraBackgroundBehaviour_get_Instance_mDD4BE85534265958FAEF13922D0CD9F1BB26459F,
	AbstractCameraBackgroundBehaviour_Awake_mDD13452A743BF9A4FDABF1B55C57ABF6B1E7F6BA,
	AbstractCameraBackgroundBehaviour_OnApplicationPause_m54B6F2856374F3DE4F33A48AADCB25D31DC35447,
	AbstractCameraBackgroundBehaviour_OnEnable_mC987AC7F22C089D75CC1C53F65F289B9F48E245B,
	AbstractCameraBackgroundBehaviour_OnDisable_mBD4B3C733EE36DC20F8BD3EE6E0A7228EDB111A2,
	AbstractCameraBackgroundBehaviour_OnDestroy_mFCFCB63BE12BADC717F59FAE6C2EC5F349600F3C,
	AbstractCameraBackgroundBehaviour_StartRendering_m2EFC0F3A181C163E4B4CDD91B23D008205887483,
	AbstractCameraBackgroundBehaviour_StopRendering_m8121621802CF60485CB87782995C7EC26E5036B9,
	AbstractCameraBackgroundBehaviour_CreateCameraTexture_m45EC199D1329EB24F36708C0D018F4309E5F28BB,
	AbstractCameraBackgroundBehaviour_UpdateCameraBackgroundImage_m7F7AFBED65DE836D04DDB7DA4021657CDA63A850,
	AbstractCameraBackgroundBehaviour_UpdateCameraTexture_m85ACA56AB1B5DE83214A883379A83A613F6DF708,
	AbstractCameraBackgroundBehaviour_TransformBackgroundPlane_m816D05BC0A05282B2A16BD1A947EA6A4079FEDE8,
	AbstractCameraBackgroundBehaviour_RenderingEnabled_mD5924B962C71C7B43EFA646979E23A7F7E2798E6,
	AbstractCameraBackgroundBehaviour__ctor_m5E482D5ADF3EB9E57F31B224B225DDACEDD579C8,
	AbstractCameraBackgroundBehaviour__cctor_mEF4042EFAFF4C788623CD2C9DC9172653DAE464D,
	AbstractCloudTrackableBehaviour_Start_m012BB5701DCED1874A666D7F140DE3F144F9EB25,
	AbstractCloudTrackableBehaviour_OnTrackerCloudName_mDE96D9A63DE9179F3EEDD951389F09AC6B418D75,
	AbstractCloudTrackableBehaviour__ctor_m65DC0C23151A3B0C2D297F516E04AD646344811A,
	AbstractConfigurationScriptableObject_GetInstance_m817B4F645D8D4D438CB6CA55CB4F369947346DB0,
	AbstractConfigurationScriptableObject__ctor_m5D4C4CAD1228274A85A46B71AB32219CE3D29CEE,
	AbstractConfigurationScriptableObject__cctor_m6CA54FE2D4930269A2306841C8EE1CF9E87996B3,
	AbstractFeaturePointBehaviour_OnApplicationPause_m267C33415C614AA97CD717A473733068ACDFA2C7,
	AbstractFeaturePointBehaviour_Generate_mB4D1BCEEEFAD26FAA8126884F8AB16FC1F03A3FE,
	AbstractFeaturePointBehaviour_convertFloatToVertex3_m5A115A7D6FCCD3AAB7F9F051670DC041A396870C,
	AbstractFeaturePointBehaviour_Start_mE75A7B77873331EECDEBCB1AB057FB8DC4F35A67,
	AbstractFeaturePointBehaviour_Update_m279837D60FE2DD0A18C575BAAF06708B6C754885,
	AbstractFeaturePointBehaviour__ctor_mBB7E11573E39269BFF27225612149E96DE46D2CF,
	AbstractImageTrackableBehaviour_get_TargetWidth_m931E767D38410ED7B6CCC96FA103BA275C0E029F,
	AbstractImageTrackableBehaviour_get_TargetHeight_m490445BAA2225BFEA0DBF5C1ED82DC0A31CBBEBC,
	AbstractImageTrackableBehaviour_Start_m8DDAD3BADD1D3267A19689D4DC27C50418F6EEED,
	AbstractImageTrackableBehaviour_OnTrackerDataFileChanged_m578DE17C773310CEE6EB1C43AE808B47C0D2A911,
	AbstractImageTrackableBehaviour_ChangeObjectProperty_mA2A71792D1024F49523A8BFF08686E3AD88504AA,
	AbstractImageTrackableBehaviour__ctor_m26F32A7990629E3445BB2F45C4324B93CB1B1618,
	U3CU3Ec__DisplayClass9_0__ctor_m0D480A6A701277BB949634E5A19E560096D3EA13,
	U3CU3Ec__DisplayClass9_0_U3CChangeObjectPropertyU3Eb__0_mAEAD3D5C272E3EB08601CEF0696161BD13D4A46E,
	AbstractInstantTrackableBehaviour__ctor_m5B9CD7A85A5E6DD6557D5E5FCD69BD5E455D5838,
	AbstractMapViewerBehaviour_get_KeyframeIndex_m1CA0B1DE92B49A8CCC34C49A7B0C622A95A4DDBC,
	AbstractMapViewerBehaviour_set_KeyframeIndex_m9FFE3D6917C0781FB92E240A42BF67F4CE04C847,
	AbstractMapViewerBehaviour_get_ShowMesh_mF02248B8DACE3DA0ACD2C1056F9D85917194BD52,
	AbstractMapViewerBehaviour_set_ShowMesh_m89B334BCD3B4B77353473B3FCC925DDF265F397F,
	AbstractMapViewerBehaviour_get_MaxKeyframeCount_mDFF547CA0AD5AF84B5AB0696110F73B1E1782F2E,
	AbstractMapViewerBehaviour_set_MaxKeyframeCount_mC15F5F82DAA55D42CC015C02547C68CB5AC2EC19,
	AbstractMapViewerBehaviour_get_Transparent_mC459A8E663D3C750DA6F93E2549089F6F22D2034,
	AbstractMapViewerBehaviour_set_Transparent_m885293F63D5C495C266D8589C8DC2E20C0FC73AC,
	AbstractMapViewerBehaviour_Load_m190CEFBD02C46C05BF9F98B8AD5F97052E8B4D92,
	AbstractMapViewerBehaviour_ReadMap_m43B02859AD5A6E692671483A44A77F7EFD824843,
	AbstractMapViewerBehaviour_SetTransparent_mA52B79F6D0D04CB94733D553A3765B5D228ACDDA,
	AbstractMapViewerBehaviour_GetCameraTexture_mF18BD7ABF5F8B802FD1755D29F70299B2D0477D8,
	AbstractMapViewerBehaviour_UpdateMapViewer_m10359D97D7E8DA229E836EF87AFF099F2CC3F7E6,
	AbstractMapViewerBehaviour_ApplyViewCamera_m4330E1C882DBA08622F03E98AD0F331BA6F29256,
	AbstractMapViewerBehaviour__ctor_m2AD5E019B3BE3C93B8F0B065E377BDE95CD69720,
	AbstractMarkerTrackableBehaviour_Start_m5D7CF7B02474BFDCD924D8434A1C9FC78A5F877E,
	AbstractMarkerTrackableBehaviour_OnTrackerDataFileChanged_m623D76CF5EE697FED673474BE5B64C5D1657D386,
	AbstractMarkerTrackableBehaviour_SetTargetTexture_mDCE5E0427B3383C2E26B543CA8F0C1680624965A,
	AbstractMarkerTrackableBehaviour__ctor_m2F63E8D20F9D29CF2584B3C16D33639E49FF5A4B,
	AbstractMarkerTrackableBehaviour_U3CSetTargetTextureU3Eb__3_0_mE25F046AC60C97C1D3C4FF60AA2C71F388CE7BF4,
	AbstractObjectTrackableBehaviour_Start_m52DC647B315DDE9CE59AE3657851EA9B3CBD1F28,
	AbstractObjectTrackableBehaviour_OnTrackerDataFileChanged_m80F1B60A137E19A2B16F4DB9AE35A75CAA3F5E26,
	AbstractObjectTrackableBehaviour__ctor_m361D0F1CBD29977E796122835EEC4F5F008789F6,
	AbstractQrCodeTrackableBehaviour_get_TargetWidth_mB6F07A9620F9CC47CADB670D39F4602903FC24CC,
	AbstractQrCodeTrackableBehaviour_get_TargetHeight_m373AD6AB97163E03646708D5C910E85D60295217,
	AbstractQrCodeTrackableBehaviour_Start_mCD00ED9B28F51A5864A56855E9109328B1E3D48D,
	AbstractQrCodeTrackableBehaviour_OnTrackerDataFileChanged_m8095D85685E404810BF43A4F4537013C00FBF9A7,
	AbstractQrCodeTrackableBehaviour_ChangeObjectProperty_m3F3ED3047AD6A634AF2A57FFC5B39BC3687EFED7,
	AbstractQrCodeTrackableBehaviour__ctor_mF316FE8B163F8D67B793FD9900627F9C0D93D950,
	U3CU3Ec__DisplayClass9_0__ctor_mA0EC57F299C6A1D988F399F458CBFE24F0610FC1,
	U3CU3Ec__DisplayClass9_0_U3CChangeObjectPropertyU3Eb__0_m94984962456FCF23E8AE4419AF71239ABED40F9E,
	AbstractTrackableBehaviour_get_StorageType_m2BBB71A55CB5FEF9CDD89D4ABDD939F30B08ED9A,
	AbstractTrackableBehaviour_set_StorageType_m45AB0B80FFCAB7225CD2B520234A581B6D0AC934,
	AbstractTrackableBehaviour_get_TrackerDataFileObject_mE565071FE69543A330D7BBC718FB2B2FD7CB775C,
	AbstractTrackableBehaviour_set_TrackerDataFileObject_mB2F4E0421376DF1C669B76E1D376961EDFEEDEEC,
	AbstractTrackableBehaviour_get_TrackerDataFileName_m0DE6B48A9BF3F2CA36CA7D3DFDF05052539C0D51,
	AbstractTrackableBehaviour_set_TrackerDataFileName_m724B0E37AFA68C112FB89F3B66FE4F2A7988CA41,
	AbstractTrackableBehaviour_get_TrackableId_m12B931DD4EE524250CE00F4D8445AE19A4DD8D58,
	AbstractTrackableBehaviour_set_TrackableId_m442B3876129827A23A2EC79A74D12A0C020833C2,
	AbstractTrackableBehaviour_get_TrackableName_m4E2D41E96FDBE9F6311123F474A53518C9959B90,
	AbstractTrackableBehaviour_set_TrackableName_m02E87D73DAAA3E30CEE3BD0CEC1181044E5F3222,
	AbstractTrackableBehaviour_OnTrackerDataFileChanged_m2CE30B54D67B011F59067B4547E37CEAA4CB97DE,
	AbstractTrackableBehaviour_OnTrackFail_m74271C81ABB8AD11949918B851268A7885B6872A,
	AbstractTrackableBehaviour_OnTrackSuccess_m7EC7E3A05A38D7E83E0C9C7BA122F70D2FDAA7AD,
	AbstractTrackableBehaviour__ctor_m87ECDD54EF4F5EF18CB24AB0CB758F8372639BE6,
	CameraDevice_GetInstance_m7B4D41A3F7D4DBE7D23EA6B9DD7E8CC47D9C4849,
	CameraDevice__ctor_mF5F0FA1C514129160D121CEF08AE7FD50A35CCC5,
	CameraDevice_Start_m0EF2AE94ED109805AAECDBBFD84849BB77072F22,
	CameraDevice_SetNewFrame_m99BC5C4CF010EA4DD9A9900F1766B93EC72312EA,
	CameraDevice_SetNewFrame_mBC9C8AC5D4A1A0CF40AA5658BCC936EA007A20F2,
	CameraDevice_SetNewFrameAndTimestamp_m70A04DC093F5DF1FB9B6856A8B5330AA6629084E,
	CameraDevice_SetNewFrameAndTimestamp_mB68F5AED6AB59BFFA302F67E2F36F2B9A30CE144,
	CameraDevice_SetFocusMode_mE967450239E66416FD9BE18839932850A0EE56B7,
	CameraDevice_SetFlashLightMode_mD750C1983547711E0C28029BAE5E23EB0552B329,
	CameraDevice_SetAutoWhiteBalanceLock_m5000BF2E7E8DF8F80D4CC90D26265348D985DFA5,
	CameraDevice_FlipVideo_m39F2804F6C7B6E45DD9BA1E91D52066693C5BA99,
	CameraDevice_IsVideoFlipped_m4121AD281EBCE4BC25A22A2FBD1FCE7150AF83F5,
	CameraDevice_SetZoom_m8F455796C6AA7A0DF54B060516923723161428EF,
	CameraDevice_getMaxZoomValue_m16B2FF622B9E3FCAA0E8FCECC9B188D7D188D641,
	CameraDevice_GetParamList_mB4A3C3F0CD66F0810FE7E644B7420E92623EE8CF,
	CameraDevice_SetParam_m2779AF750BDFE7734CFC85FE38556A71393B2A89,
	CameraDevice_SetParam_mCB589383D69DE6E20AA21E0AE6FCE461BE81904C,
	CameraDevice_SetParam_m1F021E6329BB58E534E1D0E62EBC203698810EF5,
	CameraDevice_SetParam_mD3B26FF46801948833C6FD25E1BBF76DC467A903,
	CameraDevice_Stop_m9C379AE847093944B5814516045576677915F1C1,
	CameraDevice_GetWidth_mBDC8FFBF8A3883FC4F8AEA4A188B7E9A8E2E03E9,
	CameraDevice_GetHeight_mB6E0B66A7B14EEA2B64E59838A6C0E7669D19016,
	CameraDevice_GetProjectionMatrix_m8BFDF8998C90D524AB3D32E5B1473C382CFA7F8F,
	CameraDevice_GetBackgroundPlaneInfo_m729FECA5316B236DFC59245BECC414B471A507AC,
	CameraDevice_IsFlipHorizontal_m863E8A65FC2747908D596605F0871D0E7B1EC034,
	CameraDevice_IsFlipVertical_m9ADE0CC01822BE740564256778390715601119F3,
	CameraDevice_CheckCameraMove_m52F20E396766013B043435B6BFD5CACE1B8D1600,
	CameraDevice_SetFusionEnable_mCCC1EB2C8953A4730BFA87C8309B4AC48427FD86,
	CameraDevice_SetARCoreTexture_m38D00985DD03345959DB7A15BADE71F77A37C472,
	CameraDevice__cctor_m12EC8EF441A1F68493373E31F0933273C18F9976,
	CloudRecognitionAPIController_Recognize_m432A1484A56094ABFF8D59ADFA147177383480D9,
	CloudRecognitionAPIController_DownloadCloudDataAndSave_mEE40E349F44D619C41B2964482BA90927C154D9D,
	CloudRecognitionAPIController_JWTEncode_mF2B569C90D25C079E601920B727ED9695D7523F0,
	CloudRecognitionAPIController_destroyApi_mBCD89AB176C222114107FA1D1B7130647DC063E4,
	CloudRecognitionAPIController__ctor_m316933BF81F638B30D81683B09E5537D3BF6FAF6,
	U3CU3Ec__DisplayClass1_0__ctor_mF7F58D1D6B0F86C36E7B13F6867B1D04B586CA4A,
	U3CU3Ec__DisplayClass1_0_U3CRecognizeU3Eb__0_mA57F8FFAD6650DE1895D17CB5E99FD6F48ADF242,
	U3CU3Ec__DisplayClass2_0__ctor_m3D11DDFA7D3DEDF9B8D3623BA4E9A10391DFF4D9,
	U3CU3Ec__DisplayClass2_0_U3CDownloadCloudDataAndSaveU3Eb__0_m6E96A6526E4EDABF505EF5382EAE3840C8CF85A3,
	CloudRecognitionController_SetCloudRecognitionSecretIdAndSecretKey_mC94E6E91003122DA32C20D230D4BB26620A696C2,
	CloudRecognitionController_GetCloudStatus_m917E33C7A16CFD59C79B72F893AAFECB17321516,
	CloudRecognitionController_StartTracker_m599AB716748B8719527BC5AE715BD1DABF9E502A,
	CloudRecognitionController_StopTracker_m0F415836231846E3ADD0C095BF9658BF9801D653,
	CloudRecognitionController_DestroyTracker_mC6C84ED7F76E5D2677DA283A94D52C0630B040AD,
	CloudRecognitionController_FindImageOfCloudRecognition_mF0E2390A1E5AD23671EC0B45B4CFECE269E79A04,
	CloudRecognitionController_SetAutoEnable_mB327DE2DC13502831DBBFCA35F0B0F07FC8C255E,
	CloudRecognitionController_Update_m85ADB3DF79456DF8D034F9E14B3FFEF392E4AFFC,
	CloudRecognitionController_StartCloud_mF97C95FD6E30A36AC304066AD4735BF14BDC7C44,
	CloudRecognitionController_GetFeatureClient_m8EB55CD3F15957FE9BE89067A1E126AA3FE680D4,
	CloudRecognitionController_GetCloudRecognition_m0F8938E560BBCB741596AF5505A9F6679493CE8F,
	CloudRecognitionController__ctor_mCFA809A8341EE88F74C1D2A82575602E794B3CB8,
	CloudRecognitionController_U3CFindImageOfCloudRecognitionU3Eb__18_0_m23C114CABB583CC9E10029220F013752060C0611,
	CloudRecognitionController_U3CFindImageOfCloudRecognitionU3Eb__18_1_m6AED8A10BC77B0B9E2DF0C289BB3E7F553C13AC5,
	CloudRecognitionController_U3CUpdateU3Eb__20_0_mF2B268F00305D2BE26AE3F3F94B181AB257DF1D3,
	CloudRecognitionController_U3CStartCloudU3Eb__21_0_mCCD75AC229691F2B2A17F14A1A73DA4103A5F2FF,
	U3CU3Ec__DisplayClass20_0__ctor_m9C6A0C0486AACED086721E4D1B6C322E3F5A0059,
	U3CU3Ec__DisplayClass20_1__ctor_mEC57BE075DF90D86EE44DB5E0AE42DB2EE7D098D,
	U3CU3Ec__DisplayClass20_1_U3CUpdateU3Eb__1_m18631C370200AEE3E66247605F4C28F22757D514,
	CloudRecognitionData_get_ImgId_mCA032244632C578194150EC4FBBB952453005F5F,
	CloudRecognitionData_set_ImgId_mFF7235537DCBDDDC9C0896DB42F0938BFA01DA56,
	CloudRecognitionData_get_Custom_m8C9B2490C5C61345DC912E1D00D8E1187EDC4243,
	CloudRecognitionData_set_Custom_m661AFD5D37038F4D57F8695B57892B0AB3351D34,
	CloudRecognitionData_get_Track2dMapUrl_mFACBC40BD0575A942177731CE2F35F87537CB97C,
	CloudRecognitionData_set_Track2dMapUrl_mCD55B656D52E84F0F4A225EB4C92716C627268F6,
	CloudRecognitionData_get_Name_m34A6CC8B54DACF7333920827E03AD0DF548B3D2A,
	CloudRecognitionData_set_Name_m85E503DEB98326367DA4C223A7E96A9664FF6EAA,
	CloudRecognitionData_get_ImgGSUrl_mB933724ABFBBF603C62041EA585F8BB64497403A,
	CloudRecognitionData_set_ImgGSUrl_m704D674C3A44E9C2396B3ADD2A6F905211454D49,
	CloudRecognitionData_get_RealWidth_m5C8676A9D5671EADCB4060C028AFA943E8FA0E68,
	CloudRecognitionData_set_RealWidth_m31103FE4023713DAE684C6C5A7AB0CFC52BBAD14,
	CloudRecognitionData__ctor_mDEBA3778583730D24A615AED89C22299D5D9EB06,
	CloudRecognizerCache_GetInstance_m355775F74C13974437E0775232FA4727727BB954,
	CloudRecognizerCache_ADD_m30A9064B05F3679554AE8448CD4DCFFFCF762FDE,
	CloudRecognizerCache_LOAD_m16A8CA14A5290FB0C5BE9995AEC9FDC355A10ED5,
	CloudRecognizerCache__ctor_mA182F8FB9EEB770EE1953FDECDE039338A5EDC71,
	CloudRecognizerCache__cctor_mEE11AD8113EFC1AE374B00A8DF89ACC96A10FFD4,
	CloudRecognitionLocalData_get_cloud_m11430FD5CAC6ACFBD404FDA6C0EE23A8513EE0D8,
	CloudRecognitionLocalData_set_cloud_mDA904ADA325E76221C236CA0CF6745AC277F12DD,
	CloudRecognitionLocalData_get_cloud_2dmap_path_mC6B8BBEA7C292E32DEA3D456FB60FC80927BCD2E,
	CloudRecognitionLocalData_set_cloud_2dmap_path_mD265F7FD5BAE0332C21B862F9B5B9D41A5D22D05,
	CloudRecognitionLocalData_get_output_path_m6074C3E508C32E84C683237F96D3519295EA7A4A,
	CloudRecognitionLocalData_set_output_path_mE47CC4AFC907F59B0D6C54820BECCF88DAB5B872,
	CloudRecognitionLocalData_get_cloud_image_path_mB7BF5453DF217A11208263EC723C6E006215D6E5,
	CloudRecognitionLocalData_set_cloud_image_path_m7723EAD911957C9C994CEC332713DEEC24A6C249,
	CloudRecognitionLocalData_get_image_width_m5D290176001A24243CF27305C4D3F3E04ABCBB78,
	CloudRecognitionLocalData_set_image_width_m2F0AAF622FA6A97316D6287FF3CD3DE6F3AA891D,
	CloudRecognitionLocalData_get_cloud_name_m5DA3B2595FD96D426685635DC505A24B9572A30E,
	CloudRecognitionLocalData_set_cloud_name_mEEB6D28709B3FA2DC53E93247DE0BFD96EB7E620,
	CloudRecognitionLocalData_get_cloud_meta_mA45A750F34748797F3CC92290EEA889D40F24EFC,
	CloudRecognitionLocalData_set_cloud_meta_m839B32112B787F3CE571475CBCC13A5B5B522D84,
	CloudRecognitionLocalData__ctor_mB491CD6644D8F3B7DF15F65FE82A37D5F3F2B555,
	Dimensions__ctor_m47F28DC631B1C3E6287AFFEBDB5DF13786785D9E,
	Dimensions_get_Width_m0D5022D79CF463DA9FFD663258FF2E5D931DB5A3,
	Dimensions_get_Height_m1BE5D8685B7944D3C76C7707003952EB4F4EB8C2,
	Dimensions_ToString_m8E94E585F488AD8ED0BCE7A237512ADFD8AC2FE9,
	GuideInfo_UpdateGuideInfo_mDC7A662E86640A0382D138886F24EC8E37558645,
	GuideInfo_GetInitializingProgress_m7EE3F3D2111DAA15DAF62FC4F4A75A6198C90222,
	GuideInfo_GetFeatureCount_m0D5F2D0F3384AAE528CF60B3EA5580E2BB95E82F,
	GuideInfo_GetKeyframeCount_m6DDE9F5F0DC82BE67C1C245D225EAB7BD7C0F1C3,
	GuideInfo_GetFeatureBuffer_m77E6751DE7244817A7D8DA10DEC8AC570792AB88,
	GuideInfo_GetTagAnchors_mE8966A2F34B55DCB95C6511B36E8664A923D7C48,
	GuideInfo__ctor_m5B0C6BAC596BCA0837582B2E6E62243EBA1EC8E2,
	GuideInfo__cctor_m1E0DD7BF25EDF6A8650F8D4CDDC4FDB7CC217DFD,
	JpegUtils_GetJpegDimensions_m82F6AFCF809C0A9304CFF05D61A5A002BA049E9C,
	JpegUtils_GetJpegDimensions_m4A811C429785EAC86D15C84CD900708BE7711560,
	JpegUtils_GetJpegDimensions_mA24E1653396B67026BF24161B07124AFABFBF216,
	JpegUtils__ctor_m57D4AF9468E9EA20B1700801C0B6A60B2C37A040,
	Map3D__ctor_m2EDD0B9A684F67E856306C50EA02626BB34E48F8,
	MapRendererBehaviour_Create_m324ED7489A940B64D8DD89CA401FBF993FFF76CA,
	MapRendererBehaviour_CreateAnchors_m755E309BE8D580180A29F6C941E08ADD06A43806,
	MapRendererBehaviour_CreateMapViewerMesh_m44B7C67233D5FB8AEFD378225A9E2CB81CE2A6CA,
	MapRendererBehaviour_Clear_m7D18B3E6CC4E30B81C599226BDF64C0F1114DD9E,
	MapRendererBehaviour_SetActiveImageObject_mC9648203108305B53843DFC1F56F90B9800695BF,
	MapRendererBehaviour_SetActiveMeshObject_mD2617B8AC194962AE7D72D10D1C1FCF24FED67C0,
	MapRendererBehaviour_SetDeactiveImageObjects_m68E5601664A3DCD39BA78EDB6A39A71E409FF6BD,
	MapRendererBehaviour_SetDeactiveMeshObjects_mE547D9E0FFF02DCDD64A8ADD6277A6ACAC1931F9,
	MapRendererBehaviour_OnRenderObject_m8F1C250626CCC074FA68605E4A4B5B0EF80A235A,
	MapRendererBehaviour_GetGameViewSize_mF541D50839B6BA7F067610720534B2F0E4178BF3,
	MapRendererBehaviour__ctor_m0A7EE1F41C9B7BB3D496C7279A383AB931BBBFC9,
	MapViewer_GetInstance_mFA97A42492F240612089D27D0F3AFD7FB9854681,
	MapViewer__ctor_m7824783612FD7F5FFE23B7E37C185A88FECE1A04,
	MapViewer_Initialize_m282862E10923D4E45DC3967015AFF2F58C5E44E9,
	MapViewer_Deinitialize_mAF0443059205DC5A0B863329240B047E643DE3AE,
	MapViewer_GetJson_m7B7E0343F68516B5B229B7697E53312E5C5CDFB1,
	MapViewer_Create_m87FC5F3FDC13363C6EA312ED61E79674BB358117,
	MapViewer_GetIndices_mDC9C269E6E69D9C1A1B30ACE6CCC3B77C47A24ED,
	MapViewer_GetTexCoords_mF0B2A186B37750FD8C3A32774C9E046F84031250,
	MapViewer_GetImageSize_mA4C9285C6594BC911E5373DECC7EAF1895015ABB,
	MapViewer_GetImage_mA6E41486407336824ED12780D0D73175CEB5CDDC,
	MapViewer__cctor_mDE29F81F1AC0BC7D130B158BB226195AC1506ABC,
	MatrixUtils_ConvertGLMatrixToUnityMatrix4x4_mE408249A02F624E4C773C33C4F8438046B9AA421,
	MatrixUtils_ConvertGLProjectionToUnityProjection_m951F3E0B613BB5E9F92409666A8EF3ABA47863EC,
	MatrixUtils_GetUnityPoseMatrix_mF526E85AD182AFCE3FE8D2161450AAA18642AB86,
	MatrixUtils_GetUnityPoseMatrix_m2B22301A12F00FAB404F0AED88998D0EF00496F8,
	MatrixUtils_ApplyLocalTransformFromMatrix_m49F32084AB7540B13843E5BC6418669DE156E030,
	MatrixUtils_InvQuaternionFromMatrix_mADE4AFB532ABDAFF594BA7866636195B8FAFB489,
	MatrixUtils_PositionFromMatrix_mD02D238E5913ED5BA2204310CE4B9FC907F22816,
	MatrixUtils_ScaleFromMatrix_m6A00E24E5963E7CFE55971A60FE8D7820A912049,
	MatrixUtils_MatrixFromQuaternionAndTranslate_mCAF7247A4C8C929EFA9096F6A46EE7AEFC122627,
	MatrixUtils_MatrixFromQuaternion_m3ED94B4E318E230174869ABE94A11EA7021CD862,
	MatrixUtils_QuaternionFromMatrix_mFE8CFF1F046595B8A85491CA08F7BBE8A20CFE34,
	MatrixUtils__ctor_mF5218D00D82B921750DAC6C6D2BA0CC94E1E2455,
	MaxstAR_GetVersion_m3C9089CB18A45D0BA176A06401EE52834D69D4D6,
	MaxstAR_OnSurfaceChanged_mB35F33B14AC5F1AF9526C49099185B38B45D2060,
	MaxstAR_SetScreenOrientation_m62E76A343A3BEC0B3EC28993E2755A3104EE1CC9,
	MaxstAR__ctor_m15B73A265494FE7FAC321F01F7F46E0B47202E7A,
	MaxstARUtil_IsDirectXAPI_m6BC01430AA25B8E26DEF1BBBBB3F28434FF69696,
	MaxstARUtil_LoadImageFromFileWithSizeAndTexture_mEDD961DEAE5C8F3C39D0FB9ABB0F3007760EF57A,
	MaxstARUtil_ChangeNewLine_m2E9EBBF5147CBABBACFFEBCAE5CB44AEC68DDE8A,
	MaxstARUtil_DeleteNewLine_mCB3DC0F8DE9479A0B137657C2D7E74816EC20821,
	MaxstARUtil_GetPixelFromInch_mEB11F6D02CC7381F47D532672561E0039029A75D,
	MaxstARUtil_DeviceDiagonalSizeInInches_m52E5BDBD8C90AEF3F43B66261054452401A741DE,
	MaxstARUtil_ExtractAssets_m066CE8C8AB84B844C492F6C7E6DDAC05F0EE5375,
	MaxstARUtil__ctor_m2440EF4C745F79D22AF2AB9EBC7E05EFE209070B,
	U3CLoadImageFromFileWithSizeAndTextureU3Ed__7__ctor_m71F4F81B5212E2BFB6B46F54F4BD99B07DE43BAD,
	U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_IDisposable_Dispose_mDE2B51E68F1388B563AB1E81D32CC7E6F19F9B49,
	U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_MoveNext_m31944946B7FC2348F3DF09E7DD610FB1E5B39342,
	U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF3FB7AAB4BFDA41BE7451833A4138636D4B910D2,
	U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_Collections_IEnumerator_Reset_m480B2F004F05610C7FDA4BE61427E2F6E75551C6,
	U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_Collections_IEnumerator_get_Current_m6AFD1398A09E1F103DF0432920077887D5395378,
	U3CExtractAssetsU3Ed__14__ctor_mD1ECC726D892EF7DF1B924D09260F70A71D20BD1,
	U3CExtractAssetsU3Ed__14_System_IDisposable_Dispose_m41C90A325F0E55692BE8D1A05E5CF520CD069407,
	U3CExtractAssetsU3Ed__14_MoveNext_m6CF1D91E037D70FB14FDFDC341413E822FEA6929,
	U3CExtractAssetsU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4DF288844ED548412F34B635BEBBD1901AA0F315,
	U3CExtractAssetsU3Ed__14_System_Collections_IEnumerator_Reset_m387CAEF9FB92CD551B267592DE88103740E737BB,
	U3CExtractAssetsU3Ed__14_System_Collections_IEnumerator_get_Current_m4FF8B46E1729307920440B2E159D43B2FFB3F23C,
	NULL,
	NULL,
	NULL,
	NULL,
	NativeAPI_maxst_init_mC421B3568E7E354EE758992F9FEE91AACB754906,
	NativeAPI_maxst_getVersion_mEA9CEF894DE3F5CCE4C82ED36FDA06D9A7127EFE,
	NativeAPI_maxst_onSurfaceChanged_m618861493F1D215C9987FC494E9C9DFC8DB56341,
	NativeAPI_maxst_setScreenOrientation_m4201B34755027F4B2F189C225F834443FB230239,
	NativeAPI_maxst_CameraDevice_start_mA148CCBEB395140FB888403518FA19126519FD6D,
	NativeAPI_maxst_CameraDevice_stop_mC6C881EB1195C2402F90C27D233A78C16894C1E8,
	NativeAPI_maxst_CameraDevice_setNewFrame_mC40388178C53F040C9691DFBDB31724FB2C5D99B,
	NativeAPI_maxst_CameraDevice_setNewFramePtr_mF45F0F18F951F3DF9E70EA73ACAD5B80299297A4,
	NativeAPI_maxst_CameraDevice_setNewFrameAndTimestamp_mC57FA1496ACAB11EEA5AC70B78AB10057FF977F7,
	NativeAPI_maxst_CameraDevice_setNewFramePtrAndTimestamp_mC637F15F7A426343F2C21A8F7CB84F52CA6ADF80,
	NativeAPI_maxst_CameraDevice_setFocusMode_mAC5D359262EE800885242A7B1020301F7BF97CBB,
	NativeAPI_maxst_CameraDevice_setFlashLightMode_m84432C67B669BE560BE5F0A3431D0FC655433B66,
	NativeAPI_maxst_CameraDevice_setAutoWhiteBalanceLock_mFF5C02AC8765AFCF4CCC48E3396E4F5D8B4B7575,
	NativeAPI_maxst_CameraDevice_flipVideo_m5A8B109AA322E7738DB53A4763D326B5067FB32E,
	NativeAPI_maxst_CameraDevice_isVideoFlipped_m5D17BD93BBC9ED8FD4881949FBFA00B7FA50061C,
	NativeAPI_maxst_CameraDevice_setZoom_mACA296819A0A90194F6310A9BA8BE8EB10381013,
	NativeAPI_maxst_CameraDevice_getMaxZoomValue_m489F96B138941686BF901351134E0131D437B32E,
	NativeAPI_maxst_CameraDevice_getParamList_mCE8E8049EECD925C889CA399DE1C212609DC3DAE,
	NativeAPI_maxst_CameraDevice_Param_getKeyLength_m9971AEBDE465E45C799ABFEA1C0BF4CDA4389691,
	NativeAPI_maxst_CameraDevice_Param_getKey_m88F9F08965A5C01EED7C9C19953F3EDCDBD580E2,
	NativeAPI_maxst_CameraDevice_setBoolTypeParameter_mDFEAD348009DC9DE8640BF5F953F10641CF3EC01,
	NativeAPI_maxst_CameraDevice_setIntTypeParameter_m08299249A808D02BDE5CE094151FCCE750A1496D,
	NativeAPI_maxst_CameraDevice_setRangeTypeParameter_m41CB577D38C32B1E4F9B7756F4C936E59DFF36D8,
	NativeAPI_maxst_CameraDevice_setStringTypeParameter_mB003CB420D2E5668214C3A2FF1656E89DA99E947,
	NativeAPI_maxst_CameraDevice_getWidth_mFE2F4F92B10D643E5A650ED2C6083172EC323467,
	NativeAPI_maxst_CameraDevice_getHeight_m0A24FB0D69B25D4A374051C8695B06FB759A17EC,
	NativeAPI_maxst_CameraDevice_getProjectionMatrix_m2013DA48718C146FB1285F59BB8F2A533501D70F,
	NativeAPI_maxst_CameraDevice_checkCameraMove_mC19A106F9726A5B5F05F94F2B37CB89D3F4B6970,
	NativeAPI_maxst_CameraDevice_getBackgroundPlaneInfo_mC54F5739ABBAC1352D4AD82B277FF0389368C06D,
	NativeAPI_maxst_CameraDevice_setARCoreTexture_mD0FFD802A4E0BF605514A495A1CF197AA3AB196A,
	NativeAPI_maxst_CameraDevice_setFusionEnable_mBF357029D75084A338A142AC172AF646342B92A7,
	NativeAPI_maxst_TrackerManager_startTracker_m971E03A88CA6EC5B30A3C9E7A68D2A17954FB26F,
	NativeAPI_maxst_TrackerManager_stopTracker_m5A8EE1D410208C74BC6AF5D87A7510220AFC05BC,
	NativeAPI_maxst_TrackerManager_destroyTracker_mB74A5810BD25D0CB1A57CF383031E2921F9B8BF0,
	NativeAPI_maxst_TrackerManager_addTrackerData_m50AD9A060A5D3FA3E8E38C88D2F29DC74483363A,
	NativeAPI_maxst_TrackerManager_removeTrackerData_m7D27EE780662ECF8FC8DEAE4D25594E934D449A8,
	NativeAPI_maxst_TrackerManager_loadTrackerData_mC42564153C5EF09C5D56219D1D740C12AAF4F526,
	NativeAPI_maxst_TrackerManager_setTrackingOption_m4668D35FEEC1D35AD57C731C2426F61CFEFBE435,
	NativeAPI_maxst_TrackerManager_isTrackerDataLoadCompleted_m92769B2A5E0FF8B6E89582550A976F8F6D1B0875,
	NativeAPI_maxst_TrackerManager_isFusionSupported_mA00F64266B3B24FE23AEF14578A26F4E77C31A2E,
	NativeAPI_maxst_TrackerManager_getFusionTrackingState_m5DCA51D41DABD24A99FAC65CED3B45391546F89E,
	NativeAPI_maxst_TrackerManager_setVocabulary_mB9507D1CD48277302CD4B5B38E75C2BC128F469D,
	NativeAPI_maxst_TrackerManager_updateTrackingState_m4E4B3B8E8A69E0512D69CE8D67737656F60BBD84,
	NativeAPI_maxst_TrackerManager_findSurface_mA37ED878FEE4E865EED55B91955C4075CDF95775,
	NativeAPI_maxst_TrackerManager_quitFindingSurface_m0C148526FB8A1C7BBC5C347E7F2801A9D52CD147,
	NativeAPI_maxst_TrackerManager_getGuideInfo_mEC1297F219F10A73B816CF8AE70C485DBFD3E727,
	NativeAPI_maxst_TrackerManager_saveSurfaceData_m2B54C617A28B345052F49017C2F8E1F82F4A4C3D,
	NativeAPI_maxst_TrackerManager_getWorldPositionFromScreenCoordinate_mD0EE7454B9E0D8CBF5A1984B85E753273E41D499,
	NativeAPI_maxst_CloudManager_GetFeatureClient_mC1035CF7E3C3AD0D69ABFEA7922023487CDC0D71,
	NativeAPI_maxst_CloudManager_JWTEncode_mD48952482C7A8C3933F246A5E50ACB77C7F33B60,
	NativeAPI_maxst_TrackingResult_getCount_m7597D810E7436A469DCC58B3758D535A89658002,
	NativeAPI_maxst_TrackingResult_getTrackable_m240DB6DB0F18CC92D23440D4B9A6494F059F1245,
	NativeAPI_maxst_Trackable_getId_m2DEE48E439EA8406F377D1746094036622E3C9C2,
	NativeAPI_maxst_Trackable_getName_mCEE4B36D3A74D2BCE330455736A0468DCA14479B,
	NativeAPI_maxst_Trackable_getCloudName_mA1FE53DF31A037A17D9D94F6A5E9F298CAB5767D,
	NativeAPI_maxst_Trackable_getCloudMeta_mE07FED8AB309A43528FC473CC669D222D0A9A918,
	NativeAPI_maxst_Trackable_getPose_m12E3676E586A813CE1D77D932060E3411075B7AC,
	NativeAPI_maxst_Trackable_getWidth_mC57D8EC900A2FA32B1FDD374EABC921F7C9083AE,
	NativeAPI_maxst_Trackable_getHeight_m82AE200A875BCA11B9A6ED2BF14AB915EDA8CF73,
	NativeAPI_maxst_TrackingState_getTrackingResult_m8E636950DB4BE895F91FA8097BE3531DD1781B33,
	NativeAPI_maxst_TrackingState_getImage_m9F5F305D12B2329745B96FEE05BD2337EBB2309E,
	NativeAPI_maxst_TrackingState_getCodeScanResultLength_m3041564879082248114B8FA0B49E3988C482C130,
	NativeAPI_maxst_TrackingState_getCodeScanResult_m5283C45EB7A99AD04C407CDE298EB587FFB6C17D,
	NativeAPI_maxst_GuideInfo_getInitializingProgress_m583D5D97F10BE1DA780F74D00E381B638B648028,
	NativeAPI_maxst_GuideInfo_getKeyframeCount_m8B7BACC6890DDEB91B4024CB1625B7B8823EBCC6,
	NativeAPI_maxst_GuideInfo_getFeatureCount_m3FFA629C4DDA65E0888C063FFE169DF4EAE8E0E7,
	NativeAPI_maxst_GuideInfo_getFeatureBuffer_m08D7498058158CC78F7DA4F952D6336578D552B4,
	NativeAPI_maxst_GuideInfo_getTagAnchorsLength_mCB5C3823895D5EC0D38A5013C1409636B3228814,
	NativeAPI_maxst_GuideInfo_getTagAnchors_m5002310C42B614D7D85D6852702651716BD3E2A6,
	NativeAPI_maxst_SurfaceThumbnail_getWidth_m520F114443145A6A7812774331FC953D16675ABE,
	NativeAPI_maxst_SurfaceThumbnail_getHeight_m2F72481A1AA4833B541A0CBA02E13B5988859076,
	NativeAPI_maxst_SurfaceThumbnail_getLength_m6DF005AFD8001FAAC41BC32753E9E726BB3F0AC0,
	NativeAPI_maxst_SurfaceThumbnail_getBpp_m9DD5C31DDF7A5C898B461FD129C6ABB6A3BB6484,
	NativeAPI_maxst_SurfaceThumbnail_getData_m981A55C316D8B42D85C3F3C02B2A0A8DE7BA5714,
	NativeAPI_maxst_SensorDevice_startSensor_m26113D4C45739B50831518A758334F067ACE54AB,
	NativeAPI_maxst_SensorDevice_stopSensor_mA201C5474D4484EC064AD8CE0EEB0F8F896A085D,
	NativeAPI_maxst_MapViewer_initialize_mB7A5CB51A6DDF56DCE263DE5884770C5986CBE07,
	NativeAPI_maxst_MapViewer_deInitialize_m81AABB7C65F04A0025CB948AA2450FB3E7F4A0CC,
	NativeAPI_maxst_MapViewer_getJson_m8CB6EC65BB89E26FAB81B8EFE12FFAC0E9BB4E74,
	NativeAPI_maxst_MapViewer_create_m86BC2E8591107064528F9ECA2E53857EA2EEBE8B,
	NativeAPI_maxst_MapViewer_getIndices_m89CCDE5BFC5609F1A8362D142A3DDA6596FB70F4,
	NativeAPI_maxst_MapViewer_getTexCoords_m9B09717D34C0CA260F65E6014373736B4AA5C3B9,
	NativeAPI_maxst_MapViewer_getImageSize_m3D9234DF0B7412AB69D81B903D40B78D8510AE8A,
	NativeAPI_maxst_MapViewer_getImage_m955FEB2A74476DB677754032B09242C629FA42B1,
	NativeAPI_maxst_WearableCalibration_isActivated_mBE3366086158CA44C2E084347A74061DCCCA5D6C,
	NativeAPI_maxst_WearableCalibration_init_m40FC939132D15219F92CB2A27D6F2D3FA4AFEA71,
	NativeAPI_maxst_WearableCalibration_deinit_mEAD2920ADBB6C0626B1E9E21648F40573E7347DE,
	NativeAPI_maxst_WearableCalibration_setSurfaceSize_m0948C95339C702B187AC3A34DCE87A8FE7FA97C3,
	NativeAPI_maxst_WearableCalibration_getProjectionMatrix_mDB6B7E83E5C85AF8C31CAAFAD159E78FA7CFF1FA,
	NativeAPI_maxst_TrackedImage_getIndex_m2AE2581A69F31117A9C62A8CDDC8815DF965EADC,
	NativeAPI_maxst_TrackedImage_getWidth_m434D019740805BFD6E820FE098984858C1FC98E1,
	NativeAPI_maxst_TrackedImage_getHeight_m8E1519A4CFD45053ABAA05EAADC14FFA0706ADE7,
	NativeAPI_maxst_TrackedImage_getLength_m15AF0402D86BE722F3AFD7CFBC3A96A21B95499E,
	NativeAPI_maxst_TrackedImage_getFormat_mFE9CE69AE9A34CFB087E12BE80692B0DCF676A33,
	NativeAPI_maxst_TrackedImage_getData_mD958C322ADD5331759FB0E8E60B291403660FCAE,
	NativeAPI_maxst_TrackedImage_getDataPtr_mFAE0D226B7687B01B636E949F187124AC7EF27BA,
	NativeAPI_maxst_TrackedImage_getYuv420spY_UVPtr_m1C80CFB077016B47D0C748689BBA7A9D1B286E23,
	NativeAPI_maxst_TrackedImage_getYuv420spY_U_VPtr_m09595CC3C2493A2484B109FB5B0E6F701C7AB7D0,
	NativeAPI_maxst_TrackedImage_getYuv420_888YUVPtr_m335FEC1E34B3B0BC0D779D9EBEC8F09AB277EBEB,
	NativeAPI__ctor_m30EB75B4916AFA765378C21EF8803E3CF8BEBD30,
	Point3Df__ctor_mCCBF4D230FC3AB6E910A94328554DA89F5CB46F5,
	SensorDevice_GetInstance_m3864BED3197F8B1C00021265FAFA6CFBB452F8E4,
	SensorDevice__ctor_m715604CA802B26F81CDCC0CEB0FA9EC72890456E,
	SensorDevice_Start_m1A70CC1D74E860B4716AA4D3AD699248A055C301,
	SensorDevice_Stop_mA7C93D511F50C2C21E64FF50AB9645C34169EB9D,
	SensorDevice__cctor_mC6C505263AD1BE96216C837AC5D6829A47242353,
	SurfaceThumbnail__ctor_m39DC5B565F76B4C634FE72BF6921AA502E344FC2,
	SurfaceThumbnail_GetWidth_m91FCFDC9A7507322F424251472896E61791C067E,
	SurfaceThumbnail_GetHeight_m404377B70D92E8C395E6CA33CE526E6CD5C31C2E,
	SurfaceThumbnail_GetLength_mF101214587C7027721688FEAA1AD9B1949567DAC,
	SurfaceThumbnail_GetData_m51F47401919F24C71810D68DFC75DFC5EF61177B,
	TagAnchor_get_name_mF8DAC64068A1C6CB707EE63A32DE4D6687D4BAD2,
	TagAnchor_set_name_m2D36CCCCC28CA3944617F35624A911586DD7ECEE,
	TagAnchor_get_positionX_m80D2855FF8B8E937EB6E997D78068DB759B04895,
	TagAnchor_set_positionX_mF86791611A360323B8EFE691600481952FDEFFF8,
	TagAnchor_get_positionY_mD7C3C0B2CCCDE677466B713224E3910FE67D2EE3,
	TagAnchor_set_positionY_mD099D343BDCA591701DA50EE5172CED04762B436,
	TagAnchor_get_positionZ_m630455D354909F12E49EA9E24ACE06FE4CC7D6CC,
	TagAnchor_set_positionZ_m02122FF5AEB088DAF87647A7F9210AD239C5F897,
	TagAnchor_get_rotationX_mA8B0C1AF862854107D20294F538DBD9385BE9EB0,
	TagAnchor_set_rotationX_mEA9BCCDB93EF9F4A0619800283492CF5FCC23C13,
	TagAnchor_get_rotationY_m0957DFA6D62933D3FDC0840B47DAD08FA3F64B99,
	TagAnchor_set_rotationY_m0CAB790BC87941F59A5A975296CC09D68616C58B,
	TagAnchor_get_rotationZ_mA64C688D0891B95238B67B377203E8F7BEFF3394,
	TagAnchor_set_rotationZ_mF3BCD157BADE9A13762BD1CEC108BBD583FE71F2,
	TagAnchor_get_scaleX_mE20980B9775C17E1591EEED3A74E48D2AE8DDC38,
	TagAnchor_set_scaleX_m0EBC557D8C49175793433D5E04856ECBBD61E44B,
	TagAnchor_get_scaleY_m687E77670814936BD84A5B23F199546120F19F6F,
	TagAnchor_set_scaleY_m1434272B19574017E5D07E210BD079BE8584663F,
	TagAnchor_get_scaleZ_m6BBD3E696700A729D851EA6DC8E2DD477C96734B,
	TagAnchor_set_scaleZ_mE50D43807B83716A656C8D48AC881106BE16FAB5,
	TagAnchor__ctor_m41985530F083845058B51B231DAB0806515F926C,
	TagAnchors__ctor_m2A45730268C7EA861EA6DC62346C08BADCBA9209,
	Trackable__ctor_mE378B54439913AE700448034BDD1B8929D11E7E6,
	Trackable_GetId_mC16CAC03F5C923AC6DFABA4012C687399EBE85A8,
	Trackable_GetName_mBCE42F83977C3A8E2646641F2499B67B7367FAC3,
	Trackable_GetCloudName_m4962B6B1393E277C7554B544E3F2D414AC22488E,
	Trackable_GetCloudMeta_mFD46ED3A54372C6B79F5099562863035634186A4,
	Trackable_GetPose_m9C90B668ACE195F18153E91D0FC5FFE1B85B730F,
	Trackable_GetTargetPose_m32310454DC65F14D0A910FB8E68B4E88496DB0FA,
	Trackable_GetWidth_mAD29588E187B1F996E8D02531F6B807A700025CD,
	Trackable_GetHeight_m3F71DA86EB862B9D55ED83E3C828AC732A50EDCD,
	TrackedImage__ctor_mB0D9283E25637CC0E8AC3418D6238B65F19C512E,
	TrackedImage__ctor_mEFAD2FD9910EF432CFF1A8395E9D5311DE5AE8E6,
	TrackedImage_GetIndex_mCC6DD7205D7D7E5EB34D5E3C4860BDFBCFC253B6,
	TrackedImage_GetWidth_m075E52B337A907A7183A8318962977311A806E73,
	TrackedImage_GetHeight_mAC3B5E8990EF2B35FF1EE2814780514BD6B36772,
	TrackedImage_GetLength_m240696E86D89E8B0ECA2E280E72A45AA8837152A,
	TrackedImage_GetFormat_mC940B47A6D2566D7300BA385947A43B0B3A45B90,
	TrackedImage_GetImageCptr_m73FCD1EEC8D7A60F1D4CF0F0AE956933B3A0C26C,
	TrackedImage_GetData_mC30F5B00DD8ABB3787EFCA3D833BDE9A6EE1486F,
	TrackedImage_GetDataPtr_m5C5B85ABCCE32F0F7A6870D1CA07F09AB36A8D05,
	TrackedImage_GetYuv420spYUVPtr_mC6CBC88A7016B31227FD773713C4DF903BF2070E,
	TrackedImage_GetYuv420spYUVPtr_m9AFA2C8316B80654791007B7B2FF7FB2D6390A9C,
	TrackedImage_GetYuv420_888YUVPtr_m0622CC0F943104D7052231D064EC0E4071C82021,
	TrackedImage__cctor_m83DC66AAEE0694B201752C7AE5F7573697106070,
	TrackerManager_GetInstance_m4370B1922E66F0889289DE97AD2A583766BF80CF,
	TrackerManager__ctor_m92FEAF7C4362ABCF09D6E0A903DEE33F107306D3,
	TrackerManager_InitializeCloud_mFC5508DF154B8C38C0C393A2AF4CC32001F41FE3,
	TrackerManager_SetCloudRecognitionSecretIdAndSecretKey_m9FED663DE1E5DD85805060FBC4DBC8B85972882E,
	TrackerManager_StartTracker_mD673FA4D553E5CC23BCCC6651FDA84691976D78A,
	TrackerManager_StopTracker_m8AFFA6DF22E4087D29DF9292833B027076FF5A2F,
	TrackerManager_DestroyTracker_m01E17C416B2C6D072492BFF9548C5200CA7BCBE0,
	TrackerManager_AddTrackerData_m2D6DFB2FD9766369E0239AA40BA720E14A5F572F,
	TrackerManager_RemoveTrackerData_m47F953F5F6A389B4275844EDE2F5A08407E7AF1E,
	TrackerManager_LoadTrackerData_m7D9DF2A711A8E2F4127495DE865E73480093061E,
	TrackerManager_SetTrackingOption_m033C0A6041FBB6C8922F7B2BB04BE6046F73B0CC,
	TrackerManager_IsTrackerDataLoadCompleted_m2DF8F22596F686AD7AAC118E3540877240B26714,
	TrackerManager_IsFusionSupported_mD168C27B508507E0B9E29D6E9F72C2AF06E08E9F,
	TrackerManager_RequestARCoreApk_mA4D8EE0FA42785952F16EFA5471895CCBB4D56B4,
	TrackerManager_GetFusionTrackingState_mF5B84C2E086E9D5ED76BD55FCA7ED770D804325A,
	TrackerManager_SetVocabulary_m00B62BF388C9179D1BA48371700E888BECB55653,
	TrackerManager_UpdateTrackingState_m81C68EA3F99AEBED9C39EA43B890E328A9DC30E0,
	TrackerManager_GetTrackingState_mBD05174B3AA55F3DAB45ECA4926CD644C896FC51,
	TrackerManager_GetWorldPositionFromScreenCoordinate_m1613E29BD114D0DA567C1554970270E26EA2FEBD,
	TrackerManager_FindSurface_m17263FB27620D8BA791E5F042FA20EE1E6004A00,
	TrackerManager_QuitFindingSurface_m9D5C57E58B025A72332E8DD72A7037EBD8AB21A6,
	TrackerManager_FindImageOfCloudRecognition_m8830506EE66B002666FBF62AD29864464912CC18,
	TrackerManager_GetGuideInfo_m06E47154464FF9B66701008F383C670562FD7D48,
	TrackerManager_SaveSurfaceData_m03C7860065C3D90FEEDD70635C410197C2B54693,
	TrackerManager__cctor_m8003E7294D3DC5F60860833E0833A5ED910976FF,
	TrackingResult__ctor_mD4299DA6C9FFB8F2AC472DD1A985818E821A1271,
	TrackingResult_GetCount_m3BA7A53048D257BA95637374486B78C353A74467,
	TrackingResult_GetTrackable_m2603532A1C9F802444000EA0B4E5F321DEBD560A,
	TrackingState__ctor_mC65B93BA50E1601F3C9B5A0F9FC5234D2DD5DD93,
	TrackingState_GetTrackingStateCPtr_m3324CFF0A90AD6CC15115B77F67D30122FA4000C,
	TrackingState_GetTrackingResult_mC57CB67BA5FB282CF3C089C4B8B34B8C71D21B00,
	TrackingState_GetCodeScanResult_m17FB2618B6A9DA32B303ACA451A0F81449D4850B,
	TrackingState_GetImage_m30337F216DD6DDE2A09D2629E91F6B0F64F404BC,
	TrackingState_GetImage_m33419ABA243952364A82E954EA9C9E99E459C916,
	TrackingState_GetTrackingStatus_mE8920D35D3ACEB3240A2E965F63EB01E70AA6D20,
	WearableCalibration__ctor_mBCFCF9C00ADD75726964A0E979344841D5BBB157,
	WearableCalibration_get_activeProfile_mAA579C71EB2F37E8F11F2220E9ECE186AB1A45D3,
	WearableCalibration_set_activeProfile_m9833F5729E0AD9A09C30337524CE5240C8C52337,
	WearableCalibration_IsActivated_m1C6680A0484B8605464DA9836F0FFE413B487852,
	WearableCalibration_Init_m770F0F6E86C5FEE8C68393B10FD5116B2D8AB1E6,
	WearableCalibration_Deinit_m8D0AF65A6857BAE8FDB1F7B14D82D52A39EF180C,
	WearableCalibration_GetViewport_mC83735DD3C180CF40F9864D0DA0907BF79206070,
	WearableCalibration_GetProjectionMatrix_mE286BE21D8BE85207C942307371BB6FEC1828BC8,
	WearableCalibration_CreateWearableEye_m15F3FD228A0378382B7198E02097715AA10EC3EA,
};
static const int32_t s_InvokerIndices[734] = 
{
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	3,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	3,
	23,
	26,
	23,
	23,
	23,
	1176,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	1176,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	3,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	4,
	27,
	23,
	3,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	129,
	31,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	26,
	3,
	23,
	26,
	23,
	282,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1490,
	23,
	23,
	23,
	1490,
	23,
	23,
	1490,
	23,
	23,
	23,
	664,
	282,
	102,
	31,
	23,
	10,
	32,
	664,
	282,
	916,
	1490,
	23,
	23,
	664,
	282,
	1490,
	23,
	23,
	14,
	26,
	664,
	282,
	1490,
	23,
	23,
	23,
	23,
	23,
	102,
	14,
	31,
	102,
	102,
	4,
	23,
	14,
	14,
	3,
	1491,
	0,
	2,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	4,
	23,
	23,
	23,
	23,
	10,
	14,
	32,
	23,
	23,
	3,
	4,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	38,
	26,
	26,
	23,
	102,
	23,
	3,
	23,
	26,
	23,
	4,
	23,
	3,
	31,
	26,
	58,
	23,
	23,
	23,
	664,
	664,
	23,
	26,
	844,
	23,
	23,
	557,
	23,
	10,
	32,
	102,
	31,
	10,
	32,
	102,
	31,
	1492,
	1492,
	31,
	149,
	23,
	1493,
	23,
	23,
	26,
	26,
	23,
	557,
	23,
	26,
	23,
	664,
	664,
	23,
	26,
	844,
	23,
	23,
	557,
	10,
	32,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	23,
	1490,
	23,
	4,
	23,
	10,
	1494,
	1495,
	1496,
	1497,
	30,
	176,
	176,
	579,
	30,
	423,
	664,
	14,
	413,
	425,
	1498,
	103,
	10,
	10,
	10,
	1030,
	14,
	102,
	102,
	9,
	23,
	23,
	3,
	378,
	156,
	101,
	23,
	23,
	23,
	26,
	23,
	26,
	27,
	10,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	837,
	27,
	23,
	23,
	922,
	26,
	922,
	23,
	23,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	664,
	282,
	23,
	4,
	27,
	23,
	23,
	3,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	664,
	282,
	14,
	26,
	14,
	26,
	23,
	157,
	10,
	10,
	14,
	23,
	664,
	10,
	10,
	14,
	14,
	23,
	3,
	0,
	0,
	0,
	23,
	23,
	156,
	26,
	419,
	23,
	32,
	32,
	23,
	23,
	23,
	1052,
	23,
	4,
	23,
	1499,
	23,
	124,
	37,
	6,
	6,
	37,
	758,
	3,
	1500,
	1500,
	1500,
	1110,
	1501,
	1502,
	1503,
	1503,
	1504,
	1505,
	1502,
	23,
	4,
	123,
	121,
	23,
	49,
	1,
	0,
	0,
	1506,
	1061,
	1,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	111,
	316,
	123,
	121,
	158,
	524,
	1507,
	1508,
	1509,
	1510,
	46,
	5,
	5,
	1511,
	46,
	207,
	1061,
	524,
	21,
	520,
	563,
	198,
	1512,
	99,
	524,
	524,
	111,
	206,
	111,
	3,
	3,
	121,
	3,
	3,
	556,
	111,
	3,
	121,
	49,
	49,
	524,
	556,
	106,
	3,
	3,
	106,
	107,
	122,
	1513,
	144,
	217,
	1514,
	1515,
	1515,
	1515,
	1383,
	1515,
	232,
	232,
	225,
	225,
	217,
	1516,
	232,
	217,
	217,
	1516,
	217,
	1516,
	217,
	217,
	217,
	217,
	1517,
	3,
	3,
	1518,
	3,
	316,
	21,
	17,
	17,
	21,
	1192,
	49,
	109,
	3,
	123,
	316,
	217,
	217,
	217,
	217,
	217,
	1516,
	225,
	1519,
	1520,
	1521,
	23,
	23,
	4,
	23,
	23,
	23,
	3,
	160,
	10,
	10,
	10,
	14,
	14,
	26,
	664,
	282,
	664,
	282,
	664,
	282,
	664,
	282,
	664,
	282,
	664,
	282,
	664,
	282,
	664,
	282,
	664,
	282,
	23,
	23,
	160,
	14,
	14,
	14,
	14,
	1030,
	1030,
	664,
	664,
	160,
	1522,
	10,
	10,
	10,
	10,
	10,
	130,
	14,
	15,
	512,
	659,
	1523,
	3,
	4,
	23,
	23,
	27,
	32,
	23,
	23,
	391,
	26,
	23,
	32,
	102,
	102,
	23,
	10,
	391,
	14,
	14,
	1524,
	23,
	23,
	23,
	14,
	28,
	3,
	160,
	10,
	34,
	160,
	130,
	14,
	14,
	14,
	313,
	10,
	23,
	14,
	26,
	102,
	1498,
	23,
	34,
	34,
	26,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x0200001E, { 10, 4 } },
	{ 0x0200005F, { 14, 4 } },
	{ 0x06000004, { 0, 1 } },
	{ 0x06000005, { 1, 2 } },
	{ 0x06000006, { 3, 2 } },
	{ 0x06000008, { 5, 1 } },
	{ 0x06000009, { 6, 2 } },
	{ 0x0600000A, { 8, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[18] = 
{
	{ (Il2CppRGCTXDataType)3, 6642 },
	{ (Il2CppRGCTXDataType)2, 8751 },
	{ (Il2CppRGCTXDataType)3, 6643 },
	{ (Il2CppRGCTXDataType)2, 8752 },
	{ (Il2CppRGCTXDataType)3, 6644 },
	{ (Il2CppRGCTXDataType)3, 6645 },
	{ (Il2CppRGCTXDataType)2, 8754 },
	{ (Il2CppRGCTXDataType)3, 6646 },
	{ (Il2CppRGCTXDataType)2, 8755 },
	{ (Il2CppRGCTXDataType)3, 6647 },
	{ (Il2CppRGCTXDataType)2, 8756 },
	{ (Il2CppRGCTXDataType)1, 8310 },
	{ (Il2CppRGCTXDataType)2, 8310 },
	{ (Il2CppRGCTXDataType)3, 6648 },
	{ (Il2CppRGCTXDataType)2, 8757 },
	{ (Il2CppRGCTXDataType)2, 8494 },
	{ (Il2CppRGCTXDataType)1, 8494 },
	{ (Il2CppRGCTXDataType)3, 6649 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	734,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	18,
	s_rgctxValues,
	NULL,
};
