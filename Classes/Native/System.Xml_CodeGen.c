﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String,System.Object[])
extern void SR_GetString_m846F567D256240B005CADDBE65D4793920B6474E (void);
// 0x00000002 System.String SR::GetString(System.Globalization.CultureInfo,System.String,System.Object[])
extern void SR_GetString_m6B94153BBBDF13B55321A1E0290B6E82FC8AD248 (void);
// 0x00000003 System.Void System.Xml.Base64Encoder::WriteChars(System.Char[],System.Int32,System.Int32)
// 0x00000004 System.Void System.Xml.Base64Encoder::Flush()
extern void Base64Encoder_Flush_m7C096C39FD6F6EB5AAEBB7C5ED816DD8F24CA5B0 (void);
// 0x00000005 System.Void System.Xml.XmlTextWriterBase64Encoder::WriteChars(System.Char[],System.Int32,System.Int32)
extern void XmlTextWriterBase64Encoder_WriteChars_m7BCA7FB4E1B8A1A320D13E122D15C504D8EA3EF9 (void);
// 0x00000006 System.Void System.Xml.SecureStringHasher::.ctor()
extern void SecureStringHasher__ctor_mEF97616B6D20B8D66A01B2B833C677BF356A1145 (void);
// 0x00000007 System.Boolean System.Xml.SecureStringHasher::Equals(System.String,System.String)
extern void SecureStringHasher_Equals_m4764CCB4F1A4AED78D021A8DF38A26B6C499691A (void);
// 0x00000008 System.Int32 System.Xml.SecureStringHasher::GetHashCode(System.String)
extern void SecureStringHasher_GetHashCode_m06AA20C7C8C7B52D42E039B7EBB1C9409752B54A (void);
// 0x00000009 System.Int32 System.Xml.SecureStringHasher::GetHashCodeOfString(System.String,System.Int32,System.Int64)
extern void SecureStringHasher_GetHashCodeOfString_m38C6DD4A290EE4A73135384834F8F3FA2D9CFB83 (void);
// 0x0000000A System.Xml.SecureStringHasher_HashCodeOfStringDelegate System.Xml.SecureStringHasher::GetHashCodeDelegate()
extern void SecureStringHasher_GetHashCodeDelegate_mB57EBF631C2136DB005BB3A2CA60A9D88222334B (void);
// 0x0000000B System.Void System.Xml.SecureStringHasher_HashCodeOfStringDelegate::.ctor(System.Object,System.IntPtr)
extern void HashCodeOfStringDelegate__ctor_m47EFBAB22CC6E0C04A17B8DA7101906596143BE8 (void);
// 0x0000000C System.Int32 System.Xml.SecureStringHasher_HashCodeOfStringDelegate::Invoke(System.String,System.Int32,System.Int64)
extern void HashCodeOfStringDelegate_Invoke_m29101897DFC3FF4CEC6E0F148E1E879463DA2F9D (void);
// 0x0000000D System.IAsyncResult System.Xml.SecureStringHasher_HashCodeOfStringDelegate::BeginInvoke(System.String,System.Int32,System.Int64,System.AsyncCallback,System.Object)
extern void HashCodeOfStringDelegate_BeginInvoke_m041CC1DB7FC71916966716A0BC20C28B1FD86CAD (void);
// 0x0000000E System.Int32 System.Xml.SecureStringHasher_HashCodeOfStringDelegate::EndInvoke(System.IAsyncResult)
extern void HashCodeOfStringDelegate_EndInvoke_m12CBE82586394D027E1A760D22A1138F339BEE7E (void);
// 0x0000000F System.Void System.Xml.XmlReader::.cctor()
extern void XmlReader__cctor_m8C939FA4F60E046BCAEDF0969A6E3B89D9CB0A4E (void);
// 0x00000010 System.Void System.Xml.XmlTextEncoder::.ctor(System.IO.TextWriter)
extern void XmlTextEncoder__ctor_m8142AA059B3675FE89F48542A7F013098FCDBCBC (void);
// 0x00000011 System.Void System.Xml.XmlTextEncoder::set_QuoteChar(System.Char)
extern void XmlTextEncoder_set_QuoteChar_m1897FBEC0EAB4616C3FD961A12AFC0F7ED771824 (void);
// 0x00000012 System.Void System.Xml.XmlTextEncoder::StartAttribute(System.Boolean)
extern void XmlTextEncoder_StartAttribute_m154B2203CF4424F0E8A6D1D80AC68B2E05FB7B32 (void);
// 0x00000013 System.Void System.Xml.XmlTextEncoder::EndAttribute()
extern void XmlTextEncoder_EndAttribute_mEBAE3ABA120AF2728E56ADDB2BE2A5B313093C12 (void);
// 0x00000014 System.String System.Xml.XmlTextEncoder::get_AttributeValue()
extern void XmlTextEncoder_get_AttributeValue_m9E57BB8D3D0C4BBA526DFAEC0064BD1C3F3BA38D (void);
// 0x00000015 System.Void System.Xml.XmlTextEncoder::WriteSurrogateChar(System.Char,System.Char)
extern void XmlTextEncoder_WriteSurrogateChar_m6D9CFC7F219F339DE22E679418EC68425D7EE20D (void);
// 0x00000016 System.Void System.Xml.XmlTextEncoder::Write(System.String)
extern void XmlTextEncoder_Write_m3D15126BEF6FE4BE6AA1FA9A25860A11649C8E3B (void);
// 0x00000017 System.Void System.Xml.XmlTextEncoder::WriteRaw(System.Char[],System.Int32,System.Int32)
extern void XmlTextEncoder_WriteRaw_m5B4E690DB11115A1A2EBF4228182024DD5BB8434 (void);
// 0x00000018 System.Void System.Xml.XmlTextEncoder::WriteStringFragment(System.String,System.Int32,System.Int32,System.Char[])
extern void XmlTextEncoder_WriteStringFragment_mE235366F26A1F6FAB51FDDE129A36E5A0A451229 (void);
// 0x00000019 System.Void System.Xml.XmlTextEncoder::WriteCharEntityImpl(System.Char)
extern void XmlTextEncoder_WriteCharEntityImpl_m26C18068AD20D6F0B9A5B5F3BB3D9E5A5F61BAD9 (void);
// 0x0000001A System.Void System.Xml.XmlTextEncoder::WriteCharEntityImpl(System.String)
extern void XmlTextEncoder_WriteCharEntityImpl_mCE63160635693B3E2C37693BE3401796D86ECAD9 (void);
// 0x0000001B System.Void System.Xml.XmlTextEncoder::WriteEntityRefImpl(System.String)
extern void XmlTextEncoder_WriteEntityRefImpl_m44E81A5273CF2E965652A47A9C5BBEC629E8539E (void);
// 0x0000001C System.Void System.Xml.XmlTextWriter::.ctor()
extern void XmlTextWriter__ctor_m99C6F457368131185E3DF47E3339A20D2C076357 (void);
// 0x0000001D System.Void System.Xml.XmlTextWriter::.ctor(System.IO.TextWriter)
extern void XmlTextWriter__ctor_m2561A4395C8A92B49271C2D6E6840CEDADC38D7B (void);
// 0x0000001E System.Void System.Xml.XmlTextWriter::WriteEndElement()
extern void XmlTextWriter_WriteEndElement_mE604C4B85C4996180C7EA5C45DFDC06F5FC173BD (void);
// 0x0000001F System.Xml.WriteState System.Xml.XmlTextWriter::get_WriteState()
extern void XmlTextWriter_get_WriteState_m5E47A02EFB1E27927A179424960D1FD5EA86D879 (void);
// 0x00000020 System.Void System.Xml.XmlTextWriter::Close()
extern void XmlTextWriter_Close_mCE05D92C3D65682E40B0A95F579EA2EF84F432B7 (void);
// 0x00000021 System.Void System.Xml.XmlTextWriter::AutoComplete(System.Xml.XmlTextWriter_Token)
extern void XmlTextWriter_AutoComplete_mC90A57781E031A2E18047CAF6BF09199323B48D2 (void);
// 0x00000022 System.Void System.Xml.XmlTextWriter::AutoCompleteAll()
extern void XmlTextWriter_AutoCompleteAll_m84FCFD43E3ECAE77234E1DA140AB15AE75F7E4DC (void);
// 0x00000023 System.Void System.Xml.XmlTextWriter::InternalWriteEndElement(System.Boolean)
extern void XmlTextWriter_InternalWriteEndElement_mF39D8CB533A512D50269E722957BA39D259B7D9C (void);
// 0x00000024 System.Void System.Xml.XmlTextWriter::WriteEndStartTag(System.Boolean)
extern void XmlTextWriter_WriteEndStartTag_mBA4796BAC09CD5B25F711E65D61BC2BE8375D2F0 (void);
// 0x00000025 System.Void System.Xml.XmlTextWriter::WriteEndAttributeQuote()
extern void XmlTextWriter_WriteEndAttributeQuote_m98AC4165AD8A683AE52D3683F2719E1C8CC578CD (void);
// 0x00000026 System.Void System.Xml.XmlTextWriter::Indent(System.Boolean)
extern void XmlTextWriter_Indent_m79DEE82659DC5B5F0C4E1E8E3E30C76E7F869851 (void);
// 0x00000027 System.Void System.Xml.XmlTextWriter::PushNamespace(System.String,System.String,System.Boolean)
extern void XmlTextWriter_PushNamespace_mFAE3B22D92E1EE3EB61C94917C3E9A955D9CD5C2 (void);
// 0x00000028 System.Void System.Xml.XmlTextWriter::AddNamespace(System.String,System.String,System.Boolean)
extern void XmlTextWriter_AddNamespace_mD3E28DCB8C014B090EB3FFA4C6F0A71702135E5B (void);
// 0x00000029 System.Void System.Xml.XmlTextWriter::AddToNamespaceHashtable(System.Int32)
extern void XmlTextWriter_AddToNamespaceHashtable_m35841EE9201BDF774311ED96E3D3DEDB174D24A6 (void);
// 0x0000002A System.Void System.Xml.XmlTextWriter::PopNamespaces(System.Int32,System.Int32)
extern void XmlTextWriter_PopNamespaces_mB647C786975F9BB3ADE2301184F8206D816EAFA6 (void);
// 0x0000002B System.Int32 System.Xml.XmlTextWriter::LookupNamespace(System.String)
extern void XmlTextWriter_LookupNamespace_mB4568061673EFBC795A43BB701ABE3335AFCE9A2 (void);
// 0x0000002C System.Void System.Xml.XmlTextWriter::HandleSpecialAttribute()
extern void XmlTextWriter_HandleSpecialAttribute_m743156B9137FC8E43B2E697605AAE9C225A2166E (void);
// 0x0000002D System.Void System.Xml.XmlTextWriter::VerifyPrefixXml(System.String,System.String)
extern void XmlTextWriter_VerifyPrefixXml_m7729FF5CEA9F697C2B498B672D02C4C895A287FB (void);
// 0x0000002E System.Void System.Xml.XmlTextWriter::FlushEncoders()
extern void XmlTextWriter_FlushEncoders_m6D7554550029EE5407C9149A740B03B138B44C1B (void);
// 0x0000002F System.Void System.Xml.XmlTextWriter::.cctor()
extern void XmlTextWriter__cctor_m0368A357057D3B0847BD1AC54EA1E4E609C9DEC7 (void);
// 0x00000030 System.Void System.Xml.XmlTextWriter_TagInfo::Init(System.Int32)
extern void TagInfo_Init_mFC65CB7C7A7D7852E3A2825365F1BD35CC38F8CF_AdjustorThunk (void);
// 0x00000031 System.Void System.Xml.XmlTextWriter_Namespace::Set(System.String,System.String,System.Boolean)
extern void Namespace_Set_m56A8063D5B2E5989807DF3472D0266CF828BBDA8_AdjustorThunk (void);
// 0x00000032 System.Void System.Xml.XmlWriter::WriteEndElement()
// 0x00000033 System.Xml.WriteState System.Xml.XmlWriter::get_WriteState()
// 0x00000034 System.Void System.Xml.XmlWriter::Close()
extern void XmlWriter_Close_m625EEF85A0D65E59BEEE4BA221C8E4DF531C8026 (void);
// 0x00000035 System.Void System.Xml.XmlWriter::Dispose()
extern void XmlWriter_Dispose_mD84FFF675D66861F00314F213AA637507DF74102 (void);
// 0x00000036 System.Void System.Xml.XmlWriter::Dispose(System.Boolean)
extern void XmlWriter_Dispose_m1C350DA8CB5925DBAE6073FFFD18ED5824EF61EA (void);
// 0x00000037 System.Void System.Xml.XmlWriter::.ctor()
extern void XmlWriter__ctor_mAA10A026AD230D064314D2AE472A144D18418A01 (void);
// 0x00000038 System.Void System.Xml.XmlChildEnumerator::.ctor(System.Xml.XmlNode)
extern void XmlChildEnumerator__ctor_m05CAB16D0FC521D715119DFE83D2B11C7BB2FD8D (void);
// 0x00000039 System.Boolean System.Xml.XmlChildEnumerator::System.Collections.IEnumerator.MoveNext()
extern void XmlChildEnumerator_System_Collections_IEnumerator_MoveNext_m671746486A0F64892351850041F6EAAABD95ABA7 (void);
// 0x0000003A System.Boolean System.Xml.XmlChildEnumerator::MoveNext()
extern void XmlChildEnumerator_MoveNext_mDA30FD57E7987F22769388EA4E0C84B6D65D455D (void);
// 0x0000003B System.Void System.Xml.XmlChildEnumerator::System.Collections.IEnumerator.Reset()
extern void XmlChildEnumerator_System_Collections_IEnumerator_Reset_m0F5589DECEFC71D36C32BC2C07F27704A7F37E8A (void);
// 0x0000003C System.Object System.Xml.XmlChildEnumerator::System.Collections.IEnumerator.get_Current()
extern void XmlChildEnumerator_System_Collections_IEnumerator_get_Current_m85F1ED174E2102798DC18B34AC277EC77E419863 (void);
// 0x0000003D System.Xml.XmlNode System.Xml.XmlChildEnumerator::get_Current()
extern void XmlChildEnumerator_get_Current_m9F82A181A0FEE37872C087AAF383C0AC440DB5F5 (void);
// 0x0000003E System.Void System.Xml.XmlDOMTextWriter::.ctor(System.IO.TextWriter)
extern void XmlDOMTextWriter__ctor_m7A30C41A24817419B3370A576D6B1254EBEC824E (void);
// 0x0000003F System.Xml.XmlNode System.Xml.XmlNode::get_NextSibling()
extern void XmlNode_get_NextSibling_m3A77EB7E743DF9B78C917565D969500A8DC6EDFB (void);
// 0x00000040 System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild()
extern void XmlNode_get_FirstChild_m608A11E859422F1131324FE6B6D7B4EFFB418C81 (void);
// 0x00000041 System.Xml.XmlLinkedNode System.Xml.XmlNode::get_LastNode()
extern void XmlNode_get_LastNode_m9CB959135B424720727D9A0CE6EE129DB7228E93 (void);
// 0x00000042 System.Collections.IEnumerator System.Xml.XmlNode::System.Collections.IEnumerable.GetEnumerator()
extern void XmlNode_System_Collections_IEnumerable_GetEnumerator_mC7D6329FEF45EE989276469F9E13D5E60B90C9C1 (void);
// 0x00000043 System.String System.Xml.XmlNode::get_OuterXml()
extern void XmlNode_get_OuterXml_m69AA8CA2C21CF72A80768DE8531B05A08ADCA52B (void);
// 0x00000044 System.Void System.Xml.XmlNode::WriteTo(System.Xml.XmlWriter)
// 0x00000045 System.Object System.Xml.XmlCharType::get_StaticLock()
extern void XmlCharType_get_StaticLock_m81A2863AB242D63E36753E695E04EFD800E30F62 (void);
// 0x00000046 System.Void System.Xml.XmlCharType::InitInstance()
extern void XmlCharType_InitInstance_m59CB9FA98F252136EEB7E364C62849CCC9B7E7AE (void);
// 0x00000047 System.Void System.Xml.XmlCharType::SetProperties(System.String,System.Byte)
extern void XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE (void);
// 0x00000048 System.Void System.Xml.XmlCharType::.ctor(System.Byte[])
extern void XmlCharType__ctor_mF597588002D63E65BC835ABC21221BB1525A6637_AdjustorThunk (void);
// 0x00000049 System.Xml.XmlCharType System.Xml.XmlCharType::get_Instance()
extern void XmlCharType_get_Instance_mA3CFC9BC3797565FD176224C6116F41AC8BA65B5 (void);
// 0x0000004A System.Boolean System.Xml.XmlCharType::IsHighSurrogate(System.Int32)
extern void XmlCharType_IsHighSurrogate_m05FA9C7BBB540AD6890CCDCC286E6699F7351301 (void);
// 0x0000004B System.Boolean System.Xml.XmlCharType::IsLowSurrogate(System.Int32)
extern void XmlCharType_IsLowSurrogate_m7939EE6AE78DBE960B04D2A8E957E913C2EFC6DC (void);
// 0x0000004C System.Boolean System.Xml.XmlCharType::InRange(System.Int32,System.Int32,System.Int32)
extern void XmlCharType_InRange_m53A94066F0586D01CDFA1FCF40BE0352A80826D7 (void);
// 0x0000004D System.String System.Xml.XmlConvert::TrimString(System.String)
extern void XmlConvert_TrimString_mF0E4AC16BD05053538B20B21DBD64447195A2D1B (void);
// 0x0000004E System.Exception System.Xml.XmlConvert::CreateException(System.String,System.String,System.Xml.ExceptionType,System.Int32,System.Int32)
extern void XmlConvert_CreateException_m9718225A7133DC0205AE6EC7BC322F4FEAE5A716 (void);
// 0x0000004F System.Exception System.Xml.XmlConvert::CreateException(System.String,System.String[],System.Xml.ExceptionType,System.Int32,System.Int32)
extern void XmlConvert_CreateException_mD3F12F95EFAC639FBCB9EB56C4008F5F422380D9 (void);
// 0x00000050 System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char)
extern void XmlConvert_CreateInvalidSurrogatePairException_m0DE4CC9F8620ABC31C61F6FDAFA3BC960F41F170 (void);
// 0x00000051 System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char,System.Xml.ExceptionType)
extern void XmlConvert_CreateInvalidSurrogatePairException_m97FE3D009250AE26015577D66CE3A652D787B86B (void);
// 0x00000052 System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char,System.Xml.ExceptionType,System.Int32,System.Int32)
extern void XmlConvert_CreateInvalidSurrogatePairException_mAD8DA247F0F5CAFAE622A2FC49AA8AD44746125A (void);
// 0x00000053 System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char)
extern void XmlConvert_CreateInvalidHighSurrogateCharException_mD8CAB48E86A39FACE4A0274D91C7065A6458233E (void);
// 0x00000054 System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char,System.Xml.ExceptionType)
extern void XmlConvert_CreateInvalidHighSurrogateCharException_m7963DACF73B00A03914532EC609524F30AC26F16 (void);
// 0x00000055 System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char,System.Xml.ExceptionType,System.Int32,System.Int32)
extern void XmlConvert_CreateInvalidHighSurrogateCharException_m430040D295EFE67CF3214A7375E5B2E080EF04AB (void);
// 0x00000056 System.Void System.Xml.XmlConvert::.cctor()
extern void XmlConvert__cctor_m702BEEB571815B9886A2C46DD223B7C96058857E (void);
// 0x00000057 System.Void System.Xml.XmlException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void XmlException__ctor_m6AA71014CB31FB0C9DF0EDE841514F97B7E11B93 (void);
// 0x00000058 System.Void System.Xml.XmlException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void XmlException_GetObjectData_m792FDA84BA189F6AB82F9AD4B0DF800DF9051DF4 (void);
// 0x00000059 System.Void System.Xml.XmlException::.ctor()
extern void XmlException__ctor_mCADC349BBEB808A35829D83028A25685CF19B902 (void);
// 0x0000005A System.Void System.Xml.XmlException::.ctor(System.String)
extern void XmlException__ctor_m4E096C7446FAB4E4B00FB2D530ED083A91E5C756 (void);
// 0x0000005B System.Void System.Xml.XmlException::.ctor(System.String,System.Exception,System.Int32,System.Int32)
extern void XmlException__ctor_m828060C266C3003539491FDB7F9BCEA07A24B352 (void);
// 0x0000005C System.Void System.Xml.XmlException::.ctor(System.String,System.Exception,System.Int32,System.Int32,System.String)
extern void XmlException__ctor_m69AC5BDB322F2F6A79D671CB2E262FD0C0482D7B (void);
// 0x0000005D System.Void System.Xml.XmlException::.ctor(System.String,System.String,System.Int32,System.Int32)
extern void XmlException__ctor_m819800529C3A91E8C5640AAA3F30EBD998238F10 (void);
// 0x0000005E System.Void System.Xml.XmlException::.ctor(System.String,System.String[],System.Int32,System.Int32)
extern void XmlException__ctor_m546964BF1BBE64E4A2C9853E23F25FC3295D595E (void);
// 0x0000005F System.Void System.Xml.XmlException::.ctor(System.String,System.String[],System.Exception,System.Int32,System.Int32,System.String)
extern void XmlException__ctor_mE47A3D5DED55E0DB0683E5AEB4591BCFBC03731C (void);
// 0x00000060 System.String System.Xml.XmlException::FormatUserMessage(System.String,System.Int32,System.Int32)
extern void XmlException_FormatUserMessage_m43C6731234AE7C72D5C1C88905B6E4A21C2C4CA5 (void);
// 0x00000061 System.String System.Xml.XmlException::CreateMessage(System.String,System.String[],System.Int32,System.Int32)
extern void XmlException_CreateMessage_m222F162AE354DEA3AFD7969544733DD123BA90DB (void);
// 0x00000062 System.String System.Xml.XmlException::get_Message()
extern void XmlException_get_Message_m811FB67949E8227BB095C61A936900F988E4CB6C (void);
// 0x00000063 System.String System.Xml.Res::GetString(System.String)
extern void Res_GetString_m4303E63CA6945331BF5584376F6AA7262D1C6887 (void);
// 0x00000064 System.String System.Xml.Res::GetString(System.String,System.Object[])
extern void Res_GetString_m1D06B81901B03CA849045926741403907612BB4B (void);
static Il2CppMethodPointer s_methodPointers[100] = 
{
	SR_GetString_m846F567D256240B005CADDBE65D4793920B6474E,
	SR_GetString_m6B94153BBBDF13B55321A1E0290B6E82FC8AD248,
	NULL,
	Base64Encoder_Flush_m7C096C39FD6F6EB5AAEBB7C5ED816DD8F24CA5B0,
	XmlTextWriterBase64Encoder_WriteChars_m7BCA7FB4E1B8A1A320D13E122D15C504D8EA3EF9,
	SecureStringHasher__ctor_mEF97616B6D20B8D66A01B2B833C677BF356A1145,
	SecureStringHasher_Equals_m4764CCB4F1A4AED78D021A8DF38A26B6C499691A,
	SecureStringHasher_GetHashCode_m06AA20C7C8C7B52D42E039B7EBB1C9409752B54A,
	SecureStringHasher_GetHashCodeOfString_m38C6DD4A290EE4A73135384834F8F3FA2D9CFB83,
	SecureStringHasher_GetHashCodeDelegate_mB57EBF631C2136DB005BB3A2CA60A9D88222334B,
	HashCodeOfStringDelegate__ctor_m47EFBAB22CC6E0C04A17B8DA7101906596143BE8,
	HashCodeOfStringDelegate_Invoke_m29101897DFC3FF4CEC6E0F148E1E879463DA2F9D,
	HashCodeOfStringDelegate_BeginInvoke_m041CC1DB7FC71916966716A0BC20C28B1FD86CAD,
	HashCodeOfStringDelegate_EndInvoke_m12CBE82586394D027E1A760D22A1138F339BEE7E,
	XmlReader__cctor_m8C939FA4F60E046BCAEDF0969A6E3B89D9CB0A4E,
	XmlTextEncoder__ctor_m8142AA059B3675FE89F48542A7F013098FCDBCBC,
	XmlTextEncoder_set_QuoteChar_m1897FBEC0EAB4616C3FD961A12AFC0F7ED771824,
	XmlTextEncoder_StartAttribute_m154B2203CF4424F0E8A6D1D80AC68B2E05FB7B32,
	XmlTextEncoder_EndAttribute_mEBAE3ABA120AF2728E56ADDB2BE2A5B313093C12,
	XmlTextEncoder_get_AttributeValue_m9E57BB8D3D0C4BBA526DFAEC0064BD1C3F3BA38D,
	XmlTextEncoder_WriteSurrogateChar_m6D9CFC7F219F339DE22E679418EC68425D7EE20D,
	XmlTextEncoder_Write_m3D15126BEF6FE4BE6AA1FA9A25860A11649C8E3B,
	XmlTextEncoder_WriteRaw_m5B4E690DB11115A1A2EBF4228182024DD5BB8434,
	XmlTextEncoder_WriteStringFragment_mE235366F26A1F6FAB51FDDE129A36E5A0A451229,
	XmlTextEncoder_WriteCharEntityImpl_m26C18068AD20D6F0B9A5B5F3BB3D9E5A5F61BAD9,
	XmlTextEncoder_WriteCharEntityImpl_mCE63160635693B3E2C37693BE3401796D86ECAD9,
	XmlTextEncoder_WriteEntityRefImpl_m44E81A5273CF2E965652A47A9C5BBEC629E8539E,
	XmlTextWriter__ctor_m99C6F457368131185E3DF47E3339A20D2C076357,
	XmlTextWriter__ctor_m2561A4395C8A92B49271C2D6E6840CEDADC38D7B,
	XmlTextWriter_WriteEndElement_mE604C4B85C4996180C7EA5C45DFDC06F5FC173BD,
	XmlTextWriter_get_WriteState_m5E47A02EFB1E27927A179424960D1FD5EA86D879,
	XmlTextWriter_Close_mCE05D92C3D65682E40B0A95F579EA2EF84F432B7,
	XmlTextWriter_AutoComplete_mC90A57781E031A2E18047CAF6BF09199323B48D2,
	XmlTextWriter_AutoCompleteAll_m84FCFD43E3ECAE77234E1DA140AB15AE75F7E4DC,
	XmlTextWriter_InternalWriteEndElement_mF39D8CB533A512D50269E722957BA39D259B7D9C,
	XmlTextWriter_WriteEndStartTag_mBA4796BAC09CD5B25F711E65D61BC2BE8375D2F0,
	XmlTextWriter_WriteEndAttributeQuote_m98AC4165AD8A683AE52D3683F2719E1C8CC578CD,
	XmlTextWriter_Indent_m79DEE82659DC5B5F0C4E1E8E3E30C76E7F869851,
	XmlTextWriter_PushNamespace_mFAE3B22D92E1EE3EB61C94917C3E9A955D9CD5C2,
	XmlTextWriter_AddNamespace_mD3E28DCB8C014B090EB3FFA4C6F0A71702135E5B,
	XmlTextWriter_AddToNamespaceHashtable_m35841EE9201BDF774311ED96E3D3DEDB174D24A6,
	XmlTextWriter_PopNamespaces_mB647C786975F9BB3ADE2301184F8206D816EAFA6,
	XmlTextWriter_LookupNamespace_mB4568061673EFBC795A43BB701ABE3335AFCE9A2,
	XmlTextWriter_HandleSpecialAttribute_m743156B9137FC8E43B2E697605AAE9C225A2166E,
	XmlTextWriter_VerifyPrefixXml_m7729FF5CEA9F697C2B498B672D02C4C895A287FB,
	XmlTextWriter_FlushEncoders_m6D7554550029EE5407C9149A740B03B138B44C1B,
	XmlTextWriter__cctor_m0368A357057D3B0847BD1AC54EA1E4E609C9DEC7,
	TagInfo_Init_mFC65CB7C7A7D7852E3A2825365F1BD35CC38F8CF_AdjustorThunk,
	Namespace_Set_m56A8063D5B2E5989807DF3472D0266CF828BBDA8_AdjustorThunk,
	NULL,
	NULL,
	XmlWriter_Close_m625EEF85A0D65E59BEEE4BA221C8E4DF531C8026,
	XmlWriter_Dispose_mD84FFF675D66861F00314F213AA637507DF74102,
	XmlWriter_Dispose_m1C350DA8CB5925DBAE6073FFFD18ED5824EF61EA,
	XmlWriter__ctor_mAA10A026AD230D064314D2AE472A144D18418A01,
	XmlChildEnumerator__ctor_m05CAB16D0FC521D715119DFE83D2B11C7BB2FD8D,
	XmlChildEnumerator_System_Collections_IEnumerator_MoveNext_m671746486A0F64892351850041F6EAAABD95ABA7,
	XmlChildEnumerator_MoveNext_mDA30FD57E7987F22769388EA4E0C84B6D65D455D,
	XmlChildEnumerator_System_Collections_IEnumerator_Reset_m0F5589DECEFC71D36C32BC2C07F27704A7F37E8A,
	XmlChildEnumerator_System_Collections_IEnumerator_get_Current_m85F1ED174E2102798DC18B34AC277EC77E419863,
	XmlChildEnumerator_get_Current_m9F82A181A0FEE37872C087AAF383C0AC440DB5F5,
	XmlDOMTextWriter__ctor_m7A30C41A24817419B3370A576D6B1254EBEC824E,
	XmlNode_get_NextSibling_m3A77EB7E743DF9B78C917565D969500A8DC6EDFB,
	XmlNode_get_FirstChild_m608A11E859422F1131324FE6B6D7B4EFFB418C81,
	XmlNode_get_LastNode_m9CB959135B424720727D9A0CE6EE129DB7228E93,
	XmlNode_System_Collections_IEnumerable_GetEnumerator_mC7D6329FEF45EE989276469F9E13D5E60B90C9C1,
	XmlNode_get_OuterXml_m69AA8CA2C21CF72A80768DE8531B05A08ADCA52B,
	NULL,
	XmlCharType_get_StaticLock_m81A2863AB242D63E36753E695E04EFD800E30F62,
	XmlCharType_InitInstance_m59CB9FA98F252136EEB7E364C62849CCC9B7E7AE,
	XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE,
	XmlCharType__ctor_mF597588002D63E65BC835ABC21221BB1525A6637_AdjustorThunk,
	XmlCharType_get_Instance_mA3CFC9BC3797565FD176224C6116F41AC8BA65B5,
	XmlCharType_IsHighSurrogate_m05FA9C7BBB540AD6890CCDCC286E6699F7351301,
	XmlCharType_IsLowSurrogate_m7939EE6AE78DBE960B04D2A8E957E913C2EFC6DC,
	XmlCharType_InRange_m53A94066F0586D01CDFA1FCF40BE0352A80826D7,
	XmlConvert_TrimString_mF0E4AC16BD05053538B20B21DBD64447195A2D1B,
	XmlConvert_CreateException_m9718225A7133DC0205AE6EC7BC322F4FEAE5A716,
	XmlConvert_CreateException_mD3F12F95EFAC639FBCB9EB56C4008F5F422380D9,
	XmlConvert_CreateInvalidSurrogatePairException_m0DE4CC9F8620ABC31C61F6FDAFA3BC960F41F170,
	XmlConvert_CreateInvalidSurrogatePairException_m97FE3D009250AE26015577D66CE3A652D787B86B,
	XmlConvert_CreateInvalidSurrogatePairException_mAD8DA247F0F5CAFAE622A2FC49AA8AD44746125A,
	XmlConvert_CreateInvalidHighSurrogateCharException_mD8CAB48E86A39FACE4A0274D91C7065A6458233E,
	XmlConvert_CreateInvalidHighSurrogateCharException_m7963DACF73B00A03914532EC609524F30AC26F16,
	XmlConvert_CreateInvalidHighSurrogateCharException_m430040D295EFE67CF3214A7375E5B2E080EF04AB,
	XmlConvert__cctor_m702BEEB571815B9886A2C46DD223B7C96058857E,
	XmlException__ctor_m6AA71014CB31FB0C9DF0EDE841514F97B7E11B93,
	XmlException_GetObjectData_m792FDA84BA189F6AB82F9AD4B0DF800DF9051DF4,
	XmlException__ctor_mCADC349BBEB808A35829D83028A25685CF19B902,
	XmlException__ctor_m4E096C7446FAB4E4B00FB2D530ED083A91E5C756,
	XmlException__ctor_m828060C266C3003539491FDB7F9BCEA07A24B352,
	XmlException__ctor_m69AC5BDB322F2F6A79D671CB2E262FD0C0482D7B,
	XmlException__ctor_m819800529C3A91E8C5640AAA3F30EBD998238F10,
	XmlException__ctor_m546964BF1BBE64E4A2C9853E23F25FC3295D595E,
	XmlException__ctor_mE47A3D5DED55E0DB0683E5AEB4591BCFBC03731C,
	XmlException_FormatUserMessage_m43C6731234AE7C72D5C1C88905B6E4A21C2C4CA5,
	XmlException_CreateMessage_m222F162AE354DEA3AFD7969544733DD123BA90DB,
	XmlException_get_Message_m811FB67949E8227BB095C61A936900F988E4CB6C,
	Res_GetString_m4303E63CA6945331BF5584376F6AA7262D1C6887,
	Res_GetString_m1D06B81901B03CA849045926741403907612BB4B,
};
static const int32_t s_InvokerIndices[100] = 
{
	1,
	2,
	35,
	23,
	35,
	23,
	103,
	104,
	925,
	4,
	163,
	926,
	927,
	104,
	3,
	26,
	548,
	31,
	23,
	14,
	928,
	26,
	35,
	929,
	548,
	26,
	26,
	23,
	26,
	23,
	10,
	23,
	32,
	23,
	31,
	31,
	23,
	31,
	98,
	98,
	32,
	157,
	104,
	23,
	27,
	23,
	3,
	32,
	98,
	23,
	10,
	23,
	23,
	31,
	23,
	26,
	102,
	102,
	23,
	14,
	14,
	26,
	14,
	14,
	14,
	14,
	14,
	26,
	4,
	3,
	556,
	26,
	930,
	46,
	46,
	619,
	0,
	931,
	931,
	932,
	933,
	934,
	169,
	935,
	936,
	3,
	159,
	159,
	23,
	26,
	36,
	937,
	36,
	36,
	938,
	151,
	424,
	14,
	0,
	1,
};
extern const Il2CppCodeGenModule g_System_XmlCodeGenModule;
const Il2CppCodeGenModule g_System_XmlCodeGenModule = 
{
	"System.Xml.dll",
	100,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
