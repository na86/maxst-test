﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct GenericVirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericVirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct InterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct GenericInterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericInterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};

// System.Xml.Base64Encoder
struct Base64Encoder_tF36936300CC9B84076C8F16EE4AF8074ACA40CE9;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Globalization.Calendar
struct Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E;
// System.Globalization.CompareInfo
struct CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9;
// System.Globalization.CultureData
struct CultureData_t53CDF1C5F789A28897415891667799420D3C5529;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90;
// System.Text.DecoderFallback
struct DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.Text.EncoderFallback
struct EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4;
// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827;
// System.Collections.Hashtable
struct Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t2A667D8777429024D8A3CB3D9AE29EA79FEA6176;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.Xml.SecureStringHasher
struct SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A;
// System.Globalization.TextInfo
struct TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C;
// System.Text.UnicodeEncoding
struct UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// System.Xml.XmlChildEnumerator
struct XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559;
// System.Xml.XmlDOMTextWriter
struct XmlDOMTextWriter_tF813A0468BACD6B3CC21849A38FA6AB269D5A741;
// System.Xml.XmlException
struct XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0;
// System.Xml.XmlNode
struct XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1;
// System.Xml.XmlTextEncoder
struct XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E;
// System.Xml.XmlTextWriter
struct XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2;
// System.Xml.XmlTextWriterBase64Encoder
struct XmlTextWriterBase64Encoder_t6C566B592DFF5C0E5108670EB19BA06D35F0213B;
// System.Xml.XmlWriter
struct XmlWriter_t676293C138D2D0DAB9C1BC16A7BEE618391C5B2D;
// System.Xml.SecureStringHasher/HashCodeOfStringDelegate
struct HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574;
// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1DDD2F48B87E022F599DF2452A49BB70BE95A7F8;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1A386BEF1855064FD5CC71F340A68881A52B4932;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tE6A65C5E45E33FD7D9849FD0914DE3AD32B68050;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Int32>
struct KeyCollection_t61F8738ED346768CC112B2E27863BF9F73C76D90;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Int32>
struct ValueCollection_t17EEB7B2EDD3CB5222C660D7E739F803986BF025;
// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00;
// System.ArgumentNullException
struct ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98;
// System.Delegate
struct Delegate_t;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Int32>[]
struct EntryU5BU5D_tABFC31237D6642B5D4C1DBA234CA37EE851EB0AE;
// System.Exception
struct Exception_t;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// System.IFormatProvider
struct IFormatProvider_tF2AECC4B14F41D36718920D67F930CED940412DF;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.InvalidOperationException
struct InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Xml.XmlTextWriter/Namespace[]
struct NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1;
// System.Runtime.Serialization.SerializationInfoEnumerator
struct SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.Xml.XmlTextWriter/State[]
struct StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.IO.StringWriter
struct StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62;
// System.Xml.XmlTextWriter/TagInfo[]
struct TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637;
// System.IO.TextWriter
struct TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;

IL2CPP_EXTERN_C RuntimeClass* ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MissingManifestResourceException_tAC74F21ADC46CCB2BCC710464434E3B97F72FACB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlDOMTextWriter_tF813A0468BACD6B3CC21849A38FA6AB269D5A741_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E____5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E____6A0D50D692745A6663128CD315B71079584F3E59_1_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E____B368804F0C6DAB083B253A6B106D0783D5C32E90_2_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E____EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3_FieldInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral02E5C3B8AD72F3FC46F6C829AB1FEDDC9B115473;
IL2CPP_EXTERN_C String_t* _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626;
IL2CPP_EXTERN_C String_t* _stringLiteral0BA9045FCB28C8C8B2ACED641BB5013BAC05D492;
IL2CPP_EXTERN_C String_t* _stringLiteral0E0426234071F961E725D9438C6D5A048A0394B3;
IL2CPP_EXTERN_C String_t* _stringLiteral0FD08B3C99047786420F2D9A7AC37D5DC351C9C8;
IL2CPP_EXTERN_C String_t* _stringLiteral120472D8D40924F6F8355A94DB677A8F142E2EB6;
IL2CPP_EXTERN_C String_t* _stringLiteral16C0D1A98D99C5FB32B981C3E41FDB407A083C18;
IL2CPP_EXTERN_C String_t* _stringLiteral1710B4477A01FFE20514D8AA61891F364D71C25B;
IL2CPP_EXTERN_C String_t* _stringLiteral197DB4D12C3F8F844439AC8086D04A7E9906FD24;
IL2CPP_EXTERN_C String_t* _stringLiteral1E1DB32AFA986C58B9ACC9C146BF83E67C0DCE66;
IL2CPP_EXTERN_C String_t* _stringLiteral2309CFA4F156DCCB12A14E727DCE560E1426B532;
IL2CPP_EXTERN_C String_t* _stringLiteral23114468D04FA2B7A2DA455B545DB914D0A3ED94;
IL2CPP_EXTERN_C String_t* _stringLiteral2343901946DDD83EFBB8B93C33F01CBD394063B2;
IL2CPP_EXTERN_C String_t* _stringLiteral23A6BF321FC205420788A7F2E1A1334F6C712C89;
IL2CPP_EXTERN_C String_t* _stringLiteral23E65142459FB1BF5009AD8736318547A5DFE850;
IL2CPP_EXTERN_C String_t* _stringLiteral32189949CB1CA4A6EBB1A643EBE2DB69713D5407;
IL2CPP_EXTERN_C String_t* _stringLiteral349C6DC0F34B9BA242E4C728EDD28CAB809D4917;
IL2CPP_EXTERN_C String_t* _stringLiteral35B44AA64754DEDC52915E7F763C081CF5B004D9;
IL2CPP_EXTERN_C String_t* _stringLiteral3D9E33C04AB4A0A0E8E88B74BB9D5F9003E15C78;
IL2CPP_EXTERN_C String_t* _stringLiteral3EBF7CFEC7929F196835D5D12FBBE2F845BF2A5F;
IL2CPP_EXTERN_C String_t* _stringLiteral41477B187466178A05A136C12F806B3EDCAB6349;
IL2CPP_EXTERN_C String_t* _stringLiteral420B74A52534550B0DD14DCF7D8988C2BD4936CE;
IL2CPP_EXTERN_C String_t* _stringLiteral45B9464D208C0A900770E5F7A95E5FE067CD4CCC;
IL2CPP_EXTERN_C String_t* _stringLiteral47EB169AEE74B181812673783C66416B39F5F464;
IL2CPP_EXTERN_C String_t* _stringLiteral4A962F7AAEC3B50EF4B2CD52A7A2C969B759A960;
IL2CPP_EXTERN_C String_t* _stringLiteral4B4906CA939816C30A7D9C9A911CF6A9F506CCE3;
IL2CPP_EXTERN_C String_t* _stringLiteral513659CEF285C73478E9829E41D7E4C23DB53E12;
IL2CPP_EXTERN_C String_t* _stringLiteral5296173AB8D4E282E989F7B0B7B7C9A3ACF26E18;
IL2CPP_EXTERN_C String_t* _stringLiteral544DC80A2A82A08B6321F56F8987CB7E5DEED1C4;
IL2CPP_EXTERN_C String_t* _stringLiteral56EF1F2751B48DAA3EC7F762F420920B8E40757A;
IL2CPP_EXTERN_C String_t* _stringLiteral5C7DA3CBD254CB3BB9FCC98E58CF1A56E605861E;
IL2CPP_EXTERN_C String_t* _stringLiteral63D27168A6FFA4A02D5FDBA464A248377E88F4F4;
IL2CPP_EXTERN_C String_t* _stringLiteral67171AD8B6817F7CF25B89C98234BCDA36D476FC;
IL2CPP_EXTERN_C String_t* _stringLiteral6F5EC7239B41C242FCB23B64D91DA0070FC1C044;
IL2CPP_EXTERN_C String_t* _stringLiteral7BBD8C0287448D34297B233D6604618390BA9C38;
IL2CPP_EXTERN_C String_t* _stringLiteral81ABCB006928101D1882FA2FBB7BFA00FE053221;
IL2CPP_EXTERN_C String_t* _stringLiteral8243A16D425F93AF62CAAB2BFAE01A2D6246A5FE;
IL2CPP_EXTERN_C String_t* _stringLiteral872479F10B4968F255B608A3698CA1A7EE09888B;
IL2CPP_EXTERN_C String_t* _stringLiteral893D84FF4ED81AC205FBC0C67CBEE1C0C752B406;
IL2CPP_EXTERN_C String_t* _stringLiteral916F4E7879C25AF1EA844F7068842D5508777C48;
IL2CPP_EXTERN_C String_t* _stringLiteral94A9AE3A92C04F7400ADC94B7BCB73FFC0A08DE4;
IL2CPP_EXTERN_C String_t* _stringLiteral952F621460E9250177F13FF0BEAD9E00E2B86302;
IL2CPP_EXTERN_C String_t* _stringLiteral96930EC8C6FD5250BB36A5E1040AB06A9588FD62;
IL2CPP_EXTERN_C String_t* _stringLiteral9932973D4B6AA1AA193C06D8D34B58B677685003;
IL2CPP_EXTERN_C String_t* _stringLiteral9F0051E3370AA31B69F5CBF0E35FBE94ACAE0651;
IL2CPP_EXTERN_C String_t* _stringLiteralB3F14BF976EFD974E34846B742502C802FABAE9D;
IL2CPP_EXTERN_C String_t* _stringLiteralB63A1A23B65582B8791AA4E655E90AA5647D7298;
IL2CPP_EXTERN_C String_t* _stringLiteralB6F0795DD4F409C92875D0327F58FDEA357047F1;
IL2CPP_EXTERN_C String_t* _stringLiteralB76F93064E827FE764713C55F705D2E388DF17D7;
IL2CPP_EXTERN_C String_t* _stringLiteralB829404B947F7E1629A30B5E953A49EB21CCD2ED;
IL2CPP_EXTERN_C String_t* _stringLiteralBBC1106D65ACC9B0DB541F4FAF5DBAF5B7F7BA1F;
IL2CPP_EXTERN_C String_t* _stringLiteralC3FBA6155AE85E3CB81F8437A6F1C503BB7D09B7;
IL2CPP_EXTERN_C String_t* _stringLiteralCC5753B4554091FA687FA64F4FA303B0C913E2EC;
IL2CPP_EXTERN_C String_t* _stringLiteralCD347E1307036F9337DFB643A0DA73051573F178;
IL2CPP_EXTERN_C String_t* _stringLiteralCE63895ACF2B7A447477491E6E010B297DD75B0D;
IL2CPP_EXTERN_C String_t* _stringLiteralCEB6890FC169A5D98961042EBCAD0677F2F0656F;
IL2CPP_EXTERN_C String_t* _stringLiteralD2B699E6F0658A4364F9D1A0F5A4EB7C94F6F656;
IL2CPP_EXTERN_C String_t* _stringLiteralD2D2F8D3F9F04A081FFBE6B2AF7917BAAADFC052;
IL2CPP_EXTERN_C String_t* _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE;
IL2CPP_EXTERN_C String_t* _stringLiteralD6DCC897C02A857315752249765CB47ADDF4E5C7;
IL2CPP_EXTERN_C String_t* _stringLiteralD9319D8D06606B8F2B10B57EBAABAE57F7D5B8E7;
IL2CPP_EXTERN_C String_t* _stringLiteralE1356901E9F96E6ACC91632889AE94286AFE4CD4;
IL2CPP_EXTERN_C String_t* _stringLiteralE1C34BAD815D8104249A053C9455FA539CDF5036;
IL2CPP_EXTERN_C String_t* _stringLiteralEDC12722FE0763003109C7EDBACB6977C0E31132;
IL2CPP_EXTERN_C String_t* _stringLiteralF41760006700B346FE970834ED6436CD21A1330F;
IL2CPP_EXTERN_C String_t* _stringLiteralF4ED3BDF7EB24796E98DB0F3022D762D2C5EBFB0;
IL2CPP_EXTERN_C String_t* _stringLiteralF91B9F4F51199474999FD40AF7DB23C33195BC0B;
IL2CPP_EXTERN_C String_t* _stringLiteralFBD1A9745DCC4D80697D44756437CCB57698AE21;
IL2CPP_EXTERN_C String_t* _stringLiteralFDA1C9397356AF77AAE64869A9D6288562F10D4C;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Remove_m105F8AD7F5E827D6F95768BD6C922EB155E5A7EF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m8E06A6DF244254BF419D7EC23A9A6809E19A544D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mAC2C8559F9E6A16FAD5BA5017E6A0A1EF3429867_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_mBC85AF861FB031847847F0B30707EC7AC252D572_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SecureStringHasher_GetHashCodeOfString_m38C6DD4A290EE4A73135384834F8F3FA2D9CFB83_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlChildEnumerator_get_Current_m9F82A181A0FEE37872C087AAF383C0AC440DB5F5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextEncoder_WriteRaw_m5B4E690DB11115A1A2EBF4228182024DD5BB8434_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextEncoder_WriteSurrogateChar_m6D9CFC7F219F339DE22E679418EC68425D7EE20D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextEncoder_Write_m3D15126BEF6FE4BE6AA1FA9A25860A11649C8E3B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextWriter_AutoComplete_mC90A57781E031A2E18047CAF6BF09199323B48D2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextWriter_HandleSpecialAttribute_m743156B9137FC8E43B2E697605AAE9C225A2166E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextWriter_InternalWriteEndElement_mF39D8CB533A512D50269E722957BA39D259B7D9C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextWriter_PushNamespace_mFAE3B22D92E1EE3EB61C94917C3E9A955D9CD5C2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextWriter_VerifyPrefixXml_m7729FF5CEA9F697C2B498B672D02C4C895A287FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* String_t_0_0_0_var;
IL2CPP_EXTERN_C const uint32_t Base64Encoder_Flush_m7C096C39FD6F6EB5AAEBB7C5ED816DD8F24CA5B0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HashCodeOfStringDelegate_BeginInvoke_m041CC1DB7FC71916966716A0BC20C28B1FD86CAD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SR_GetString_m846F567D256240B005CADDBE65D4793920B6474E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SecureStringHasher_GetHashCodeDelegate_mB57EBF631C2136DB005BB3A2CA60A9D88222334B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SecureStringHasher_GetHashCode_m06AA20C7C8C7B52D42E039B7EBB1C9409752B54A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TagInfo_Init_mFC65CB7C7A7D7852E3A2825365F1BD35CC38F8CF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlCharType_InitInstance_m59CB9FA98F252136EEB7E364C62849CCC9B7E7AE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlCharType_get_Instance_mA3CFC9BC3797565FD176224C6116F41AC8BA65B5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlCharType_get_StaticLock_m81A2863AB242D63E36753E695E04EFD800E30F62_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_com_FromNativeMethodDefinition_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlChildEnumerator_get_Current_m9F82A181A0FEE37872C087AAF383C0AC440DB5F5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateException_m9718225A7133DC0205AE6EC7BC322F4FEAE5A716_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateException_mD3F12F95EFAC639FBCB9EB56C4008F5F422380D9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateInvalidHighSurrogateCharException_m430040D295EFE67CF3214A7375E5B2E080EF04AB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateInvalidHighSurrogateCharException_m7963DACF73B00A03914532EC609524F30AC26F16_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateInvalidHighSurrogateCharException_mD8CAB48E86A39FACE4A0274D91C7065A6458233E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateInvalidSurrogatePairException_m0DE4CC9F8620ABC31C61F6FDAFA3BC960F41F170_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateInvalidSurrogatePairException_m97FE3D009250AE26015577D66CE3A652D787B86B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateInvalidSurrogatePairException_mAD8DA247F0F5CAFAE622A2FC49AA8AD44746125A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_TrimString_mF0E4AC16BD05053538B20B21DBD64447195A2D1B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert__cctor_m702BEEB571815B9886A2C46DD223B7C96058857E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlDOMTextWriter__ctor_m7A30C41A24817419B3370A576D6B1254EBEC824E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlException_CreateMessage_m222F162AE354DEA3AFD7969544733DD123BA90DB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlException_FormatUserMessage_m43C6731234AE7C72D5C1C88905B6E4A21C2C4CA5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlException_GetObjectData_m792FDA84BA189F6AB82F9AD4B0DF800DF9051DF4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlException__ctor_m69AC5BDB322F2F6A79D671CB2E262FD0C0482D7B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlException__ctor_m6AA71014CB31FB0C9DF0EDE841514F97B7E11B93_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlException__ctor_m819800529C3A91E8C5640AAA3F30EBD998238F10_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlNode_System_Collections_IEnumerable_GetEnumerator_mC7D6329FEF45EE989276469F9E13D5E60B90C9C1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlNode_get_OuterXml_m69AA8CA2C21CF72A80768DE8531B05A08ADCA52B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlReader__cctor_m8C939FA4F60E046BCAEDF0969A6E3B89D9CB0A4E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_StartAttribute_m154B2203CF4424F0E8A6D1D80AC68B2E05FB7B32_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_WriteCharEntityImpl_m26C18068AD20D6F0B9A5B5F3BB3D9E5A5F61BAD9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_WriteCharEntityImpl_mCE63160635693B3E2C37693BE3401796D86ECAD9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_WriteRaw_m5B4E690DB11115A1A2EBF4228182024DD5BB8434_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_WriteSurrogateChar_m6D9CFC7F219F339DE22E679418EC68425D7EE20D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_Write_m3D15126BEF6FE4BE6AA1FA9A25860A11649C8E3B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_get_AttributeValue_m9E57BB8D3D0C4BBA526DFAEC0064BD1C3F3BA38D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_AddNamespace_mD3E28DCB8C014B090EB3FFA4C6F0A71702135E5B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_AddToNamespaceHashtable_m35841EE9201BDF774311ED96E3D3DEDB174D24A6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_AutoComplete_mC90A57781E031A2E18047CAF6BF09199323B48D2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_Close_mCE05D92C3D65682E40B0A95F579EA2EF84F432B7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_HandleSpecialAttribute_m743156B9137FC8E43B2E697605AAE9C225A2166E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_InternalWriteEndElement_mF39D8CB533A512D50269E722957BA39D259B7D9C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_LookupNamespace_mB4568061673EFBC795A43BB701ABE3335AFCE9A2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_PopNamespaces_mB647C786975F9BB3ADE2301184F8206D816EAFA6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_PushNamespace_mFAE3B22D92E1EE3EB61C94917C3E9A955D9CD5C2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_VerifyPrefixXml_m7729FF5CEA9F697C2B498B672D02C4C895A287FB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_WriteEndStartTag_mBA4796BAC09CD5B25F711E65D61BC2BE8375D2F0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter__cctor_m0368A357057D3B0847BD1AC54EA1E4E609C9DEC7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter__ctor_m2561A4395C8A92B49271C2D6E6840CEDADC38D7B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter__ctor_m99C6F457368131185E3DF47E3339A20D2C076357_MetadataUsageId;
struct CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_com;
struct CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_pinvoke;
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_com;
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_pinvoke;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
struct NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6;
struct StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A;
struct TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t0405602968139A4E20850A743CF2ADF054445765 
{
public:

public:
};


// System.Object


// SR
struct  SR_t18DDAC0A0AC9B7F52E81CFEC14051580FFAAB957  : public RuntimeObject
{
public:

public:
};

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct  Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tABFC31237D6642B5D4C1DBA234CA37EE851EB0AE* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t61F8738ED346768CC112B2E27863BF9F73C76D90 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t17EEB7B2EDD3CB5222C660D7E739F803986BF025 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162, ___entries_1)); }
	inline EntryU5BU5D_tABFC31237D6642B5D4C1DBA234CA37EE851EB0AE* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tABFC31237D6642B5D4C1DBA234CA37EE851EB0AE** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tABFC31237D6642B5D4C1DBA234CA37EE851EB0AE* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162, ___keys_7)); }
	inline KeyCollection_t61F8738ED346768CC112B2E27863BF9F73C76D90 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t61F8738ED346768CC112B2E27863BF9F73C76D90 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t61F8738ED346768CC112B2E27863BF9F73C76D90 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162, ___values_8)); }
	inline ValueCollection_t17EEB7B2EDD3CB5222C660D7E739F803986BF025 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t17EEB7B2EDD3CB5222C660D7E739F803986BF025 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t17EEB7B2EDD3CB5222C660D7E739F803986BF025 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Globalization.CultureInfo
struct  CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_t53CDF1C5F789A28897415891667799420D3C5529 * ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;

public:
	inline static int32_t get_offset_of_m_isReadOnly_3() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_isReadOnly_3)); }
	inline bool get_m_isReadOnly_3() const { return ___m_isReadOnly_3; }
	inline bool* get_address_of_m_isReadOnly_3() { return &___m_isReadOnly_3; }
	inline void set_m_isReadOnly_3(bool value)
	{
		___m_isReadOnly_3 = value;
	}

	inline static int32_t get_offset_of_cultureID_4() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___cultureID_4)); }
	inline int32_t get_cultureID_4() const { return ___cultureID_4; }
	inline int32_t* get_address_of_cultureID_4() { return &___cultureID_4; }
	inline void set_cultureID_4(int32_t value)
	{
		___cultureID_4 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_5() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___parent_lcid_5)); }
	inline int32_t get_parent_lcid_5() const { return ___parent_lcid_5; }
	inline int32_t* get_address_of_parent_lcid_5() { return &___parent_lcid_5; }
	inline void set_parent_lcid_5(int32_t value)
	{
		___parent_lcid_5 = value;
	}

	inline static int32_t get_offset_of_datetime_index_6() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___datetime_index_6)); }
	inline int32_t get_datetime_index_6() const { return ___datetime_index_6; }
	inline int32_t* get_address_of_datetime_index_6() { return &___datetime_index_6; }
	inline void set_datetime_index_6(int32_t value)
	{
		___datetime_index_6 = value;
	}

	inline static int32_t get_offset_of_number_index_7() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___number_index_7)); }
	inline int32_t get_number_index_7() const { return ___number_index_7; }
	inline int32_t* get_address_of_number_index_7() { return &___number_index_7; }
	inline void set_number_index_7(int32_t value)
	{
		___number_index_7 = value;
	}

	inline static int32_t get_offset_of_default_calendar_type_8() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___default_calendar_type_8)); }
	inline int32_t get_default_calendar_type_8() const { return ___default_calendar_type_8; }
	inline int32_t* get_address_of_default_calendar_type_8() { return &___default_calendar_type_8; }
	inline void set_default_calendar_type_8(int32_t value)
	{
		___default_calendar_type_8 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_9() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_useUserOverride_9)); }
	inline bool get_m_useUserOverride_9() const { return ___m_useUserOverride_9; }
	inline bool* get_address_of_m_useUserOverride_9() { return &___m_useUserOverride_9; }
	inline void set_m_useUserOverride_9(bool value)
	{
		___m_useUserOverride_9 = value;
	}

	inline static int32_t get_offset_of_numInfo_10() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___numInfo_10)); }
	inline NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * get_numInfo_10() const { return ___numInfo_10; }
	inline NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D ** get_address_of_numInfo_10() { return &___numInfo_10; }
	inline void set_numInfo_10(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * value)
	{
		___numInfo_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numInfo_10), (void*)value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_11() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___dateTimeInfo_11)); }
	inline DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * get_dateTimeInfo_11() const { return ___dateTimeInfo_11; }
	inline DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 ** get_address_of_dateTimeInfo_11() { return &___dateTimeInfo_11; }
	inline void set_dateTimeInfo_11(DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * value)
	{
		___dateTimeInfo_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dateTimeInfo_11), (void*)value);
	}

	inline static int32_t get_offset_of_textInfo_12() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___textInfo_12)); }
	inline TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * get_textInfo_12() const { return ___textInfo_12; }
	inline TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C ** get_address_of_textInfo_12() { return &___textInfo_12; }
	inline void set_textInfo_12(TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * value)
	{
		___textInfo_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInfo_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_name_13() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_name_13)); }
	inline String_t* get_m_name_13() const { return ___m_name_13; }
	inline String_t** get_address_of_m_name_13() { return &___m_name_13; }
	inline void set_m_name_13(String_t* value)
	{
		___m_name_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_name_13), (void*)value);
	}

	inline static int32_t get_offset_of_englishname_14() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___englishname_14)); }
	inline String_t* get_englishname_14() const { return ___englishname_14; }
	inline String_t** get_address_of_englishname_14() { return &___englishname_14; }
	inline void set_englishname_14(String_t* value)
	{
		___englishname_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___englishname_14), (void*)value);
	}

	inline static int32_t get_offset_of_nativename_15() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___nativename_15)); }
	inline String_t* get_nativename_15() const { return ___nativename_15; }
	inline String_t** get_address_of_nativename_15() { return &___nativename_15; }
	inline void set_nativename_15(String_t* value)
	{
		___nativename_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nativename_15), (void*)value);
	}

	inline static int32_t get_offset_of_iso3lang_16() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___iso3lang_16)); }
	inline String_t* get_iso3lang_16() const { return ___iso3lang_16; }
	inline String_t** get_address_of_iso3lang_16() { return &___iso3lang_16; }
	inline void set_iso3lang_16(String_t* value)
	{
		___iso3lang_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iso3lang_16), (void*)value);
	}

	inline static int32_t get_offset_of_iso2lang_17() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___iso2lang_17)); }
	inline String_t* get_iso2lang_17() const { return ___iso2lang_17; }
	inline String_t** get_address_of_iso2lang_17() { return &___iso2lang_17; }
	inline void set_iso2lang_17(String_t* value)
	{
		___iso2lang_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iso2lang_17), (void*)value);
	}

	inline static int32_t get_offset_of_win3lang_18() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___win3lang_18)); }
	inline String_t* get_win3lang_18() const { return ___win3lang_18; }
	inline String_t** get_address_of_win3lang_18() { return &___win3lang_18; }
	inline void set_win3lang_18(String_t* value)
	{
		___win3lang_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___win3lang_18), (void*)value);
	}

	inline static int32_t get_offset_of_territory_19() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___territory_19)); }
	inline String_t* get_territory_19() const { return ___territory_19; }
	inline String_t** get_address_of_territory_19() { return &___territory_19; }
	inline void set_territory_19(String_t* value)
	{
		___territory_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___territory_19), (void*)value);
	}

	inline static int32_t get_offset_of_native_calendar_names_20() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___native_calendar_names_20)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_native_calendar_names_20() const { return ___native_calendar_names_20; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_native_calendar_names_20() { return &___native_calendar_names_20; }
	inline void set_native_calendar_names_20(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___native_calendar_names_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_calendar_names_20), (void*)value);
	}

	inline static int32_t get_offset_of_compareInfo_21() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___compareInfo_21)); }
	inline CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * get_compareInfo_21() const { return ___compareInfo_21; }
	inline CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 ** get_address_of_compareInfo_21() { return &___compareInfo_21; }
	inline void set_compareInfo_21(CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * value)
	{
		___compareInfo_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___compareInfo_21), (void*)value);
	}

	inline static int32_t get_offset_of_textinfo_data_22() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___textinfo_data_22)); }
	inline void* get_textinfo_data_22() const { return ___textinfo_data_22; }
	inline void** get_address_of_textinfo_data_22() { return &___textinfo_data_22; }
	inline void set_textinfo_data_22(void* value)
	{
		___textinfo_data_22 = value;
	}

	inline static int32_t get_offset_of_m_dataItem_23() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_dataItem_23)); }
	inline int32_t get_m_dataItem_23() const { return ___m_dataItem_23; }
	inline int32_t* get_address_of_m_dataItem_23() { return &___m_dataItem_23; }
	inline void set_m_dataItem_23(int32_t value)
	{
		___m_dataItem_23 = value;
	}

	inline static int32_t get_offset_of_calendar_24() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___calendar_24)); }
	inline Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * get_calendar_24() const { return ___calendar_24; }
	inline Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A ** get_address_of_calendar_24() { return &___calendar_24; }
	inline void set_calendar_24(Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * value)
	{
		___calendar_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___calendar_24), (void*)value);
	}

	inline static int32_t get_offset_of_parent_culture_25() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___parent_culture_25)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_parent_culture_25() const { return ___parent_culture_25; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_parent_culture_25() { return &___parent_culture_25; }
	inline void set_parent_culture_25(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___parent_culture_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_culture_25), (void*)value);
	}

	inline static int32_t get_offset_of_constructed_26() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___constructed_26)); }
	inline bool get_constructed_26() const { return ___constructed_26; }
	inline bool* get_address_of_constructed_26() { return &___constructed_26; }
	inline void set_constructed_26(bool value)
	{
		___constructed_26 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_27() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___cached_serialized_form_27)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_cached_serialized_form_27() const { return ___cached_serialized_form_27; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_cached_serialized_form_27() { return &___cached_serialized_form_27; }
	inline void set_cached_serialized_form_27(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___cached_serialized_form_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cached_serialized_form_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_cultureData_28() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_cultureData_28)); }
	inline CultureData_t53CDF1C5F789A28897415891667799420D3C5529 * get_m_cultureData_28() const { return ___m_cultureData_28; }
	inline CultureData_t53CDF1C5F789A28897415891667799420D3C5529 ** get_address_of_m_cultureData_28() { return &___m_cultureData_28; }
	inline void set_m_cultureData_28(CultureData_t53CDF1C5F789A28897415891667799420D3C5529 * value)
	{
		___m_cultureData_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_cultureData_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_isInherited_29() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_isInherited_29)); }
	inline bool get_m_isInherited_29() const { return ___m_isInherited_29; }
	inline bool* get_address_of_m_isInherited_29() { return &___m_isInherited_29; }
	inline void set_m_isInherited_29(bool value)
	{
		___m_isInherited_29 = value;
	}
};

struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___s_DefaultThreadCurrentUICulture_33;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___s_DefaultThreadCurrentCulture_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 * ___shared_by_number_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC * ___shared_by_name_36;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_37;

public:
	inline static int32_t get_offset_of_invariant_culture_info_0() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___invariant_culture_info_0)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_invariant_culture_info_0() const { return ___invariant_culture_info_0; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_invariant_culture_info_0() { return &___invariant_culture_info_0; }
	inline void set_invariant_culture_info_0(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___invariant_culture_info_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invariant_culture_info_0), (void*)value);
	}

	inline static int32_t get_offset_of_shared_table_lock_1() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___shared_table_lock_1)); }
	inline RuntimeObject * get_shared_table_lock_1() const { return ___shared_table_lock_1; }
	inline RuntimeObject ** get_address_of_shared_table_lock_1() { return &___shared_table_lock_1; }
	inline void set_shared_table_lock_1(RuntimeObject * value)
	{
		___shared_table_lock_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_table_lock_1), (void*)value);
	}

	inline static int32_t get_offset_of_default_current_culture_2() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___default_current_culture_2)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_default_current_culture_2() const { return ___default_current_culture_2; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_default_current_culture_2() { return &___default_current_culture_2; }
	inline void set_default_current_culture_2(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___default_current_culture_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___default_current_culture_2), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentUICulture_33() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___s_DefaultThreadCurrentUICulture_33)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_s_DefaultThreadCurrentUICulture_33() const { return ___s_DefaultThreadCurrentUICulture_33; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_s_DefaultThreadCurrentUICulture_33() { return &___s_DefaultThreadCurrentUICulture_33; }
	inline void set_s_DefaultThreadCurrentUICulture_33(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___s_DefaultThreadCurrentUICulture_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultThreadCurrentUICulture_33), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentCulture_34() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___s_DefaultThreadCurrentCulture_34)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_s_DefaultThreadCurrentCulture_34() const { return ___s_DefaultThreadCurrentCulture_34; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_s_DefaultThreadCurrentCulture_34() { return &___s_DefaultThreadCurrentCulture_34; }
	inline void set_s_DefaultThreadCurrentCulture_34(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___s_DefaultThreadCurrentCulture_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultThreadCurrentCulture_34), (void*)value);
	}

	inline static int32_t get_offset_of_shared_by_number_35() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___shared_by_number_35)); }
	inline Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 * get_shared_by_number_35() const { return ___shared_by_number_35; }
	inline Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 ** get_address_of_shared_by_number_35() { return &___shared_by_number_35; }
	inline void set_shared_by_number_35(Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 * value)
	{
		___shared_by_number_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_by_number_35), (void*)value);
	}

	inline static int32_t get_offset_of_shared_by_name_36() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___shared_by_name_36)); }
	inline Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC * get_shared_by_name_36() const { return ___shared_by_name_36; }
	inline Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC ** get_address_of_shared_by_name_36() { return &___shared_by_name_36; }
	inline void set_shared_by_name_36(Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC * value)
	{
		___shared_by_name_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_by_name_36), (void*)value);
	}

	inline static int32_t get_offset_of_IsTaiwanSku_37() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___IsTaiwanSku_37)); }
	inline bool get_IsTaiwanSku_37() const { return ___IsTaiwanSku_37; }
	inline bool* get_address_of_IsTaiwanSku_37() { return &___IsTaiwanSku_37; }
	inline void set_IsTaiwanSku_37(bool value)
	{
		___IsTaiwanSku_37 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___numInfo_10;
	DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * ___dateTimeInfo_11;
	TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * ___calendar_24;
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___numInfo_10;
	DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * ___dateTimeInfo_11;
	TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * ___calendar_24;
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};

// System.MarshalByRefObject
struct  MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____identity_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.Runtime.Serialization.SerializationInfo
struct  SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1  : public RuntimeObject
{
public:
	// System.String[] System.Runtime.Serialization.SerializationInfo::m_members
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___m_members_3;
	// System.Object[] System.Runtime.Serialization.SerializationInfo::m_data
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_data_4;
	// System.Type[] System.Runtime.Serialization.SerializationInfo::m_types
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___m_types_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Runtime.Serialization.SerializationInfo::m_nameToIndex
	Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * ___m_nameToIndex_6;
	// System.Int32 System.Runtime.Serialization.SerializationInfo::m_currMember
	int32_t ___m_currMember_7;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.SerializationInfo::m_converter
	RuntimeObject* ___m_converter_8;
	// System.String System.Runtime.Serialization.SerializationInfo::m_fullTypeName
	String_t* ___m_fullTypeName_9;
	// System.String System.Runtime.Serialization.SerializationInfo::m_assemName
	String_t* ___m_assemName_10;
	// System.Type System.Runtime.Serialization.SerializationInfo::objectType
	Type_t * ___objectType_11;
	// System.Boolean System.Runtime.Serialization.SerializationInfo::isFullTypeNameSetExplicit
	bool ___isFullTypeNameSetExplicit_12;
	// System.Boolean System.Runtime.Serialization.SerializationInfo::isAssemblyNameSetExplicit
	bool ___isAssemblyNameSetExplicit_13;
	// System.Boolean System.Runtime.Serialization.SerializationInfo::requireSameTokenInPartialTrust
	bool ___requireSameTokenInPartialTrust_14;

public:
	inline static int32_t get_offset_of_m_members_3() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_members_3)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_m_members_3() const { return ___m_members_3; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_m_members_3() { return &___m_members_3; }
	inline void set_m_members_3(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___m_members_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_members_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_data_4() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_data_4)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_data_4() const { return ___m_data_4; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_data_4() { return &___m_data_4; }
	inline void set_m_data_4(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_data_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_data_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_types_5() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_types_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_m_types_5() const { return ___m_types_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_m_types_5() { return &___m_types_5; }
	inline void set_m_types_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___m_types_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_types_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_nameToIndex_6() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_nameToIndex_6)); }
	inline Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * get_m_nameToIndex_6() const { return ___m_nameToIndex_6; }
	inline Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 ** get_address_of_m_nameToIndex_6() { return &___m_nameToIndex_6; }
	inline void set_m_nameToIndex_6(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * value)
	{
		___m_nameToIndex_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_nameToIndex_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_currMember_7() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_currMember_7)); }
	inline int32_t get_m_currMember_7() const { return ___m_currMember_7; }
	inline int32_t* get_address_of_m_currMember_7() { return &___m_currMember_7; }
	inline void set_m_currMember_7(int32_t value)
	{
		___m_currMember_7 = value;
	}

	inline static int32_t get_offset_of_m_converter_8() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_converter_8)); }
	inline RuntimeObject* get_m_converter_8() const { return ___m_converter_8; }
	inline RuntimeObject** get_address_of_m_converter_8() { return &___m_converter_8; }
	inline void set_m_converter_8(RuntimeObject* value)
	{
		___m_converter_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_converter_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_fullTypeName_9() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_fullTypeName_9)); }
	inline String_t* get_m_fullTypeName_9() const { return ___m_fullTypeName_9; }
	inline String_t** get_address_of_m_fullTypeName_9() { return &___m_fullTypeName_9; }
	inline void set_m_fullTypeName_9(String_t* value)
	{
		___m_fullTypeName_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_fullTypeName_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_assemName_10() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_assemName_10)); }
	inline String_t* get_m_assemName_10() const { return ___m_assemName_10; }
	inline String_t** get_address_of_m_assemName_10() { return &___m_assemName_10; }
	inline void set_m_assemName_10(String_t* value)
	{
		___m_assemName_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_assemName_10), (void*)value);
	}

	inline static int32_t get_offset_of_objectType_11() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___objectType_11)); }
	inline Type_t * get_objectType_11() const { return ___objectType_11; }
	inline Type_t ** get_address_of_objectType_11() { return &___objectType_11; }
	inline void set_objectType_11(Type_t * value)
	{
		___objectType_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectType_11), (void*)value);
	}

	inline static int32_t get_offset_of_isFullTypeNameSetExplicit_12() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___isFullTypeNameSetExplicit_12)); }
	inline bool get_isFullTypeNameSetExplicit_12() const { return ___isFullTypeNameSetExplicit_12; }
	inline bool* get_address_of_isFullTypeNameSetExplicit_12() { return &___isFullTypeNameSetExplicit_12; }
	inline void set_isFullTypeNameSetExplicit_12(bool value)
	{
		___isFullTypeNameSetExplicit_12 = value;
	}

	inline static int32_t get_offset_of_isAssemblyNameSetExplicit_13() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___isAssemblyNameSetExplicit_13)); }
	inline bool get_isAssemblyNameSetExplicit_13() const { return ___isAssemblyNameSetExplicit_13; }
	inline bool* get_address_of_isAssemblyNameSetExplicit_13() { return &___isAssemblyNameSetExplicit_13; }
	inline void set_isAssemblyNameSetExplicit_13(bool value)
	{
		___isAssemblyNameSetExplicit_13 = value;
	}

	inline static int32_t get_offset_of_requireSameTokenInPartialTrust_14() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___requireSameTokenInPartialTrust_14)); }
	inline bool get_requireSameTokenInPartialTrust_14() const { return ___requireSameTokenInPartialTrust_14; }
	inline bool* get_address_of_requireSameTokenInPartialTrust_14() { return &___requireSameTokenInPartialTrust_14; }
	inline void set_requireSameTokenInPartialTrust_14(bool value)
	{
		___requireSameTokenInPartialTrust_14 = value;
	}
};


// System.Runtime.Serialization.SerializationInfoEnumerator
struct  SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6  : public RuntimeObject
{
public:
	// System.String[] System.Runtime.Serialization.SerializationInfoEnumerator::m_members
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___m_members_0;
	// System.Object[] System.Runtime.Serialization.SerializationInfoEnumerator::m_data
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_data_1;
	// System.Type[] System.Runtime.Serialization.SerializationInfoEnumerator::m_types
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___m_types_2;
	// System.Int32 System.Runtime.Serialization.SerializationInfoEnumerator::m_numItems
	int32_t ___m_numItems_3;
	// System.Int32 System.Runtime.Serialization.SerializationInfoEnumerator::m_currItem
	int32_t ___m_currItem_4;
	// System.Boolean System.Runtime.Serialization.SerializationInfoEnumerator::m_current
	bool ___m_current_5;

public:
	inline static int32_t get_offset_of_m_members_0() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6, ___m_members_0)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_m_members_0() const { return ___m_members_0; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_m_members_0() { return &___m_members_0; }
	inline void set_m_members_0(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___m_members_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_members_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_data_1() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6, ___m_data_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_data_1() const { return ___m_data_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_data_1() { return &___m_data_1; }
	inline void set_m_data_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_data_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_data_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_types_2() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6, ___m_types_2)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_m_types_2() const { return ___m_types_2; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_m_types_2() { return &___m_types_2; }
	inline void set_m_types_2(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___m_types_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_types_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_numItems_3() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6, ___m_numItems_3)); }
	inline int32_t get_m_numItems_3() const { return ___m_numItems_3; }
	inline int32_t* get_address_of_m_numItems_3() { return &___m_numItems_3; }
	inline void set_m_numItems_3(int32_t value)
	{
		___m_numItems_3 = value;
	}

	inline static int32_t get_offset_of_m_currItem_4() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6, ___m_currItem_4)); }
	inline int32_t get_m_currItem_4() const { return ___m_currItem_4; }
	inline int32_t* get_address_of_m_currItem_4() { return &___m_currItem_4; }
	inline void set_m_currItem_4(int32_t value)
	{
		___m_currItem_4 = value;
	}

	inline static int32_t get_offset_of_m_current_5() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6, ___m_current_5)); }
	inline bool get_m_current_5() const { return ___m_current_5; }
	inline bool* get_address_of_m_current_5() { return &___m_current_5; }
	inline void set_m_current_5(bool value)
	{
		___m_current_5 = value;
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.Encoding
struct  Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___dataItem_10)); }
	inline CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataItem_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___encoderFallback_13)); }
	inline EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoderFallback_13), (void*)value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___decoderFallback_14)); }
	inline DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___decoderFallback_14), (void*)value);
	}
};

struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultEncoding_0), (void*)value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unicodeEncoding_1), (void*)value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bigEndianUnicode_2), (void*)value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf7Encoding_3), (void*)value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf8Encoding_4), (void*)value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf32Encoding_5), (void*)value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asciiEncoding_6), (void*)value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___latin1Encoding_7), (void*)value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___encodings_8)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encodings_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalSyncObject_15), (void*)value);
	}
};


// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkChars_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkPrevious_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Xml.Base64Encoder
struct  Base64Encoder_tF36936300CC9B84076C8F16EE4AF8074ACA40CE9  : public RuntimeObject
{
public:
	// System.Byte[] System.Xml.Base64Encoder::leftOverBytes
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___leftOverBytes_0;
	// System.Int32 System.Xml.Base64Encoder::leftOverBytesCount
	int32_t ___leftOverBytesCount_1;
	// System.Char[] System.Xml.Base64Encoder::charsLine
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___charsLine_2;

public:
	inline static int32_t get_offset_of_leftOverBytes_0() { return static_cast<int32_t>(offsetof(Base64Encoder_tF36936300CC9B84076C8F16EE4AF8074ACA40CE9, ___leftOverBytes_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_leftOverBytes_0() const { return ___leftOverBytes_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_leftOverBytes_0() { return &___leftOverBytes_0; }
	inline void set_leftOverBytes_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___leftOverBytes_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___leftOverBytes_0), (void*)value);
	}

	inline static int32_t get_offset_of_leftOverBytesCount_1() { return static_cast<int32_t>(offsetof(Base64Encoder_tF36936300CC9B84076C8F16EE4AF8074ACA40CE9, ___leftOverBytesCount_1)); }
	inline int32_t get_leftOverBytesCount_1() const { return ___leftOverBytesCount_1; }
	inline int32_t* get_address_of_leftOverBytesCount_1() { return &___leftOverBytesCount_1; }
	inline void set_leftOverBytesCount_1(int32_t value)
	{
		___leftOverBytesCount_1 = value;
	}

	inline static int32_t get_offset_of_charsLine_2() { return static_cast<int32_t>(offsetof(Base64Encoder_tF36936300CC9B84076C8F16EE4AF8074ACA40CE9, ___charsLine_2)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_charsLine_2() const { return ___charsLine_2; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_charsLine_2() { return &___charsLine_2; }
	inline void set_charsLine_2(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___charsLine_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___charsLine_2), (void*)value);
	}
};


// System.Xml.Res
struct  Res_t8707C30091CC714404BA967631E1F74B8ACBB1E4  : public RuntimeObject
{
public:

public:
};


// System.Xml.SecureStringHasher
struct  SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.SecureStringHasher::hashCodeRandomizer
	int32_t ___hashCodeRandomizer_1;

public:
	inline static int32_t get_offset_of_hashCodeRandomizer_1() { return static_cast<int32_t>(offsetof(SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A, ___hashCodeRandomizer_1)); }
	inline int32_t get_hashCodeRandomizer_1() const { return ___hashCodeRandomizer_1; }
	inline int32_t* get_address_of_hashCodeRandomizer_1() { return &___hashCodeRandomizer_1; }
	inline void set_hashCodeRandomizer_1(int32_t value)
	{
		___hashCodeRandomizer_1 = value;
	}
};

struct SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A_StaticFields
{
public:
	// System.Xml.SecureStringHasher_HashCodeOfStringDelegate System.Xml.SecureStringHasher::hashCodeDelegate
	HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * ___hashCodeDelegate_0;

public:
	inline static int32_t get_offset_of_hashCodeDelegate_0() { return static_cast<int32_t>(offsetof(SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A_StaticFields, ___hashCodeDelegate_0)); }
	inline HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * get_hashCodeDelegate_0() const { return ___hashCodeDelegate_0; }
	inline HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 ** get_address_of_hashCodeDelegate_0() { return &___hashCodeDelegate_0; }
	inline void set_hashCodeDelegate_0(HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * value)
	{
		___hashCodeDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hashCodeDelegate_0), (void*)value);
	}
};


// System.Xml.XmlChildEnumerator
struct  XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlChildEnumerator::container
	XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * ___container_0;
	// System.Xml.XmlNode System.Xml.XmlChildEnumerator::child
	XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * ___child_1;
	// System.Boolean System.Xml.XmlChildEnumerator::isFirst
	bool ___isFirst_2;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559, ___container_0)); }
	inline XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * get_container_0() const { return ___container_0; }
	inline XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___container_0), (void*)value);
	}

	inline static int32_t get_offset_of_child_1() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559, ___child_1)); }
	inline XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * get_child_1() const { return ___child_1; }
	inline XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 ** get_address_of_child_1() { return &___child_1; }
	inline void set_child_1(XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * value)
	{
		___child_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___child_1), (void*)value);
	}

	inline static int32_t get_offset_of_isFirst_2() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559, ___isFirst_2)); }
	inline bool get_isFirst_2() const { return ___isFirst_2; }
	inline bool* get_address_of_isFirst_2() { return &___isFirst_2; }
	inline void set_isFirst_2(bool value)
	{
		___isFirst_2 = value;
	}
};


// System.Xml.XmlNode
struct  XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1  : public RuntimeObject
{
public:

public:
};


// System.Xml.XmlReader
struct  XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138  : public RuntimeObject
{
public:

public:
};

struct XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138_StaticFields
{
public:
	// System.UInt32 System.Xml.XmlReader::IsTextualNodeBitmap
	uint32_t ___IsTextualNodeBitmap_0;
	// System.UInt32 System.Xml.XmlReader::CanReadContentAsBitmap
	uint32_t ___CanReadContentAsBitmap_1;
	// System.UInt32 System.Xml.XmlReader::HasValueBitmap
	uint32_t ___HasValueBitmap_2;

public:
	inline static int32_t get_offset_of_IsTextualNodeBitmap_0() { return static_cast<int32_t>(offsetof(XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138_StaticFields, ___IsTextualNodeBitmap_0)); }
	inline uint32_t get_IsTextualNodeBitmap_0() const { return ___IsTextualNodeBitmap_0; }
	inline uint32_t* get_address_of_IsTextualNodeBitmap_0() { return &___IsTextualNodeBitmap_0; }
	inline void set_IsTextualNodeBitmap_0(uint32_t value)
	{
		___IsTextualNodeBitmap_0 = value;
	}

	inline static int32_t get_offset_of_CanReadContentAsBitmap_1() { return static_cast<int32_t>(offsetof(XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138_StaticFields, ___CanReadContentAsBitmap_1)); }
	inline uint32_t get_CanReadContentAsBitmap_1() const { return ___CanReadContentAsBitmap_1; }
	inline uint32_t* get_address_of_CanReadContentAsBitmap_1() { return &___CanReadContentAsBitmap_1; }
	inline void set_CanReadContentAsBitmap_1(uint32_t value)
	{
		___CanReadContentAsBitmap_1 = value;
	}

	inline static int32_t get_offset_of_HasValueBitmap_2() { return static_cast<int32_t>(offsetof(XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138_StaticFields, ___HasValueBitmap_2)); }
	inline uint32_t get_HasValueBitmap_2() const { return ___HasValueBitmap_2; }
	inline uint32_t* get_address_of_HasValueBitmap_2() { return &___HasValueBitmap_2; }
	inline void set_HasValueBitmap_2(uint32_t value)
	{
		___HasValueBitmap_2 = value;
	}
};


// System.Xml.XmlWriter
struct  XmlWriter_t676293C138D2D0DAB9C1BC16A7BEE618391C5B2D  : public RuntimeObject
{
public:

public:
};


// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D416
struct  __StaticArrayInitTypeSizeU3D416_tEA2021331600112E61D2C04468109C687F093BF4 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D416_tEA2021331600112E61D2C04468109C687F093BF4__padding[416];
	};

public:
};


// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D6
struct  __StaticArrayInitTypeSizeU3D6_tDF2537259373F423B466710F7B6BCCCCB9F570AB 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_tDF2537259373F423B466710F7B6BCCCCB9F570AB__padding[6];
	};

public:
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Char
struct  Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IO.TextWriter
struct  TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643  : public MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8
{
public:
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___CoreNewLine_9;
	// System.IFormatProvider System.IO.TextWriter::InternalFormatProvider
	RuntimeObject* ___InternalFormatProvider_10;

public:
	inline static int32_t get_offset_of_CoreNewLine_9() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643, ___CoreNewLine_9)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_CoreNewLine_9() const { return ___CoreNewLine_9; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_CoreNewLine_9() { return &___CoreNewLine_9; }
	inline void set_CoreNewLine_9(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___CoreNewLine_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CoreNewLine_9), (void*)value);
	}

	inline static int32_t get_offset_of_InternalFormatProvider_10() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643, ___InternalFormatProvider_10)); }
	inline RuntimeObject* get_InternalFormatProvider_10() const { return ___InternalFormatProvider_10; }
	inline RuntimeObject** get_address_of_InternalFormatProvider_10() { return &___InternalFormatProvider_10; }
	inline void set_InternalFormatProvider_10(RuntimeObject* value)
	{
		___InternalFormatProvider_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InternalFormatProvider_10), (void*)value);
	}
};

struct TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields
{
public:
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___Null_1;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteCharDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteCharDelegate_2;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteStringDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteStringDelegate_3;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteCharArrayRangeDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteCharArrayRangeDelegate_4;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineCharDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteLineCharDelegate_5;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineStringDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteLineStringDelegate_6;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineCharArrayRangeDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteLineCharArrayRangeDelegate_7;
	// System.Action`1<System.Object> System.IO.TextWriter::_FlushDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____FlushDelegate_8;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ___Null_1)); }
	inline TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * get_Null_1() const { return ___Null_1; }
	inline TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Null_1), (void*)value);
	}

	inline static int32_t get_offset_of__WriteCharDelegate_2() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteCharDelegate_2)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteCharDelegate_2() const { return ____WriteCharDelegate_2; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteCharDelegate_2() { return &____WriteCharDelegate_2; }
	inline void set__WriteCharDelegate_2(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteCharDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteCharDelegate_2), (void*)value);
	}

	inline static int32_t get_offset_of__WriteStringDelegate_3() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteStringDelegate_3)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteStringDelegate_3() const { return ____WriteStringDelegate_3; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteStringDelegate_3() { return &____WriteStringDelegate_3; }
	inline void set__WriteStringDelegate_3(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteStringDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteStringDelegate_3), (void*)value);
	}

	inline static int32_t get_offset_of__WriteCharArrayRangeDelegate_4() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteCharArrayRangeDelegate_4)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteCharArrayRangeDelegate_4() const { return ____WriteCharArrayRangeDelegate_4; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteCharArrayRangeDelegate_4() { return &____WriteCharArrayRangeDelegate_4; }
	inline void set__WriteCharArrayRangeDelegate_4(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteCharArrayRangeDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteCharArrayRangeDelegate_4), (void*)value);
	}

	inline static int32_t get_offset_of__WriteLineCharDelegate_5() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteLineCharDelegate_5)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteLineCharDelegate_5() const { return ____WriteLineCharDelegate_5; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteLineCharDelegate_5() { return &____WriteLineCharDelegate_5; }
	inline void set__WriteLineCharDelegate_5(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteLineCharDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteLineCharDelegate_5), (void*)value);
	}

	inline static int32_t get_offset_of__WriteLineStringDelegate_6() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteLineStringDelegate_6)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteLineStringDelegate_6() const { return ____WriteLineStringDelegate_6; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteLineStringDelegate_6() { return &____WriteLineStringDelegate_6; }
	inline void set__WriteLineStringDelegate_6(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteLineStringDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteLineStringDelegate_6), (void*)value);
	}

	inline static int32_t get_offset_of__WriteLineCharArrayRangeDelegate_7() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteLineCharArrayRangeDelegate_7)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteLineCharArrayRangeDelegate_7() const { return ____WriteLineCharArrayRangeDelegate_7; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteLineCharArrayRangeDelegate_7() { return &____WriteLineCharArrayRangeDelegate_7; }
	inline void set__WriteLineCharArrayRangeDelegate_7(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteLineCharArrayRangeDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteLineCharArrayRangeDelegate_7), (void*)value);
	}

	inline static int32_t get_offset_of__FlushDelegate_8() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____FlushDelegate_8)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__FlushDelegate_8() const { return ____FlushDelegate_8; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__FlushDelegate_8() { return &____FlushDelegate_8; }
	inline void set__FlushDelegate_8(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____FlushDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____FlushDelegate_8), (void*)value);
	}
};


// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct  Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};


// System.Runtime.Serialization.SerializationEntry
struct  SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E 
{
public:
	// System.Type System.Runtime.Serialization.SerializationEntry::m_type
	Type_t * ___m_type_0;
	// System.Object System.Runtime.Serialization.SerializationEntry::m_value
	RuntimeObject * ___m_value_1;
	// System.String System.Runtime.Serialization.SerializationEntry::m_name
	String_t* ___m_name_2;

public:
	inline static int32_t get_offset_of_m_type_0() { return static_cast<int32_t>(offsetof(SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E, ___m_type_0)); }
	inline Type_t * get_m_type_0() const { return ___m_type_0; }
	inline Type_t ** get_address_of_m_type_0() { return &___m_type_0; }
	inline void set_m_type_0(Type_t * value)
	{
		___m_type_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_type_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_value_1() { return static_cast<int32_t>(offsetof(SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E, ___m_value_1)); }
	inline RuntimeObject * get_m_value_1() const { return ___m_value_1; }
	inline RuntimeObject ** get_address_of_m_value_1() { return &___m_value_1; }
	inline void set_m_value_1(RuntimeObject * value)
	{
		___m_value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_value_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_name_2() { return static_cast<int32_t>(offsetof(SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E, ___m_name_2)); }
	inline String_t* get_m_name_2() const { return ___m_name_2; }
	inline String_t** get_address_of_m_name_2() { return &___m_name_2; }
	inline void set_m_name_2(String_t* value)
	{
		___m_name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_name_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.Serialization.SerializationEntry
struct SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E_marshaled_pinvoke
{
	Type_t * ___m_type_0;
	Il2CppIUnknown* ___m_value_1;
	char* ___m_name_2;
};
// Native definition for COM marshalling of System.Runtime.Serialization.SerializationEntry
struct SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E_marshaled_com
{
	Type_t * ___m_type_0;
	Il2CppIUnknown* ___m_value_1;
	Il2CppChar* ___m_name_2;
};

// System.UInt32
struct  UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Xml.Serialization.XmlIgnoreAttribute
struct  XmlIgnoreAttribute_t478DB7B508B6987B3CC531BABD12177E5CD7DDF6  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Xml.XmlCharType
struct  XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA, ___charProperties_2)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___charProperties_2), (void*)value);
	}
};

struct XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Lock_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_CharProperties_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshaled_pinvoke
{
	Il2CppSafeArray/*NONE*/* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshaled_com
{
	Il2CppSafeArray/*NONE*/* ___charProperties_2;
};

// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0  : public XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::next
	XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * ___next_0;

public:
	inline static int32_t get_offset_of_next_0() { return static_cast<int32_t>(offsetof(XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0, ___next_0)); }
	inline XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * get_next_0() const { return ___next_0; }
	inline XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 ** get_address_of_next_0() { return &___next_0; }
	inline void set_next_0(XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * value)
	{
		___next_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___next_0), (void*)value);
	}
};


// System.Xml.XmlTextWriter_Namespace
struct  Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB 
{
public:
	// System.String System.Xml.XmlTextWriter_Namespace::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlTextWriter_Namespace::ns
	String_t* ___ns_1;
	// System.Boolean System.Xml.XmlTextWriter_Namespace::declared
	bool ___declared_2;
	// System.Int32 System.Xml.XmlTextWriter_Namespace::prevNsIndex
	int32_t ___prevNsIndex_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefix_0), (void*)value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ns_1), (void*)value);
	}

	inline static int32_t get_offset_of_declared_2() { return static_cast<int32_t>(offsetof(Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB, ___declared_2)); }
	inline bool get_declared_2() const { return ___declared_2; }
	inline bool* get_address_of_declared_2() { return &___declared_2; }
	inline void set_declared_2(bool value)
	{
		___declared_2 = value;
	}

	inline static int32_t get_offset_of_prevNsIndex_3() { return static_cast<int32_t>(offsetof(Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB, ___prevNsIndex_3)); }
	inline int32_t get_prevNsIndex_3() const { return ___prevNsIndex_3; }
	inline int32_t* get_address_of_prevNsIndex_3() { return &___prevNsIndex_3; }
	inline void set_prevNsIndex_3(int32_t value)
	{
		___prevNsIndex_3 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Xml.XmlTextWriter/Namespace
struct Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___ns_1;
	int32_t ___declared_2;
	int32_t ___prevNsIndex_3;
};
// Native definition for COM marshalling of System.Xml.XmlTextWriter/Namespace
struct Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___ns_1;
	int32_t ___declared_2;
	int32_t ___prevNsIndex_3;
};

// System.Xml.XmlTextWriterBase64Encoder
struct  XmlTextWriterBase64Encoder_t6C566B592DFF5C0E5108670EB19BA06D35F0213B  : public Base64Encoder_tF36936300CC9B84076C8F16EE4AF8074ACA40CE9
{
public:
	// System.Xml.XmlTextEncoder System.Xml.XmlTextWriterBase64Encoder::xmlTextEncoder
	XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * ___xmlTextEncoder_3;

public:
	inline static int32_t get_offset_of_xmlTextEncoder_3() { return static_cast<int32_t>(offsetof(XmlTextWriterBase64Encoder_t6C566B592DFF5C0E5108670EB19BA06D35F0213B, ___xmlTextEncoder_3)); }
	inline XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * get_xmlTextEncoder_3() const { return ___xmlTextEncoder_3; }
	inline XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E ** get_address_of_xmlTextEncoder_3() { return &___xmlTextEncoder_3; }
	inline void set_xmlTextEncoder_3(XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * value)
	{
		___xmlTextEncoder_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___xmlTextEncoder_3), (void*)value);
	}
};


// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D6 <PrivateImplementationDetails>::5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98
	__StaticArrayInitTypeSizeU3D6_tDF2537259373F423B466710F7B6BCCCCB9F570AB  ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D416 <PrivateImplementationDetails>::6A0D50D692745A6663128CD315B71079584F3E59
	__StaticArrayInitTypeSizeU3D416_tEA2021331600112E61D2C04468109C687F093BF4  ___6A0D50D692745A6663128CD315B71079584F3E59_1;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D416 <PrivateImplementationDetails>::B368804F0C6DAB083B253A6B106D0783D5C32E90
	__StaticArrayInitTypeSizeU3D416_tEA2021331600112E61D2C04468109C687F093BF4  ___B368804F0C6DAB083B253A6B106D0783D5C32E90_2;
	// System.Int64 <PrivateImplementationDetails>::EBC658B067B5C785A3F0BB67D73755F6FEE7F70C
	int64_t ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3;

public:
	inline static int32_t get_offset_of_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E_StaticFields, ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0)); }
	inline __StaticArrayInitTypeSizeU3D6_tDF2537259373F423B466710F7B6BCCCCB9F570AB  get_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0() const { return ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0; }
	inline __StaticArrayInitTypeSizeU3D6_tDF2537259373F423B466710F7B6BCCCCB9F570AB * get_address_of_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0() { return &___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0; }
	inline void set_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0(__StaticArrayInitTypeSizeU3D6_tDF2537259373F423B466710F7B6BCCCCB9F570AB  value)
	{
		___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0 = value;
	}

	inline static int32_t get_offset_of_U36A0D50D692745A6663128CD315B71079584F3E59_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E_StaticFields, ___6A0D50D692745A6663128CD315B71079584F3E59_1)); }
	inline __StaticArrayInitTypeSizeU3D416_tEA2021331600112E61D2C04468109C687F093BF4  get_U36A0D50D692745A6663128CD315B71079584F3E59_1() const { return ___6A0D50D692745A6663128CD315B71079584F3E59_1; }
	inline __StaticArrayInitTypeSizeU3D416_tEA2021331600112E61D2C04468109C687F093BF4 * get_address_of_U36A0D50D692745A6663128CD315B71079584F3E59_1() { return &___6A0D50D692745A6663128CD315B71079584F3E59_1; }
	inline void set_U36A0D50D692745A6663128CD315B71079584F3E59_1(__StaticArrayInitTypeSizeU3D416_tEA2021331600112E61D2C04468109C687F093BF4  value)
	{
		___6A0D50D692745A6663128CD315B71079584F3E59_1 = value;
	}

	inline static int32_t get_offset_of_B368804F0C6DAB083B253A6B106D0783D5C32E90_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E_StaticFields, ___B368804F0C6DAB083B253A6B106D0783D5C32E90_2)); }
	inline __StaticArrayInitTypeSizeU3D416_tEA2021331600112E61D2C04468109C687F093BF4  get_B368804F0C6DAB083B253A6B106D0783D5C32E90_2() const { return ___B368804F0C6DAB083B253A6B106D0783D5C32E90_2; }
	inline __StaticArrayInitTypeSizeU3D416_tEA2021331600112E61D2C04468109C687F093BF4 * get_address_of_B368804F0C6DAB083B253A6B106D0783D5C32E90_2() { return &___B368804F0C6DAB083B253A6B106D0783D5C32E90_2; }
	inline void set_B368804F0C6DAB083B253A6B106D0783D5C32E90_2(__StaticArrayInitTypeSizeU3D416_tEA2021331600112E61D2C04468109C687F093BF4  value)
	{
		___B368804F0C6DAB083B253A6B106D0783D5C32E90_2 = value;
	}

	inline static int32_t get_offset_of_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E_StaticFields, ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3)); }
	inline int64_t get_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3() const { return ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3; }
	inline int64_t* get_address_of_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3() { return &___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3; }
	inline void set_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3(int64_t value)
	{
		___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Globalization.NumberStyles
struct  NumberStyles_t379EFBF2535E1C950DEC8042704BB663BF636594 
{
public:
	// System.Int32 System.Globalization.NumberStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NumberStyles_t379EFBF2535E1C950DEC8042704BB663BF636594, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.IO.StringWriter
struct  StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839  : public TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643
{
public:
	// System.Text.StringBuilder System.IO.StringWriter::_sb
	StringBuilder_t * ____sb_12;
	// System.Boolean System.IO.StringWriter::_isOpen
	bool ____isOpen_13;

public:
	inline static int32_t get_offset_of__sb_12() { return static_cast<int32_t>(offsetof(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839, ____sb_12)); }
	inline StringBuilder_t * get__sb_12() const { return ____sb_12; }
	inline StringBuilder_t ** get_address_of__sb_12() { return &____sb_12; }
	inline void set__sb_12(StringBuilder_t * value)
	{
		____sb_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sb_12), (void*)value);
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}
};

struct StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_StaticFields
{
public:
	// System.Text.UnicodeEncoding modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.StringWriter::m_encoding
	UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68 * ___m_encoding_11;

public:
	inline static int32_t get_offset_of_m_encoding_11() { return static_cast<int32_t>(offsetof(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_StaticFields, ___m_encoding_11)); }
	inline UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68 * get_m_encoding_11() const { return ___m_encoding_11; }
	inline UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68 ** get_address_of_m_encoding_11() { return &___m_encoding_11; }
	inline void set_m_encoding_11(UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68 * value)
	{
		___m_encoding_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_encoding_11), (void*)value);
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};


// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_tF4C7FE6D6121BD4C67699869C8269A60B36B42C3 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamingContextStates_tF4C7FE6D6121BD4C67699869C8269A60B36B42C3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.StringComparison
struct  StringComparison_tCC9F72B9B1E2C3C6D2566DD0D3A61E1621048998 
{
public:
	// System.Int32 System.StringComparison::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringComparison_tCC9F72B9B1E2C3C6D2566DD0D3A61E1621048998, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.ExceptionType
struct  ExceptionType_t7FE8075D25307AFE6AA02C34899023BC4C9B7B23 
{
public:
	// System.Int32 System.Xml.ExceptionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExceptionType_t7FE8075D25307AFE6AA02C34899023BC4C9B7B23, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.Formatting
struct  Formatting_t1E6C9EEFAFBEA3953A61247936613F766513FCCB 
{
public:
	// System.Int32 System.Xml.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_t1E6C9EEFAFBEA3953A61247936613F766513FCCB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.WriteState
struct  WriteState_t5FC2A1D32436519584C900786EA9C0E15D9317C6 
{
public:
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WriteState_t5FC2A1D32436519584C900786EA9C0E15D9317C6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlConvert
struct  XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A  : public RuntimeObject
{
public:

public:
};

struct XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_StaticFields
{
public:
	// System.Xml.XmlCharType System.Xml.XmlConvert::xmlCharType
	XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  ___xmlCharType_0;
	// System.Char[] System.Xml.XmlConvert::crt
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___crt_1;
	// System.Int32 System.Xml.XmlConvert::c_EncodedCharLength
	int32_t ___c_EncodedCharLength_2;
	// System.Char[] System.Xml.XmlConvert::WhitespaceChars
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___WhitespaceChars_3;

public:
	inline static int32_t get_offset_of_xmlCharType_0() { return static_cast<int32_t>(offsetof(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_StaticFields, ___xmlCharType_0)); }
	inline XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  get_xmlCharType_0() const { return ___xmlCharType_0; }
	inline XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA * get_address_of_xmlCharType_0() { return &___xmlCharType_0; }
	inline void set_xmlCharType_0(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  value)
	{
		___xmlCharType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___xmlCharType_0))->___charProperties_2), (void*)NULL);
	}

	inline static int32_t get_offset_of_crt_1() { return static_cast<int32_t>(offsetof(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_StaticFields, ___crt_1)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_crt_1() const { return ___crt_1; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_crt_1() { return &___crt_1; }
	inline void set_crt_1(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___crt_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___crt_1), (void*)value);
	}

	inline static int32_t get_offset_of_c_EncodedCharLength_2() { return static_cast<int32_t>(offsetof(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_StaticFields, ___c_EncodedCharLength_2)); }
	inline int32_t get_c_EncodedCharLength_2() const { return ___c_EncodedCharLength_2; }
	inline int32_t* get_address_of_c_EncodedCharLength_2() { return &___c_EncodedCharLength_2; }
	inline void set_c_EncodedCharLength_2(int32_t value)
	{
		___c_EncodedCharLength_2 = value;
	}

	inline static int32_t get_offset_of_WhitespaceChars_3() { return static_cast<int32_t>(offsetof(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_StaticFields, ___WhitespaceChars_3)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_WhitespaceChars_3() const { return ___WhitespaceChars_3; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_WhitespaceChars_3() { return &___WhitespaceChars_3; }
	inline void set_WhitespaceChars_3(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___WhitespaceChars_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WhitespaceChars_3), (void*)value);
	}
};


// System.Xml.XmlSpace
struct  XmlSpace_t16F1125FF443A131844086CD412753BFE485A047 
{
public:
	// System.Int32 System.Xml.XmlSpace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSpace_t16F1125FF443A131844086CD412753BFE485A047, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlTextEncoder
struct  XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E  : public RuntimeObject
{
public:
	// System.IO.TextWriter System.Xml.XmlTextEncoder::textWriter
	TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___textWriter_0;
	// System.Boolean System.Xml.XmlTextEncoder::inAttribute
	bool ___inAttribute_1;
	// System.Char System.Xml.XmlTextEncoder::quoteChar
	Il2CppChar ___quoteChar_2;
	// System.Text.StringBuilder System.Xml.XmlTextEncoder::attrValue
	StringBuilder_t * ___attrValue_3;
	// System.Boolean System.Xml.XmlTextEncoder::cacheAttrValue
	bool ___cacheAttrValue_4;
	// System.Xml.XmlCharType System.Xml.XmlTextEncoder::xmlCharType
	XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  ___xmlCharType_5;

public:
	inline static int32_t get_offset_of_textWriter_0() { return static_cast<int32_t>(offsetof(XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E, ___textWriter_0)); }
	inline TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * get_textWriter_0() const { return ___textWriter_0; }
	inline TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 ** get_address_of_textWriter_0() { return &___textWriter_0; }
	inline void set_textWriter_0(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * value)
	{
		___textWriter_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textWriter_0), (void*)value);
	}

	inline static int32_t get_offset_of_inAttribute_1() { return static_cast<int32_t>(offsetof(XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E, ___inAttribute_1)); }
	inline bool get_inAttribute_1() const { return ___inAttribute_1; }
	inline bool* get_address_of_inAttribute_1() { return &___inAttribute_1; }
	inline void set_inAttribute_1(bool value)
	{
		___inAttribute_1 = value;
	}

	inline static int32_t get_offset_of_quoteChar_2() { return static_cast<int32_t>(offsetof(XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E, ___quoteChar_2)); }
	inline Il2CppChar get_quoteChar_2() const { return ___quoteChar_2; }
	inline Il2CppChar* get_address_of_quoteChar_2() { return &___quoteChar_2; }
	inline void set_quoteChar_2(Il2CppChar value)
	{
		___quoteChar_2 = value;
	}

	inline static int32_t get_offset_of_attrValue_3() { return static_cast<int32_t>(offsetof(XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E, ___attrValue_3)); }
	inline StringBuilder_t * get_attrValue_3() const { return ___attrValue_3; }
	inline StringBuilder_t ** get_address_of_attrValue_3() { return &___attrValue_3; }
	inline void set_attrValue_3(StringBuilder_t * value)
	{
		___attrValue_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___attrValue_3), (void*)value);
	}

	inline static int32_t get_offset_of_cacheAttrValue_4() { return static_cast<int32_t>(offsetof(XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E, ___cacheAttrValue_4)); }
	inline bool get_cacheAttrValue_4() const { return ___cacheAttrValue_4; }
	inline bool* get_address_of_cacheAttrValue_4() { return &___cacheAttrValue_4; }
	inline void set_cacheAttrValue_4(bool value)
	{
		___cacheAttrValue_4 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_5() { return static_cast<int32_t>(offsetof(XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E, ___xmlCharType_5)); }
	inline XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  get_xmlCharType_5() const { return ___xmlCharType_5; }
	inline XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA * get_address_of_xmlCharType_5() { return &___xmlCharType_5; }
	inline void set_xmlCharType_5(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  value)
	{
		___xmlCharType_5 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___xmlCharType_5))->___charProperties_2), (void*)NULL);
	}
};


// System.Xml.XmlTextWriter_NamespaceState
struct  NamespaceState_t70DDAB16B7FAF8E9437F174C42B9CA023C7ABB8B 
{
public:
	// System.Int32 System.Xml.XmlTextWriter_NamespaceState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamespaceState_t70DDAB16B7FAF8E9437F174C42B9CA023C7ABB8B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlTextWriter_SpecialAttr
struct  SpecialAttr_tB7E41418E95FF4D8293A70FB57D901B5FE8CADD7 
{
public:
	// System.Int32 System.Xml.XmlTextWriter_SpecialAttr::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialAttr_tB7E41418E95FF4D8293A70FB57D901B5FE8CADD7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlTextWriter_State
struct  State_tE0E12FF743D37B3B0B6D1DE2EF74B81B632AA8F6 
{
public:
	// System.Int32 System.Xml.XmlTextWriter_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_tE0E12FF743D37B3B0B6D1DE2EF74B81B632AA8F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlTextWriter_Token
struct  Token_t1C2DE327268C7DBF13A132FF4E0E3DC05B5672B9 
{
public:
	// System.Int32 System.Xml.XmlTextWriter_Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_t1C2DE327268C7DBF13A132FF4E0E3DC05B5672B9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Globalization.NumberFormatInfo
struct  NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D  : public RuntimeObject
{
public:
	// System.Int32[] System.Globalization.NumberFormatInfo::numberGroupSizes
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___numberGroupSizes_1;
	// System.Int32[] System.Globalization.NumberFormatInfo::currencyGroupSizes
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___currencyGroupSizes_2;
	// System.Int32[] System.Globalization.NumberFormatInfo::percentGroupSizes
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___percentGroupSizes_3;
	// System.String System.Globalization.NumberFormatInfo::positiveSign
	String_t* ___positiveSign_4;
	// System.String System.Globalization.NumberFormatInfo::negativeSign
	String_t* ___negativeSign_5;
	// System.String System.Globalization.NumberFormatInfo::numberDecimalSeparator
	String_t* ___numberDecimalSeparator_6;
	// System.String System.Globalization.NumberFormatInfo::numberGroupSeparator
	String_t* ___numberGroupSeparator_7;
	// System.String System.Globalization.NumberFormatInfo::currencyGroupSeparator
	String_t* ___currencyGroupSeparator_8;
	// System.String System.Globalization.NumberFormatInfo::currencyDecimalSeparator
	String_t* ___currencyDecimalSeparator_9;
	// System.String System.Globalization.NumberFormatInfo::currencySymbol
	String_t* ___currencySymbol_10;
	// System.String System.Globalization.NumberFormatInfo::ansiCurrencySymbol
	String_t* ___ansiCurrencySymbol_11;
	// System.String System.Globalization.NumberFormatInfo::nanSymbol
	String_t* ___nanSymbol_12;
	// System.String System.Globalization.NumberFormatInfo::positiveInfinitySymbol
	String_t* ___positiveInfinitySymbol_13;
	// System.String System.Globalization.NumberFormatInfo::negativeInfinitySymbol
	String_t* ___negativeInfinitySymbol_14;
	// System.String System.Globalization.NumberFormatInfo::percentDecimalSeparator
	String_t* ___percentDecimalSeparator_15;
	// System.String System.Globalization.NumberFormatInfo::percentGroupSeparator
	String_t* ___percentGroupSeparator_16;
	// System.String System.Globalization.NumberFormatInfo::percentSymbol
	String_t* ___percentSymbol_17;
	// System.String System.Globalization.NumberFormatInfo::perMilleSymbol
	String_t* ___perMilleSymbol_18;
	// System.String[] System.Globalization.NumberFormatInfo::nativeDigits
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___nativeDigits_19;
	// System.Int32 System.Globalization.NumberFormatInfo::m_dataItem
	int32_t ___m_dataItem_20;
	// System.Int32 System.Globalization.NumberFormatInfo::numberDecimalDigits
	int32_t ___numberDecimalDigits_21;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyDecimalDigits
	int32_t ___currencyDecimalDigits_22;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyPositivePattern
	int32_t ___currencyPositivePattern_23;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyNegativePattern
	int32_t ___currencyNegativePattern_24;
	// System.Int32 System.Globalization.NumberFormatInfo::numberNegativePattern
	int32_t ___numberNegativePattern_25;
	// System.Int32 System.Globalization.NumberFormatInfo::percentPositivePattern
	int32_t ___percentPositivePattern_26;
	// System.Int32 System.Globalization.NumberFormatInfo::percentNegativePattern
	int32_t ___percentNegativePattern_27;
	// System.Int32 System.Globalization.NumberFormatInfo::percentDecimalDigits
	int32_t ___percentDecimalDigits_28;
	// System.Int32 System.Globalization.NumberFormatInfo::digitSubstitution
	int32_t ___digitSubstitution_29;
	// System.Boolean System.Globalization.NumberFormatInfo::isReadOnly
	bool ___isReadOnly_30;
	// System.Boolean System.Globalization.NumberFormatInfo::m_useUserOverride
	bool ___m_useUserOverride_31;
	// System.Boolean System.Globalization.NumberFormatInfo::m_isInvariant
	bool ___m_isInvariant_32;
	// System.Boolean System.Globalization.NumberFormatInfo::validForParseAsNumber
	bool ___validForParseAsNumber_33;
	// System.Boolean System.Globalization.NumberFormatInfo::validForParseAsCurrency
	bool ___validForParseAsCurrency_34;

public:
	inline static int32_t get_offset_of_numberGroupSizes_1() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___numberGroupSizes_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_numberGroupSizes_1() const { return ___numberGroupSizes_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_numberGroupSizes_1() { return &___numberGroupSizes_1; }
	inline void set_numberGroupSizes_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___numberGroupSizes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numberGroupSizes_1), (void*)value);
	}

	inline static int32_t get_offset_of_currencyGroupSizes_2() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___currencyGroupSizes_2)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_currencyGroupSizes_2() const { return ___currencyGroupSizes_2; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_currencyGroupSizes_2() { return &___currencyGroupSizes_2; }
	inline void set_currencyGroupSizes_2(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___currencyGroupSizes_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currencyGroupSizes_2), (void*)value);
	}

	inline static int32_t get_offset_of_percentGroupSizes_3() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___percentGroupSizes_3)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_percentGroupSizes_3() const { return ___percentGroupSizes_3; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_percentGroupSizes_3() { return &___percentGroupSizes_3; }
	inline void set_percentGroupSizes_3(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___percentGroupSizes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___percentGroupSizes_3), (void*)value);
	}

	inline static int32_t get_offset_of_positiveSign_4() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___positiveSign_4)); }
	inline String_t* get_positiveSign_4() const { return ___positiveSign_4; }
	inline String_t** get_address_of_positiveSign_4() { return &___positiveSign_4; }
	inline void set_positiveSign_4(String_t* value)
	{
		___positiveSign_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___positiveSign_4), (void*)value);
	}

	inline static int32_t get_offset_of_negativeSign_5() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___negativeSign_5)); }
	inline String_t* get_negativeSign_5() const { return ___negativeSign_5; }
	inline String_t** get_address_of_negativeSign_5() { return &___negativeSign_5; }
	inline void set_negativeSign_5(String_t* value)
	{
		___negativeSign_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___negativeSign_5), (void*)value);
	}

	inline static int32_t get_offset_of_numberDecimalSeparator_6() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___numberDecimalSeparator_6)); }
	inline String_t* get_numberDecimalSeparator_6() const { return ___numberDecimalSeparator_6; }
	inline String_t** get_address_of_numberDecimalSeparator_6() { return &___numberDecimalSeparator_6; }
	inline void set_numberDecimalSeparator_6(String_t* value)
	{
		___numberDecimalSeparator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numberDecimalSeparator_6), (void*)value);
	}

	inline static int32_t get_offset_of_numberGroupSeparator_7() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___numberGroupSeparator_7)); }
	inline String_t* get_numberGroupSeparator_7() const { return ___numberGroupSeparator_7; }
	inline String_t** get_address_of_numberGroupSeparator_7() { return &___numberGroupSeparator_7; }
	inline void set_numberGroupSeparator_7(String_t* value)
	{
		___numberGroupSeparator_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numberGroupSeparator_7), (void*)value);
	}

	inline static int32_t get_offset_of_currencyGroupSeparator_8() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___currencyGroupSeparator_8)); }
	inline String_t* get_currencyGroupSeparator_8() const { return ___currencyGroupSeparator_8; }
	inline String_t** get_address_of_currencyGroupSeparator_8() { return &___currencyGroupSeparator_8; }
	inline void set_currencyGroupSeparator_8(String_t* value)
	{
		___currencyGroupSeparator_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currencyGroupSeparator_8), (void*)value);
	}

	inline static int32_t get_offset_of_currencyDecimalSeparator_9() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___currencyDecimalSeparator_9)); }
	inline String_t* get_currencyDecimalSeparator_9() const { return ___currencyDecimalSeparator_9; }
	inline String_t** get_address_of_currencyDecimalSeparator_9() { return &___currencyDecimalSeparator_9; }
	inline void set_currencyDecimalSeparator_9(String_t* value)
	{
		___currencyDecimalSeparator_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currencyDecimalSeparator_9), (void*)value);
	}

	inline static int32_t get_offset_of_currencySymbol_10() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___currencySymbol_10)); }
	inline String_t* get_currencySymbol_10() const { return ___currencySymbol_10; }
	inline String_t** get_address_of_currencySymbol_10() { return &___currencySymbol_10; }
	inline void set_currencySymbol_10(String_t* value)
	{
		___currencySymbol_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currencySymbol_10), (void*)value);
	}

	inline static int32_t get_offset_of_ansiCurrencySymbol_11() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___ansiCurrencySymbol_11)); }
	inline String_t* get_ansiCurrencySymbol_11() const { return ___ansiCurrencySymbol_11; }
	inline String_t** get_address_of_ansiCurrencySymbol_11() { return &___ansiCurrencySymbol_11; }
	inline void set_ansiCurrencySymbol_11(String_t* value)
	{
		___ansiCurrencySymbol_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ansiCurrencySymbol_11), (void*)value);
	}

	inline static int32_t get_offset_of_nanSymbol_12() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___nanSymbol_12)); }
	inline String_t* get_nanSymbol_12() const { return ___nanSymbol_12; }
	inline String_t** get_address_of_nanSymbol_12() { return &___nanSymbol_12; }
	inline void set_nanSymbol_12(String_t* value)
	{
		___nanSymbol_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nanSymbol_12), (void*)value);
	}

	inline static int32_t get_offset_of_positiveInfinitySymbol_13() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___positiveInfinitySymbol_13)); }
	inline String_t* get_positiveInfinitySymbol_13() const { return ___positiveInfinitySymbol_13; }
	inline String_t** get_address_of_positiveInfinitySymbol_13() { return &___positiveInfinitySymbol_13; }
	inline void set_positiveInfinitySymbol_13(String_t* value)
	{
		___positiveInfinitySymbol_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___positiveInfinitySymbol_13), (void*)value);
	}

	inline static int32_t get_offset_of_negativeInfinitySymbol_14() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___negativeInfinitySymbol_14)); }
	inline String_t* get_negativeInfinitySymbol_14() const { return ___negativeInfinitySymbol_14; }
	inline String_t** get_address_of_negativeInfinitySymbol_14() { return &___negativeInfinitySymbol_14; }
	inline void set_negativeInfinitySymbol_14(String_t* value)
	{
		___negativeInfinitySymbol_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___negativeInfinitySymbol_14), (void*)value);
	}

	inline static int32_t get_offset_of_percentDecimalSeparator_15() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___percentDecimalSeparator_15)); }
	inline String_t* get_percentDecimalSeparator_15() const { return ___percentDecimalSeparator_15; }
	inline String_t** get_address_of_percentDecimalSeparator_15() { return &___percentDecimalSeparator_15; }
	inline void set_percentDecimalSeparator_15(String_t* value)
	{
		___percentDecimalSeparator_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___percentDecimalSeparator_15), (void*)value);
	}

	inline static int32_t get_offset_of_percentGroupSeparator_16() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___percentGroupSeparator_16)); }
	inline String_t* get_percentGroupSeparator_16() const { return ___percentGroupSeparator_16; }
	inline String_t** get_address_of_percentGroupSeparator_16() { return &___percentGroupSeparator_16; }
	inline void set_percentGroupSeparator_16(String_t* value)
	{
		___percentGroupSeparator_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___percentGroupSeparator_16), (void*)value);
	}

	inline static int32_t get_offset_of_percentSymbol_17() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___percentSymbol_17)); }
	inline String_t* get_percentSymbol_17() const { return ___percentSymbol_17; }
	inline String_t** get_address_of_percentSymbol_17() { return &___percentSymbol_17; }
	inline void set_percentSymbol_17(String_t* value)
	{
		___percentSymbol_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___percentSymbol_17), (void*)value);
	}

	inline static int32_t get_offset_of_perMilleSymbol_18() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___perMilleSymbol_18)); }
	inline String_t* get_perMilleSymbol_18() const { return ___perMilleSymbol_18; }
	inline String_t** get_address_of_perMilleSymbol_18() { return &___perMilleSymbol_18; }
	inline void set_perMilleSymbol_18(String_t* value)
	{
		___perMilleSymbol_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___perMilleSymbol_18), (void*)value);
	}

	inline static int32_t get_offset_of_nativeDigits_19() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___nativeDigits_19)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_nativeDigits_19() const { return ___nativeDigits_19; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_nativeDigits_19() { return &___nativeDigits_19; }
	inline void set_nativeDigits_19(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___nativeDigits_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nativeDigits_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_dataItem_20() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___m_dataItem_20)); }
	inline int32_t get_m_dataItem_20() const { return ___m_dataItem_20; }
	inline int32_t* get_address_of_m_dataItem_20() { return &___m_dataItem_20; }
	inline void set_m_dataItem_20(int32_t value)
	{
		___m_dataItem_20 = value;
	}

	inline static int32_t get_offset_of_numberDecimalDigits_21() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___numberDecimalDigits_21)); }
	inline int32_t get_numberDecimalDigits_21() const { return ___numberDecimalDigits_21; }
	inline int32_t* get_address_of_numberDecimalDigits_21() { return &___numberDecimalDigits_21; }
	inline void set_numberDecimalDigits_21(int32_t value)
	{
		___numberDecimalDigits_21 = value;
	}

	inline static int32_t get_offset_of_currencyDecimalDigits_22() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___currencyDecimalDigits_22)); }
	inline int32_t get_currencyDecimalDigits_22() const { return ___currencyDecimalDigits_22; }
	inline int32_t* get_address_of_currencyDecimalDigits_22() { return &___currencyDecimalDigits_22; }
	inline void set_currencyDecimalDigits_22(int32_t value)
	{
		___currencyDecimalDigits_22 = value;
	}

	inline static int32_t get_offset_of_currencyPositivePattern_23() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___currencyPositivePattern_23)); }
	inline int32_t get_currencyPositivePattern_23() const { return ___currencyPositivePattern_23; }
	inline int32_t* get_address_of_currencyPositivePattern_23() { return &___currencyPositivePattern_23; }
	inline void set_currencyPositivePattern_23(int32_t value)
	{
		___currencyPositivePattern_23 = value;
	}

	inline static int32_t get_offset_of_currencyNegativePattern_24() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___currencyNegativePattern_24)); }
	inline int32_t get_currencyNegativePattern_24() const { return ___currencyNegativePattern_24; }
	inline int32_t* get_address_of_currencyNegativePattern_24() { return &___currencyNegativePattern_24; }
	inline void set_currencyNegativePattern_24(int32_t value)
	{
		___currencyNegativePattern_24 = value;
	}

	inline static int32_t get_offset_of_numberNegativePattern_25() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___numberNegativePattern_25)); }
	inline int32_t get_numberNegativePattern_25() const { return ___numberNegativePattern_25; }
	inline int32_t* get_address_of_numberNegativePattern_25() { return &___numberNegativePattern_25; }
	inline void set_numberNegativePattern_25(int32_t value)
	{
		___numberNegativePattern_25 = value;
	}

	inline static int32_t get_offset_of_percentPositivePattern_26() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___percentPositivePattern_26)); }
	inline int32_t get_percentPositivePattern_26() const { return ___percentPositivePattern_26; }
	inline int32_t* get_address_of_percentPositivePattern_26() { return &___percentPositivePattern_26; }
	inline void set_percentPositivePattern_26(int32_t value)
	{
		___percentPositivePattern_26 = value;
	}

	inline static int32_t get_offset_of_percentNegativePattern_27() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___percentNegativePattern_27)); }
	inline int32_t get_percentNegativePattern_27() const { return ___percentNegativePattern_27; }
	inline int32_t* get_address_of_percentNegativePattern_27() { return &___percentNegativePattern_27; }
	inline void set_percentNegativePattern_27(int32_t value)
	{
		___percentNegativePattern_27 = value;
	}

	inline static int32_t get_offset_of_percentDecimalDigits_28() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___percentDecimalDigits_28)); }
	inline int32_t get_percentDecimalDigits_28() const { return ___percentDecimalDigits_28; }
	inline int32_t* get_address_of_percentDecimalDigits_28() { return &___percentDecimalDigits_28; }
	inline void set_percentDecimalDigits_28(int32_t value)
	{
		___percentDecimalDigits_28 = value;
	}

	inline static int32_t get_offset_of_digitSubstitution_29() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___digitSubstitution_29)); }
	inline int32_t get_digitSubstitution_29() const { return ___digitSubstitution_29; }
	inline int32_t* get_address_of_digitSubstitution_29() { return &___digitSubstitution_29; }
	inline void set_digitSubstitution_29(int32_t value)
	{
		___digitSubstitution_29 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_30() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___isReadOnly_30)); }
	inline bool get_isReadOnly_30() const { return ___isReadOnly_30; }
	inline bool* get_address_of_isReadOnly_30() { return &___isReadOnly_30; }
	inline void set_isReadOnly_30(bool value)
	{
		___isReadOnly_30 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_31() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___m_useUserOverride_31)); }
	inline bool get_m_useUserOverride_31() const { return ___m_useUserOverride_31; }
	inline bool* get_address_of_m_useUserOverride_31() { return &___m_useUserOverride_31; }
	inline void set_m_useUserOverride_31(bool value)
	{
		___m_useUserOverride_31 = value;
	}

	inline static int32_t get_offset_of_m_isInvariant_32() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___m_isInvariant_32)); }
	inline bool get_m_isInvariant_32() const { return ___m_isInvariant_32; }
	inline bool* get_address_of_m_isInvariant_32() { return &___m_isInvariant_32; }
	inline void set_m_isInvariant_32(bool value)
	{
		___m_isInvariant_32 = value;
	}

	inline static int32_t get_offset_of_validForParseAsNumber_33() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___validForParseAsNumber_33)); }
	inline bool get_validForParseAsNumber_33() const { return ___validForParseAsNumber_33; }
	inline bool* get_address_of_validForParseAsNumber_33() { return &___validForParseAsNumber_33; }
	inline void set_validForParseAsNumber_33(bool value)
	{
		___validForParseAsNumber_33 = value;
	}

	inline static int32_t get_offset_of_validForParseAsCurrency_34() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D, ___validForParseAsCurrency_34)); }
	inline bool get_validForParseAsCurrency_34() const { return ___validForParseAsCurrency_34; }
	inline bool* get_address_of_validForParseAsCurrency_34() { return &___validForParseAsCurrency_34; }
	inline void set_validForParseAsCurrency_34(bool value)
	{
		___validForParseAsCurrency_34 = value;
	}
};

struct NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D_StaticFields
{
public:
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.NumberFormatInfo::invariantInfo
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___invariantInfo_0;

public:
	inline static int32_t get_offset_of_invariantInfo_0() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D_StaticFields, ___invariantInfo_0)); }
	inline NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * get_invariantInfo_0() const { return ___invariantInfo_0; }
	inline NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D ** get_address_of_invariantInfo_0() { return &___invariantInfo_0; }
	inline void set_invariantInfo_0(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * value)
	{
		___invariantInfo_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invariantInfo_0), (void*)value);
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505 
{
public:
	// System.Object System.Runtime.Serialization.StreamingContext::m_additionalContext
	RuntimeObject * ___m_additionalContext_0;
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::m_state
	int32_t ___m_state_1;

public:
	inline static int32_t get_offset_of_m_additionalContext_0() { return static_cast<int32_t>(offsetof(StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505, ___m_additionalContext_0)); }
	inline RuntimeObject * get_m_additionalContext_0() const { return ___m_additionalContext_0; }
	inline RuntimeObject ** get_address_of_m_additionalContext_0() { return &___m_additionalContext_0; }
	inline void set_m_additionalContext_0(RuntimeObject * value)
	{
		___m_additionalContext_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_additionalContext_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_state_1() { return static_cast<int32_t>(offsetof(StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505, ___m_state_1)); }
	inline int32_t get_m_state_1() const { return ___m_state_1; }
	inline int32_t* get_address_of_m_state_1() { return &___m_state_1; }
	inline void set_m_state_1(int32_t value)
	{
		___m_state_1 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};

// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Xml.XmlTextWriter
struct  XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2  : public XmlWriter_t676293C138D2D0DAB9C1BC16A7BEE618391C5B2D
{
public:
	// System.IO.TextWriter System.Xml.XmlTextWriter::textWriter
	TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___textWriter_0;
	// System.Xml.XmlTextEncoder System.Xml.XmlTextWriter::xmlEncoder
	XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * ___xmlEncoder_1;
	// System.Text.Encoding System.Xml.XmlTextWriter::encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___encoding_2;
	// System.Xml.Formatting System.Xml.XmlTextWriter::formatting
	int32_t ___formatting_3;
	// System.Boolean System.Xml.XmlTextWriter::indented
	bool ___indented_4;
	// System.Int32 System.Xml.XmlTextWriter::indentation
	int32_t ___indentation_5;
	// System.Char System.Xml.XmlTextWriter::indentChar
	Il2CppChar ___indentChar_6;
	// System.Xml.XmlTextWriter_TagInfo[] System.Xml.XmlTextWriter::stack
	TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* ___stack_7;
	// System.Int32 System.Xml.XmlTextWriter::top
	int32_t ___top_8;
	// System.Xml.XmlTextWriter_State[] System.Xml.XmlTextWriter::stateTable
	StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* ___stateTable_9;
	// System.Xml.XmlTextWriter_State System.Xml.XmlTextWriter::currentState
	int32_t ___currentState_10;
	// System.Xml.XmlTextWriter_Token System.Xml.XmlTextWriter::lastToken
	int32_t ___lastToken_11;
	// System.Xml.XmlTextWriterBase64Encoder System.Xml.XmlTextWriter::base64Encoder
	XmlTextWriterBase64Encoder_t6C566B592DFF5C0E5108670EB19BA06D35F0213B * ___base64Encoder_12;
	// System.Char System.Xml.XmlTextWriter::quoteChar
	Il2CppChar ___quoteChar_13;
	// System.Char System.Xml.XmlTextWriter::curQuoteChar
	Il2CppChar ___curQuoteChar_14;
	// System.Boolean System.Xml.XmlTextWriter::namespaces
	bool ___namespaces_15;
	// System.Xml.XmlTextWriter_SpecialAttr System.Xml.XmlTextWriter::specialAttr
	int32_t ___specialAttr_16;
	// System.String System.Xml.XmlTextWriter::prefixForXmlNs
	String_t* ___prefixForXmlNs_17;
	// System.Boolean System.Xml.XmlTextWriter::flush
	bool ___flush_18;
	// System.Xml.XmlTextWriter_Namespace[] System.Xml.XmlTextWriter::nsStack
	NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* ___nsStack_19;
	// System.Int32 System.Xml.XmlTextWriter::nsTop
	int32_t ___nsTop_20;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlTextWriter::nsHashtable
	Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * ___nsHashtable_21;
	// System.Boolean System.Xml.XmlTextWriter::useNsHashtable
	bool ___useNsHashtable_22;
	// System.Xml.XmlCharType System.Xml.XmlTextWriter::xmlCharType
	XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  ___xmlCharType_23;

public:
	inline static int32_t get_offset_of_textWriter_0() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___textWriter_0)); }
	inline TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * get_textWriter_0() const { return ___textWriter_0; }
	inline TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 ** get_address_of_textWriter_0() { return &___textWriter_0; }
	inline void set_textWriter_0(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * value)
	{
		___textWriter_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textWriter_0), (void*)value);
	}

	inline static int32_t get_offset_of_xmlEncoder_1() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___xmlEncoder_1)); }
	inline XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * get_xmlEncoder_1() const { return ___xmlEncoder_1; }
	inline XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E ** get_address_of_xmlEncoder_1() { return &___xmlEncoder_1; }
	inline void set_xmlEncoder_1(XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * value)
	{
		___xmlEncoder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___xmlEncoder_1), (void*)value);
	}

	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___encoding_2)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_encoding_2() const { return ___encoding_2; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoding_2), (void*)value);
	}

	inline static int32_t get_offset_of_formatting_3() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___formatting_3)); }
	inline int32_t get_formatting_3() const { return ___formatting_3; }
	inline int32_t* get_address_of_formatting_3() { return &___formatting_3; }
	inline void set_formatting_3(int32_t value)
	{
		___formatting_3 = value;
	}

	inline static int32_t get_offset_of_indented_4() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___indented_4)); }
	inline bool get_indented_4() const { return ___indented_4; }
	inline bool* get_address_of_indented_4() { return &___indented_4; }
	inline void set_indented_4(bool value)
	{
		___indented_4 = value;
	}

	inline static int32_t get_offset_of_indentation_5() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___indentation_5)); }
	inline int32_t get_indentation_5() const { return ___indentation_5; }
	inline int32_t* get_address_of_indentation_5() { return &___indentation_5; }
	inline void set_indentation_5(int32_t value)
	{
		___indentation_5 = value;
	}

	inline static int32_t get_offset_of_indentChar_6() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___indentChar_6)); }
	inline Il2CppChar get_indentChar_6() const { return ___indentChar_6; }
	inline Il2CppChar* get_address_of_indentChar_6() { return &___indentChar_6; }
	inline void set_indentChar_6(Il2CppChar value)
	{
		___indentChar_6 = value;
	}

	inline static int32_t get_offset_of_stack_7() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___stack_7)); }
	inline TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* get_stack_7() const { return ___stack_7; }
	inline TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637** get_address_of_stack_7() { return &___stack_7; }
	inline void set_stack_7(TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* value)
	{
		___stack_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stack_7), (void*)value);
	}

	inline static int32_t get_offset_of_top_8() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___top_8)); }
	inline int32_t get_top_8() const { return ___top_8; }
	inline int32_t* get_address_of_top_8() { return &___top_8; }
	inline void set_top_8(int32_t value)
	{
		___top_8 = value;
	}

	inline static int32_t get_offset_of_stateTable_9() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___stateTable_9)); }
	inline StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* get_stateTable_9() const { return ___stateTable_9; }
	inline StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A** get_address_of_stateTable_9() { return &___stateTable_9; }
	inline void set_stateTable_9(StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* value)
	{
		___stateTable_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateTable_9), (void*)value);
	}

	inline static int32_t get_offset_of_currentState_10() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___currentState_10)); }
	inline int32_t get_currentState_10() const { return ___currentState_10; }
	inline int32_t* get_address_of_currentState_10() { return &___currentState_10; }
	inline void set_currentState_10(int32_t value)
	{
		___currentState_10 = value;
	}

	inline static int32_t get_offset_of_lastToken_11() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___lastToken_11)); }
	inline int32_t get_lastToken_11() const { return ___lastToken_11; }
	inline int32_t* get_address_of_lastToken_11() { return &___lastToken_11; }
	inline void set_lastToken_11(int32_t value)
	{
		___lastToken_11 = value;
	}

	inline static int32_t get_offset_of_base64Encoder_12() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___base64Encoder_12)); }
	inline XmlTextWriterBase64Encoder_t6C566B592DFF5C0E5108670EB19BA06D35F0213B * get_base64Encoder_12() const { return ___base64Encoder_12; }
	inline XmlTextWriterBase64Encoder_t6C566B592DFF5C0E5108670EB19BA06D35F0213B ** get_address_of_base64Encoder_12() { return &___base64Encoder_12; }
	inline void set_base64Encoder_12(XmlTextWriterBase64Encoder_t6C566B592DFF5C0E5108670EB19BA06D35F0213B * value)
	{
		___base64Encoder_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___base64Encoder_12), (void*)value);
	}

	inline static int32_t get_offset_of_quoteChar_13() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___quoteChar_13)); }
	inline Il2CppChar get_quoteChar_13() const { return ___quoteChar_13; }
	inline Il2CppChar* get_address_of_quoteChar_13() { return &___quoteChar_13; }
	inline void set_quoteChar_13(Il2CppChar value)
	{
		___quoteChar_13 = value;
	}

	inline static int32_t get_offset_of_curQuoteChar_14() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___curQuoteChar_14)); }
	inline Il2CppChar get_curQuoteChar_14() const { return ___curQuoteChar_14; }
	inline Il2CppChar* get_address_of_curQuoteChar_14() { return &___curQuoteChar_14; }
	inline void set_curQuoteChar_14(Il2CppChar value)
	{
		___curQuoteChar_14 = value;
	}

	inline static int32_t get_offset_of_namespaces_15() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___namespaces_15)); }
	inline bool get_namespaces_15() const { return ___namespaces_15; }
	inline bool* get_address_of_namespaces_15() { return &___namespaces_15; }
	inline void set_namespaces_15(bool value)
	{
		___namespaces_15 = value;
	}

	inline static int32_t get_offset_of_specialAttr_16() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___specialAttr_16)); }
	inline int32_t get_specialAttr_16() const { return ___specialAttr_16; }
	inline int32_t* get_address_of_specialAttr_16() { return &___specialAttr_16; }
	inline void set_specialAttr_16(int32_t value)
	{
		___specialAttr_16 = value;
	}

	inline static int32_t get_offset_of_prefixForXmlNs_17() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___prefixForXmlNs_17)); }
	inline String_t* get_prefixForXmlNs_17() const { return ___prefixForXmlNs_17; }
	inline String_t** get_address_of_prefixForXmlNs_17() { return &___prefixForXmlNs_17; }
	inline void set_prefixForXmlNs_17(String_t* value)
	{
		___prefixForXmlNs_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefixForXmlNs_17), (void*)value);
	}

	inline static int32_t get_offset_of_flush_18() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___flush_18)); }
	inline bool get_flush_18() const { return ___flush_18; }
	inline bool* get_address_of_flush_18() { return &___flush_18; }
	inline void set_flush_18(bool value)
	{
		___flush_18 = value;
	}

	inline static int32_t get_offset_of_nsStack_19() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___nsStack_19)); }
	inline NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* get_nsStack_19() const { return ___nsStack_19; }
	inline NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6** get_address_of_nsStack_19() { return &___nsStack_19; }
	inline void set_nsStack_19(NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* value)
	{
		___nsStack_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nsStack_19), (void*)value);
	}

	inline static int32_t get_offset_of_nsTop_20() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___nsTop_20)); }
	inline int32_t get_nsTop_20() const { return ___nsTop_20; }
	inline int32_t* get_address_of_nsTop_20() { return &___nsTop_20; }
	inline void set_nsTop_20(int32_t value)
	{
		___nsTop_20 = value;
	}

	inline static int32_t get_offset_of_nsHashtable_21() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___nsHashtable_21)); }
	inline Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * get_nsHashtable_21() const { return ___nsHashtable_21; }
	inline Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 ** get_address_of_nsHashtable_21() { return &___nsHashtable_21; }
	inline void set_nsHashtable_21(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * value)
	{
		___nsHashtable_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nsHashtable_21), (void*)value);
	}

	inline static int32_t get_offset_of_useNsHashtable_22() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___useNsHashtable_22)); }
	inline bool get_useNsHashtable_22() const { return ___useNsHashtable_22; }
	inline bool* get_address_of_useNsHashtable_22() { return &___useNsHashtable_22; }
	inline void set_useNsHashtable_22(bool value)
	{
		___useNsHashtable_22 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_23() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2, ___xmlCharType_23)); }
	inline XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  get_xmlCharType_23() const { return ___xmlCharType_23; }
	inline XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA * get_address_of_xmlCharType_23() { return &___xmlCharType_23; }
	inline void set_xmlCharType_23(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  value)
	{
		___xmlCharType_23 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___xmlCharType_23))->___charProperties_2), (void*)NULL);
	}
};

struct XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields
{
public:
	// System.String[] System.Xml.XmlTextWriter::stateName
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___stateName_24;
	// System.String[] System.Xml.XmlTextWriter::tokenName
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___tokenName_25;
	// System.Xml.XmlTextWriter_State[] System.Xml.XmlTextWriter::stateTableDefault
	StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* ___stateTableDefault_26;
	// System.Xml.XmlTextWriter_State[] System.Xml.XmlTextWriter::stateTableDocument
	StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* ___stateTableDocument_27;

public:
	inline static int32_t get_offset_of_stateName_24() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields, ___stateName_24)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_stateName_24() const { return ___stateName_24; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_stateName_24() { return &___stateName_24; }
	inline void set_stateName_24(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___stateName_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateName_24), (void*)value);
	}

	inline static int32_t get_offset_of_tokenName_25() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields, ___tokenName_25)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_tokenName_25() const { return ___tokenName_25; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_tokenName_25() { return &___tokenName_25; }
	inline void set_tokenName_25(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___tokenName_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tokenName_25), (void*)value);
	}

	inline static int32_t get_offset_of_stateTableDefault_26() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields, ___stateTableDefault_26)); }
	inline StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* get_stateTableDefault_26() const { return ___stateTableDefault_26; }
	inline StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A** get_address_of_stateTableDefault_26() { return &___stateTableDefault_26; }
	inline void set_stateTableDefault_26(StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* value)
	{
		___stateTableDefault_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateTableDefault_26), (void*)value);
	}

	inline static int32_t get_offset_of_stateTableDocument_27() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields, ___stateTableDocument_27)); }
	inline StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* get_stateTableDocument_27() const { return ___stateTableDocument_27; }
	inline StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A** get_address_of_stateTableDocument_27() { return &___stateTableDocument_27; }
	inline void set_stateTableDocument_27(StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* value)
	{
		___stateTableDocument_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateTableDocument_27), (void*)value);
	}
};


// System.Xml.XmlTextWriter_TagInfo
struct  TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2 
{
public:
	// System.String System.Xml.XmlTextWriter_TagInfo::name
	String_t* ___name_0;
	// System.String System.Xml.XmlTextWriter_TagInfo::prefix
	String_t* ___prefix_1;
	// System.String System.Xml.XmlTextWriter_TagInfo::defaultNs
	String_t* ___defaultNs_2;
	// System.Xml.XmlTextWriter_NamespaceState System.Xml.XmlTextWriter_TagInfo::defaultNsState
	int32_t ___defaultNsState_3;
	// System.Xml.XmlSpace System.Xml.XmlTextWriter_TagInfo::xmlSpace
	int32_t ___xmlSpace_4;
	// System.String System.Xml.XmlTextWriter_TagInfo::xmlLang
	String_t* ___xmlLang_5;
	// System.Int32 System.Xml.XmlTextWriter_TagInfo::prevNsTop
	int32_t ___prevNsTop_6;
	// System.Int32 System.Xml.XmlTextWriter_TagInfo::prefixCount
	int32_t ___prefixCount_7;
	// System.Boolean System.Xml.XmlTextWriter_TagInfo::mixed
	bool ___mixed_8;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefix_1), (void*)value);
	}

	inline static int32_t get_offset_of_defaultNs_2() { return static_cast<int32_t>(offsetof(TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2, ___defaultNs_2)); }
	inline String_t* get_defaultNs_2() const { return ___defaultNs_2; }
	inline String_t** get_address_of_defaultNs_2() { return &___defaultNs_2; }
	inline void set_defaultNs_2(String_t* value)
	{
		___defaultNs_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultNs_2), (void*)value);
	}

	inline static int32_t get_offset_of_defaultNsState_3() { return static_cast<int32_t>(offsetof(TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2, ___defaultNsState_3)); }
	inline int32_t get_defaultNsState_3() const { return ___defaultNsState_3; }
	inline int32_t* get_address_of_defaultNsState_3() { return &___defaultNsState_3; }
	inline void set_defaultNsState_3(int32_t value)
	{
		___defaultNsState_3 = value;
	}

	inline static int32_t get_offset_of_xmlSpace_4() { return static_cast<int32_t>(offsetof(TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2, ___xmlSpace_4)); }
	inline int32_t get_xmlSpace_4() const { return ___xmlSpace_4; }
	inline int32_t* get_address_of_xmlSpace_4() { return &___xmlSpace_4; }
	inline void set_xmlSpace_4(int32_t value)
	{
		___xmlSpace_4 = value;
	}

	inline static int32_t get_offset_of_xmlLang_5() { return static_cast<int32_t>(offsetof(TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2, ___xmlLang_5)); }
	inline String_t* get_xmlLang_5() const { return ___xmlLang_5; }
	inline String_t** get_address_of_xmlLang_5() { return &___xmlLang_5; }
	inline void set_xmlLang_5(String_t* value)
	{
		___xmlLang_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___xmlLang_5), (void*)value);
	}

	inline static int32_t get_offset_of_prevNsTop_6() { return static_cast<int32_t>(offsetof(TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2, ___prevNsTop_6)); }
	inline int32_t get_prevNsTop_6() const { return ___prevNsTop_6; }
	inline int32_t* get_address_of_prevNsTop_6() { return &___prevNsTop_6; }
	inline void set_prevNsTop_6(int32_t value)
	{
		___prevNsTop_6 = value;
	}

	inline static int32_t get_offset_of_prefixCount_7() { return static_cast<int32_t>(offsetof(TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2, ___prefixCount_7)); }
	inline int32_t get_prefixCount_7() const { return ___prefixCount_7; }
	inline int32_t* get_address_of_prefixCount_7() { return &___prefixCount_7; }
	inline void set_prefixCount_7(int32_t value)
	{
		___prefixCount_7 = value;
	}

	inline static int32_t get_offset_of_mixed_8() { return static_cast<int32_t>(offsetof(TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2, ___mixed_8)); }
	inline bool get_mixed_8() const { return ___mixed_8; }
	inline bool* get_address_of_mixed_8() { return &___mixed_8; }
	inline void set_mixed_8(bool value)
	{
		___mixed_8 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Xml.XmlTextWriter/TagInfo
struct TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshaled_pinvoke
{
	char* ___name_0;
	char* ___prefix_1;
	char* ___defaultNs_2;
	int32_t ___defaultNsState_3;
	int32_t ___xmlSpace_4;
	char* ___xmlLang_5;
	int32_t ___prevNsTop_6;
	int32_t ___prefixCount_7;
	int32_t ___mixed_8;
};
// Native definition for COM marshalling of System.Xml.XmlTextWriter/TagInfo
struct TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___prefix_1;
	Il2CppChar* ___defaultNs_2;
	int32_t ___defaultNsState_3;
	int32_t ___xmlSpace_4;
	Il2CppChar* ___xmlLang_5;
	int32_t ___prevNsTop_6;
	int32_t ___prefixCount_7;
	int32_t ___mixed_8;
};

// System.ArgumentException
struct  ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.AsyncCallback
struct  AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA  : public MulticastDelegate_t
{
public:

public:
};


// System.InvalidOperationException
struct  InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// System.Resources.MissingManifestResourceException
struct  MissingManifestResourceException_tAC74F21ADC46CCB2BCC710464434E3B97F72FACB  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// System.Xml.SecureStringHasher_HashCodeOfStringDelegate
struct  HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574  : public MulticastDelegate_t
{
public:

public:
};


// System.Xml.XmlDOMTextWriter
struct  XmlDOMTextWriter_tF813A0468BACD6B3CC21849A38FA6AB269D5A741  : public XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2
{
public:

public:
};


// System.Xml.XmlException
struct  XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.Xml.XmlException::res
	String_t* ___res_17;
	// System.String[] System.Xml.XmlException::args
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___args_18;
	// System.Int32 System.Xml.XmlException::lineNumber
	int32_t ___lineNumber_19;
	// System.Int32 System.Xml.XmlException::linePosition
	int32_t ___linePosition_20;
	// System.String System.Xml.XmlException::sourceUri
	String_t* ___sourceUri_21;
	// System.String System.Xml.XmlException::message
	String_t* ___message_22;

public:
	inline static int32_t get_offset_of_res_17() { return static_cast<int32_t>(offsetof(XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918, ___res_17)); }
	inline String_t* get_res_17() const { return ___res_17; }
	inline String_t** get_address_of_res_17() { return &___res_17; }
	inline void set_res_17(String_t* value)
	{
		___res_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___res_17), (void*)value);
	}

	inline static int32_t get_offset_of_args_18() { return static_cast<int32_t>(offsetof(XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918, ___args_18)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_args_18() const { return ___args_18; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_args_18() { return &___args_18; }
	inline void set_args_18(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___args_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_18), (void*)value);
	}

	inline static int32_t get_offset_of_lineNumber_19() { return static_cast<int32_t>(offsetof(XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918, ___lineNumber_19)); }
	inline int32_t get_lineNumber_19() const { return ___lineNumber_19; }
	inline int32_t* get_address_of_lineNumber_19() { return &___lineNumber_19; }
	inline void set_lineNumber_19(int32_t value)
	{
		___lineNumber_19 = value;
	}

	inline static int32_t get_offset_of_linePosition_20() { return static_cast<int32_t>(offsetof(XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918, ___linePosition_20)); }
	inline int32_t get_linePosition_20() const { return ___linePosition_20; }
	inline int32_t* get_address_of_linePosition_20() { return &___linePosition_20; }
	inline void set_linePosition_20(int32_t value)
	{
		___linePosition_20 = value;
	}

	inline static int32_t get_offset_of_sourceUri_21() { return static_cast<int32_t>(offsetof(XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918, ___sourceUri_21)); }
	inline String_t* get_sourceUri_21() const { return ___sourceUri_21; }
	inline String_t** get_address_of_sourceUri_21() { return &___sourceUri_21; }
	inline void set_sourceUri_21(String_t* value)
	{
		___sourceUri_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sourceUri_21), (void*)value);
	}

	inline static int32_t get_offset_of_message_22() { return static_cast<int32_t>(offsetof(XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918, ___message_22)); }
	inline String_t* get_message_22() const { return ___message_22; }
	inline String_t** get_address_of_message_22() { return &___message_22; }
	inline void set_message_22(String_t* value)
	{
		___message_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___message_22), (void*)value);
	}
};


// System.ArgumentNullException
struct  ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB  : public ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00
{
public:

public:
};


// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8  : public ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00
{
public:
	// System.Object System.ArgumentOutOfRangeException::m_actualValue
	RuntimeObject * ___m_actualValue_19;

public:
	inline static int32_t get_offset_of_m_actualValue_19() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8, ___m_actualValue_19)); }
	inline RuntimeObject * get_m_actualValue_19() const { return ___m_actualValue_19; }
	inline RuntimeObject ** get_address_of_m_actualValue_19() { return &___m_actualValue_19; }
	inline void set_m_actualValue_19(RuntimeObject * value)
	{
		___m_actualValue_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_actualValue_19), (void*)value);
	}
};

struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.ArgumentOutOfRangeException::_rangeMessage
	String_t* ____rangeMessage_18;

public:
	inline static int32_t get_offset_of__rangeMessage_18() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_StaticFields, ____rangeMessage_18)); }
	inline String_t* get__rangeMessage_18() const { return ____rangeMessage_18; }
	inline String_t** get_address_of__rangeMessage_18() { return &____rangeMessage_18; }
	inline void set__rangeMessage_18(String_t* value)
	{
		____rangeMessage_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rangeMessage_18), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Xml.XmlTextWriter_Namespace[]
struct NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB  m_Items[1];

public:
	inline Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___prefix_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___ns_1), (void*)NULL);
		#endif
	}
	inline Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___prefix_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___ns_1), (void*)NULL);
		#endif
	}
};
// System.Xml.XmlTextWriter_TagInfo[]
struct TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2  m_Items[1];

public:
	inline TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___prefix_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___defaultNs_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___xmlLang_5), (void*)NULL);
		#endif
	}
	inline TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___prefix_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___defaultNs_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___xmlLang_5), (void*)NULL);
		#endif
	}
};
// System.Xml.XmlTextWriter_State[]
struct StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2F6EEA5A421346472B973CEC54962959F5A7A7E9_gshared (Dictionary_2_t1DDD2F48B87E022F599DF2452A49BB70BE95A7F8 * __this, RuntimeObject* ___comparer0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_mD30FE5F3EA63CD6831E4AA9E6901323436D6F9A4_gshared (Dictionary_2_t1DDD2F48B87E022F599DF2452A49BB70BE95A7F8 * __this, RuntimeObject * ___key0, int32_t* ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m9BDED5248054C2E86ECBA732FE7BCDAA32D0A118_gshared (Dictionary_2_t1DDD2F48B87E022F599DF2452A49BB70BE95A7F8 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_Remove_m1E56E91A9BB341EE54712AF20BAF2ADC09D267A0_gshared (Dictionary_2_t1DDD2F48B87E022F599DF2452A49BB70BE95A7F8 * __this, RuntimeObject * ___key0, const RuntimeMethod* method);

// System.Int32 System.Convert::ToBase64CharArray(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Convert_ToBase64CharArray_mD2322C916F75D3414A56A4CECD164D5483636031 (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___inArray0, int32_t ___offsetIn1, int32_t ___length2, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___outArray3, int32_t ___offsetOut4, const RuntimeMethod* method);
// System.String SR::GetString(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SR_GetString_m846F567D256240B005CADDBE65D4793920B6474E (String_t* ___name0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args1, const RuntimeMethod* method);
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164 (const RuntimeMethod* method);
// System.String SR::GetString(System.Globalization.CultureInfo,System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SR_GetString_m6B94153BBBDF13B55321A1E0290B6E82FC8AD248 (CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___culture0, String_t* ___name1, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args2, const RuntimeMethod* method);
// System.String System.String::Format(System.IFormatProvider,System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mF96F0621DC567D09C07F1EAC66B31AD261A9DC21 (RuntimeObject* ___provider0, String_t* ___format1, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args2, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Int32 System.Environment::get_TickCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Environment_get_TickCount_mBA4279B1C0BC197BF2121166E7C1F6A46D2B5D4E (const RuntimeMethod* method);
// System.Boolean System.String::Equals(System.String,System.String,System.StringComparison)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Equals_mD65682B0BB7933CC7A8561AE34DED02E4F3BBBE5 (String_t* ___a0, String_t* ___b1, int32_t ___comparisonType2, const RuntimeMethod* method);
// System.Xml.SecureStringHasher/HashCodeOfStringDelegate System.Xml.SecureStringHasher::GetHashCodeDelegate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * SecureStringHasher_GetHashCodeDelegate_mB57EBF631C2136DB005BB3A2CA60A9D88222334B (const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method);
// System.Int32 System.Xml.SecureStringHasher/HashCodeOfStringDelegate::Invoke(System.String,System.Int32,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HashCodeOfStringDelegate_Invoke_m29101897DFC3FF4CEC6E0F148E1E879463DA2F9D (HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * __this, String_t* ___s0, int32_t ___sLen1, int64_t ___additionalEntropy2, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70 (String_t* __this, int32_t ___index0, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MethodInfo_t * Type_GetMethod_m2DDEF37DF33D28F579BA8E03F4376B67D91F0DEB (Type_t * __this, String_t* ___name0, int32_t ___bindingAttr1, const RuntimeMethod* method);
// System.Boolean System.Reflection.MethodInfo::op_Inequality(System.Reflection.MethodInfo,System.Reflection.MethodInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MethodInfo_op_Inequality_mDE1DAA5D330E9C975AC6423FC2D06862637BE68D (MethodInfo_t * ___left0, MethodInfo_t * ___right1, const RuntimeMethod* method);
// System.Delegate System.Delegate::CreateDelegate(System.Type,System.Reflection.MethodInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_CreateDelegate_m4B6F4C6A4A3C1B2C55CD01FEA15E0C801CADCB24 (Type_t * ___type0, MethodInfo_t * ___method1, const RuntimeMethod* method);
// System.Void System.Xml.SecureStringHasher/HashCodeOfStringDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HashCodeOfStringDelegate__ctor_m47EFBAB22CC6E0C04A17B8DA7101906596143BE8 (HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Object System.Xml.XmlCharType::get_StaticLock()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * XmlCharType_get_StaticLock_m81A2863AB242D63E36753E695E04EFD800E30F62 (const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4 (RuntimeObject * ___obj0, bool* ___lockTaken1, const RuntimeMethod* method);
// System.Void System.Xml.XmlCharType::SetProperties(System.String,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE (String_t* ___ranges0, uint8_t ___value1, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Exit(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.Xml.XmlCharType::.ctor(System.Byte[])
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void XmlCharType__ctor_mF597588002D63E65BC835ABC21221BB1525A6637_inline (XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___charProperties0, const RuntimeMethod* method);
// System.Void System.Xml.XmlCharType::InitInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlCharType_InitInstance_m59CB9FA98F252136EEB7E364C62849CCC9B7E7AE (const RuntimeMethod* method);
// System.Boolean System.Xml.XmlCharType::InRange(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlCharType_InRange_m53A94066F0586D01CDFA1FCF40BE0352A80826D7 (int32_t ___value0, int32_t ___start1, int32_t ___end2, const RuntimeMethod* method);
// System.Boolean System.Xml.XmlChildEnumerator::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlChildEnumerator_MoveNext_mDA30FD57E7987F22769388EA4E0C84B6D65D455D (XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559 * __this, const RuntimeMethod* method);
// System.Xml.XmlNode System.Xml.XmlChildEnumerator::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * XmlChildEnumerator_get_Current_m9F82A181A0FEE37872C087AAF383C0AC440DB5F5 (XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559 * __this, const RuntimeMethod* method);
// System.String System.Xml.Res::GetString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Res_GetString_m4303E63CA6945331BF5584376F6AA7262D1C6887 (String_t* ___name0, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * __this, String_t* ___message0, const RuntimeMethod* method);
// System.String System.String::Trim(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Trim_m10D967E03EDCB170227406426558B7FEA27CD6CC (String_t* __this, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___trimChars0, const RuntimeMethod* method);
// System.String System.Xml.Res::GetString(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Res_GetString_m1D06B81901B03CA849045926741403907612BB4B (String_t* ___name0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args1, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Xml.XmlException::.ctor(System.String,System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m819800529C3A91E8C5640AAA3F30EBD998238F10 (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, String_t* ___res0, String_t* ___arg1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method);
// System.Void System.Xml.XmlException::.ctor(System.String,System.String[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m546964BF1BBE64E4A2C9853E23F25FC3295D595E (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, String_t* ___res0, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___args1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char,System.Xml.ExceptionType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidSurrogatePairException_m97FE3D009250AE26015577D66CE3A652D787B86B (Il2CppChar ___low0, Il2CppChar ___hi1, int32_t ___exceptionType2, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char,System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidSurrogatePairException_mAD8DA247F0F5CAFAE622A2FC49AA8AD44746125A (Il2CppChar ___low0, Il2CppChar ___hi1, int32_t ___exceptionType2, int32_t ___lineNo3, int32_t ___linePos4, const RuntimeMethod* method);
// System.String System.UInt32::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B (uint32_t* __this, String_t* ___format0, RuntimeObject* ___provider1, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateException(System.String,System.String[],System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateException_mD3F12F95EFAC639FBCB9EB56C4008F5F422380D9 (String_t* ___res0, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___args1, int32_t ___exceptionType2, int32_t ___lineNo3, int32_t ___linePos4, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char,System.Xml.ExceptionType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidHighSurrogateCharException_m7963DACF73B00A03914532EC609524F30AC26F16 (Il2CppChar ___hi0, int32_t ___exceptionType1, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char,System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidHighSurrogateCharException_m430040D295EFE67CF3214A7375E5B2E080EF04AB (Il2CppChar ___hi0, int32_t ___exceptionType1, int32_t ___lineNo2, int32_t ___linePos3, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateException(System.String,System.String,System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateException_m9718225A7133DC0205AE6EC7BC322F4FEAE5A716 (String_t* ___res0, String_t* ___arg1, int32_t ___exceptionType2, int32_t ___lineNo3, int32_t ___linePos4, const RuntimeMethod* method);
// System.Xml.XmlCharType System.Xml.XmlCharType::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  XmlCharType_get_Instance_mA3CFC9BC3797565FD176224C6116F41AC8BA65B5 (const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F (RuntimeArray * ___array0, RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  ___fldHandle1, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter__ctor_m2561A4395C8A92B49271C2D6E6840CEDADC38D7B (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___w0, const RuntimeMethod* method);
// System.Void System.SystemException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SystemException__ctor_m20F619D15EAA349C6CE181A65A47C336200EBD19 (SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62 * __this, SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ___info0, StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  ___context1, const RuntimeMethod* method);
// System.Object System.Runtime.Serialization.SerializationInfo::GetValue(System.String,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * SerializationInfo_GetValue_mF6E311779D55AD7C80B2D19FF2A7E9683AEF2A99 (SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * __this, String_t* ___name0, Type_t * ___type1, const RuntimeMethod* method);
// System.Runtime.Serialization.SerializationInfoEnumerator System.Runtime.Serialization.SerializationInfo::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6 * SerializationInfo_GetEnumerator_m88A3A4E9DD1E1F276016B0205CF62DDB876B9575 (SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * __this, const RuntimeMethod* method);
// System.Runtime.Serialization.SerializationEntry System.Runtime.Serialization.SerializationInfoEnumerator::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E  SerializationInfoEnumerator_get_Current_mD46A02033DA35A55D982FB2B3DD56CE56FDFB314 (SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6 * __this, const RuntimeMethod* method);
// System.String System.Runtime.Serialization.SerializationEntry::get_Name()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* SerializationEntry_get_Name_m1314B9BFC9D6CFCC607FF1B2B748583DAEB1D1E2_inline (SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Object System.Runtime.Serialization.SerializationEntry::get_Value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * SerializationEntry_get_Value_m7D4406AB9EF2F4ADE65FFC23C4F79EB1A3749BD6_inline (SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E * __this, const RuntimeMethod* method);
// System.Boolean System.Runtime.Serialization.SerializationInfoEnumerator::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SerializationInfoEnumerator_MoveNext_m661034C94476113FEB5A3C98A5EA9456ACFA2E9F (SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6 * __this, const RuntimeMethod* method);
// System.String System.Xml.XmlException::CreateMessage(System.String,System.String[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlException_CreateMessage_m222F162AE354DEA3AFD7969544733DD123BA90DB (String_t* ___res0, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___args1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method);
// System.Void System.Exception::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception_GetObjectData_m2031046D41E7BEE3C743E918B358A336F99D6882 (Exception_t * __this, SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ___info0, StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  ___context1, const RuntimeMethod* method);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializationInfo_AddValue_mA50C2668EF700C2239DDC362F8DB409020BB763D (SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * __this, String_t* ___name0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializationInfo_AddValue_m3DF5B182A63FFCD12287E97EA38944D0C6405BB5 (SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * __this, String_t* ___name0, int32_t ___value1, const RuntimeMethod* method);
// System.Void System.Xml.XmlException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m4E096C7446FAB4E4B00FB2D530ED083A91E5C756 (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Xml.XmlException::.ctor(System.String,System.Exception,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m828060C266C3003539491FDB7F9BCEA07A24B352 (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method);
// System.Void System.Xml.XmlException::.ctor(System.String,System.Exception,System.Int32,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m69AC5BDB322F2F6A79D671CB2E262FD0C0482D7B (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___lineNumber2, int32_t ___linePosition3, String_t* ___sourceUri4, const RuntimeMethod* method);
// System.String System.Xml.XmlException::FormatUserMessage(System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlException_FormatUserMessage_m43C6731234AE7C72D5C1C88905B6E4A21C2C4CA5 (String_t* ___message0, int32_t ___lineNumber1, int32_t ___linePosition2, const RuntimeMethod* method);
// System.Void System.SystemException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SystemException__ctor_m14A39C396B94BEE4EFEA201FB748572011855A94 (SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62 * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void System.Exception::set_HResult(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Exception_set_HResult_mB9E603303A0678B32684B0EEC144334BAB0E6392_inline (Exception_t * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Xml.XmlException::.ctor(System.String,System.String[],System.Exception,System.Int32,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_mE47A3D5DED55E0DB0683E5AEB4591BCFBC03731C (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, String_t* ___res0, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___args1, Exception_t * ___innerException2, int32_t ___lineNumber3, int32_t ___linePosition4, String_t* ___sourceUri5, const RuntimeMethod* method);
// System.String System.Int32::ToString(System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m027A8C9419D2FE56ED5D2EE42A6F3B3CE0130471 (int32_t* __this, RuntimeObject* ___provider0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.String System.Exception::get_Message()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Exception_get_Message_mC7A96CEBF52567CEF612C8C75A99A735A83E883F (Exception_t * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlChildEnumerator::.ctor(System.Xml.XmlNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlChildEnumerator__ctor_m05CAB16D0FC521D715119DFE83D2B11C7BB2FD8D (XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559 * __this, XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * ___container0, const RuntimeMethod* method);
// System.Void System.IO.StringWriter::.ctor(System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringWriter__ctor_mDF4AB6FD46E8B9824F2F7A9C26EA086A2C1AE5CF (StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * __this, RuntimeObject* ___formatProvider0, const RuntimeMethod* method);
// System.Void System.Xml.XmlDOMTextWriter::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlDOMTextWriter__ctor_m7A30C41A24817419B3370A576D6B1254EBEC824E (XmlDOMTextWriter_tF813A0468BACD6B3CC21849A38FA6AB269D5A741 * __this, TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___w0, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_m5A81DE19E748F748E19FF13FB6FFD2547F9212D9 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::set_Length(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder_set_Length_m7C1756193B05DCA5A23C5DC98EE90A9FC685A27A (StringBuilder_t * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean System.Xml.XmlCharType::IsLowSurrogate(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlCharType_IsLowSurrogate_m7939EE6AE78DBE960B04D2A8E957E913C2EFC6DC (int32_t ___ch0, const RuntimeMethod* method);
// System.Boolean System.Xml.XmlCharType::IsHighSurrogate(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlCharType_IsHighSurrogate_m05FA9C7BBB540AD6890CCDCC286E6699F7351301 (int32_t ___ch0, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidSurrogatePairException_m0DE4CC9F8620ABC31C61F6FDAFA3BC960F41F170 (Il2CppChar ___low0, Il2CppChar ___hi1, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1 (StringBuilder_t * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::WriteStringFragment(System.String,System.Int32,System.Int32,System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteStringFragment_mE235366F26A1F6FAB51FDDE129A36E5A0A451229 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, String_t* ___str0, int32_t ___offset1, int32_t ___count2, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___helperBuffer3, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::WriteCharEntityImpl(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteCharEntityImpl_m26C18068AD20D6F0B9A5B5F3BB3D9E5A5F61BAD9 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, Il2CppChar ___ch0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::WriteEntityRefImpl(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteEntityRefImpl_m44E81A5273CF2E965652A47A9C5BBEC629E8539E (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::WriteSurrogateChar(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteSurrogateChar_m6D9CFC7F219F339DE22E679418EC68425D7EE20D (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, Il2CppChar ___lowChar0, Il2CppChar ___highChar1, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidHighSurrogateCharException_mD8CAB48E86A39FACE4A0274D91C7065A6458233E (Il2CppChar ___hi0, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97 (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m4B771D7BFE8A65C9A504EC5847A699EB678B02DB (StringBuilder_t * __this, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___value0, int32_t ___startIndex1, int32_t ___charCount2, const RuntimeMethod* method);
// System.Void System.String::CopyTo(System.Int32,System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void String_CopyTo_mF77EF8D4E5ABBD2AB7F509FFE9C0C70DC15A27E1 (String_t* __this, int32_t ___sourceIndex0, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___destination1, int32_t ___destinationIndex2, int32_t ___count3, const RuntimeMethod* method);
// System.Globalization.NumberFormatInfo System.Globalization.NumberFormatInfo::get_InvariantInfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * NumberFormatInfo_get_InvariantInfo_m286BBD095BFCA546BD2CD67E856D1A205436C03F (const RuntimeMethod* method);
// System.String System.Int32::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8 (int32_t* __this, String_t* ___format0, RuntimeObject* ___provider1, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::WriteCharEntityImpl(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteCharEntityImpl_mCE63160635693B3E2C37693BE3401796D86ECAD9 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, String_t* ___strVal0, const RuntimeMethod* method);
// System.Void System.Xml.XmlWriter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriter__ctor_mAA10A026AD230D064314D2AE472A144D18418A01 (XmlWriter_t676293C138D2D0DAB9C1BC16A7BEE618391C5B2D * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter/TagInfo::Init(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagInfo_Init_mFC65CB7C7A7D7852E3A2825365F1BD35CC38F8CF (TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2 * __this, int32_t ___nsTop0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter__ctor_m99C6F457368131185E3DF47E3339A20D2C076357 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder__ctor_m8142AA059B3675FE89F48542A7F013098FCDBCBC (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___textWriter0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::set_QuoteChar(System.Char)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void XmlTextEncoder_set_QuoteChar_m1897FBEC0EAB4616C3FD961A12AFC0F7ED771824_inline (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, Il2CppChar ___value0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::InternalWriteEndElement(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_InternalWriteEndElement_mF39D8CB533A512D50269E722957BA39D259B7D9C (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, bool ___longFormat0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::AutoCompleteAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AutoCompleteAll_m84FCFD43E3ECAE77234E1DA140AB15AE75F7E4DC (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::Indent(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_Indent_m79DEE82659DC5B5F0C4E1E8E3E30C76E7F869851 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, bool ___beforeEndElement0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::WriteEndAttributeQuote()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_WriteEndAttributeQuote_m98AC4165AD8A683AE52D3683F2719E1C8CC578CD (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::WriteEndStartTag(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_WriteEndStartTag_mBA4796BAC09CD5B25F711E65D61BC2BE8375D2F0 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, bool ___empty0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::FlushEncoders()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_FlushEncoders_m6D7554550029EE5407C9149A740B03B138B44C1B (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::AutoComplete(System.Xml.XmlTextWriter/Token)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AutoComplete_mC90A57781E031A2E18047CAF6BF09199323B48D2 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, int32_t ___token0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::PopNamespaces(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_PopNamespaces_mB647C786975F9BB3ADE2301184F8206D816EAFA6 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, int32_t ___indexFrom0, int32_t ___indexTo1, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::StartAttribute(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_StartAttribute_m154B2203CF4424F0E8A6D1D80AC68B2E05FB7B32 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, bool ___cacheAttrValue0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::Write(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_Write_m3D15126BEF6FE4BE6AA1FA9A25860A11649C8E3B (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, String_t* ___text0, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::EndAttribute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_EndAttribute_mEBAE3ABA120AF2728E56ADDB2BE2A5B313093C12 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::HandleSpecialAttribute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_HandleSpecialAttribute_m743156B9137FC8E43B2E697605AAE9C225A2166E (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method);
// System.Int32 System.Xml.XmlTextWriter::LookupNamespace(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t XmlTextWriter_LookupNamespace_mB4568061673EFBC795A43BB701ABE3335AFCE9A2 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, String_t* ___prefix0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::AddNamespace(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AddNamespace_mD3E28DCB8C014B090EB3FFA4C6F0A71702135E5B (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method);
// System.Void System.Array::Copy(System.Array,System.Array,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Copy_m40103AA97DC582C557B912CF4BBE86A4D166F803 (RuntimeArray * ___sourceArray0, RuntimeArray * ___destinationArray1, int32_t ___length2, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter/Namespace::Set(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Namespace_Set_m56A8063D5B2E5989807DF3472D0266CF828BBDA8 (Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::AddToNamespaceHashtable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AddToNamespaceHashtable_m35841EE9201BDF774311ED96E3D3DEDB174D24A6 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, int32_t ___namespaceIndex0, const RuntimeMethod* method);
// System.Void System.Xml.SecureStringHasher::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SecureStringHasher__ctor_mEF97616B6D20B8D66A01B2B833C677BF356A1145 (SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<!0>)
inline void Dictionary_2__ctor_mAC2C8559F9E6A16FAD5BA5017E6A0A1EF3429867 (Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * __this, RuntimeObject* ___comparer0, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 *, RuntimeObject*, const RuntimeMethod*))Dictionary_2__ctor_m2F6EEA5A421346472B973CEC54962959F5A7A7E9_gshared)(__this, ___comparer0, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m8E06A6DF244254BF419D7EC23A9A6809E19A544D (Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * __this, String_t* ___key0, int32_t* ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 *, String_t*, int32_t*, const RuntimeMethod*))Dictionary_2_TryGetValue_mD30FE5F3EA63CD6831E4AA9E6901323436D6F9A4_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_mBC85AF861FB031847847F0B30707EC7AC252D572 (Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * __this, String_t* ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 *, String_t*, int32_t, const RuntimeMethod*))Dictionary_2_set_Item_m9BDED5248054C2E86ECBA732FE7BCDAA32D0A118_gshared)(__this, ___key0, ___value1, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Remove(!0)
inline bool Dictionary_2_Remove_m105F8AD7F5E827D6F95768BD6C922EB155E5A7EF (Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 *, String_t*, const RuntimeMethod*))Dictionary_2_Remove_m1E56E91A9BB341EE54712AF20BAF2ADC09D267A0_gshared)(__this, ___key0, method);
}
// System.String System.Xml.XmlTextEncoder::get_AttributeValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlTextEncoder_get_AttributeValue_m9E57BB8D3D0C4BBA526DFAEC0064BD1C3F3BA38D (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, const RuntimeMethod* method);
// System.String System.Xml.XmlConvert::TrimString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlConvert_TrimString_mF0E4AC16BD05053538B20B21DBD64447195A2D1B (String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::VerifyPrefixXml(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_VerifyPrefixXml_m7729FF5CEA9F697C2B498B672D02C4C895A287FB (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, String_t* ___prefix0, String_t* ___ns1, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::PushNamespace(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_PushNamespace_mFAE3B22D92E1EE3EB61C94917C3E9A955D9CD5C2 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method);
// System.Void System.Xml.Base64Encoder::Flush()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Base64Encoder_Flush_m7C096C39FD6F6EB5AAEBB7C5ED816DD8F24CA5B0 (Base64Encoder_tF36936300CC9B84076C8F16EE4AF8074ACA40CE9 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::WriteRaw(System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteRaw_m5B4E690DB11115A1A2EBF4228182024DD5BB8434 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___array0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.Base64Encoder::Flush()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Base64Encoder_Flush_m7C096C39FD6F6EB5AAEBB7C5ED816DD8F24CA5B0 (Base64Encoder_tF36936300CC9B84076C8F16EE4AF8074ACA40CE9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Base64Encoder_Flush_m7C096C39FD6F6EB5AAEBB7C5ED816DD8F24CA5B0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_leftOverBytesCount_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = __this->get_leftOverBytes_0();
		int32_t L_2 = __this->get_leftOverBytesCount_1();
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_3 = __this->get_charsLine_2();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		int32_t L_4 = Convert_ToBase64CharArray_mD2322C916F75D3414A56A4CECD164D5483636031(L_1, 0, L_2, L_3, 0, /*hidden argument*/NULL);
		V_0 = L_4;
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_5 = __this->get_charsLine_2();
		int32_t L_6 = V_0;
		VirtActionInvoker3< CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*, int32_t, int32_t >::Invoke(4 /* System.Void System.Xml.Base64Encoder::WriteChars(System.Char[],System.Int32,System.Int32) */, __this, L_5, 0, L_6);
		__this->set_leftOverBytesCount_1(0);
	}

IL_0038:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String System.Xml.Res::GetString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Res_GetString_m4303E63CA6945331BF5584376F6AA7262D1C6887 (String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		return L_0;
	}
}
// System.String System.Xml.Res::GetString(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Res_GetString_m1D06B81901B03CA849045926741403907612BB4B (String_t* ___name0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args1, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = ___args1;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		String_t* L_1 = ___name0;
		return L_1;
	}

IL_0005:
	{
		String_t* L_2 = ___name0;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_3 = ___args1;
		String_t* L_4 = SR_GetString_m846F567D256240B005CADDBE65D4793920B6474E(L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String SR::GetString(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SR_GetString_m846F567D256240B005CADDBE65D4793920B6474E (String_t* ___name0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SR_GetString_m846F567D256240B005CADDBE65D4793920B6474E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var);
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_0 = CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164(/*hidden argument*/NULL);
		String_t* L_1 = ___name0;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = ___args1;
		String_t* L_3 = SR_GetString_m6B94153BBBDF13B55321A1E0290B6E82FC8AD248(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String SR::GetString(System.Globalization.CultureInfo,System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SR_GetString_m6B94153BBBDF13B55321A1E0290B6E82FC8AD248 (CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___culture0, String_t* ___name1, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args2, const RuntimeMethod* method)
{
	{
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_0 = ___culture0;
		String_t* L_1 = ___name1;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = ___args2;
		String_t* L_3 = String_Format_mF96F0621DC567D09C07F1EAC66B31AD261A9DC21(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.SecureStringHasher::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SecureStringHasher__ctor_mEF97616B6D20B8D66A01B2B833C677BF356A1145 (SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = Environment_get_TickCount_mBA4279B1C0BC197BF2121166E7C1F6A46D2B5D4E(/*hidden argument*/NULL);
		__this->set_hashCodeRandomizer_1(L_0);
		return;
	}
}
// System.Boolean System.Xml.SecureStringHasher::Equals(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SecureStringHasher_Equals_m4764CCB4F1A4AED78D021A8DF38A26B6C499691A (SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A * __this, String_t* ___x0, String_t* ___y1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___x0;
		String_t* L_1 = ___y1;
		bool L_2 = String_Equals_mD65682B0BB7933CC7A8561AE34DED02E4F3BBBE5(L_0, L_1, 4, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 System.Xml.SecureStringHasher::GetHashCode(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SecureStringHasher_GetHashCode_m06AA20C7C8C7B52D42E039B7EBB1C9409752B54A (SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SecureStringHasher_GetHashCode_m06AA20C7C8C7B52D42E039B7EBB1C9409752B54A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * L_0 = ((SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A_StaticFields*)il2cpp_codegen_static_fields_for(SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A_il2cpp_TypeInfo_var))->get_hashCodeDelegate_0();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * L_1 = SecureStringHasher_GetHashCodeDelegate_mB57EBF631C2136DB005BB3A2CA60A9D88222334B(/*hidden argument*/NULL);
		((SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A_StaticFields*)il2cpp_codegen_static_fields_for(SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A_il2cpp_TypeInfo_var))->set_hashCodeDelegate_0(L_1);
	}

IL_0011:
	{
		HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * L_2 = ((SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A_StaticFields*)il2cpp_codegen_static_fields_for(SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A_il2cpp_TypeInfo_var))->get_hashCodeDelegate_0();
		String_t* L_3 = ___key0;
		String_t* L_4 = ___key0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_4, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_hashCodeRandomizer_1();
		NullCheck(L_2);
		int32_t L_7 = HashCodeOfStringDelegate_Invoke_m29101897DFC3FF4CEC6E0F148E1E879463DA2F9D(L_2, L_3, L_5, (((int64_t)((int64_t)L_6))), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Xml.SecureStringHasher::GetHashCodeOfString(System.String,System.Int32,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SecureStringHasher_GetHashCodeOfString_m38C6DD4A290EE4A73135384834F8F3FA2D9CFB83 (String_t* ___key0, int32_t ___sLen1, int64_t ___additionalEntropy2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int64_t L_0 = ___additionalEntropy2;
		V_0 = (((int32_t)((int32_t)L_0)));
		V_1 = 0;
		goto IL_0019;
	}

IL_0007:
	{
		int32_t L_1 = V_0;
		int32_t L_2 = V_0;
		String_t* L_3 = ___key0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_3, L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2<<(int32_t)7))^(int32_t)L_5))));
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0019:
	{
		int32_t L_7 = V_1;
		String_t* L_8 = ___key0;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0007;
		}
	}
	{
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)((int32_t)((int32_t)L_11>>(int32_t)((int32_t)17)))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)11)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)((int32_t)((int32_t)L_15>>(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.Xml.SecureStringHasher_HashCodeOfStringDelegate System.Xml.SecureStringHasher::GetHashCodeDelegate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * SecureStringHasher_GetHashCodeDelegate_mB57EBF631C2136DB005BB3A2CA60A9D88222334B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SecureStringHasher_GetHashCodeDelegate_mB57EBF631C2136DB005BB3A2CA60A9D88222334B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (String_t_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		MethodInfo_t * L_2 = Type_GetMethod_m2DDEF37DF33D28F579BA8E03F4376B67D91F0DEB(L_1, _stringLiteralD9319D8D06606B8F2B10B57EBAABAE57F7D5B8E7, ((int32_t)40), /*hidden argument*/NULL);
		V_0 = L_2;
		MethodInfo_t * L_3 = V_0;
		bool L_4 = MethodInfo_op_Inequality_mDE1DAA5D330E9C975AC6423FC2D06862637BE68D(L_3, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_5 = { reinterpret_cast<intptr_t> (HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_5, /*hidden argument*/NULL);
		MethodInfo_t * L_7 = V_0;
		Delegate_t * L_8 = Delegate_CreateDelegate_m4B6F4C6A4A3C1B2C55CD01FEA15E0C801CADCB24(L_6, L_7, /*hidden argument*/NULL);
		return ((HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 *)CastclassSealed((RuntimeObject*)L_8, HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574_il2cpp_TypeInfo_var));
	}

IL_0036:
	{
		HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * L_9 = (HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 *)il2cpp_codegen_object_new(HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574_il2cpp_TypeInfo_var);
		HashCodeOfStringDelegate__ctor_m47EFBAB22CC6E0C04A17B8DA7101906596143BE8(L_9, NULL, (intptr_t)((intptr_t)SecureStringHasher_GetHashCodeOfString_m38C6DD4A290EE4A73135384834F8F3FA2D9CFB83_RuntimeMethod_var), /*hidden argument*/NULL);
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: System.Xml.XmlCharType
IL2CPP_EXTERN_C void XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshal_pinvoke(const XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA& unmarshaled, XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshaled_pinvoke& marshaled)
{
	marshaled.___charProperties_2 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.get_charProperties_2());
}
IL2CPP_EXTERN_C void XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshal_pinvoke_back(const XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshaled_pinvoke& marshaled, XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_charProperties_2((ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var, marshaled.___charProperties_2));
}
// Conversion method for clean up from marshalling of: System.Xml.XmlCharType
IL2CPP_EXTERN_C void XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshal_pinvoke_cleanup(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_com_destroy_safe_array(marshaled.___charProperties_2);
	marshaled.___charProperties_2 = NULL;
}
// Conversion methods for marshalling of: System.Xml.XmlCharType
IL2CPP_EXTERN_C void XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshal_com(const XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA& unmarshaled, XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshaled_com& marshaled)
{
	marshaled.___charProperties_2 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.get_charProperties_2());
}
IL2CPP_EXTERN_C void XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshal_com_back(const XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshaled_com& marshaled, XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_charProperties_2((ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var, marshaled.___charProperties_2));
}
// Conversion method for clean up from marshalling of: System.Xml.XmlCharType
IL2CPP_EXTERN_C void XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshal_com_cleanup(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_marshaled_com& marshaled)
{
	il2cpp_codegen_com_destroy_safe_array(marshaled.___charProperties_2);
	marshaled.___charProperties_2 = NULL;
}
// System.Object System.Xml.XmlCharType::get_StaticLock()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * XmlCharType_get_StaticLock_m81A2863AB242D63E36753E695E04EFD800E30F62 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_get_StaticLock_m81A2863AB242D63E36753E695E04EFD800E30F62_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = ((XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_il2cpp_TypeInfo_var))->get_s_Lock_0();
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject * L_1 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		InterlockedCompareExchangeImpl<RuntimeObject *>((RuntimeObject **)(((XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_il2cpp_TypeInfo_var))->get_address_of_s_Lock_0()), L_2, NULL);
	}

IL_001a:
	{
		RuntimeObject * L_3 = ((XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_il2cpp_TypeInfo_var))->get_s_Lock_0();
		return L_3;
	}
}
// System.Void System.Xml.XmlCharType::InitInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlCharType_InitInstance_m59CB9FA98F252136EEB7E364C62849CCC9B7E7AE (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_InitInstance_m59CB9FA98F252136EEB7E364C62849CCC9B7E7AE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		RuntimeObject * L_0 = XmlCharType_get_StaticLock_m81A2863AB242D63E36753E695E04EFD800E30F62(/*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_1 = V_0;
			Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4(L_1, (bool*)(&V_1), /*hidden argument*/NULL);
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = ((XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_il2cpp_TypeInfo_var))->get_s_CharProperties_1();
			il2cpp_codegen_memory_barrier();
			if (!L_2)
			{
				goto IL_001b;
			}
		}

IL_0019:
		{
			IL2CPP_LEAVE(0x97, FINALLY_008d);
		}

IL_001b:
		{
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)65536));
			il2cpp_codegen_memory_barrier();
			((XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_il2cpp_TypeInfo_var))->set_s_CharProperties_1(L_3);
			XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE(_stringLiteral2309CFA4F156DCCB12A14E727DCE560E1426B532, (uint8_t)1, /*hidden argument*/NULL);
			XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE(_stringLiteral0E0426234071F961E725D9438C6D5A048A0394B3, (uint8_t)2, /*hidden argument*/NULL);
			XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE(_stringLiteralCC5753B4554091FA687FA64F4FA303B0C913E2EC, (uint8_t)4, /*hidden argument*/NULL);
			XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE(_stringLiteral02E5C3B8AD72F3FC46F6C829AB1FEDDC9B115473, (uint8_t)8, /*hidden argument*/NULL);
			XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE(_stringLiteralFDA1C9397356AF77AAE64869A9D6288562F10D4C, (uint8_t)((int32_t)16), /*hidden argument*/NULL);
			XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE(_stringLiteral02E5C3B8AD72F3FC46F6C829AB1FEDDC9B115473, (uint8_t)((int32_t)32), /*hidden argument*/NULL);
			XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE(_stringLiteral94A9AE3A92C04F7400ADC94B7BCB73FFC0A08DE4, (uint8_t)((int32_t)64), /*hidden argument*/NULL);
			XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE(_stringLiteral81ABCB006928101D1882FA2FBB7BFA00FE053221, (uint8_t)((int32_t)128), /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x97, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_4 = V_1;
			if (!L_4)
			{
				goto IL_0096;
			}
		}

IL_0090:
		{
			RuntimeObject * L_5 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A(L_5, /*hidden argument*/NULL);
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0097:
	{
		return;
	}
}
// System.Void System.Xml.XmlCharType::SetProperties(System.String,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE (String_t* ___ranges0, uint8_t ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_SetProperties_m95495DAAF50A803B8B25CF4DB4A6A34A07B04DAE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		goto IL_0037;
	}

IL_0004:
	{
		String_t* L_0 = ___ranges0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		String_t* L_3 = ___ranges0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_3, ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)), /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_002f;
	}

IL_0018:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = ((XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_il2cpp_TypeInfo_var))->get_s_CharProperties_1();
		il2cpp_codegen_memory_barrier();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		uint8_t* L_8 = ((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)));
		int32_t L_9 = *((uint8_t*)L_8);
		uint8_t L_10 = ___value1;
		*((int8_t*)L_8) = (int8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_9|(int32_t)L_10)))));
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_002f:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_2;
		if ((((int32_t)L_12) <= ((int32_t)L_13)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)2));
	}

IL_0037:
	{
		int32_t L_15 = V_0;
		String_t* L_16 = ___ranges0;
		NullCheck(L_16);
		int32_t L_17 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void System.Xml.XmlCharType::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlCharType__ctor_mF597588002D63E65BC835ABC21221BB1525A6637 (XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___charProperties0, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___charProperties0;
		__this->set_charProperties_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void XmlCharType__ctor_mF597588002D63E65BC835ABC21221BB1525A6637_AdjustorThunk (RuntimeObject * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___charProperties0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA * _thisAdjusted = reinterpret_cast<XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA *>(__this + _offset);
	XmlCharType__ctor_mF597588002D63E65BC835ABC21221BB1525A6637_inline(_thisAdjusted, ___charProperties0, method);
}
// System.Xml.XmlCharType System.Xml.XmlCharType::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  XmlCharType_get_Instance_mA3CFC9BC3797565FD176224C6116F41AC8BA65B5 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_get_Instance_mA3CFC9BC3797565FD176224C6116F41AC8BA65B5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ((XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_il2cpp_TypeInfo_var))->get_s_CharProperties_1();
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		XmlCharType_InitInstance_m59CB9FA98F252136EEB7E364C62849CCC9B7E7AE(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ((XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA_il2cpp_TypeInfo_var))->get_s_CharProperties_1();
		il2cpp_codegen_memory_barrier();
		XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  L_2;
		memset((&L_2), 0, sizeof(L_2));
		XmlCharType__ctor_mF597588002D63E65BC835ABC21221BB1525A6637_inline((&L_2), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Xml.XmlCharType::IsHighSurrogate(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlCharType_IsHighSurrogate_m05FA9C7BBB540AD6890CCDCC286E6699F7351301 (int32_t ___ch0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___ch0;
		bool L_1 = XmlCharType_InRange_m53A94066F0586D01CDFA1FCF40BE0352A80826D7(L_0, ((int32_t)55296), ((int32_t)56319), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XmlCharType::IsLowSurrogate(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlCharType_IsLowSurrogate_m7939EE6AE78DBE960B04D2A8E957E913C2EFC6DC (int32_t ___ch0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___ch0;
		bool L_1 = XmlCharType_InRange_m53A94066F0586D01CDFA1FCF40BE0352A80826D7(L_0, ((int32_t)56320), ((int32_t)57343), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XmlCharType::InRange(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlCharType_InRange_m53A94066F0586D01CDFA1FCF40BE0352A80826D7 (int32_t ___value0, int32_t ___start1, int32_t ___end2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = ___start1;
		int32_t L_2 = ___end2;
		int32_t L_3 = ___start1;
		return (bool)((((int32_t)((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1))) <= ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)L_3)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlChildEnumerator::.ctor(System.Xml.XmlNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlChildEnumerator__ctor_m05CAB16D0FC521D715119DFE83D2B11C7BB2FD8D (XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559 * __this, XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * ___container0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_0 = ___container0;
		__this->set_container_0(L_0);
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_1 = ___container0;
		NullCheck(L_1);
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_2 = VirtFuncInvoker0< XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * >::Invoke(6 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_1);
		__this->set_child_1(L_2);
		__this->set_isFirst_2((bool)1);
		return;
	}
}
// System.Boolean System.Xml.XmlChildEnumerator::System.Collections.IEnumerator.MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlChildEnumerator_System_Collections_IEnumerator_MoveNext_m671746486A0F64892351850041F6EAAABD95ABA7 (XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = XmlChildEnumerator_MoveNext_mDA30FD57E7987F22769388EA4E0C84B6D65D455D(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XmlChildEnumerator::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlChildEnumerator_MoveNext_mDA30FD57E7987F22769388EA4E0C84B6D65D455D (XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isFirst_2();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_1 = __this->get_container_0();
		NullCheck(L_1);
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_2 = VirtFuncInvoker0< XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * >::Invoke(6 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_1);
		__this->set_child_1(L_2);
		__this->set_isFirst_2((bool)0);
		goto IL_003b;
	}

IL_0022:
	{
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_3 = __this->get_child_1();
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_4 = __this->get_child_1();
		NullCheck(L_4);
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_5 = VirtFuncInvoker0< XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * >::Invoke(5 /* System.Xml.XmlNode System.Xml.XmlNode::get_NextSibling() */, L_4);
		__this->set_child_1(L_5);
	}

IL_003b:
	{
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_6 = __this->get_child_1();
		return (bool)((!(((RuntimeObject*)(XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 *)L_6) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Void System.Xml.XmlChildEnumerator::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlChildEnumerator_System_Collections_IEnumerator_Reset_m0F5589DECEFC71D36C32BC2C07F27704A7F37E8A (XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559 * __this, const RuntimeMethod* method)
{
	{
		__this->set_isFirst_2((bool)1);
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_0 = __this->get_container_0();
		NullCheck(L_0);
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_1 = VirtFuncInvoker0< XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * >::Invoke(6 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_0);
		__this->set_child_1(L_1);
		return;
	}
}
// System.Object System.Xml.XmlChildEnumerator::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * XmlChildEnumerator_System_Collections_IEnumerator_get_Current_m85F1ED174E2102798DC18B34AC277EC77E419863 (XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559 * __this, const RuntimeMethod* method)
{
	{
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_0 = XmlChildEnumerator_get_Current_m9F82A181A0FEE37872C087AAF383C0AC440DB5F5(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Xml.XmlNode System.Xml.XmlChildEnumerator::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * XmlChildEnumerator_get_Current_m9F82A181A0FEE37872C087AAF383C0AC440DB5F5 (XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlChildEnumerator_get_Current_m9F82A181A0FEE37872C087AAF383C0AC440DB5F5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_isFirst_2();
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_1 = __this->get_child_1();
		if (L_1)
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		String_t* L_2 = Res_GetString_m4303E63CA6945331BF5584376F6AA7262D1C6887(_stringLiteralFBD1A9745DCC4D80697D44756437CCB57698AE21, /*hidden argument*/NULL);
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_3 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, XmlChildEnumerator_get_Current_m9F82A181A0FEE37872C087AAF383C0AC440DB5F5_RuntimeMethod_var);
	}

IL_0020:
	{
		XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * L_4 = __this->get_child_1();
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String System.Xml.XmlConvert::TrimString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlConvert_TrimString_mF0E4AC16BD05053538B20B21DBD64447195A2D1B (String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_TrimString_mF0E4AC16BD05053538B20B21DBD64447195A2D1B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_1 = ((XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var))->get_WhitespaceChars_3();
		NullCheck(L_0);
		String_t* L_2 = String_Trim_m10D967E03EDCB170227406426558B7FEA27CD6CC(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Exception System.Xml.XmlConvert::CreateException(System.String,System.String,System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateException_m9718225A7133DC0205AE6EC7BC322F4FEAE5A716 (String_t* ___res0, String_t* ___arg1, int32_t ___exceptionType2, int32_t ___lineNo3, int32_t ___linePos4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateException_m9718225A7133DC0205AE6EC7BC322F4FEAE5A716_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___exceptionType2;
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___exceptionType2;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_001f;
	}

IL_0009:
	{
		String_t* L_2 = ___res0;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_3 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_4 = L_3;
		String_t* L_5 = ___arg1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		String_t* L_6 = Res_GetString_m1D06B81901B03CA849045926741403907612BB4B(L_2, L_4, /*hidden argument*/NULL);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_7 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_7, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_001f:
	{
		String_t* L_8 = ___res0;
		String_t* L_9 = ___arg1;
		int32_t L_10 = ___lineNo3;
		int32_t L_11 = ___linePos4;
		XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * L_12 = (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 *)il2cpp_codegen_object_new(XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918_il2cpp_TypeInfo_var);
		XmlException__ctor_m819800529C3A91E8C5640AAA3F30EBD998238F10(L_12, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Exception System.Xml.XmlConvert::CreateException(System.String,System.String[],System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateException_mD3F12F95EFAC639FBCB9EB56C4008F5F422380D9 (String_t* ___res0, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___args1, int32_t ___exceptionType2, int32_t ___lineNo3, int32_t ___linePos4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateException_mD3F12F95EFAC639FBCB9EB56C4008F5F422380D9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___exceptionType2;
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___exceptionType2;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0016;
		}
	}
	{
		goto IL_0016;
	}

IL_0009:
	{
		String_t* L_2 = ___res0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = ___args1;
		String_t* L_4 = Res_GetString_m1D06B81901B03CA849045926741403907612BB4B(L_2, (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_3, /*hidden argument*/NULL);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_5 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0016:
	{
		String_t* L_6 = ___res0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = ___args1;
		int32_t L_8 = ___lineNo3;
		int32_t L_9 = ___linePos4;
		XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * L_10 = (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 *)il2cpp_codegen_object_new(XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918_il2cpp_TypeInfo_var);
		XmlException__ctor_m546964BF1BBE64E4A2C9853E23F25FC3295D595E(L_10, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidSurrogatePairException_m0DE4CC9F8620ABC31C61F6FDAFA3BC960F41F170 (Il2CppChar ___low0, Il2CppChar ___hi1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateInvalidSurrogatePairException_m0DE4CC9F8620ABC31C61F6FDAFA3BC960F41F170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___low0;
		Il2CppChar L_1 = ___hi1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var);
		Exception_t * L_2 = XmlConvert_CreateInvalidSurrogatePairException_m97FE3D009250AE26015577D66CE3A652D787B86B(L_0, L_1, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char,System.Xml.ExceptionType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidSurrogatePairException_m97FE3D009250AE26015577D66CE3A652D787B86B (Il2CppChar ___low0, Il2CppChar ___hi1, int32_t ___exceptionType2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateInvalidSurrogatePairException_m97FE3D009250AE26015577D66CE3A652D787B86B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___low0;
		Il2CppChar L_1 = ___hi1;
		int32_t L_2 = ___exceptionType2;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var);
		Exception_t * L_3 = XmlConvert_CreateInvalidSurrogatePairException_mAD8DA247F0F5CAFAE622A2FC49AA8AD44746125A(L_0, L_1, L_2, 0, 0, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char,System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidSurrogatePairException_mAD8DA247F0F5CAFAE622A2FC49AA8AD44746125A (Il2CppChar ___low0, Il2CppChar ___hi1, int32_t ___exceptionType2, int32_t ___lineNo3, int32_t ___linePos4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateInvalidSurrogatePairException_mAD8DA247F0F5CAFAE622A2FC49AA8AD44746125A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* V_0 = NULL;
	uint32_t V_1 = 0;
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)2);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		Il2CppChar L_2 = ___hi1;
		V_1 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var);
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_3 = CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164(/*hidden argument*/NULL);
		String_t* L_4 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)(&V_1), _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = L_1;
		Il2CppChar L_6 = ___low0;
		V_1 = L_6;
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_7 = CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164(/*hidden argument*/NULL);
		String_t* L_8 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)(&V_1), _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_8);
		V_0 = L_5;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = V_0;
		int32_t L_10 = ___exceptionType2;
		int32_t L_11 = ___lineNo3;
		int32_t L_12 = ___linePos4;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var);
		Exception_t * L_13 = XmlConvert_CreateException_mD3F12F95EFAC639FBCB9EB56C4008F5F422380D9(_stringLiteralF91B9F4F51199474999FD40AF7DB23C33195BC0B, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidHighSurrogateCharException_mD8CAB48E86A39FACE4A0274D91C7065A6458233E (Il2CppChar ___hi0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateInvalidHighSurrogateCharException_mD8CAB48E86A39FACE4A0274D91C7065A6458233E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___hi0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var);
		Exception_t * L_1 = XmlConvert_CreateInvalidHighSurrogateCharException_m7963DACF73B00A03914532EC609524F30AC26F16(L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char,System.Xml.ExceptionType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidHighSurrogateCharException_m7963DACF73B00A03914532EC609524F30AC26F16 (Il2CppChar ___hi0, int32_t ___exceptionType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateInvalidHighSurrogateCharException_m7963DACF73B00A03914532EC609524F30AC26F16_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___hi0;
		int32_t L_1 = ___exceptionType1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var);
		Exception_t * L_2 = XmlConvert_CreateInvalidHighSurrogateCharException_m430040D295EFE67CF3214A7375E5B2E080EF04AB(L_0, L_1, 0, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char,System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidHighSurrogateCharException_m430040D295EFE67CF3214A7375E5B2E080EF04AB (Il2CppChar ___hi0, int32_t ___exceptionType1, int32_t ___lineNo2, int32_t ___linePos3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateInvalidHighSurrogateCharException_m430040D295EFE67CF3214A7375E5B2E080EF04AB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		Il2CppChar L_0 = ___hi0;
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var);
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_1 = CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164(/*hidden argument*/NULL);
		String_t* L_2 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)(&V_0), _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___exceptionType1;
		int32_t L_4 = ___lineNo2;
		int32_t L_5 = ___linePos3;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var);
		Exception_t * L_6 = XmlConvert_CreateException_m9718225A7133DC0205AE6EC7BC322F4FEAE5A716(_stringLiteralF4ED3BDF7EB24796E98DB0F3022D762D2C5EBFB0, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void System.Xml.XmlConvert::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlConvert__cctor_m702BEEB571815B9886A2C46DD223B7C96058857E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert__cctor_m702BEEB571815B9886A2C46DD223B7C96058857E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  L_0 = XmlCharType_get_Instance_mA3CFC9BC3797565FD176224C6116F41AC8BA65B5(/*hidden argument*/NULL);
		((XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var))->set_xmlCharType_0(L_0);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_1 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)3);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_2 = L_1;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_3 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E____5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_2, L_3, /*hidden argument*/NULL);
		((XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var))->set_crt_1(L_2);
		((XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var))->set_c_EncodedCharLength_2(7);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_4 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)4);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_5 = L_4;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_6 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E____EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_5, L_6, /*hidden argument*/NULL);
		((XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var))->set_WhitespaceChars_3(L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlDOMTextWriter::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlDOMTextWriter__ctor_m7A30C41A24817419B3370A576D6B1254EBEC824E (XmlDOMTextWriter_tF813A0468BACD6B3CC21849A38FA6AB269D5A741 * __this, TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___w0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlDOMTextWriter__ctor_m7A30C41A24817419B3370A576D6B1254EBEC824E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_0 = ___w0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var);
		XmlTextWriter__ctor_m2561A4395C8A92B49271C2D6E6840CEDADC38D7B(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m6AA71014CB31FB0C9DF0EDE841514F97B7E11B93 (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ___info0, StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlException__ctor_m6AA71014CB31FB0C9DF0EDE841514F97B7E11B93_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6 * V_1 = NULL;
	SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	String_t* V_3 = NULL;
	{
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_0 = ___info0;
		StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  L_1 = ___context1;
		SystemException__ctor_m20F619D15EAA349C6CE181A65A47C336200EBD19(__this, L_0, L_1, /*hidden argument*/NULL);
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_2 = ___info0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_3 = { reinterpret_cast<intptr_t> (String_t_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		RuntimeObject * L_5 = SerializationInfo_GetValue_mF6E311779D55AD7C80B2D19FF2A7E9683AEF2A99(L_2, _stringLiteral9F0051E3370AA31B69F5CBF0E35FBE94ACAE0651, L_4, /*hidden argument*/NULL);
		__this->set_res_17(((String_t*)CastclassSealed((RuntimeObject*)L_5, String_t_il2cpp_TypeInfo_var)));
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_6 = ___info0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_7 = { reinterpret_cast<intptr_t> (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_0_0_0_var) };
		Type_t * L_8 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		RuntimeObject * L_9 = SerializationInfo_GetValue_mF6E311779D55AD7C80B2D19FF2A7E9683AEF2A99(L_6, _stringLiteralEDC12722FE0763003109C7EDBACB6977C0E31132, L_8, /*hidden argument*/NULL);
		__this->set_args_18(((StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)Castclass((RuntimeObject*)L_9, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var)));
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_10 = ___info0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_11 = { reinterpret_cast<intptr_t> (Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var) };
		Type_t * L_12 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		RuntimeObject * L_13 = SerializationInfo_GetValue_mF6E311779D55AD7C80B2D19FF2A7E9683AEF2A99(L_10, _stringLiteral0BA9045FCB28C8C8B2ACED641BB5013BAC05D492, L_12, /*hidden argument*/NULL);
		__this->set_lineNumber_19(((*(int32_t*)((int32_t*)UnBox(L_13, Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var)))));
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_14 = ___info0;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_15 = { reinterpret_cast<intptr_t> (Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var) };
		Type_t * L_16 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		RuntimeObject * L_17 = SerializationInfo_GetValue_mF6E311779D55AD7C80B2D19FF2A7E9683AEF2A99(L_14, _stringLiteralE1356901E9F96E6ACC91632889AE94286AFE4CD4, L_16, /*hidden argument*/NULL);
		__this->set_linePosition_20(((*(int32_t*)((int32_t*)UnBox(L_17, Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var)))));
		String_t* L_18 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_sourceUri_21(L_18);
		V_0 = (String_t*)NULL;
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_19 = ___info0;
		NullCheck(L_19);
		SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6 * L_20 = SerializationInfo_GetEnumerator_m88A3A4E9DD1E1F276016B0205CF62DDB876B9575(L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		goto IL_00ea;
	}

IL_009e:
	{
		SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6 * L_21 = V_1;
		NullCheck(L_21);
		SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E  L_22 = SerializationInfoEnumerator_get_Current_mD46A02033DA35A55D982FB2B3DD56CE56FDFB314(L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		String_t* L_23 = SerializationEntry_get_Name_m1314B9BFC9D6CFCC607FF1B2B748583DAEB1D1E2_inline((SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E *)(&V_2), /*hidden argument*/NULL);
		V_3 = L_23;
		String_t* L_24 = V_3;
		bool L_25 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_24, _stringLiteral47EB169AEE74B181812673783C66416B39F5F464, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00c9;
		}
	}
	{
		String_t* L_26 = V_3;
		bool L_27 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_26, _stringLiteralD2D2F8D3F9F04A081FFBE6B2AF7917BAAADFC052, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00dd;
		}
	}
	{
		goto IL_00ea;
	}

IL_00c9:
	{
		RuntimeObject * L_28 = SerializationEntry_get_Value_m7D4406AB9EF2F4ADE65FFC23C4F79EB1A3749BD6_inline((SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E *)(&V_2), /*hidden argument*/NULL);
		__this->set_sourceUri_21(((String_t*)CastclassSealed((RuntimeObject*)L_28, String_t_il2cpp_TypeInfo_var)));
		goto IL_00ea;
	}

IL_00dd:
	{
		RuntimeObject * L_29 = SerializationEntry_get_Value_m7D4406AB9EF2F4ADE65FFC23C4F79EB1A3749BD6_inline((SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E *)(&V_2), /*hidden argument*/NULL);
		V_0 = ((String_t*)CastclassSealed((RuntimeObject*)L_29, String_t_il2cpp_TypeInfo_var));
	}

IL_00ea:
	{
		SerializationInfoEnumerator_t0548359AF7DB5798EBA19FE6BFCC8CDB8E6B1AF6 * L_30 = V_1;
		NullCheck(L_30);
		bool L_31 = SerializationInfoEnumerator_MoveNext_m661034C94476113FEB5A3C98A5EA9456ACFA2E9F(L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_009e;
		}
	}
	{
		String_t* L_32 = V_0;
		if (L_32)
		{
			goto IL_0119;
		}
	}
	{
		String_t* L_33 = __this->get_res_17();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_34 = __this->get_args_18();
		int32_t L_35 = __this->get_lineNumber_19();
		int32_t L_36 = __this->get_linePosition_20();
		String_t* L_37 = XmlException_CreateMessage_m222F162AE354DEA3AFD7969544733DD123BA90DB(L_33, L_34, L_35, L_36, /*hidden argument*/NULL);
		__this->set_message_22(L_37);
		return;
	}

IL_0119:
	{
		__this->set_message_22((String_t*)NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException_GetObjectData_m792FDA84BA189F6AB82F9AD4B0DF800DF9051DF4 (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ___info0, StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlException_GetObjectData_m792FDA84BA189F6AB82F9AD4B0DF800DF9051DF4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_0 = ___info0;
		StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  L_1 = ___context1;
		Exception_GetObjectData_m2031046D41E7BEE3C743E918B358A336F99D6882(__this, L_0, L_1, /*hidden argument*/NULL);
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_2 = ___info0;
		String_t* L_3 = __this->get_res_17();
		NullCheck(L_2);
		SerializationInfo_AddValue_mA50C2668EF700C2239DDC362F8DB409020BB763D(L_2, _stringLiteral9F0051E3370AA31B69F5CBF0E35FBE94ACAE0651, L_3, /*hidden argument*/NULL);
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_4 = ___info0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = __this->get_args_18();
		NullCheck(L_4);
		SerializationInfo_AddValue_mA50C2668EF700C2239DDC362F8DB409020BB763D(L_4, _stringLiteralEDC12722FE0763003109C7EDBACB6977C0E31132, (RuntimeObject *)(RuntimeObject *)L_5, /*hidden argument*/NULL);
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_6 = ___info0;
		int32_t L_7 = __this->get_lineNumber_19();
		NullCheck(L_6);
		SerializationInfo_AddValue_m3DF5B182A63FFCD12287E97EA38944D0C6405BB5(L_6, _stringLiteral0BA9045FCB28C8C8B2ACED641BB5013BAC05D492, L_7, /*hidden argument*/NULL);
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_8 = ___info0;
		int32_t L_9 = __this->get_linePosition_20();
		NullCheck(L_8);
		SerializationInfo_AddValue_m3DF5B182A63FFCD12287E97EA38944D0C6405BB5(L_8, _stringLiteralE1356901E9F96E6ACC91632889AE94286AFE4CD4, L_9, /*hidden argument*/NULL);
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_10 = ___info0;
		String_t* L_11 = __this->get_sourceUri_21();
		NullCheck(L_10);
		SerializationInfo_AddValue_mA50C2668EF700C2239DDC362F8DB409020BB763D(L_10, _stringLiteral47EB169AEE74B181812673783C66416B39F5F464, L_11, /*hidden argument*/NULL);
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_12 = ___info0;
		NullCheck(L_12);
		SerializationInfo_AddValue_mA50C2668EF700C2239DDC362F8DB409020BB763D(L_12, _stringLiteralD2D2F8D3F9F04A081FFBE6B2AF7917BAAADFC052, _stringLiteral4A962F7AAEC3B50EF4B2CD52A7A2C969B759A960, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_mCADC349BBEB808A35829D83028A25685CF19B902 (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, const RuntimeMethod* method)
{
	{
		XmlException__ctor_m4E096C7446FAB4E4B00FB2D530ED083A91E5C756(__this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m4E096C7446FAB4E4B00FB2D530ED083A91E5C756 (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		XmlException__ctor_m828060C266C3003539491FDB7F9BCEA07A24B352(__this, L_0, (Exception_t *)NULL, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor(System.String,System.Exception,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m828060C266C3003539491FDB7F9BCEA07A24B352 (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerException1;
		int32_t L_2 = ___lineNumber2;
		int32_t L_3 = ___linePosition3;
		XmlException__ctor_m69AC5BDB322F2F6A79D671CB2E262FD0C0482D7B(__this, L_0, L_1, L_2, L_3, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor(System.String,System.Exception,System.Int32,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m69AC5BDB322F2F6A79D671CB2E262FD0C0482D7B (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___lineNumber2, int32_t ___linePosition3, String_t* ___sourceUri4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlException__ctor_m69AC5BDB322F2F6A79D671CB2E262FD0C0482D7B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * G_B2_0 = NULL;
	XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * G_B3_1 = NULL;
	{
		String_t* L_0 = ___message0;
		int32_t L_1 = ___lineNumber2;
		int32_t L_2 = ___linePosition3;
		String_t* L_3 = XmlException_FormatUserMessage_m43C6731234AE7C72D5C1C88905B6E4A21C2C4CA5(L_0, L_1, L_2, /*hidden argument*/NULL);
		Exception_t * L_4 = ___innerException1;
		SystemException__ctor_m14A39C396B94BEE4EFEA201FB748572011855A94(__this, L_3, L_4, /*hidden argument*/NULL);
		Exception_set_HResult_mB9E603303A0678B32684B0EEC144334BAB0E6392_inline(__this, ((int32_t)-2146232000), /*hidden argument*/NULL);
		String_t* L_5 = ___message0;
		G_B1_0 = __this;
		if (!L_5)
		{
			G_B2_0 = __this;
			goto IL_0026;
		}
	}
	{
		G_B3_0 = _stringLiteral23114468D04FA2B7A2DA455B545DB914D0A3ED94;
		G_B3_1 = G_B1_0;
		goto IL_002b;
	}

IL_0026:
	{
		G_B3_0 = _stringLiteral872479F10B4968F255B608A3698CA1A7EE09888B;
		G_B3_1 = G_B2_0;
	}

IL_002b:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_res_17(G_B3_0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = L_6;
		String_t* L_8 = ___message0;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_8);
		__this->set_args_18(L_7);
		String_t* L_9 = ___sourceUri4;
		__this->set_sourceUri_21(L_9);
		int32_t L_10 = ___lineNumber2;
		__this->set_lineNumber_19(L_10);
		int32_t L_11 = ___linePosition3;
		__this->set_linePosition_20(L_11);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor(System.String,System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m819800529C3A91E8C5640AAA3F30EBD998238F10 (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, String_t* ___res0, String_t* ___arg1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlException__ctor_m819800529C3A91E8C5640AAA3F30EBD998238F10_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___res0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		String_t* L_3 = ___arg1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_3);
		int32_t L_4 = ___lineNumber2;
		int32_t L_5 = ___linePosition3;
		XmlException__ctor_mE47A3D5DED55E0DB0683E5AEB4591BCFBC03731C(__this, L_0, L_2, (Exception_t *)NULL, L_4, L_5, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor(System.String,System.String[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m546964BF1BBE64E4A2C9853E23F25FC3295D595E (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, String_t* ___res0, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___args1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___res0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___args1;
		int32_t L_2 = ___lineNumber2;
		int32_t L_3 = ___linePosition3;
		XmlException__ctor_mE47A3D5DED55E0DB0683E5AEB4591BCFBC03731C(__this, L_0, L_1, (Exception_t *)NULL, L_2, L_3, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor(System.String,System.String[],System.Exception,System.Int32,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_mE47A3D5DED55E0DB0683E5AEB4591BCFBC03731C (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, String_t* ___res0, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___args1, Exception_t * ___innerException2, int32_t ___lineNumber3, int32_t ___linePosition4, String_t* ___sourceUri5, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___res0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___args1;
		int32_t L_2 = ___lineNumber3;
		int32_t L_3 = ___linePosition4;
		String_t* L_4 = XmlException_CreateMessage_m222F162AE354DEA3AFD7969544733DD123BA90DB(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		Exception_t * L_5 = ___innerException2;
		SystemException__ctor_m14A39C396B94BEE4EFEA201FB748572011855A94(__this, L_4, L_5, /*hidden argument*/NULL);
		Exception_set_HResult_mB9E603303A0678B32684B0EEC144334BAB0E6392_inline(__this, ((int32_t)-2146232000), /*hidden argument*/NULL);
		String_t* L_6 = ___res0;
		__this->set_res_17(L_6);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = ___args1;
		__this->set_args_18(L_7);
		String_t* L_8 = ___sourceUri5;
		__this->set_sourceUri_21(L_8);
		int32_t L_9 = ___lineNumber3;
		__this->set_lineNumber_19(L_9);
		int32_t L_10 = ___linePosition4;
		__this->set_linePosition_20(L_10);
		return;
	}
}
// System.String System.Xml.XmlException::FormatUserMessage(System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlException_FormatUserMessage_m43C6731234AE7C72D5C1C88905B6E4A21C2C4CA5 (String_t* ___message0, int32_t ___lineNumber1, int32_t ___linePosition2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlException_FormatUserMessage_m43C6731234AE7C72D5C1C88905B6E4A21C2C4CA5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ___lineNumber1;
		int32_t L_2 = ___linePosition2;
		String_t* L_3 = XmlException_CreateMessage_m222F162AE354DEA3AFD7969544733DD123BA90DB(_stringLiteral872479F10B4968F255B608A3698CA1A7EE09888B, (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)NULL, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0011:
	{
		int32_t L_4 = ___lineNumber1;
		if (L_4)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_5 = ___linePosition2;
		if (L_5)
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_6 = ___message0;
		return L_6;
	}

IL_0019:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_7;
		String_t* L_9 = ___message0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_9);
		int32_t L_10 = ___lineNumber1;
		int32_t L_11 = ___linePosition2;
		String_t* L_12 = XmlException_CreateMessage_m222F162AE354DEA3AFD7969544733DD123BA90DB(_stringLiteral23114468D04FA2B7A2DA455B545DB914D0A3ED94, L_8, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String System.Xml.XmlException::CreateMessage(System.String,System.String[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlException_CreateMessage_m222F162AE354DEA3AFD7969544733DD123BA90DB (String_t* ___res0, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___args1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlException_CreateMessage_m222F162AE354DEA3AFD7969544733DD123BA90DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___lineNumber2;
			if (L_0)
			{
				goto IL_000d;
			}
		}

IL_0003:
		{
			String_t* L_1 = ___res0;
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = ___args1;
			String_t* L_3 = Res_GetString_m1D06B81901B03CA849045926741403907612BB4B(L_1, (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			goto IL_004c;
		}

IL_000d:
		{
			IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var);
			CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_4 = CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164(/*hidden argument*/NULL);
			String_t* L_5 = Int32_ToString_m027A8C9419D2FE56ED5D2EE42A6F3B3CE0130471((int32_t*)(&___lineNumber2), L_4, /*hidden argument*/NULL);
			V_1 = L_5;
			CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_6 = CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164(/*hidden argument*/NULL);
			String_t* L_7 = Int32_ToString_m027A8C9419D2FE56ED5D2EE42A6F3B3CE0130471((int32_t*)(&___linePosition3), L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			String_t* L_8 = ___res0;
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = ___args1;
			String_t* L_10 = Res_GetString_m1D06B81901B03CA849045926741403907612BB4B(L_8, (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_9, /*hidden argument*/NULL);
			V_0 = L_10;
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)3);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = L_11;
			String_t* L_13 = V_0;
			NullCheck(L_12);
			ArrayElementTypeCheck (L_12, L_13);
			(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_13);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = L_12;
			String_t* L_15 = V_1;
			NullCheck(L_14);
			ArrayElementTypeCheck (L_14, L_15);
			(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_15);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = L_14;
			String_t* L_17 = V_2;
			NullCheck(L_16);
			ArrayElementTypeCheck (L_16, L_17);
			(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_17);
			String_t* L_18 = Res_GetString_m1D06B81901B03CA849045926741403907612BB4B(_stringLiteral63D27168A6FFA4A02D5FDBA464A248377E88F4F4, (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_16, /*hidden argument*/NULL);
			V_0 = L_18;
		}

IL_004c:
		{
			String_t* L_19 = V_0;
			V_3 = L_19;
			goto IL_0064;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (MissingManifestResourceException_tAC74F21ADC46CCB2BCC710464434E3B97F72FACB_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0050;
		throw e;
	}

CATCH_0050:
	{ // begin catch(System.Resources.MissingManifestResourceException)
		String_t* L_20 = ___res0;
		String_t* L_21 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(_stringLiteral16C0D1A98D99C5FB32B981C3E41FDB407A083C18, L_20, _stringLiteralB3F14BF976EFD974E34846B742502C802FABAE9D, /*hidden argument*/NULL);
		V_3 = L_21;
		goto IL_0064;
	} // end catch (depth: 1)

IL_0064:
	{
		String_t* L_22 = V_3;
		return L_22;
	}
}
// System.String System.Xml.XmlException::get_Message()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlException_get_Message_m811FB67949E8227BB095C61A936900F988E4CB6C (XmlException_tBD65EFA0B5CA26D7D8F4906BEC7C83A76394C918 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_message_22();
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		String_t* L_1 = __this->get_message_22();
		return L_1;
	}

IL_000f:
	{
		String_t* L_2 = Exception_get_Message_mC7A96CEBF52567CEF612C8C75A99A735A83E883F(__this, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Xml.XmlNode System.Xml.XmlNode::get_NextSibling()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * XmlNode_get_NextSibling_m3A77EB7E743DF9B78C917565D969500A8DC6EDFB (XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * __this, const RuntimeMethod* method)
{
	{
		return (XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 *)NULL;
	}
}
// System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * XmlNode_get_FirstChild_m608A11E859422F1131324FE6B6D7B4EFFB418C81 (XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * __this, const RuntimeMethod* method)
{
	XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * V_0 = NULL;
	{
		XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * L_0 = VirtFuncInvoker0< XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * >::Invoke(7 /* System.Xml.XmlLinkedNode System.Xml.XmlNode::get_LastNode() */, __this);
		V_0 = L_0;
		XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * L_2 = V_0;
		NullCheck(L_2);
		XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * L_3 = L_2->get_next_0();
		return L_3;
	}

IL_0011:
	{
		return (XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 *)NULL;
	}
}
// System.Xml.XmlLinkedNode System.Xml.XmlNode::get_LastNode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * XmlNode_get_LastNode_m9CB959135B424720727D9A0CE6EE129DB7228E93 (XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * __this, const RuntimeMethod* method)
{
	{
		return (XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 *)NULL;
	}
}
// System.Collections.IEnumerator System.Xml.XmlNode::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* XmlNode_System_Collections_IEnumerable_GetEnumerator_mC7D6329FEF45EE989276469F9E13D5E60B90C9C1 (XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlNode_System_Collections_IEnumerable_GetEnumerator_mC7D6329FEF45EE989276469F9E13D5E60B90C9C1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559 * L_0 = (XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559 *)il2cpp_codegen_object_new(XmlChildEnumerator_t4D3EFEF86EDA656B48EC532A24FF67A30C599559_il2cpp_TypeInfo_var);
		XmlChildEnumerator__ctor_m05CAB16D0FC521D715119DFE83D2B11C7BB2FD8D(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String System.Xml.XmlNode::get_OuterXml()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlNode_get_OuterXml_m69AA8CA2C21CF72A80768DE8531B05A08ADCA52B (XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlNode_get_OuterXml_m69AA8CA2C21CF72A80768DE8531B05A08ADCA52B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * V_0 = NULL;
	XmlDOMTextWriter_tF813A0468BACD6B3CC21849A38FA6AB269D5A741 * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var);
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_0 = CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164(/*hidden argument*/NULL);
		StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_1 = (StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 *)il2cpp_codegen_object_new(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_il2cpp_TypeInfo_var);
		StringWriter__ctor_mDF4AB6FD46E8B9824F2F7A9C26EA086A2C1AE5CF(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_2 = V_0;
		XmlDOMTextWriter_tF813A0468BACD6B3CC21849A38FA6AB269D5A741 * L_3 = (XmlDOMTextWriter_tF813A0468BACD6B3CC21849A38FA6AB269D5A741 *)il2cpp_codegen_object_new(XmlDOMTextWriter_tF813A0468BACD6B3CC21849A38FA6AB269D5A741_il2cpp_TypeInfo_var);
		XmlDOMTextWriter__ctor_m7A30C41A24817419B3370A576D6B1254EBEC824E(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		XmlDOMTextWriter_tF813A0468BACD6B3CC21849A38FA6AB269D5A741 * L_4 = V_1;
		VirtActionInvoker1< XmlWriter_t676293C138D2D0DAB9C1BC16A7BEE618391C5B2D * >::Invoke(9 /* System.Void System.Xml.XmlNode::WriteTo(System.Xml.XmlWriter) */, __this, L_4);
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		XmlDOMTextWriter_tF813A0468BACD6B3CC21849A38FA6AB269D5A741 * L_5 = V_1;
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(7 /* System.Void System.Xml.XmlWriter::Close() */, L_5);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0022:
	{
		StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlReader::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlReader__cctor_m8C939FA4F60E046BCAEDF0969A6E3B89D9CB0A4E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlReader__cctor_m8C939FA4F60E046BCAEDF0969A6E3B89D9CB0A4E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138_StaticFields*)il2cpp_codegen_static_fields_for(XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138_il2cpp_TypeInfo_var))->set_IsTextualNodeBitmap_0(((int32_t)24600));
		((XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138_StaticFields*)il2cpp_codegen_static_fields_for(XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138_il2cpp_TypeInfo_var))->set_CanReadContentAsBitmap_1(((int32_t)123324));
		((XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138_StaticFields*)il2cpp_codegen_static_fields_for(XmlReader_tECCB3D8B757F8CE744EF0430F338BEF15E060138_il2cpp_TypeInfo_var))->set_HasValueBitmap_2(((int32_t)157084));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlTextEncoder::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder__ctor_m8142AA059B3675FE89F48542A7F013098FCDBCBC (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___textWriter0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_0 = ___textWriter0;
		__this->set_textWriter_0(L_0);
		__this->set_quoteChar_2(((int32_t)34));
		XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  L_1 = XmlCharType_get_Instance_mA3CFC9BC3797565FD176224C6116F41AC8BA65B5(/*hidden argument*/NULL);
		__this->set_xmlCharType_5(L_1);
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::set_QuoteChar(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_set_QuoteChar_m1897FBEC0EAB4616C3FD961A12AFC0F7ED771824 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, Il2CppChar ___value0, const RuntimeMethod* method)
{
	{
		Il2CppChar L_0 = ___value0;
		__this->set_quoteChar_2(L_0);
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::StartAttribute(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_StartAttribute_m154B2203CF4424F0E8A6D1D80AC68B2E05FB7B32 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, bool ___cacheAttrValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_StartAttribute_m154B2203CF4424F0E8A6D1D80AC68B2E05FB7B32_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_inAttribute_1((bool)1);
		bool L_0 = ___cacheAttrValue0;
		__this->set_cacheAttrValue_4(L_0);
		bool L_1 = ___cacheAttrValue0;
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		StringBuilder_t * L_2 = __this->get_attrValue_3();
		if (L_2)
		{
			goto IL_0025;
		}
	}
	{
		StringBuilder_t * L_3 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m5A81DE19E748F748E19FF13FB6FFD2547F9212D9(L_3, /*hidden argument*/NULL);
		__this->set_attrValue_3(L_3);
		return;
	}

IL_0025:
	{
		StringBuilder_t * L_4 = __this->get_attrValue_3();
		NullCheck(L_4);
		StringBuilder_set_Length_m7C1756193B05DCA5A23C5DC98EE90A9FC685A27A(L_4, 0, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::EndAttribute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_EndAttribute_mEBAE3ABA120AF2728E56ADDB2BE2A5B313093C12 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_cacheAttrValue_4();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		StringBuilder_t * L_1 = __this->get_attrValue_3();
		NullCheck(L_1);
		StringBuilder_set_Length_m7C1756193B05DCA5A23C5DC98EE90A9FC685A27A(L_1, 0, /*hidden argument*/NULL);
	}

IL_0014:
	{
		__this->set_inAttribute_1((bool)0);
		__this->set_cacheAttrValue_4((bool)0);
		return;
	}
}
// System.String System.Xml.XmlTextEncoder::get_AttributeValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlTextEncoder_get_AttributeValue_m9E57BB8D3D0C4BBA526DFAEC0064BD1C3F3BA38D (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_get_AttributeValue_m9E57BB8D3D0C4BBA526DFAEC0064BD1C3F3BA38D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_cacheAttrValue_4();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		StringBuilder_t * L_1 = __this->get_attrValue_3();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		return L_2;
	}

IL_0014:
	{
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		return L_3;
	}
}
// System.Void System.Xml.XmlTextEncoder::WriteSurrogateChar(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteSurrogateChar_m6D9CFC7F219F339DE22E679418EC68425D7EE20D (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, Il2CppChar ___lowChar0, Il2CppChar ___highChar1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_WriteSurrogateChar_m6D9CFC7F219F339DE22E679418EC68425D7EE20D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___lowChar0;
		bool L_1 = XmlCharType_IsLowSurrogate_m7939EE6AE78DBE960B04D2A8E957E913C2EFC6DC(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		Il2CppChar L_2 = ___highChar1;
		bool L_3 = XmlCharType_IsHighSurrogate_m05FA9C7BBB540AD6890CCDCC286E6699F7351301(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0018;
		}
	}

IL_0010:
	{
		Il2CppChar L_4 = ___lowChar0;
		Il2CppChar L_5 = ___highChar1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var);
		Exception_t * L_6 = XmlConvert_CreateInvalidSurrogatePairException_m0DE4CC9F8620ABC31C61F6FDAFA3BC960F41F170(L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, XmlTextEncoder_WriteSurrogateChar_m6D9CFC7F219F339DE22E679418EC68425D7EE20D_RuntimeMethod_var);
	}

IL_0018:
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_7 = __this->get_textWriter_0();
		Il2CppChar L_8 = ___highChar1;
		NullCheck(L_7);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_7, L_8);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_9 = __this->get_textWriter_0();
		Il2CppChar L_10 = ___lowChar0;
		NullCheck(L_9);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_9, L_10);
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::Write(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_Write_m3D15126BEF6FE4BE6AA1FA9A25860A11649C8E3B (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, String_t* ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_Write_m3D15126BEF6FE4BE6AA1FA9A25860A11649C8E3B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Il2CppChar V_3 = 0x0;
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* V_4 = NULL;
	{
		String_t* L_0 = ___text0;
		if (L_0)
		{
			goto IL_0004;
		}
	}
	{
		return;
	}

IL_0004:
	{
		bool L_1 = __this->get_cacheAttrValue_4();
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		StringBuilder_t * L_2 = __this->get_attrValue_3();
		String_t* L_3 = ___text0;
		NullCheck(L_2);
		StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0019:
	{
		String_t* L_4 = ___text0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		V_1 = 0;
		V_2 = 0;
		V_3 = 0;
		goto IL_002c;
	}

IL_0028:
	{
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_002c:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_004d;
		}
	}
	{
		XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA * L_9 = __this->get_address_of_xmlCharType_5();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = L_9->get_charProperties_2();
		String_t* L_11 = ___text0;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		Il2CppChar L_13 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_11, L_12, /*hidden argument*/NULL);
		Il2CppChar L_14 = L_13;
		V_3 = L_14;
		NullCheck(L_10);
		Il2CppChar L_15 = L_14;
		uint8_t L_16 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		if (((int32_t)((int32_t)L_16&(int32_t)((int32_t)128))))
		{
			goto IL_0028;
		}
	}

IL_004d:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((!(((uint32_t)L_17) == ((uint32_t)L_18))))
		{
			goto IL_005e;
		}
	}
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_19 = __this->get_textWriter_0();
		String_t* L_20 = ___text0;
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_19, L_20);
		return;
	}

IL_005e:
	{
		bool L_21 = __this->get_inAttribute_1();
		if (!L_21)
		{
			goto IL_0071;
		}
	}
	{
		Il2CppChar L_22 = V_3;
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
		goto IL_002c;
	}

IL_0071:
	{
		Il2CppChar L_24 = V_3;
		if ((((int32_t)L_24) == ((int32_t)((int32_t)9))))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppChar L_25 = V_3;
		if ((((int32_t)L_25) == ((int32_t)((int32_t)10))))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppChar L_26 = V_3;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)13))))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppChar L_27 = V_3;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)34))))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppChar L_28 = V_3;
		if ((!(((uint32_t)L_28) == ((uint32_t)((int32_t)39)))))
		{
			goto IL_0090;
		}
	}

IL_008a:
	{
		int32_t L_29 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
		goto IL_002c;
	}

IL_0090:
	{
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_30 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256));
		V_4 = L_30;
	}

IL_009c:
	{
		int32_t L_31 = V_2;
		int32_t L_32 = V_1;
		if ((((int32_t)L_31) >= ((int32_t)L_32)))
		{
			goto IL_00ad;
		}
	}
	{
		String_t* L_33 = ___text0;
		int32_t L_34 = V_2;
		int32_t L_35 = V_1;
		int32_t L_36 = V_2;
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_37 = V_4;
		XmlTextEncoder_WriteStringFragment_mE235366F26A1F6FAB51FDDE129A36E5A0A451229(__this, L_33, L_34, ((int32_t)il2cpp_codegen_subtract((int32_t)L_35, (int32_t)L_36)), L_37, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		int32_t L_38 = V_1;
		int32_t L_39 = V_0;
		if ((((int32_t)L_38) == ((int32_t)L_39)))
		{
			goto IL_023a;
		}
	}
	{
		Il2CppChar L_40 = V_3;
		if ((!(((uint32_t)L_40) <= ((uint32_t)((int32_t)38)))))
		{
			goto IL_00e8;
		}
	}
	{
		Il2CppChar L_41 = V_3;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_41, (int32_t)((int32_t)9))))
		{
			case 0:
			{
				goto IL_00fc;
			}
			case 1:
			{
				goto IL_010d;
			}
			case 2:
			{
				goto IL_01bf;
			}
			case 3:
			{
				goto IL_01bf;
			}
			case 4:
			{
				goto IL_010d;
			}
		}
	}
	{
		Il2CppChar L_42 = V_3;
		if ((((int32_t)L_42) == ((int32_t)((int32_t)34))))
		{
			goto IL_0192;
		}
	}
	{
		Il2CppChar L_43 = V_3;
		if ((((int32_t)L_43) == ((int32_t)((int32_t)38))))
		{
			goto IL_0152;
		}
	}
	{
		goto IL_01bf;
	}

IL_00e8:
	{
		Il2CppChar L_44 = V_3;
		if ((((int32_t)L_44) == ((int32_t)((int32_t)39))))
		{
			goto IL_0162;
		}
	}
	{
		Il2CppChar L_45 = V_3;
		if ((((int32_t)L_45) == ((int32_t)((int32_t)60))))
		{
			goto IL_0132;
		}
	}
	{
		Il2CppChar L_46 = V_3;
		if ((((int32_t)L_46) == ((int32_t)((int32_t)62))))
		{
			goto IL_0142;
		}
	}
	{
		goto IL_01bf;
	}

IL_00fc:
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_47 = __this->get_textWriter_0();
		Il2CppChar L_48 = V_3;
		NullCheck(L_47);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_47, L_48);
		goto IL_0205;
	}

IL_010d:
	{
		bool L_49 = __this->get_inAttribute_1();
		if (!L_49)
		{
			goto IL_0121;
		}
	}
	{
		Il2CppChar L_50 = V_3;
		XmlTextEncoder_WriteCharEntityImpl_m26C18068AD20D6F0B9A5B5F3BB3D9E5A5F61BAD9(__this, L_50, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_0121:
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_51 = __this->get_textWriter_0();
		Il2CppChar L_52 = V_3;
		NullCheck(L_51);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_51, L_52);
		goto IL_0205;
	}

IL_0132:
	{
		XmlTextEncoder_WriteEntityRefImpl_m44E81A5273CF2E965652A47A9C5BBEC629E8539E(__this, _stringLiteral35B44AA64754DEDC52915E7F763C081CF5B004D9, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_0142:
	{
		XmlTextEncoder_WriteEntityRefImpl_m44E81A5273CF2E965652A47A9C5BBEC629E8539E(__this, _stringLiteral1710B4477A01FFE20514D8AA61891F364D71C25B, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_0152:
	{
		XmlTextEncoder_WriteEntityRefImpl_m44E81A5273CF2E965652A47A9C5BBEC629E8539E(__this, _stringLiteral96930EC8C6FD5250BB36A5E1040AB06A9588FD62, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_0162:
	{
		bool L_53 = __this->get_inAttribute_1();
		if (!L_53)
		{
			goto IL_0183;
		}
	}
	{
		Il2CppChar L_54 = __this->get_quoteChar_2();
		Il2CppChar L_55 = V_3;
		if ((!(((uint32_t)L_54) == ((uint32_t)L_55))))
		{
			goto IL_0183;
		}
	}
	{
		XmlTextEncoder_WriteEntityRefImpl_m44E81A5273CF2E965652A47A9C5BBEC629E8539E(__this, _stringLiteral513659CEF285C73478E9829E41D7E4C23DB53E12, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_0183:
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_56 = __this->get_textWriter_0();
		NullCheck(L_56);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_56, ((int32_t)39));
		goto IL_0205;
	}

IL_0192:
	{
		bool L_57 = __this->get_inAttribute_1();
		if (!L_57)
		{
			goto IL_01b0;
		}
	}
	{
		Il2CppChar L_58 = __this->get_quoteChar_2();
		Il2CppChar L_59 = V_3;
		if ((!(((uint32_t)L_58) == ((uint32_t)L_59))))
		{
			goto IL_01b0;
		}
	}
	{
		XmlTextEncoder_WriteEntityRefImpl_m44E81A5273CF2E965652A47A9C5BBEC629E8539E(__this, _stringLiteral893D84FF4ED81AC205FBC0C67CBEE1C0C752B406, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_01b0:
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_60 = __this->get_textWriter_0();
		NullCheck(L_60);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_60, ((int32_t)34));
		goto IL_0205;
	}

IL_01bf:
	{
		Il2CppChar L_61 = V_3;
		bool L_62 = XmlCharType_IsHighSurrogate_m05FA9C7BBB540AD6890CCDCC286E6699F7351301(L_61, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_01ef;
		}
	}
	{
		int32_t L_63 = V_1;
		int32_t L_64 = V_0;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_63, (int32_t)1))) >= ((int32_t)L_64)))
		{
			goto IL_01e1;
		}
	}
	{
		String_t* L_65 = ___text0;
		int32_t L_66 = V_1;
		int32_t L_67 = ((int32_t)il2cpp_codegen_add((int32_t)L_66, (int32_t)1));
		V_1 = L_67;
		NullCheck(L_65);
		Il2CppChar L_68 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_65, L_67, /*hidden argument*/NULL);
		Il2CppChar L_69 = V_3;
		XmlTextEncoder_WriteSurrogateChar_m6D9CFC7F219F339DE22E679418EC68425D7EE20D(__this, L_68, L_69, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_01e1:
	{
		String_t* L_70 = ___text0;
		int32_t L_71 = V_1;
		NullCheck(L_70);
		Il2CppChar L_72 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_70, L_71, /*hidden argument*/NULL);
		Il2CppChar L_73 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var);
		Exception_t * L_74 = XmlConvert_CreateInvalidSurrogatePairException_m0DE4CC9F8620ABC31C61F6FDAFA3BC960F41F170(L_72, L_73, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_74, XmlTextEncoder_Write_m3D15126BEF6FE4BE6AA1FA9A25860A11649C8E3B_RuntimeMethod_var);
	}

IL_01ef:
	{
		Il2CppChar L_75 = V_3;
		bool L_76 = XmlCharType_IsLowSurrogate_m7939EE6AE78DBE960B04D2A8E957E913C2EFC6DC(L_75, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_01fe;
		}
	}
	{
		Il2CppChar L_77 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var);
		Exception_t * L_78 = XmlConvert_CreateInvalidHighSurrogateCharException_mD8CAB48E86A39FACE4A0274D91C7065A6458233E(L_77, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_78, XmlTextEncoder_Write_m3D15126BEF6FE4BE6AA1FA9A25860A11649C8E3B_RuntimeMethod_var);
	}

IL_01fe:
	{
		Il2CppChar L_79 = V_3;
		XmlTextEncoder_WriteCharEntityImpl_m26C18068AD20D6F0B9A5B5F3BB3D9E5A5F61BAD9(__this, L_79, /*hidden argument*/NULL);
	}

IL_0205:
	{
		int32_t L_80 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_80, (int32_t)1));
		int32_t L_81 = V_1;
		V_2 = L_81;
		goto IL_0211;
	}

IL_020d:
	{
		int32_t L_82 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_82, (int32_t)1));
	}

IL_0211:
	{
		int32_t L_83 = V_1;
		int32_t L_84 = V_0;
		if ((((int32_t)L_83) >= ((int32_t)L_84)))
		{
			goto IL_009c;
		}
	}
	{
		XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA * L_85 = __this->get_address_of_xmlCharType_5();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_86 = L_85->get_charProperties_2();
		String_t* L_87 = ___text0;
		int32_t L_88 = V_1;
		NullCheck(L_87);
		Il2CppChar L_89 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_87, L_88, /*hidden argument*/NULL);
		Il2CppChar L_90 = L_89;
		V_3 = L_90;
		NullCheck(L_86);
		Il2CppChar L_91 = L_90;
		uint8_t L_92 = (L_86)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		if (((int32_t)((int32_t)L_92&(int32_t)((int32_t)128))))
		{
			goto IL_020d;
		}
	}
	{
		goto IL_009c;
	}

IL_023a:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::WriteRaw(System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteRaw_m5B4E690DB11115A1A2EBF4228182024DD5BB8434 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___array0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_WriteRaw_m5B4E690DB11115A1A2EBF4228182024DD5BB8434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_0 = ___array0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_1 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_1, _stringLiteralB829404B947F7E1629A30B5E953A49EB21CCD2ED, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, XmlTextEncoder_WriteRaw_m5B4E690DB11115A1A2EBF4228182024DD5BB8434_RuntimeMethod_var);
	}

IL_000e:
	{
		int32_t L_2 = ___count2;
		if ((((int32_t)0) <= ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_3 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_3, _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, XmlTextEncoder_WriteRaw_m5B4E690DB11115A1A2EBF4228182024DD5BB8434_RuntimeMethod_var);
	}

IL_001d:
	{
		int32_t L_4 = ___offset1;
		if ((((int32_t)0) <= ((int32_t)L_4)))
		{
			goto IL_002c;
		}
	}
	{
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_5 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_5, _stringLiteral544DC80A2A82A08B6321F56F8987CB7E5DEED1C4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, XmlTextEncoder_WriteRaw_m5B4E690DB11115A1A2EBF4228182024DD5BB8434_RuntimeMethod_var);
	}

IL_002c:
	{
		int32_t L_6 = ___count2;
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_7 = ___array0;
		NullCheck(L_7);
		int32_t L_8 = ___offset1;
		if ((((int32_t)L_6) <= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))), (int32_t)L_8)))))
		{
			goto IL_003f;
		}
	}
	{
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_9 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_9, _stringLiteral07624473F417C06C74D59C64840A1532FCE2C626, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, XmlTextEncoder_WriteRaw_m5B4E690DB11115A1A2EBF4228182024DD5BB8434_RuntimeMethod_var);
	}

IL_003f:
	{
		bool L_10 = __this->get_cacheAttrValue_4();
		if (!L_10)
		{
			goto IL_0056;
		}
	}
	{
		StringBuilder_t * L_11 = __this->get_attrValue_3();
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_12 = ___array0;
		int32_t L_13 = ___offset1;
		int32_t L_14 = ___count2;
		NullCheck(L_11);
		StringBuilder_Append_m4B771D7BFE8A65C9A504EC5847A699EB678B02DB(L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
	}

IL_0056:
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_15 = __this->get_textWriter_0();
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_16 = ___array0;
		int32_t L_17 = ___offset1;
		int32_t L_18 = ___count2;
		NullCheck(L_15);
		VirtActionInvoker3< CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*, int32_t, int32_t >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.Char[],System.Int32,System.Int32) */, L_15, L_16, L_17, L_18);
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::WriteStringFragment(System.String,System.Int32,System.Int32,System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteStringFragment_mE235366F26A1F6FAB51FDDE129A36E5A0A451229 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, String_t* ___str0, int32_t ___offset1, int32_t ___count2, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___helperBuffer3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_0 = ___helperBuffer3;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length))));
		goto IL_0033;
	}

IL_0007:
	{
		int32_t L_1 = ___count2;
		V_1 = L_1;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)L_3)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_4 = V_0;
		V_1 = L_4;
	}

IL_000f:
	{
		String_t* L_5 = ___str0;
		int32_t L_6 = ___offset1;
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_7 = ___helperBuffer3;
		int32_t L_8 = V_1;
		NullCheck(L_5);
		String_CopyTo_mF77EF8D4E5ABBD2AB7F509FFE9C0C70DC15A27E1(L_5, L_6, L_7, 0, L_8, /*hidden argument*/NULL);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_9 = __this->get_textWriter_0();
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_10 = ___helperBuffer3;
		int32_t L_11 = V_1;
		NullCheck(L_9);
		VirtActionInvoker3< CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*, int32_t, int32_t >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.Char[],System.Int32,System.Int32) */, L_9, L_10, 0, L_11);
		int32_t L_12 = ___offset1;
		int32_t L_13 = V_1;
		___offset1 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)L_13));
		int32_t L_14 = ___count2;
		int32_t L_15 = V_1;
		___count2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)L_15));
	}

IL_0033:
	{
		int32_t L_16 = ___count2;
		if ((((int32_t)L_16) > ((int32_t)0)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::WriteCharEntityImpl(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteCharEntityImpl_m26C18068AD20D6F0B9A5B5F3BB3D9E5A5F61BAD9 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, Il2CppChar ___ch0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_WriteCharEntityImpl_m26C18068AD20D6F0B9A5B5F3BB3D9E5A5F61BAD9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppChar L_0 = ___ch0;
		V_0 = L_0;
		NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * L_1 = NumberFormatInfo_get_InvariantInfo_m286BBD095BFCA546BD2CD67E856D1A205436C03F(/*hidden argument*/NULL);
		String_t* L_2 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)(&V_0), _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_1, /*hidden argument*/NULL);
		XmlTextEncoder_WriteCharEntityImpl_mCE63160635693B3E2C37693BE3401796D86ECAD9(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::WriteCharEntityImpl(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteCharEntityImpl_mCE63160635693B3E2C37693BE3401796D86ECAD9 (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, String_t* ___strVal0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_WriteCharEntityImpl_mCE63160635693B3E2C37693BE3401796D86ECAD9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_0 = __this->get_textWriter_0();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_0, _stringLiteralCEB6890FC169A5D98961042EBCAD0677F2F0656F);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_1 = __this->get_textWriter_0();
		String_t* L_2 = ___strVal0;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_1, L_2);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_3 = __this->get_textWriter_0();
		NullCheck(L_3);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_3, ((int32_t)59));
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::WriteEntityRefImpl(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteEntityRefImpl_m44E81A5273CF2E965652A47A9C5BBEC629E8539E (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_0 = __this->get_textWriter_0();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_0, ((int32_t)38));
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_1 = __this->get_textWriter_0();
		String_t* L_2 = ___name0;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_1, L_2);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_3 = __this->get_textWriter_0();
		NullCheck(L_3);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_3, ((int32_t)59));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlTextWriter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter__ctor_m99C6F457368131185E3DF47E3339A20D2C076357 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter__ctor_m99C6F457368131185E3DF47E3339A20D2C076357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA  L_0 = XmlCharType_get_Instance_mA3CFC9BC3797565FD176224C6116F41AC8BA65B5(/*hidden argument*/NULL);
		__this->set_xmlCharType_23(L_0);
		XmlWriter__ctor_mAA10A026AD230D064314D2AE472A144D18418A01(__this, /*hidden argument*/NULL);
		__this->set_namespaces_15((bool)1);
		__this->set_formatting_3(0);
		__this->set_indentation_5(2);
		__this->set_indentChar_6(((int32_t)32));
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_1 = (NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6*)(NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6*)SZArrayNew(NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6_il2cpp_TypeInfo_var, (uint32_t)8);
		__this->set_nsStack_19(L_1);
		__this->set_nsTop_20((-1));
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_2 = (TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637*)(TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637*)SZArrayNew(TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		__this->set_stack_7(L_2);
		__this->set_top_8(0);
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_3 = __this->get_stack_7();
		int32_t L_4 = __this->get_top_8();
		NullCheck(L_3);
		TagInfo_Init_mFC65CB7C7A7D7852E3A2825365F1BD35CC38F8CF((TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4))), (-1), /*hidden argument*/NULL);
		__this->set_quoteChar_13(((int32_t)34));
		IL2CPP_RUNTIME_CLASS_INIT(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var);
		StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* L_5 = ((XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var))->get_stateTableDefault_26();
		__this->set_stateTable_9(L_5);
		__this->set_currentState_10(0);
		__this->set_lastToken_11(((int32_t)13));
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter__ctor_m2561A4395C8A92B49271C2D6E6840CEDADC38D7B (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___w0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter__ctor_m2561A4395C8A92B49271C2D6E6840CEDADC38D7B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XmlTextWriter__ctor_m99C6F457368131185E3DF47E3339A20D2C076357(__this, /*hidden argument*/NULL);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_0 = ___w0;
		__this->set_textWriter_0(L_0);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_1 = ___w0;
		NullCheck(L_1);
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_2 = VirtFuncInvoker0< Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * >::Invoke(11 /* System.Text.Encoding System.IO.TextWriter::get_Encoding() */, L_1);
		__this->set_encoding_2(L_2);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_3 = ___w0;
		XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * L_4 = (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E *)il2cpp_codegen_object_new(XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E_il2cpp_TypeInfo_var);
		XmlTextEncoder__ctor_m8142AA059B3675FE89F48542A7F013098FCDBCBC(L_4, L_3, /*hidden argument*/NULL);
		__this->set_xmlEncoder_1(L_4);
		XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * L_5 = __this->get_xmlEncoder_1();
		Il2CppChar L_6 = __this->get_quoteChar_13();
		NullCheck(L_5);
		XmlTextEncoder_set_QuoteChar_m1897FBEC0EAB4616C3FD961A12AFC0F7ED771824_inline(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::WriteEndElement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_WriteEndElement_mE604C4B85C4996180C7EA5C45DFDC06F5FC173BD (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method)
{
	{
		XmlTextWriter_InternalWriteEndElement_mF39D8CB533A512D50269E722957BA39D259B7D9C(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.WriteState System.Xml.XmlTextWriter::get_WriteState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t XmlTextWriter_get_WriteState_m5E47A02EFB1E27927A179424960D1FD5EA86D879 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_currentState_10();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0037;
			}
			case 1:
			{
				goto IL_0039;
			}
			case 2:
			{
				goto IL_0039;
			}
			case 3:
			{
				goto IL_003b;
			}
			case 4:
			{
				goto IL_003d;
			}
			case 5:
			{
				goto IL_003f;
			}
			case 6:
			{
				goto IL_003d;
			}
			case 7:
			{
				goto IL_003f;
			}
			case 8:
			{
				goto IL_0041;
			}
			case 9:
			{
				goto IL_0043;
			}
		}
	}
	{
		goto IL_0045;
	}

IL_0037:
	{
		return (int32_t)(0);
	}

IL_0039:
	{
		return (int32_t)(1);
	}

IL_003b:
	{
		return (int32_t)(2);
	}

IL_003d:
	{
		return (int32_t)(3);
	}

IL_003f:
	{
		return (int32_t)(4);
	}

IL_0041:
	{
		return (int32_t)(6);
	}

IL_0043:
	{
		return (int32_t)(5);
	}

IL_0045:
	{
		return (int32_t)(6);
	}
}
// System.Void System.Xml.XmlTextWriter::Close()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_Close_mCE05D92C3D65682E40B0A95F579EA2EF84F432B7 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_Close_mCE05D92C3D65682E40B0A95F579EA2EF84F432B7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			XmlTextWriter_AutoCompleteAll_m84FCFD43E3ECAE77234E1DA140AB15AE75F7E4DC(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x1F, FINALLY_000b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
				goto CATCH_0008;
			throw e;
		}

CATCH_0008:
		{ // begin catch(System.Object)
			IL2CPP_LEAVE(0x1F, FINALLY_000b);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		__this->set_currentState_10(((int32_t)9));
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_0 = __this->get_textWriter_0();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(8 /* System.Void System.IO.TextWriter::Close() */, L_0);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x1F, IL_001f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_001f:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::AutoComplete(System.Xml.XmlTextWriter_Token)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AutoComplete_mC90A57781E031A2E18047CAF6BF09199323B48D2 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, int32_t ___token0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_AutoComplete_mC90A57781E031A2E18047CAF6BF09199323B48D2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_currentState_10();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_1 = Res_GetString_m4303E63CA6945331BF5584376F6AA7262D1C6887(_stringLiteral952F621460E9250177F13FF0BEAD9E00E2B86302, /*hidden argument*/NULL);
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_2 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, XmlTextWriter_AutoComplete_mC90A57781E031A2E18047CAF6BF09199323B48D2_RuntimeMethod_var);
	}

IL_001a:
	{
		int32_t L_3 = __this->get_currentState_10();
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_004d;
		}
	}
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_4 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_5 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = ((XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var))->get_tokenName_25();
		int32_t L_7 = ___token0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		String_t* L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_9);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_9);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_10 = L_5;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = ((XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var))->get_stateName_24();
		NullCheck(L_11);
		int32_t L_12 = 8;
		String_t* L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_13);
		String_t* L_14 = Res_GetString_m1D06B81901B03CA849045926741403907612BB4B(_stringLiteral23E65142459FB1BF5009AD8736318547A5DFE850, L_10, /*hidden argument*/NULL);
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_15 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_15, L_14, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15, XmlTextWriter_AutoComplete_mC90A57781E031A2E18047CAF6BF09199323B48D2_RuntimeMethod_var);
	}

IL_004d:
	{
		StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* L_16 = __this->get_stateTable_9();
		int32_t L_17 = ___token0;
		int32_t L_18 = __this->get_currentState_10();
		NullCheck(L_16);
		int32_t L_19 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_17, (int32_t)8)), (int32_t)L_18));
		int32_t L_20 = (int32_t)(L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_0 = L_20;
		int32_t L_21 = V_0;
		if ((!(((uint32_t)L_21) == ((uint32_t)8))))
		{
			goto IL_0092;
		}
	}
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_22 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_23 = L_22;
		IL2CPP_RUNTIME_CLASS_INIT(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_24 = ((XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var))->get_tokenName_25();
		int32_t L_25 = ___token0;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		String_t* L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_27);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_27);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_28 = L_23;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_29 = ((XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var))->get_stateName_24();
		int32_t L_30 = __this->get_currentState_10();
		NullCheck(L_29);
		int32_t L_31 = L_30;
		String_t* L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_32);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_32);
		String_t* L_33 = Res_GetString_m1D06B81901B03CA849045926741403907612BB4B(_stringLiteral23E65142459FB1BF5009AD8736318547A5DFE850, L_28, /*hidden argument*/NULL);
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_34 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_34, L_33, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_34, XmlTextWriter_AutoComplete_mC90A57781E031A2E18047CAF6BF09199323B48D2_RuntimeMethod_var);
	}

IL_0092:
	{
		int32_t L_35 = ___token0;
		switch (L_35)
		{
			case 0:
			{
				goto IL_00f3;
			}
			case 1:
			{
				goto IL_00d1;
			}
			case 2:
			{
				goto IL_00f3;
			}
			case 3:
			{
				goto IL_00f3;
			}
			case 4:
			{
				goto IL_00f3;
			}
			case 5:
			{
				goto IL_015d;
			}
			case 6:
			{
				goto IL_015d;
			}
			case 7:
			{
				goto IL_01b5;
			}
			case 8:
			{
				goto IL_01fc;
			}
			case 9:
			{
				goto IL_0212;
			}
			case 10:
			{
				goto IL_0212;
			}
			case 11:
			{
				goto IL_0212;
			}
			case 12:
			{
				goto IL_0212;
			}
		}
	}
	{
		goto IL_025c;
	}

IL_00d1:
	{
		bool L_36 = __this->get_indented_4();
		if (!L_36)
		{
			goto IL_026c;
		}
	}
	{
		int32_t L_37 = __this->get_currentState_10();
		if (!L_37)
		{
			goto IL_026c;
		}
	}
	{
		XmlTextWriter_Indent_m79DEE82659DC5B5F0C4E1E8E3E30C76E7F869851(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_026c;
	}

IL_00f3:
	{
		int32_t L_38 = __this->get_currentState_10();
		if ((!(((uint32_t)L_38) == ((uint32_t)4))))
		{
			goto IL_010b;
		}
	}
	{
		XmlTextWriter_WriteEndAttributeQuote_m98AC4165AD8A683AE52D3683F2719E1C8CC578CD(__this, /*hidden argument*/NULL);
		XmlTextWriter_WriteEndStartTag_mBA4796BAC09CD5B25F711E65D61BC2BE8375D2F0(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_011b;
	}

IL_010b:
	{
		int32_t L_39 = __this->get_currentState_10();
		if ((!(((uint32_t)L_39) == ((uint32_t)3))))
		{
			goto IL_011b;
		}
	}
	{
		XmlTextWriter_WriteEndStartTag_mBA4796BAC09CD5B25F711E65D61BC2BE8375D2F0(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_011b:
	{
		int32_t L_40 = ___token0;
		if ((!(((uint32_t)L_40) == ((uint32_t)3))))
		{
			goto IL_013b;
		}
	}
	{
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_41 = __this->get_stack_7();
		int32_t L_42 = __this->get_top_8();
		NullCheck(L_41);
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_mixed_8((bool)1);
		goto IL_026c;
	}

IL_013b:
	{
		bool L_43 = __this->get_indented_4();
		if (!L_43)
		{
			goto IL_026c;
		}
	}
	{
		int32_t L_44 = __this->get_currentState_10();
		if (!L_44)
		{
			goto IL_026c;
		}
	}
	{
		XmlTextWriter_Indent_m79DEE82659DC5B5F0C4E1E8E3E30C76E7F869851(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_026c;
	}

IL_015d:
	{
		bool L_45 = __this->get_flush_18();
		if (!L_45)
		{
			goto IL_016b;
		}
	}
	{
		XmlTextWriter_FlushEncoders_m6D7554550029EE5407C9149A740B03B138B44C1B(__this, /*hidden argument*/NULL);
	}

IL_016b:
	{
		int32_t L_46 = __this->get_currentState_10();
		if ((!(((uint32_t)L_46) == ((uint32_t)4))))
		{
			goto IL_017a;
		}
	}
	{
		XmlTextWriter_WriteEndAttributeQuote_m98AC4165AD8A683AE52D3683F2719E1C8CC578CD(__this, /*hidden argument*/NULL);
	}

IL_017a:
	{
		int32_t L_47 = __this->get_currentState_10();
		if ((!(((uint32_t)L_47) == ((uint32_t)5))))
		{
			goto IL_0188;
		}
	}
	{
		___token0 = 6;
		goto IL_0192;
	}

IL_0188:
	{
		int32_t L_48 = ___token0;
		XmlTextWriter_WriteEndStartTag_mBA4796BAC09CD5B25F711E65D61BC2BE8375D2F0(__this, (bool)((((int32_t)L_48) == ((int32_t)5))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0192:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var);
		StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* L_49 = ((XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var))->get_stateTableDocument_27();
		StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* L_50 = __this->get_stateTable_9();
		if ((!(((RuntimeObject*)(StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A*)L_49) == ((RuntimeObject*)(StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A*)L_50))))
		{
			goto IL_026c;
		}
	}
	{
		int32_t L_51 = __this->get_top_8();
		if ((!(((uint32_t)L_51) == ((uint32_t)1))))
		{
			goto IL_026c;
		}
	}
	{
		V_0 = 7;
		goto IL_026c;
	}

IL_01b5:
	{
		bool L_52 = __this->get_flush_18();
		if (!L_52)
		{
			goto IL_01c3;
		}
	}
	{
		XmlTextWriter_FlushEncoders_m6D7554550029EE5407C9149A740B03B138B44C1B(__this, /*hidden argument*/NULL);
	}

IL_01c3:
	{
		int32_t L_53 = __this->get_currentState_10();
		if ((!(((uint32_t)L_53) == ((uint32_t)4))))
		{
			goto IL_01e4;
		}
	}
	{
		XmlTextWriter_WriteEndAttributeQuote_m98AC4165AD8A683AE52D3683F2719E1C8CC578CD(__this, /*hidden argument*/NULL);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_54 = __this->get_textWriter_0();
		NullCheck(L_54);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_54, ((int32_t)32));
		goto IL_026c;
	}

IL_01e4:
	{
		int32_t L_55 = __this->get_currentState_10();
		if ((!(((uint32_t)L_55) == ((uint32_t)3))))
		{
			goto IL_026c;
		}
	}
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_56 = __this->get_textWriter_0();
		NullCheck(L_56);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_56, ((int32_t)32));
		goto IL_026c;
	}

IL_01fc:
	{
		bool L_57 = __this->get_flush_18();
		if (!L_57)
		{
			goto IL_020a;
		}
	}
	{
		XmlTextWriter_FlushEncoders_m6D7554550029EE5407C9149A740B03B138B44C1B(__this, /*hidden argument*/NULL);
	}

IL_020a:
	{
		XmlTextWriter_WriteEndAttributeQuote_m98AC4165AD8A683AE52D3683F2719E1C8CC578CD(__this, /*hidden argument*/NULL);
		goto IL_026c;
	}

IL_0212:
	{
		int32_t L_58 = ___token0;
		if ((((int32_t)L_58) == ((int32_t)((int32_t)10))))
		{
			goto IL_0225;
		}
	}
	{
		bool L_59 = __this->get_flush_18();
		if (!L_59)
		{
			goto IL_0225;
		}
	}
	{
		XmlTextWriter_FlushEncoders_m6D7554550029EE5407C9149A740B03B138B44C1B(__this, /*hidden argument*/NULL);
	}

IL_0225:
	{
		int32_t L_60 = __this->get_currentState_10();
		if ((!(((uint32_t)L_60) == ((uint32_t)3))))
		{
			goto IL_023f;
		}
	}
	{
		int32_t L_61 = __this->get_lastToken_11();
		if ((((int32_t)L_61) == ((int32_t)((int32_t)9))))
		{
			goto IL_023f;
		}
	}
	{
		XmlTextWriter_WriteEndStartTag_mBA4796BAC09CD5B25F711E65D61BC2BE8375D2F0(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_023f:
	{
		int32_t L_62 = V_0;
		if ((!(((uint32_t)L_62) == ((uint32_t)5))))
		{
			goto IL_026c;
		}
	}
	{
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_63 = __this->get_stack_7();
		int32_t L_64 = __this->get_top_8();
		NullCheck(L_63);
		((L_63)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_64)))->set_mixed_8((bool)1);
		goto IL_026c;
	}

IL_025c:
	{
		String_t* L_65 = Res_GetString_m4303E63CA6945331BF5584376F6AA7262D1C6887(_stringLiteralFBD1A9745DCC4D80697D44756437CCB57698AE21, /*hidden argument*/NULL);
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_66 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_66, L_65, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_66, XmlTextWriter_AutoComplete_mC90A57781E031A2E18047CAF6BF09199323B48D2_RuntimeMethod_var);
	}

IL_026c:
	{
		int32_t L_67 = V_0;
		__this->set_currentState_10(L_67);
		int32_t L_68 = ___token0;
		__this->set_lastToken_11(L_68);
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::AutoCompleteAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AutoCompleteAll_m84FCFD43E3ECAE77234E1DA140AB15AE75F7E4DC (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_flush_18();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		XmlTextWriter_FlushEncoders_m6D7554550029EE5407C9149A740B03B138B44C1B(__this, /*hidden argument*/NULL);
		goto IL_0016;
	}

IL_0010:
	{
		VirtActionInvoker0::Invoke(5 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, __this);
	}

IL_0016:
	{
		int32_t L_1 = __this->get_top_8();
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0010;
		}
	}
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::InternalWriteEndElement(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_InternalWriteEndElement_mF39D8CB533A512D50269E722957BA39D259B7D9C (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, bool ___longFormat0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_InternalWriteEndElement_mF39D8CB533A512D50269E722957BA39D259B7D9C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * G_B4_0 = NULL;
	XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * G_B5_1 = NULL;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = __this->get_top_8();
			if ((((int32_t)L_0) > ((int32_t)0)))
			{
				goto IL_0019;
			}
		}

IL_0009:
		{
			String_t* L_1 = Res_GetString_m4303E63CA6945331BF5584376F6AA7262D1C6887(_stringLiteral67171AD8B6817F7CF25B89C98234BCDA36D476FC, /*hidden argument*/NULL);
			InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_2 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_2, L_1, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, XmlTextWriter_InternalWriteEndElement_mF39D8CB533A512D50269E722957BA39D259B7D9C_RuntimeMethod_var);
		}

IL_0019:
		{
			bool L_3 = ___longFormat0;
			G_B3_0 = __this;
			if (L_3)
			{
				G_B4_0 = __this;
				goto IL_0020;
			}
		}

IL_001d:
		{
			G_B5_0 = 5;
			G_B5_1 = G_B3_0;
			goto IL_0021;
		}

IL_0020:
		{
			G_B5_0 = 6;
			G_B5_1 = G_B4_0;
		}

IL_0021:
		{
			NullCheck(G_B5_1);
			XmlTextWriter_AutoComplete_mC90A57781E031A2E18047CAF6BF09199323B48D2(G_B5_1, G_B5_0, /*hidden argument*/NULL);
			int32_t L_4 = __this->get_lastToken_11();
			if ((!(((uint32_t)L_4) == ((uint32_t)6))))
			{
				goto IL_00d7;
			}
		}

IL_0032:
		{
			bool L_5 = __this->get_indented_4();
			if (!L_5)
			{
				goto IL_0041;
			}
		}

IL_003a:
		{
			XmlTextWriter_Indent_m79DEE82659DC5B5F0C4E1E8E3E30C76E7F869851(__this, (bool)1, /*hidden argument*/NULL);
		}

IL_0041:
		{
			TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_6 = __this->get_textWriter_0();
			NullCheck(L_6);
			VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_6, ((int32_t)60));
			TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_7 = __this->get_textWriter_0();
			NullCheck(L_7);
			VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_7, ((int32_t)47));
			bool L_8 = __this->get_namespaces_15();
			if (!L_8)
			{
				goto IL_00a9;
			}
		}

IL_0063:
		{
			TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_9 = __this->get_stack_7();
			int32_t L_10 = __this->get_top_8();
			NullCheck(L_9);
			String_t* L_11 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))->get_prefix_1();
			if (!L_11)
			{
				goto IL_00a9;
			}
		}

IL_007b:
		{
			TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_12 = __this->get_textWriter_0();
			TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_13 = __this->get_stack_7();
			int32_t L_14 = __this->get_top_8();
			NullCheck(L_13);
			String_t* L_15 = ((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14)))->get_prefix_1();
			NullCheck(L_12);
			VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_12, L_15);
			TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_16 = __this->get_textWriter_0();
			NullCheck(L_16);
			VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_16, ((int32_t)58));
		}

IL_00a9:
		{
			TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_17 = __this->get_textWriter_0();
			TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_18 = __this->get_stack_7();
			int32_t L_19 = __this->get_top_8();
			NullCheck(L_18);
			String_t* L_20 = ((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19)))->get_name_0();
			NullCheck(L_17);
			VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_17, L_20);
			TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_21 = __this->get_textWriter_0();
			NullCheck(L_21);
			VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_21, ((int32_t)62));
		}

IL_00d7:
		{
			TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_22 = __this->get_stack_7();
			int32_t L_23 = __this->get_top_8();
			NullCheck(L_22);
			int32_t L_24 = ((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))->get_prevNsTop_6();
			V_0 = L_24;
			bool L_25 = __this->get_useNsHashtable_22();
			if (!L_25)
			{
				goto IL_010e;
			}
		}

IL_00f6:
		{
			int32_t L_26 = V_0;
			int32_t L_27 = __this->get_nsTop_20();
			if ((((int32_t)L_26) >= ((int32_t)L_27)))
			{
				goto IL_010e;
			}
		}

IL_00ff:
		{
			int32_t L_28 = V_0;
			int32_t L_29 = __this->get_nsTop_20();
			XmlTextWriter_PopNamespaces_mB647C786975F9BB3ADE2301184F8206D816EAFA6(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1)), L_29, /*hidden argument*/NULL);
		}

IL_010e:
		{
			int32_t L_30 = V_0;
			__this->set_nsTop_20(L_30);
			int32_t L_31 = __this->get_top_8();
			__this->set_top_8(((int32_t)il2cpp_codegen_subtract((int32_t)L_31, (int32_t)1)));
			goto IL_012f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0125;
		throw e;
	}

CATCH_0125:
	{ // begin catch(System.Object)
		__this->set_currentState_10(8);
		IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local, XmlTextWriter_InternalWriteEndElement_mF39D8CB533A512D50269E722957BA39D259B7D9C_RuntimeMethod_var);
	} // end catch (depth: 1)

IL_012f:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::WriteEndStartTag(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_WriteEndStartTag_mBA4796BAC09CD5B25F711E65D61BC2BE8375D2F0 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, bool ___empty0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_WriteEndStartTag_mBA4796BAC09CD5B25F711E65D61BC2BE8375D2F0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * L_0 = __this->get_xmlEncoder_1();
		NullCheck(L_0);
		XmlTextEncoder_StartAttribute_m154B2203CF4424F0E8A6D1D80AC68B2E05FB7B32(L_0, (bool)0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_nsTop_20();
		V_0 = L_1;
		goto IL_00b6;
	}

IL_0018:
	{
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_2 = __this->get_nsStack_19();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		bool L_4 = ((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3)))->get_declared_2();
		if (L_4)
		{
			goto IL_00b2;
		}
	}
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_5 = __this->get_textWriter_0();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_5, _stringLiteral5C7DA3CBD254CB3BB9FCC98E58CF1A56E605861E);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_6 = __this->get_textWriter_0();
		NullCheck(L_6);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_6, ((int32_t)58));
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_7 = __this->get_textWriter_0();
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_8 = __this->get_nsStack_19();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		String_t* L_10 = ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->get_prefix_0();
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_7, L_10);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_11 = __this->get_textWriter_0();
		NullCheck(L_11);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_11, ((int32_t)61));
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_12 = __this->get_textWriter_0();
		Il2CppChar L_13 = __this->get_quoteChar_13();
		NullCheck(L_12);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_12, L_13);
		XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * L_14 = __this->get_xmlEncoder_1();
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_15 = __this->get_nsStack_19();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		String_t* L_17 = ((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))->get_ns_1();
		NullCheck(L_14);
		XmlTextEncoder_Write_m3D15126BEF6FE4BE6AA1FA9A25860A11649C8E3B(L_14, L_17, /*hidden argument*/NULL);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_18 = __this->get_textWriter_0();
		Il2CppChar L_19 = __this->get_quoteChar_13();
		NullCheck(L_18);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_18, L_19);
	}

IL_00b2:
	{
		int32_t L_20 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)1));
	}

IL_00b6:
	{
		int32_t L_21 = V_0;
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_22 = __this->get_stack_7();
		int32_t L_23 = __this->get_top_8();
		NullCheck(L_22);
		int32_t L_24 = ((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))->get_prevNsTop_6();
		if ((((int32_t)L_21) > ((int32_t)L_24)))
		{
			goto IL_0018;
		}
	}
	{
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_25 = __this->get_stack_7();
		int32_t L_26 = __this->get_top_8();
		NullCheck(L_25);
		String_t* L_27 = ((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_defaultNs_2();
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_28 = __this->get_stack_7();
		int32_t L_29 = __this->get_top_8();
		NullCheck(L_28);
		String_t* L_30 = ((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_29, (int32_t)1)))))->get_defaultNs_2();
		bool L_31 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_27, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_019a;
		}
	}
	{
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_32 = __this->get_stack_7();
		int32_t L_33 = __this->get_top_8();
		NullCheck(L_32);
		int32_t L_34 = ((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33)))->get_defaultNsState_3();
		if ((!(((uint32_t)L_34) == ((uint32_t)2))))
		{
			goto IL_019a;
		}
	}
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_35 = __this->get_textWriter_0();
		NullCheck(L_35);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_35, _stringLiteral5C7DA3CBD254CB3BB9FCC98E58CF1A56E605861E);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_36 = __this->get_textWriter_0();
		NullCheck(L_36);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_36, ((int32_t)61));
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_37 = __this->get_textWriter_0();
		Il2CppChar L_38 = __this->get_quoteChar_13();
		NullCheck(L_37);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_37, L_38);
		XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * L_39 = __this->get_xmlEncoder_1();
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_40 = __this->get_stack_7();
		int32_t L_41 = __this->get_top_8();
		NullCheck(L_40);
		String_t* L_42 = ((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))->get_defaultNs_2();
		NullCheck(L_39);
		XmlTextEncoder_Write_m3D15126BEF6FE4BE6AA1FA9A25860A11649C8E3B(L_39, L_42, /*hidden argument*/NULL);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_43 = __this->get_textWriter_0();
		Il2CppChar L_44 = __this->get_quoteChar_13();
		NullCheck(L_43);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_43, L_44);
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_45 = __this->get_stack_7();
		int32_t L_46 = __this->get_top_8();
		NullCheck(L_45);
		((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_46)))->set_defaultNsState_3(3);
	}

IL_019a:
	{
		XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * L_47 = __this->get_xmlEncoder_1();
		NullCheck(L_47);
		XmlTextEncoder_EndAttribute_mEBAE3ABA120AF2728E56ADDB2BE2A5B313093C12(L_47, /*hidden argument*/NULL);
		bool L_48 = ___empty0;
		if (!L_48)
		{
			goto IL_01b8;
		}
	}
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_49 = __this->get_textWriter_0();
		NullCheck(L_49);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_49, _stringLiteralCE63895ACF2B7A447477491E6E010B297DD75B0D);
	}

IL_01b8:
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_50 = __this->get_textWriter_0();
		NullCheck(L_50);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_50, ((int32_t)62));
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::WriteEndAttributeQuote()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_WriteEndAttributeQuote_m98AC4165AD8A683AE52D3683F2719E1C8CC578CD (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_specialAttr_16();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		XmlTextWriter_HandleSpecialAttribute_m743156B9137FC8E43B2E697605AAE9C225A2166E(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * L_1 = __this->get_xmlEncoder_1();
		NullCheck(L_1);
		XmlTextEncoder_EndAttribute_mEBAE3ABA120AF2728E56ADDB2BE2A5B313093C12(L_1, /*hidden argument*/NULL);
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_2 = __this->get_textWriter_0();
		Il2CppChar L_3 = __this->get_curQuoteChar_14();
		NullCheck(L_2);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_2, L_3);
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::Indent(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_Indent_m79DEE82659DC5B5F0C4E1E8E3E30C76E7F869851 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, bool ___beforeEndElement0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = __this->get_top_8();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_1 = __this->get_textWriter_0();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(18 /* System.Void System.IO.TextWriter::WriteLine() */, L_1);
		return;
	}

IL_0014:
	{
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_2 = __this->get_stack_7();
		int32_t L_3 = __this->get_top_8();
		NullCheck(L_2);
		bool L_4 = ((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3)))->get_mixed_8();
		if (L_4)
		{
			goto IL_006f;
		}
	}
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_5 = __this->get_textWriter_0();
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(18 /* System.Void System.IO.TextWriter::WriteLine() */, L_5);
		bool L_6 = ___beforeEndElement0;
		if (L_6)
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_7 = __this->get_top_8();
		G_B6_0 = L_7;
		goto IL_004a;
	}

IL_0042:
	{
		int32_t L_8 = __this->get_top_8();
		G_B6_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1));
	}

IL_004a:
	{
		V_0 = G_B6_0;
		int32_t L_9 = V_0;
		int32_t L_10 = __this->get_indentation_5();
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)L_10));
		goto IL_006b;
	}

IL_0056:
	{
		TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * L_11 = __this->get_textWriter_0();
		Il2CppChar L_12 = __this->get_indentChar_6();
		NullCheck(L_11);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_11, L_12);
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)1));
	}

IL_006b:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) > ((int32_t)0)))
		{
			goto IL_0056;
		}
	}

IL_006f:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::PushNamespace(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_PushNamespace_mFAE3B22D92E1EE3EB61C94917C3E9A955D9CD5C2 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_PushNamespace_mFAE3B22D92E1EE3EB61C94917C3E9A955D9CD5C2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2 * G_B9_0 = NULL;
	TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2 * G_B8_0 = NULL;
	int32_t G_B10_0 = 0;
	TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2 * G_B10_1 = NULL;
	{
		String_t* L_0 = ___ns1;
		bool L_1 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(_stringLiteral420B74A52534550B0DD14DCF7D8988C2BD4936CE, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_2 = Res_GetString_m4303E63CA6945331BF5584376F6AA7262D1C6887(_stringLiteral197DB4D12C3F8F844439AC8086D04A7E9906FD24, /*hidden argument*/NULL);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_3 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, XmlTextWriter_PushNamespace_mFAE3B22D92E1EE3EB61C94917C3E9A955D9CD5C2_RuntimeMethod_var);
	}

IL_001d:
	{
		String_t* L_4 = ___prefix0;
		if (L_4)
		{
			goto IL_0075;
		}
	}
	{
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_5 = __this->get_stack_7();
		int32_t L_6 = __this->get_top_8();
		NullCheck(L_5);
		int32_t L_7 = ((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_defaultNsState_3();
		V_0 = L_7;
		int32_t L_8 = V_0;
		if ((!(((uint32_t)L_8) > ((uint32_t)1))))
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) == ((int32_t)2)))
		{
			goto IL_0057;
		}
	}
	{
		return;
	}

IL_0040:
	{
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_10 = __this->get_stack_7();
		int32_t L_11 = __this->get_top_8();
		NullCheck(L_10);
		String_t* L_12 = ___ns1;
		((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->set_defaultNs_2(L_12);
	}

IL_0057:
	{
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_13 = __this->get_stack_7();
		int32_t L_14 = __this->get_top_8();
		NullCheck(L_13);
		bool L_15 = ___declared2;
		G_B8_0 = ((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14)));
		if (L_15)
		{
			G_B9_0 = ((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14)));
			goto IL_006e;
		}
	}
	{
		G_B10_0 = 2;
		G_B10_1 = G_B8_0;
		goto IL_006f;
	}

IL_006e:
	{
		G_B10_0 = 3;
		G_B10_1 = G_B9_0;
	}

IL_006f:
	{
		G_B10_1->set_defaultNsState_3(G_B10_0);
		return;
	}

IL_0075:
	{
		String_t* L_16 = ___prefix0;
		NullCheck(L_16);
		int32_t L_17 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0095;
		}
	}
	{
		String_t* L_18 = ___ns1;
		NullCheck(L_18);
		int32_t L_19 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0095;
		}
	}
	{
		String_t* L_20 = Res_GetString_m4303E63CA6945331BF5584376F6AA7262D1C6887(_stringLiteralB63A1A23B65582B8791AA4E655E90AA5647D7298, /*hidden argument*/NULL);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_21 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21, XmlTextWriter_PushNamespace_mFAE3B22D92E1EE3EB61C94917C3E9A955D9CD5C2_RuntimeMethod_var);
	}

IL_0095:
	{
		String_t* L_22 = ___prefix0;
		int32_t L_23 = XmlTextWriter_LookupNamespace_mB4568061673EFBC795A43BB701ABE3335AFCE9A2(__this, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) == ((int32_t)(-1))))
		{
			goto IL_00d0;
		}
	}
	{
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_25 = __this->get_nsStack_19();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		String_t* L_27 = ((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_ns_1();
		String_t* L_28 = ___ns1;
		bool L_29 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_27, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00d0;
		}
	}
	{
		bool L_30 = ___declared2;
		if (!L_30)
		{
			goto IL_010b;
		}
	}
	{
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_31 = __this->get_nsStack_19();
		int32_t L_32 = V_1;
		NullCheck(L_31);
		((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))->set_declared_2((bool)1);
		return;
	}

IL_00d0:
	{
		bool L_33 = ___declared2;
		if (!L_33)
		{
			goto IL_0102;
		}
	}
	{
		int32_t L_34 = V_1;
		if ((((int32_t)L_34) == ((int32_t)(-1))))
		{
			goto IL_0102;
		}
	}
	{
		int32_t L_35 = V_1;
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_36 = __this->get_stack_7();
		int32_t L_37 = __this->get_top_8();
		NullCheck(L_36);
		int32_t L_38 = ((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))->get_prevNsTop_6();
		if ((((int32_t)L_35) <= ((int32_t)L_38)))
		{
			goto IL_0102;
		}
	}
	{
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_39 = __this->get_nsStack_19();
		int32_t L_40 = V_1;
		NullCheck(L_39);
		((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)))->set_declared_2((bool)1);
	}

IL_0102:
	{
		String_t* L_41 = ___prefix0;
		String_t* L_42 = ___ns1;
		bool L_43 = ___declared2;
		XmlTextWriter_AddNamespace_mD3E28DCB8C014B090EB3FFA4C6F0A71702135E5B(__this, L_41, L_42, L_43, /*hidden argument*/NULL);
	}

IL_010b:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::AddNamespace(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AddNamespace_mD3E28DCB8C014B090EB3FFA4C6F0A71702135E5B (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_AddNamespace_mD3E28DCB8C014B090EB3FFA4C6F0A71702135E5B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* V_2 = NULL;
	int32_t V_3 = 0;
	{
		int32_t L_0 = __this->get_nsTop_20();
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1));
		int32_t L_1 = V_1;
		__this->set_nsTop_20(L_1);
		int32_t L_2 = V_1;
		V_0 = L_2;
		int32_t L_3 = V_0;
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_4 = __this->get_nsStack_19();
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_5 = V_0;
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_6 = (NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6*)(NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6*)SZArrayNew(NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)2)));
		V_2 = L_6;
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_7 = __this->get_nsStack_19();
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_8 = V_2;
		int32_t L_9 = V_0;
		Array_Copy_m40103AA97DC582C557B912CF4BBE86A4D166F803((RuntimeArray *)(RuntimeArray *)L_7, (RuntimeArray *)(RuntimeArray *)L_8, L_9, /*hidden argument*/NULL);
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_10 = V_2;
		__this->set_nsStack_19(L_10);
	}

IL_003a:
	{
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_11 = __this->get_nsStack_19();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		String_t* L_13 = ___prefix0;
		String_t* L_14 = ___ns1;
		bool L_15 = ___declared2;
		Namespace_Set_m56A8063D5B2E5989807DF3472D0266CF828BBDA8((Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12))), L_13, L_14, L_15, /*hidden argument*/NULL);
		bool L_16 = __this->get_useNsHashtable_22();
		if (!L_16)
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_17 = V_0;
		XmlTextWriter_AddToNamespaceHashtable_m35841EE9201BDF774311ED96E3D3DEDB174D24A6(__this, L_17, /*hidden argument*/NULL);
		return;
	}

IL_005e:
	{
		int32_t L_18 = V_0;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_008d;
		}
	}
	{
		SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A * L_19 = (SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A *)il2cpp_codegen_object_new(SecureStringHasher_t5F3BC4AE212133FAD80F39ED81D0338B8A21A87A_il2cpp_TypeInfo_var);
		SecureStringHasher__ctor_mEF97616B6D20B8D66A01B2B833C677BF356A1145(L_19, /*hidden argument*/NULL);
		Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * L_20 = (Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 *)il2cpp_codegen_object_new(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mAC2C8559F9E6A16FAD5BA5017E6A0A1EF3429867(L_20, L_19, /*hidden argument*/Dictionary_2__ctor_mAC2C8559F9E6A16FAD5BA5017E6A0A1EF3429867_RuntimeMethod_var);
		__this->set_nsHashtable_21(L_20);
		V_3 = 0;
		goto IL_0082;
	}

IL_0077:
	{
		int32_t L_21 = V_3;
		XmlTextWriter_AddToNamespaceHashtable_m35841EE9201BDF774311ED96E3D3DEDB174D24A6(__this, L_21, /*hidden argument*/NULL);
		int32_t L_22 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0082:
	{
		int32_t L_23 = V_3;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) <= ((int32_t)L_24)))
		{
			goto IL_0077;
		}
	}
	{
		__this->set_useNsHashtable_22((bool)1);
	}

IL_008d:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::AddToNamespaceHashtable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AddToNamespaceHashtable_m35841EE9201BDF774311ED96E3D3DEDB174D24A6 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, int32_t ___namespaceIndex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_AddToNamespaceHashtable_m35841EE9201BDF774311ED96E3D3DEDB174D24A6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_0 = __this->get_nsStack_19();
		int32_t L_1 = ___namespaceIndex0;
		NullCheck(L_0);
		String_t* L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_prefix_0();
		V_0 = L_2;
		Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * L_3 = __this->get_nsHashtable_21();
		String_t* L_4 = V_0;
		NullCheck(L_3);
		bool L_5 = Dictionary_2_TryGetValue_m8E06A6DF244254BF419D7EC23A9A6809E19A544D(L_3, L_4, (int32_t*)(&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m8E06A6DF244254BF419D7EC23A9A6809E19A544D_RuntimeMethod_var);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_6 = __this->get_nsStack_19();
		int32_t L_7 = ___namespaceIndex0;
		NullCheck(L_6);
		int32_t L_8 = V_1;
		((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))->set_prevNsIndex_3(L_8);
	}

IL_0034:
	{
		Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * L_9 = __this->get_nsHashtable_21();
		String_t* L_10 = V_0;
		int32_t L_11 = ___namespaceIndex0;
		NullCheck(L_9);
		Dictionary_2_set_Item_mBC85AF861FB031847847F0B30707EC7AC252D572(L_9, L_10, L_11, /*hidden argument*/Dictionary_2_set_Item_mBC85AF861FB031847847F0B30707EC7AC252D572_RuntimeMethod_var);
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::PopNamespaces(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_PopNamespaces_mB647C786975F9BB3ADE2301184F8206D816EAFA6 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, int32_t ___indexFrom0, int32_t ___indexTo1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_PopNamespaces_mB647C786975F9BB3ADE2301184F8206D816EAFA6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___indexTo1;
		V_0 = L_0;
		goto IL_0068;
	}

IL_0004:
	{
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_1 = __this->get_nsStack_19();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))->get_prevNsIndex_3();
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0037;
		}
	}
	{
		Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * L_4 = __this->get_nsHashtable_21();
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_5 = __this->get_nsStack_19();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		String_t* L_7 = ((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_prefix_0();
		NullCheck(L_4);
		Dictionary_2_Remove_m105F8AD7F5E827D6F95768BD6C922EB155E5A7EF(L_4, L_7, /*hidden argument*/Dictionary_2_Remove_m105F8AD7F5E827D6F95768BD6C922EB155E5A7EF_RuntimeMethod_var);
		goto IL_0064;
	}

IL_0037:
	{
		Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * L_8 = __this->get_nsHashtable_21();
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_9 = __this->get_nsStack_19();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))->get_prefix_0();
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_12 = __this->get_nsStack_19();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = ((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_prevNsIndex_3();
		NullCheck(L_8);
		Dictionary_2_set_Item_mBC85AF861FB031847847F0B30707EC7AC252D572(L_8, L_11, L_14, /*hidden argument*/Dictionary_2_set_Item_mBC85AF861FB031847847F0B30707EC7AC252D572_RuntimeMethod_var);
	}

IL_0064:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)1));
	}

IL_0068:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = ___indexFrom0;
		if ((((int32_t)L_16) >= ((int32_t)L_17)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Xml.XmlTextWriter::LookupNamespace(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t XmlTextWriter_LookupNamespace_mB4568061673EFBC795A43BB701ABE3335AFCE9A2 (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, String_t* ___prefix0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_LookupNamespace_mB4568061673EFBC795A43BB701ABE3335AFCE9A2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = __this->get_useNsHashtable_22();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * L_1 = __this->get_nsHashtable_21();
		String_t* L_2 = ___prefix0;
		NullCheck(L_1);
		bool L_3 = Dictionary_2_TryGetValue_m8E06A6DF244254BF419D7EC23A9A6809E19A544D(L_1, L_2, (int32_t*)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m8E06A6DF244254BF419D7EC23A9A6809E19A544D_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_4 = V_0;
		return L_4;
	}

IL_001a:
	{
		int32_t L_5 = __this->get_nsTop_20();
		V_1 = L_5;
		goto IL_0042;
	}

IL_0023:
	{
		NamespaceU5BU5D_tD2B59E106C73A8DCA543AC2E02889F4A200F63F6* L_6 = __this->get_nsStack_19();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		String_t* L_8 = ((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))->get_prefix_0();
		String_t* L_9 = ___prefix0;
		bool L_10 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_11 = V_1;
		return L_11;
	}

IL_003e:
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1));
	}

IL_0042:
	{
		int32_t L_13 = V_1;
		if ((((int32_t)L_13) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}

IL_0046:
	{
		return (-1);
	}
}
// System.Void System.Xml.XmlTextWriter::HandleSpecialAttribute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_HandleSpecialAttribute_m743156B9137FC8E43B2E697605AAE9C225A2166E (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_HandleSpecialAttribute_m743156B9137FC8E43B2E697605AAE9C225A2166E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * L_0 = __this->get_xmlEncoder_1();
		NullCheck(L_0);
		String_t* L_1 = XmlTextEncoder_get_AttributeValue_m9E57BB8D3D0C4BBA526DFAEC0064BD1C3F3BA38D(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_specialAttr_16();
		V_1 = L_2;
		int32_t L_3 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)1)))
		{
			case 0:
			{
				goto IL_0040;
			}
			case 1:
			{
				goto IL_0028;
			}
			case 2:
			{
				goto IL_00ab;
			}
		}
	}
	{
		return;
	}

IL_0028:
	{
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_4 = __this->get_stack_7();
		int32_t L_5 = __this->get_top_8();
		NullCheck(L_4);
		String_t* L_6 = V_0;
		((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))->set_xmlLang_5(L_6);
		return;
	}

IL_0040:
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t5D0BE0A0EE15E2D3EC7F4881C519B5137DFA370A_il2cpp_TypeInfo_var);
		String_t* L_8 = XmlConvert_TrimString_mF0E4AC16BD05053538B20B21DBD64447195A2D1B(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		String_t* L_9 = V_0;
		bool L_10 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_9, _stringLiteral6F5EC7239B41C242FCB23B64D91DA0070FC1C044, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006c;
		}
	}
	{
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_11 = __this->get_stack_7();
		int32_t L_12 = __this->get_top_8();
		NullCheck(L_11);
		((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))->set_xmlSpace_4(1);
		return;
	}

IL_006c:
	{
		String_t* L_13 = V_0;
		bool L_14 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_13, _stringLiteral916F4E7879C25AF1EA844F7068842D5508777C48, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0091;
		}
	}
	{
		TagInfoU5BU5D_t363419634609512E208D2FC275865D41A9CFF637* L_15 = __this->get_stack_7();
		int32_t L_16 = __this->get_top_8();
		NullCheck(L_15);
		((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))->set_xmlSpace_4(2);
		return;
	}

IL_0091:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_17 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_18 = L_17;
		String_t* L_19 = V_0;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_19);
		String_t* L_20 = Res_GetString_m1D06B81901B03CA849045926741403907612BB4B(_stringLiteralB76F93064E827FE764713C55F705D2E388DF17D7, L_18, /*hidden argument*/NULL);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_21 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21, XmlTextWriter_HandleSpecialAttribute_m743156B9137FC8E43B2E697605AAE9C225A2166E_RuntimeMethod_var);
	}

IL_00ab:
	{
		String_t* L_22 = __this->get_prefixForXmlNs_17();
		String_t* L_23 = V_0;
		XmlTextWriter_VerifyPrefixXml_m7729FF5CEA9F697C2B498B672D02C4C895A287FB(__this, L_22, L_23, /*hidden argument*/NULL);
		String_t* L_24 = __this->get_prefixForXmlNs_17();
		String_t* L_25 = V_0;
		XmlTextWriter_PushNamespace_mFAE3B22D92E1EE3EB61C94917C3E9A955D9CD5C2(__this, L_24, L_25, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::VerifyPrefixXml(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_VerifyPrefixXml_m7729FF5CEA9F697C2B498B672D02C4C895A287FB (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, String_t* ___prefix0, String_t* ___ns1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_VerifyPrefixXml_m7729FF5CEA9F697C2B498B672D02C4C895A287FB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___prefix0;
		if (!L_0)
		{
			goto IL_006b;
		}
	}
	{
		String_t* L_1 = ___prefix0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)3))))
		{
			goto IL_006b;
		}
	}
	{
		String_t* L_3 = ___prefix0;
		NullCheck(L_3);
		Il2CppChar L_4 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_3, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)120))))
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_5 = ___prefix0;
		NullCheck(L_5);
		Il2CppChar L_6 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_5, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)88)))))
		{
			goto IL_006b;
		}
	}

IL_0022:
	{
		String_t* L_7 = ___prefix0;
		NullCheck(L_7);
		Il2CppChar L_8 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_7, 1, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)((int32_t)109))))
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_9 = ___prefix0;
		NullCheck(L_9);
		Il2CppChar L_10 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_9, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)77)))))
		{
			goto IL_006b;
		}
	}

IL_0038:
	{
		String_t* L_11 = ___prefix0;
		NullCheck(L_11);
		Il2CppChar L_12 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_11, 2, /*hidden argument*/NULL);
		if ((((int32_t)L_12) == ((int32_t)((int32_t)108))))
		{
			goto IL_004e;
		}
	}
	{
		String_t* L_13 = ___prefix0;
		NullCheck(L_13);
		Il2CppChar L_14 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_13, 2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)76)))))
		{
			goto IL_006b;
		}
	}

IL_004e:
	{
		String_t* L_15 = ___ns1;
		bool L_16 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(_stringLiteral349C6DC0F34B9BA242E4C728EDD28CAB809D4917, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_006b;
		}
	}
	{
		String_t* L_17 = Res_GetString_m4303E63CA6945331BF5584376F6AA7262D1C6887(_stringLiteralC3FBA6155AE85E3CB81F8437A6F1C503BB7D09B7, /*hidden argument*/NULL);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_18 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_18, L_17, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18, XmlTextWriter_VerifyPrefixXml_m7729FF5CEA9F697C2B498B672D02C4C895A287FB_RuntimeMethod_var);
	}

IL_006b:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::FlushEncoders()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_FlushEncoders_m6D7554550029EE5407C9149A740B03B138B44C1B (XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2 * __this, const RuntimeMethod* method)
{
	{
		XmlTextWriterBase64Encoder_t6C566B592DFF5C0E5108670EB19BA06D35F0213B * L_0 = __this->get_base64Encoder_12();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		XmlTextWriterBase64Encoder_t6C566B592DFF5C0E5108670EB19BA06D35F0213B * L_1 = __this->get_base64Encoder_12();
		NullCheck(L_1);
		Base64Encoder_Flush_m7C096C39FD6F6EB5AAEBB7C5ED816DD8F24CA5B0(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		__this->set_flush_18((bool)0);
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter__cctor_m0368A357057D3B0847BD1AC54EA1E4E609C9DEC7 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter__cctor_m0368A357057D3B0847BD1AC54EA1E4E609C9DEC7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral8243A16D425F93AF62CAAB2BFAE01A2D6246A5FE);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral8243A16D425F93AF62CAAB2BFAE01A2D6246A5FE);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteralD2B699E6F0658A4364F9D1A0F5A4EB7C94F6F656);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralD2B699E6F0658A4364F9D1A0F5A4EB7C94F6F656);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteralE1C34BAD815D8104249A053C9455FA539CDF5036);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralE1C34BAD815D8104249A053C9455FA539CDF5036);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral9932973D4B6AA1AA193C06D8D34B58B677685003);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral9932973D4B6AA1AA193C06D8D34B58B677685003);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral120472D8D40924F6F8355A94DB677A8F142E2EB6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral120472D8D40924F6F8355A94DB677A8F142E2EB6);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteralB6F0795DD4F409C92875D0327F58FDEA357047F1);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteralB6F0795DD4F409C92875D0327F58FDEA357047F1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral41477B187466178A05A136C12F806B3EDCAB6349);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral41477B187466178A05A136C12F806B3EDCAB6349);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_7;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteralCD347E1307036F9337DFB643A0DA73051573F178);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteralCD347E1307036F9337DFB643A0DA73051573F178);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteralD6DCC897C02A857315752249765CB47ADDF4E5C7);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteralD6DCC897C02A857315752249765CB47ADDF4E5C7);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteralBBC1106D65ACC9B0DB541F4FAF5DBAF5B7F7BA1F);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteralBBC1106D65ACC9B0DB541F4FAF5DBAF5B7F7BA1F);
		((XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var))->set_stateName_24(L_10);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)((int32_t)14));
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral5296173AB8D4E282E989F7B0B7B7C9A3ACF26E18);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral5296173AB8D4E282E989F7B0B7B7C9A3ACF26E18);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral3D9E33C04AB4A0A0E8E88B74BB9D5F9003E15C78);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3D9E33C04AB4A0A0E8E88B74BB9D5F9003E15C78);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral3EBF7CFEC7929F196835D5D12FBBE2F845BF2A5F);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3EBF7CFEC7929F196835D5D12FBBE2F845BF2A5F);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = L_14;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral4B4906CA939816C30A7D9C9A911CF6A9F506CCE3);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral4B4906CA939816C30A7D9C9A911CF6A9F506CCE3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral56EF1F2751B48DAA3EC7F762F420920B8E40757A);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral56EF1F2751B48DAA3EC7F762F420920B8E40757A);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral7BBD8C0287448D34297B233D6604618390BA9C38);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral7BBD8C0287448D34297B233D6604618390BA9C38);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral0FD08B3C99047786420F2D9A7AC37D5DC351C9C8);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral0FD08B3C99047786420F2D9A7AC37D5DC351C9C8);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral23A6BF321FC205420788A7F2E1A1334F6C712C89);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral23A6BF321FC205420788A7F2E1A1334F6C712C89);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_20 = L_19;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral2343901946DDD83EFBB8B93C33F01CBD394063B2);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral2343901946DDD83EFBB8B93C33F01CBD394063B2);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteralB6F0795DD4F409C92875D0327F58FDEA357047F1);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteralB6F0795DD4F409C92875D0327F58FDEA357047F1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral45B9464D208C0A900770E5F7A95E5FE067CD4CCC);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral45B9464D208C0A900770E5F7A95E5FE067CD4CCC);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteralF41760006700B346FE970834ED6436CD21A1330F);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteralF41760006700B346FE970834ED6436CD21A1330F);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_24 = L_23;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral1E1DB32AFA986C58B9ACC9C146BF83E67C0DCE66);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral1E1DB32AFA986C58B9ACC9C146BF83E67C0DCE66);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_25 = L_24;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral32189949CB1CA4A6EBB1A643EBE2DB69713D5407);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral32189949CB1CA4A6EBB1A643EBE2DB69713D5407);
		((XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var))->set_tokenName_25(L_25);
		StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* L_26 = (StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A*)(StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A*)SZArrayNew(StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A_il2cpp_TypeInfo_var, (uint32_t)((int32_t)104));
		StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* L_27 = L_26;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_28 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E____B368804F0C6DAB083B253A6B106D0783D5C32E90_2_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_27, L_28, /*hidden argument*/NULL);
		((XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var))->set_stateTableDefault_26(L_27);
		StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* L_29 = (StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A*)(StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A*)SZArrayNew(StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A_il2cpp_TypeInfo_var, (uint32_t)((int32_t)104));
		StateU5BU5D_tB7B678399A1D27FF20B86135AC6A0FBFA9564F3A* L_30 = L_29;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_31 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tAA330E6B4295DC1363094EDE988D3B524C40486E____6A0D50D692745A6663128CD315B71079584F3E59_1_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_30, L_31, /*hidden argument*/NULL);
		((XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_t2CC07B7AAF1DF89CE65E7782EBC8CA1BB179DAD2_il2cpp_TypeInfo_var))->set_stateTableDocument_27(L_30);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlTextWriterBase64Encoder::WriteChars(System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriterBase64Encoder_WriteChars_m7BCA7FB4E1B8A1A320D13E122D15C504D8EA3EF9 (XmlTextWriterBase64Encoder_t6C566B592DFF5C0E5108670EB19BA06D35F0213B * __this, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___chars0, int32_t ___index1, int32_t ___count2, const RuntimeMethod* method)
{
	{
		XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * L_0 = __this->get_xmlTextEncoder_3();
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_1 = ___chars0;
		int32_t L_2 = ___index1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		XmlTextEncoder_WriteRaw_m5B4E690DB11115A1A2EBF4228182024DD5BB8434(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlWriter::Close()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriter_Close_m625EEF85A0D65E59BEEE4BA221C8E4DF531C8026 (XmlWriter_t676293C138D2D0DAB9C1BC16A7BEE618391C5B2D * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void System.Xml.XmlWriter::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriter_Dispose_mD84FFF675D66861F00314F213AA637507DF74102 (XmlWriter_t676293C138D2D0DAB9C1BC16A7BEE618391C5B2D * __this, const RuntimeMethod* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(8 /* System.Void System.Xml.XmlWriter::Dispose(System.Boolean) */, __this, (bool)1);
		return;
	}
}
// System.Void System.Xml.XmlWriter::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriter_Dispose_m1C350DA8CB5925DBAE6073FFFD18ED5824EF61EA (XmlWriter_t676293C138D2D0DAB9C1BC16A7BEE618391C5B2D * __this, bool ___disposing0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___disposing0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Xml.WriteState System.Xml.XmlWriter::get_WriteState() */, __this);
		if ((((int32_t)L_1) == ((int32_t)5)))
		{
			goto IL_0012;
		}
	}
	{
		VirtActionInvoker0::Invoke(7 /* System.Void System.Xml.XmlWriter::Close() */, __this);
	}

IL_0012:
	{
		return;
	}
}
// System.Void System.Xml.XmlWriter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriter__ctor_mAA10A026AD230D064314D2AE472A144D18418A01 (XmlWriter_t676293C138D2D0DAB9C1BC16A7BEE618391C5B2D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  int32_t DelegatePInvokeWrapper_HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 (HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * __this, String_t* ___s0, int32_t ___sLen1, int64_t ___additionalEntropy2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc)(char*, int32_t, int64_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___s0' to native representation
	char* ____s0_marshaled = NULL;
	____s0_marshaled = il2cpp_codegen_marshal_string(___s0);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____s0_marshaled, ___sLen1, ___additionalEntropy2);

	// Marshaling cleanup of parameter '___s0' native representation
	il2cpp_codegen_marshal_free(____s0_marshaled);
	____s0_marshaled = NULL;

	return returnValue;
}
// System.Void System.Xml.SecureStringHasher_HashCodeOfStringDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HashCodeOfStringDelegate__ctor_m47EFBAB22CC6E0C04A17B8DA7101906596143BE8 (HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Xml.SecureStringHasher_HashCodeOfStringDelegate::Invoke(System.String,System.Int32,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HashCodeOfStringDelegate_Invoke_m29101897DFC3FF4CEC6E0F148E1E879463DA2F9D (HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * __this, String_t* ___s0, int32_t ___sLen1, int64_t ___additionalEntropy2, const RuntimeMethod* method)
{
	int32_t result = 0;
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 3)
			{
				// open
				typedef int32_t (*FunctionPointerType) (String_t*, int32_t, int64_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___s0, ___sLen1, ___additionalEntropy2, targetMethod);
			}
			else
			{
				// closed
				typedef int32_t (*FunctionPointerType) (void*, String_t*, int32_t, int64_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___s0, ___sLen1, ___additionalEntropy2, targetMethod);
			}
		}
		else if (___parameterCount != 3)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker2< int32_t, int32_t, int64_t >::Invoke(targetMethod, ___s0, ___sLen1, ___additionalEntropy2);
					else
						result = GenericVirtFuncInvoker2< int32_t, int32_t, int64_t >::Invoke(targetMethod, ___s0, ___sLen1, ___additionalEntropy2);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker2< int32_t, int32_t, int64_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___s0, ___sLen1, ___additionalEntropy2);
					else
						result = VirtFuncInvoker2< int32_t, int32_t, int64_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___s0, ___sLen1, ___additionalEntropy2);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef int32_t (*FunctionPointerType) (RuntimeObject*, int64_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(&___sLen1) - 1), ___additionalEntropy2, targetMethod);
				}
				typedef int32_t (*FunctionPointerType) (String_t*, int32_t, int64_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___s0, ___sLen1, ___additionalEntropy2, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker3< int32_t, String_t*, int32_t, int64_t >::Invoke(targetMethod, targetThis, ___s0, ___sLen1, ___additionalEntropy2);
					else
						result = GenericVirtFuncInvoker3< int32_t, String_t*, int32_t, int64_t >::Invoke(targetMethod, targetThis, ___s0, ___sLen1, ___additionalEntropy2);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker3< int32_t, String_t*, int32_t, int64_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___s0, ___sLen1, ___additionalEntropy2);
					else
						result = VirtFuncInvoker3< int32_t, String_t*, int32_t, int64_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___s0, ___sLen1, ___additionalEntropy2);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef int32_t (*FunctionPointerType) (RuntimeObject*, int32_t, int64_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___s0) - 1), ___sLen1, ___additionalEntropy2, targetMethod);
				}
				if (targetThis == NULL)
				{
					typedef int32_t (*FunctionPointerType) (String_t*, int32_t, int64_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___s0, ___sLen1, ___additionalEntropy2, targetMethod);
				}
				else
				{
					typedef int32_t (*FunctionPointerType) (void*, String_t*, int32_t, int64_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___s0, ___sLen1, ___additionalEntropy2, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult System.Xml.SecureStringHasher_HashCodeOfStringDelegate::BeginInvoke(System.String,System.Int32,System.Int64,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* HashCodeOfStringDelegate_BeginInvoke_m041CC1DB7FC71916966716A0BC20C28B1FD86CAD (HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * __this, String_t* ___s0, int32_t ___sLen1, int64_t ___additionalEntropy2, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HashCodeOfStringDelegate_BeginInvoke_m041CC1DB7FC71916966716A0BC20C28B1FD86CAD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___s0;
	__d_args[1] = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &___sLen1);
	__d_args[2] = Box(Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3_il2cpp_TypeInfo_var, &___additionalEntropy2);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// System.Int32 System.Xml.SecureStringHasher_HashCodeOfStringDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HashCodeOfStringDelegate_EndInvoke_m12CBE82586394D027E1A760D22A1138F339BEE7E (HashCodeOfStringDelegate_tC5A341CE8A72771BCCF8A0CC95A12B13CD4F2574 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((RuntimeObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: System.Xml.XmlTextWriter/Namespace
IL2CPP_EXTERN_C void Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshal_pinvoke(const Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB& unmarshaled, Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshaled_pinvoke& marshaled)
{
	marshaled.___prefix_0 = il2cpp_codegen_marshal_string(unmarshaled.get_prefix_0());
	marshaled.___ns_1 = il2cpp_codegen_marshal_string(unmarshaled.get_ns_1());
	marshaled.___declared_2 = static_cast<int32_t>(unmarshaled.get_declared_2());
	marshaled.___prevNsIndex_3 = unmarshaled.get_prevNsIndex_3();
}
IL2CPP_EXTERN_C void Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshal_pinvoke_back(const Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshaled_pinvoke& marshaled, Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB& unmarshaled)
{
	unmarshaled.set_prefix_0(il2cpp_codegen_marshal_string_result(marshaled.___prefix_0));
	unmarshaled.set_ns_1(il2cpp_codegen_marshal_string_result(marshaled.___ns_1));
	bool unmarshaled_declared_temp_2 = false;
	unmarshaled_declared_temp_2 = static_cast<bool>(marshaled.___declared_2);
	unmarshaled.set_declared_2(unmarshaled_declared_temp_2);
	int32_t unmarshaled_prevNsIndex_temp_3 = 0;
	unmarshaled_prevNsIndex_temp_3 = marshaled.___prevNsIndex_3;
	unmarshaled.set_prevNsIndex_3(unmarshaled_prevNsIndex_temp_3);
}
// Conversion method for clean up from marshalling of: System.Xml.XmlTextWriter/Namespace
IL2CPP_EXTERN_C void Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshal_pinvoke_cleanup(Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___prefix_0);
	marshaled.___prefix_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___ns_1);
	marshaled.___ns_1 = NULL;
}
// Conversion methods for marshalling of: System.Xml.XmlTextWriter/Namespace
IL2CPP_EXTERN_C void Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshal_com(const Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB& unmarshaled, Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshaled_com& marshaled)
{
	marshaled.___prefix_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_prefix_0());
	marshaled.___ns_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_ns_1());
	marshaled.___declared_2 = static_cast<int32_t>(unmarshaled.get_declared_2());
	marshaled.___prevNsIndex_3 = unmarshaled.get_prevNsIndex_3();
}
IL2CPP_EXTERN_C void Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshal_com_back(const Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshaled_com& marshaled, Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB& unmarshaled)
{
	unmarshaled.set_prefix_0(il2cpp_codegen_marshal_bstring_result(marshaled.___prefix_0));
	unmarshaled.set_ns_1(il2cpp_codegen_marshal_bstring_result(marshaled.___ns_1));
	bool unmarshaled_declared_temp_2 = false;
	unmarshaled_declared_temp_2 = static_cast<bool>(marshaled.___declared_2);
	unmarshaled.set_declared_2(unmarshaled_declared_temp_2);
	int32_t unmarshaled_prevNsIndex_temp_3 = 0;
	unmarshaled_prevNsIndex_temp_3 = marshaled.___prevNsIndex_3;
	unmarshaled.set_prevNsIndex_3(unmarshaled_prevNsIndex_temp_3);
}
// Conversion method for clean up from marshalling of: System.Xml.XmlTextWriter/Namespace
IL2CPP_EXTERN_C void Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshal_com_cleanup(Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___prefix_0);
	marshaled.___prefix_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___ns_1);
	marshaled.___ns_1 = NULL;
}
// System.Void System.Xml.XmlTextWriter_Namespace::Set(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Namespace_Set_m56A8063D5B2E5989807DF3472D0266CF828BBDA8 (Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___prefix0;
		__this->set_prefix_0(L_0);
		String_t* L_1 = ___ns1;
		__this->set_ns_1(L_1);
		bool L_2 = ___declared2;
		__this->set_declared_2(L_2);
		__this->set_prevNsIndex_3((-1));
		return;
	}
}
IL2CPP_EXTERN_C  void Namespace_Set_m56A8063D5B2E5989807DF3472D0266CF828BBDA8_AdjustorThunk (RuntimeObject * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB * _thisAdjusted = reinterpret_cast<Namespace_tFB1EBA6EE8C3E28B35158FCCE171FDC302CD20EB *>(__this + _offset);
	Namespace_Set_m56A8063D5B2E5989807DF3472D0266CF828BBDA8(_thisAdjusted, ___prefix0, ___ns1, ___declared2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: System.Xml.XmlTextWriter/TagInfo
IL2CPP_EXTERN_C void TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshal_pinvoke(const TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2& unmarshaled, TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___prefix_1 = il2cpp_codegen_marshal_string(unmarshaled.get_prefix_1());
	marshaled.___defaultNs_2 = il2cpp_codegen_marshal_string(unmarshaled.get_defaultNs_2());
	marshaled.___defaultNsState_3 = unmarshaled.get_defaultNsState_3();
	marshaled.___xmlSpace_4 = unmarshaled.get_xmlSpace_4();
	marshaled.___xmlLang_5 = il2cpp_codegen_marshal_string(unmarshaled.get_xmlLang_5());
	marshaled.___prevNsTop_6 = unmarshaled.get_prevNsTop_6();
	marshaled.___prefixCount_7 = unmarshaled.get_prefixCount_7();
	marshaled.___mixed_8 = static_cast<int32_t>(unmarshaled.get_mixed_8());
}
IL2CPP_EXTERN_C void TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshal_pinvoke_back(const TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshaled_pinvoke& marshaled, TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	unmarshaled.set_prefix_1(il2cpp_codegen_marshal_string_result(marshaled.___prefix_1));
	unmarshaled.set_defaultNs_2(il2cpp_codegen_marshal_string_result(marshaled.___defaultNs_2));
	int32_t unmarshaled_defaultNsState_temp_3 = 0;
	unmarshaled_defaultNsState_temp_3 = marshaled.___defaultNsState_3;
	unmarshaled.set_defaultNsState_3(unmarshaled_defaultNsState_temp_3);
	int32_t unmarshaled_xmlSpace_temp_4 = 0;
	unmarshaled_xmlSpace_temp_4 = marshaled.___xmlSpace_4;
	unmarshaled.set_xmlSpace_4(unmarshaled_xmlSpace_temp_4);
	unmarshaled.set_xmlLang_5(il2cpp_codegen_marshal_string_result(marshaled.___xmlLang_5));
	int32_t unmarshaled_prevNsTop_temp_6 = 0;
	unmarshaled_prevNsTop_temp_6 = marshaled.___prevNsTop_6;
	unmarshaled.set_prevNsTop_6(unmarshaled_prevNsTop_temp_6);
	int32_t unmarshaled_prefixCount_temp_7 = 0;
	unmarshaled_prefixCount_temp_7 = marshaled.___prefixCount_7;
	unmarshaled.set_prefixCount_7(unmarshaled_prefixCount_temp_7);
	bool unmarshaled_mixed_temp_8 = false;
	unmarshaled_mixed_temp_8 = static_cast<bool>(marshaled.___mixed_8);
	unmarshaled.set_mixed_8(unmarshaled_mixed_temp_8);
}
// Conversion method for clean up from marshalling of: System.Xml.XmlTextWriter/TagInfo
IL2CPP_EXTERN_C void TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshal_pinvoke_cleanup(TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___prefix_1);
	marshaled.___prefix_1 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___defaultNs_2);
	marshaled.___defaultNs_2 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___xmlLang_5);
	marshaled.___xmlLang_5 = NULL;
}
// Conversion methods for marshalling of: System.Xml.XmlTextWriter/TagInfo
IL2CPP_EXTERN_C void TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshal_com(const TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2& unmarshaled, TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___prefix_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_prefix_1());
	marshaled.___defaultNs_2 = il2cpp_codegen_marshal_bstring(unmarshaled.get_defaultNs_2());
	marshaled.___defaultNsState_3 = unmarshaled.get_defaultNsState_3();
	marshaled.___xmlSpace_4 = unmarshaled.get_xmlSpace_4();
	marshaled.___xmlLang_5 = il2cpp_codegen_marshal_bstring(unmarshaled.get_xmlLang_5());
	marshaled.___prevNsTop_6 = unmarshaled.get_prevNsTop_6();
	marshaled.___prefixCount_7 = unmarshaled.get_prefixCount_7();
	marshaled.___mixed_8 = static_cast<int32_t>(unmarshaled.get_mixed_8());
}
IL2CPP_EXTERN_C void TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshal_com_back(const TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshaled_com& marshaled, TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	unmarshaled.set_prefix_1(il2cpp_codegen_marshal_bstring_result(marshaled.___prefix_1));
	unmarshaled.set_defaultNs_2(il2cpp_codegen_marshal_bstring_result(marshaled.___defaultNs_2));
	int32_t unmarshaled_defaultNsState_temp_3 = 0;
	unmarshaled_defaultNsState_temp_3 = marshaled.___defaultNsState_3;
	unmarshaled.set_defaultNsState_3(unmarshaled_defaultNsState_temp_3);
	int32_t unmarshaled_xmlSpace_temp_4 = 0;
	unmarshaled_xmlSpace_temp_4 = marshaled.___xmlSpace_4;
	unmarshaled.set_xmlSpace_4(unmarshaled_xmlSpace_temp_4);
	unmarshaled.set_xmlLang_5(il2cpp_codegen_marshal_bstring_result(marshaled.___xmlLang_5));
	int32_t unmarshaled_prevNsTop_temp_6 = 0;
	unmarshaled_prevNsTop_temp_6 = marshaled.___prevNsTop_6;
	unmarshaled.set_prevNsTop_6(unmarshaled_prevNsTop_temp_6);
	int32_t unmarshaled_prefixCount_temp_7 = 0;
	unmarshaled_prefixCount_temp_7 = marshaled.___prefixCount_7;
	unmarshaled.set_prefixCount_7(unmarshaled_prefixCount_temp_7);
	bool unmarshaled_mixed_temp_8 = false;
	unmarshaled_mixed_temp_8 = static_cast<bool>(marshaled.___mixed_8);
	unmarshaled.set_mixed_8(unmarshaled_mixed_temp_8);
}
// Conversion method for clean up from marshalling of: System.Xml.XmlTextWriter/TagInfo
IL2CPP_EXTERN_C void TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshal_com_cleanup(TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___prefix_1);
	marshaled.___prefix_1 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___defaultNs_2);
	marshaled.___defaultNs_2 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___xmlLang_5);
	marshaled.___xmlLang_5 = NULL;
}
// System.Void System.Xml.XmlTextWriter_TagInfo::Init(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagInfo_Init_mFC65CB7C7A7D7852E3A2825365F1BD35CC38F8CF (TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2 * __this, int32_t ___nsTop0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TagInfo_Init_mFC65CB7C7A7D7852E3A2825365F1BD35CC38F8CF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_name_0((String_t*)NULL);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_defaultNs_2(L_0);
		__this->set_defaultNsState_3(0);
		__this->set_xmlSpace_4(0);
		__this->set_xmlLang_5((String_t*)NULL);
		int32_t L_1 = ___nsTop0;
		__this->set_prevNsTop_6(L_1);
		__this->set_prefixCount_7(0);
		__this->set_mixed_8((bool)0);
		return;
	}
}
IL2CPP_EXTERN_C  void TagInfo_Init_mFC65CB7C7A7D7852E3A2825365F1BD35CC38F8CF_AdjustorThunk (RuntimeObject * __this, int32_t ___nsTop0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2 * _thisAdjusted = reinterpret_cast<TagInfo_tCB16E7242088C97871045AB8D1E6B0D12350D6B2 *>(__this + _offset);
	TagInfo_Init_mFC65CB7C7A7D7852E3A2825365F1BD35CC38F8CF(_thisAdjusted, ___nsTop0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void XmlCharType__ctor_mF597588002D63E65BC835ABC21221BB1525A6637_inline (XmlCharType_t0B35CAE2B2E20F28A418270966E9989BBDB004BA * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___charProperties0, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___charProperties0;
		__this->set_charProperties_2(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* SerializationEntry_get_Name_m1314B9BFC9D6CFCC607FF1B2B748583DAEB1D1E2_inline (SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_m_name_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * SerializationEntry_get_Value_m7D4406AB9EF2F4ADE65FFC23C4F79EB1A3749BD6_inline (SerializationEntry_t33A292618975AD7AC936CB98B2F28256817A467E * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_m_value_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Exception_set_HResult_mB9E603303A0678B32684B0EEC144334BAB0E6392_inline (Exception_t * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set__HResult_11(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void XmlTextEncoder_set_QuoteChar_m1897FBEC0EAB4616C3FD961A12AFC0F7ED771824_inline (XmlTextEncoder_tA6BBE279AC4571C495CD910D11CA79B33979ED0E * __this, Il2CppChar ___value0, const RuntimeMethod* method)
{
	{
		Il2CppChar L_0 = ___value0;
		__this->set_quoteChar_2(L_0);
		return;
	}
}
