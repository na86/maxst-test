﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// AndroidRuntimePermissions/PermissionResult
struct PermissionResult_t6B10043A6076E68C28B3AF4A5D25231B9D35D3C5;
// AndroidRuntimePermissions/PermissionResultMultiple
struct PermissionResultMultiple_t2CD1F439CAC132F6C973F54FAC4F63B834C4B280;
// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// AndroidRuntimePermissions/Permission[]
struct PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C RuntimeClass* ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Permission_t472A96823E2CCCEAE2337194F2F9C6D377C72DB5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral445D5FAD900CBCFA0E910C25BABCD6438958620F;
IL2CPP_EXTERN_C String_t* _stringLiteral5BE2DC525B76C096079B960B32063FCBD00D9A1B;
IL2CPP_EXTERN_C String_t* _stringLiteral9E010293A81ADD6752DB430F04400D8B1CF2A6E7;
IL2CPP_EXTERN_C String_t* _stringLiteral9EDAF9B1F97AF662638A0E9509DEDFFBCE4504EB;
IL2CPP_EXTERN_C String_t* _stringLiteralCC0D9E65EE8CF25F6D9BF8FAC3A75877D5906329;
IL2CPP_EXTERN_C const RuntimeMethod* AndroidRuntimePermissions_ValidateArgument_mDCC23BBFDE3060C39D8B20ABA8F0D372457B3A14_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t AndroidRuntimePermissions_CachePermission_m85462AC6DFCDB6707611790CF03A99CE68CFED03_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AndroidRuntimePermissions_GetCachedPermission_m59CC59493EB088760A3CB5DBA541F3DFDEF31940_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AndroidRuntimePermissions_GetCachedPermissions_mD380D1D47209B5DBE60662E7C04B2F42CC681E0B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AndroidRuntimePermissions_GetDummyResult_mFC7331AF1F204B0F5BB6D07F8D126DF237DC0C37_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AndroidRuntimePermissions_OpenSettings_m715FB844FD87C0BC0E0081CF0F235F52BFCCD1E2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AndroidRuntimePermissions_ProcessPermissionRequest_m36D72CEA24CA06FA203C4D76464BC7B6F2995756_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AndroidRuntimePermissions_ValidateArgument_mDCC23BBFDE3060C39D8B20ABA8F0D372457B3A14_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PermissionResult_BeginInvoke_mBD30EDF3AB3D28A043775BAD07C015322B6315EC_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75;
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t00DDB26693B7BCCD40544DBD546A67E6CCF39740 
{
public:

public:
};


// System.Object


// AndroidRuntimePermissions
struct  AndroidRuntimePermissions_t8ECDF0E44083BB188D3992864AE2FA5C5FFBF8B3  : public RuntimeObject
{
public:

public:
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkChars_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkPrevious_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct  Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// AndroidRuntimePermissions_Permission
struct  Permission_t472A96823E2CCCEAE2337194F2F9C6D377C72DB5 
{
public:
	// System.Int32 AndroidRuntimePermissions_Permission::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Permission_t472A96823E2CCCEAE2337194F2F9C6D377C72DB5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// AndroidRuntimePermissions_PermissionResult
struct  PermissionResult_t6B10043A6076E68C28B3AF4A5D25231B9D35D3C5  : public MulticastDelegate_t
{
public:

public:
};


// AndroidRuntimePermissions_PermissionResultMultiple
struct  PermissionResultMultiple_t2CD1F439CAC132F6C973F54FAC4F63B834C4B280  : public MulticastDelegate_t
{
public:

public:
};


// System.ArgumentException
struct  ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.AsyncCallback
struct  AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// AndroidRuntimePermissions_Permission[]
struct PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void AndroidRuntimePermissions::ValidateArgument(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidRuntimePermissions_ValidateArgument_mDCC23BBFDE3060C39D8B20ABA8F0D372457B3A14 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, const RuntimeMethod* method);
// AndroidRuntimePermissions/Permission[] AndroidRuntimePermissions::GetDummyResult(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* AndroidRuntimePermissions_GetDummyResult_mFC7331AF1F204B0F5BB6D07F8D126DF237DC0C37 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, const RuntimeMethod* method);
// System.Void AndroidRuntimePermissions/PermissionResult::Invoke(System.String,AndroidRuntimePermissions/Permission)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PermissionResult_Invoke_m32F0AA474CE06E5032395BE731466AD0CBDB0DBF (PermissionResult_t6B10043A6076E68C28B3AF4A5D25231B9D35D3C5 * __this, String_t* ___permission0, int32_t ___result1, const RuntimeMethod* method);
// System.Void AndroidRuntimePermissions/PermissionResultMultiple::Invoke(System.String[],AndroidRuntimePermissions/Permission[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PermissionResultMultiple_Invoke_m1D91F914B4496A34B16943BBDC43ADB39624EACC (PermissionResultMultiple_t2CD1F439CAC132F6C973F54FAC4F63B834C4B280 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* ___result1, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70 (String_t* __this, int32_t ___index0, const RuntimeMethod* method);
// AndroidRuntimePermissions/Permission AndroidRuntimePermissions::ToPermission(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AndroidRuntimePermissions_ToPermission_m8CA4B19E11156AD2D0BD8F3DCBA7CD9D704944B1 (Il2CppChar ___ch0, const RuntimeMethod* method);
// System.Boolean AndroidRuntimePermissions::CachePermission(System.String,AndroidRuntimePermissions/Permission)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AndroidRuntimePermissions_CachePermission_m85462AC6DFCDB6707611790CF03A99CE68CFED03 (String_t* ___permission0, int32_t ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::Save()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_Save_m2C1E628FA335095CD88D0DA1CB50ACC924667EEC (const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_mDA4476C10FCFF55FC65816E5E519B0EAFCB2AC14 (String_t* ___key0, int32_t ___defaultValue1, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_mEDFFE2D378A15F6DAB54D52661C84C1B52E7BA2E (StringBuilder_t * __this, int32_t ___capacity0, const RuntimeMethod* method);
// AndroidRuntimePermissions/Permission AndroidRuntimePermissions::GetCachedPermission(System.String,AndroidRuntimePermissions/Permission)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AndroidRuntimePermissions_GetCachedPermission_m59CC59493EB088760A3CB5DBA541F3DFDEF31940 (String_t* ___permission0, int32_t ___defaultValue1, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m796285D173EEA5261E85B95FC79DD4F996CC93DD (StringBuilder_t * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_m0C5C977E960B9CA8F9AB73AF4129C3DCABD067B6 (String_t* ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C (String_t* ___value0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AndroidRuntimePermissions::OpenSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidRuntimePermissions_OpenSettings_m715FB844FD87C0BC0E0081CF0F235F52BFCCD1E2 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidRuntimePermissions_OpenSettings_m715FB844FD87C0BC0E0081CF0F235F52BFCCD1E2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log( "Opening settings..." );
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralCC0D9E65EE8CF25F6D9BF8FAC3A75877D5906329, /*hidden argument*/NULL);
		// }
		return;
	}
}
// AndroidRuntimePermissions_Permission AndroidRuntimePermissions::CheckPermission(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AndroidRuntimePermissions_CheckPermission_m9676FDC4EDE1C338FFC3E778B6F12E19C2401011 (String_t* ___permission0, const RuntimeMethod* method)
{
	{
		// return Permission.Granted;
		return (int32_t)(1);
	}
}
// AndroidRuntimePermissions_Permission[] AndroidRuntimePermissions::CheckPermissions(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* AndroidRuntimePermissions_CheckPermissions_m24C852E59C67909178D03CAABB910D2326643456 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, const RuntimeMethod* method)
{
	{
		// ValidateArgument( permissions );
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = ___permissions0;
		AndroidRuntimePermissions_ValidateArgument_mDCC23BBFDE3060C39D8B20ABA8F0D372457B3A14(L_0, /*hidden argument*/NULL);
		// return GetDummyResult( permissions );
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___permissions0;
		PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* L_2 = AndroidRuntimePermissions_GetDummyResult_mFC7331AF1F204B0F5BB6D07F8D126DF237DC0C37(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// AndroidRuntimePermissions_Permission AndroidRuntimePermissions::RequestPermission(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AndroidRuntimePermissions_RequestPermission_m35016EA84A4A94A6A203F682640BDE27808D0256 (String_t* ___permission0, const RuntimeMethod* method)
{
	{
		// return Permission.Granted;
		return (int32_t)(1);
	}
}
// AndroidRuntimePermissions_Permission[] AndroidRuntimePermissions::RequestPermissions(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* AndroidRuntimePermissions_RequestPermissions_m4B9EB300BF4B380EB5C389E6D5D902B990AF0496 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, const RuntimeMethod* method)
{
	{
		// ValidateArgument( permissions );
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = ___permissions0;
		AndroidRuntimePermissions_ValidateArgument_mDCC23BBFDE3060C39D8B20ABA8F0D372457B3A14(L_0, /*hidden argument*/NULL);
		// return GetDummyResult( permissions );
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___permissions0;
		PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* L_2 = AndroidRuntimePermissions_GetDummyResult_mFC7331AF1F204B0F5BB6D07F8D126DF237DC0C37(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void AndroidRuntimePermissions::RequestPermissionAsync(System.String,AndroidRuntimePermissions_PermissionResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidRuntimePermissions_RequestPermissionAsync_m478F1F61FB20065004EB4F675A2D114D9BA6B339 (String_t* ___permission0, PermissionResult_t6B10043A6076E68C28B3AF4A5D25231B9D35D3C5 * ___callback1, const RuntimeMethod* method)
{
	{
		// if( callback != null )
		PermissionResult_t6B10043A6076E68C28B3AF4A5D25231B9D35D3C5 * L_0 = ___callback1;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		// callback( permission, Permission.Granted );
		PermissionResult_t6B10043A6076E68C28B3AF4A5D25231B9D35D3C5 * L_1 = ___callback1;
		String_t* L_2 = ___permission0;
		NullCheck(L_1);
		PermissionResult_Invoke_m32F0AA474CE06E5032395BE731466AD0CBDB0DBF(L_1, L_2, 1, /*hidden argument*/NULL);
	}

IL_000b:
	{
		// }
		return;
	}
}
// System.Void AndroidRuntimePermissions::RequestPermissionsAsync(System.String[],AndroidRuntimePermissions_PermissionResultMultiple)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidRuntimePermissions_RequestPermissionsAsync_mE94000E0AA742978A8E78E5BF9F12E1EFD8AB834 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, PermissionResultMultiple_t2CD1F439CAC132F6C973F54FAC4F63B834C4B280 * ___callback1, const RuntimeMethod* method)
{
	{
		// ValidateArgument( permissions );
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = ___permissions0;
		AndroidRuntimePermissions_ValidateArgument_mDCC23BBFDE3060C39D8B20ABA8F0D372457B3A14(L_0, /*hidden argument*/NULL);
		// if( callback != null )
		PermissionResultMultiple_t2CD1F439CAC132F6C973F54FAC4F63B834C4B280 * L_1 = ___callback1;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		// callback( permissions, GetDummyResult( permissions ) );
		PermissionResultMultiple_t2CD1F439CAC132F6C973F54FAC4F63B834C4B280 * L_2 = ___callback1;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = ___permissions0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = ___permissions0;
		PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* L_5 = AndroidRuntimePermissions_GetDummyResult_mFC7331AF1F204B0F5BB6D07F8D126DF237DC0C37(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		PermissionResultMultiple_Invoke_m1D91F914B4496A34B16943BBDC43ADB39624EACC(L_2, L_3, L_5, /*hidden argument*/NULL);
	}

IL_0016:
	{
		// }
		return;
	}
}
// AndroidRuntimePermissions_Permission[] AndroidRuntimePermissions::ProcessPermissionRequest(System.String[],System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* AndroidRuntimePermissions_ProcessPermissionRequest_m36D72CEA24CA06FA203C4D76464BC7B6F2995756 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, String_t* ___resultRaw1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidRuntimePermissions_ProcessPermissionRequest_m36D72CEA24CA06FA203C4D76464BC7B6F2995756_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		// if( resultRaw.Length != permissions.Length )
		String_t* L_0 = ___resultRaw1;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_0, /*hidden argument*/NULL);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = ___permissions0;
		NullCheck(L_2);
		if ((((int32_t)L_1) == ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length)))))))
		{
			goto IL_0017;
		}
	}
	{
		// Debug.LogError( "RequestPermissions: something went wrong" );
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(_stringLiteral445D5FAD900CBCFA0E910C25BABCD6438958620F, /*hidden argument*/NULL);
		// return null;
		return (PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75*)(NULL);
	}

IL_0017:
	{
		// bool shouldUpdateCache = false;
		V_0 = (bool)0;
		// Permission[] result = new Permission[permissions.Length];
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = ___permissions0;
		NullCheck(L_3);
		PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* L_4 = (PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75*)(PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75*)SZArrayNew(PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_3)->max_length)))));
		V_1 = L_4;
		// for( int i = 0; i < result.Length; i++ )
		V_2 = 0;
		goto IL_0048;
	}

IL_0026:
	{
		// Permission _permission = resultRaw[i].ToPermission();
		String_t* L_5 = ___resultRaw1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_5, L_6, /*hidden argument*/NULL);
		int32_t L_8 = AndroidRuntimePermissions_ToPermission_m8CA4B19E11156AD2D0BD8F3DCBA7CD9D704944B1(L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		// result[i] = _permission;
		PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* L_9 = V_1;
		int32_t L_10 = V_2;
		int32_t L_11 = V_3;
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (int32_t)L_11);
		// if( CachePermission( permissions[i], _permission ) )
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = ___permissions0;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		String_t* L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		int32_t L_16 = V_3;
		bool L_17 = AndroidRuntimePermissions_CachePermission_m85462AC6DFCDB6707611790CF03A99CE68CFED03(L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0044;
		}
	}
	{
		// shouldUpdateCache = true;
		V_0 = (bool)1;
	}

IL_0044:
	{
		// for( int i = 0; i < result.Length; i++ )
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0048:
	{
		// for( int i = 0; i < result.Length; i++ )
		int32_t L_19 = V_2;
		PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* L_20 = V_1;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length)))))))
		{
			goto IL_0026;
		}
	}
	{
		// if( shouldUpdateCache )
		bool L_21 = V_0;
		if (!L_21)
		{
			goto IL_0056;
		}
	}
	{
		// PlayerPrefs.Save();
		PlayerPrefs_Save_m2C1E628FA335095CD88D0DA1CB50ACC924667EEC(/*hidden argument*/NULL);
	}

IL_0056:
	{
		// return result;
		PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* L_22 = V_1;
		return L_22;
	}
}
// AndroidRuntimePermissions_Permission AndroidRuntimePermissions::GetCachedPermission(System.String,AndroidRuntimePermissions_Permission)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AndroidRuntimePermissions_GetCachedPermission_m59CC59493EB088760A3CB5DBA541F3DFDEF31940 (String_t* ___permission0, int32_t ___defaultValue1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidRuntimePermissions_GetCachedPermission_m59CC59493EB088760A3CB5DBA541F3DFDEF31940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return (Permission) PlayerPrefs.GetInt( "ARTP_" + permission, (int) defaultValue );
		String_t* L_0 = ___permission0;
		String_t* L_1 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral9EDAF9B1F97AF662638A0E9509DEDFFBCE4504EB, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___defaultValue1;
		int32_t L_3 = PlayerPrefs_GetInt_mDA4476C10FCFF55FC65816E5E519B0EAFCB2AC14(L_1, L_2, /*hidden argument*/NULL);
		return (int32_t)(L_3);
	}
}
// System.String AndroidRuntimePermissions::GetCachedPermissions(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AndroidRuntimePermissions_GetCachedPermissions_mD380D1D47209B5DBE60662E7C04B2F42CC681E0B (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidRuntimePermissions_GetCachedPermissions_mD380D1D47209B5DBE60662E7C04B2F42CC681E0B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// StringBuilder cachedPermissions = new StringBuilder( permissions.Length );
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = ___permissions0;
		NullCheck(L_0);
		StringBuilder_t * L_1 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_mEDFFE2D378A15F6DAB54D52661C84C1B52E7BA2E(L_1, (((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_1;
		// for( int i = 0; i < permissions.Length; i++ )
		V_1 = 0;
		goto IL_0021;
	}

IL_000d:
	{
		// cachedPermissions.Append( (int) GetCachedPermission( permissions[i], Permission.ShouldAsk ) );
		StringBuilder_t * L_2 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = ___permissions0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		String_t* L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int32_t L_7 = AndroidRuntimePermissions_GetCachedPermission_m59CC59493EB088760A3CB5DBA541F3DFDEF31940(L_6, 2, /*hidden argument*/NULL);
		NullCheck(L_2);
		StringBuilder_Append_m796285D173EEA5261E85B95FC79DD4F996CC93DD(L_2, L_7, /*hidden argument*/NULL);
		// for( int i = 0; i < permissions.Length; i++ )
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0021:
	{
		// for( int i = 0; i < permissions.Length; i++ )
		int32_t L_9 = V_1;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = ___permissions0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		// return cachedPermissions.ToString();
		StringBuilder_t * L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_11);
		return L_12;
	}
}
// System.Boolean AndroidRuntimePermissions::CachePermission(System.String,AndroidRuntimePermissions_Permission)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AndroidRuntimePermissions_CachePermission_m85462AC6DFCDB6707611790CF03A99CE68CFED03 (String_t* ___permission0, int32_t ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidRuntimePermissions_CachePermission_m85462AC6DFCDB6707611790CF03A99CE68CFED03_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( PlayerPrefs.GetInt( "ARTP_" + permission, -1 ) != (int) value )
		String_t* L_0 = ___permission0;
		String_t* L_1 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral9EDAF9B1F97AF662638A0E9509DEDFFBCE4504EB, L_0, /*hidden argument*/NULL);
		int32_t L_2 = PlayerPrefs_GetInt_mDA4476C10FCFF55FC65816E5E519B0EAFCB2AC14(L_1, (-1), /*hidden argument*/NULL);
		int32_t L_3 = ___value1;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_0027;
		}
	}
	{
		// PlayerPrefs.SetInt( "ARTP_" + permission, (int) value );
		String_t* L_4 = ___permission0;
		String_t* L_5 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral9EDAF9B1F97AF662638A0E9509DEDFFBCE4504EB, L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___value1;
		PlayerPrefs_SetInt_m0C5C977E960B9CA8F9AB73AF4129C3DCABD067B6(L_5, L_6, /*hidden argument*/NULL);
		// return true;
		return (bool)1;
	}

IL_0027:
	{
		// return false;
		return (bool)0;
	}
}
// System.Void AndroidRuntimePermissions::ValidateArgument(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidRuntimePermissions_ValidateArgument_mDCC23BBFDE3060C39D8B20ABA8F0D372457B3A14 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidRuntimePermissions_ValidateArgument_mDCC23BBFDE3060C39D8B20ABA8F0D372457B3A14_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if( permissions == null || permissions.Length == 0 )
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = ___permissions0;
		if (!L_0)
		{
			goto IL_0007;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___permissions0;
		NullCheck(L_1);
		if ((((RuntimeArray*)L_1)->max_length))
		{
			goto IL_0012;
		}
	}

IL_0007:
	{
		// throw new ArgumentException( "Parameter 'permissions' is null or empty!" );
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_2 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_2, _stringLiteral9E010293A81ADD6752DB430F04400D8B1CF2A6E7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, AndroidRuntimePermissions_ValidateArgument_mDCC23BBFDE3060C39D8B20ABA8F0D372457B3A14_RuntimeMethod_var);
	}

IL_0012:
	{
		// for( int i = 0; i < permissions.Length; i++ )
		V_0 = 0;
		goto IL_002f;
	}

IL_0016:
	{
		// if( string.IsNullOrEmpty( permissions[i] ) )
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = ___permissions0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		String_t* L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		bool L_7 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_002b;
		}
	}
	{
		// throw new ArgumentException( "A permission is null or empty!" );
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_8 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_8, _stringLiteral5BE2DC525B76C096079B960B32063FCBD00D9A1B, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, AndroidRuntimePermissions_ValidateArgument_mDCC23BBFDE3060C39D8B20ABA8F0D372457B3A14_RuntimeMethod_var);
	}

IL_002b:
	{
		// for( int i = 0; i < permissions.Length; i++ )
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_002f:
	{
		// for( int i = 0; i < permissions.Length; i++ )
		int32_t L_10 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = ___permissions0;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length)))))))
		{
			goto IL_0016;
		}
	}
	{
		// }
		return;
	}
}
// AndroidRuntimePermissions_Permission[] AndroidRuntimePermissions::GetDummyResult(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* AndroidRuntimePermissions_GetDummyResult_mFC7331AF1F204B0F5BB6D07F8D126DF237DC0C37 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidRuntimePermissions_GetDummyResult_mFC7331AF1F204B0F5BB6D07F8D126DF237DC0C37_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// Permission[] result = new Permission[permissions.Length];
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = ___permissions0;
		NullCheck(L_0);
		PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* L_1 = (PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75*)(PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75*)SZArrayNew(PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length)))));
		V_0 = L_1;
		// for( int i = 0; i < result.Length; i++ )
		V_1 = 0;
		goto IL_0015;
	}

IL_000d:
	{
		// result[i] = Permission.Granted;
		PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (int32_t)1);
		// for( int i = 0; i < result.Length; i++ )
		int32_t L_4 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0015:
	{
		// for( int i = 0; i < result.Length; i++ )
		int32_t L_5 = V_1;
		PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* L_6 = V_0;
		NullCheck(L_6);
		if ((((int32_t)L_5) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		// return result;
		PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* L_7 = V_0;
		return L_7;
	}
}
// AndroidRuntimePermissions_Permission AndroidRuntimePermissions::ToPermission(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AndroidRuntimePermissions_ToPermission_m8CA4B19E11156AD2D0BD8F3DCBA7CD9D704944B1 (Il2CppChar ___ch0, const RuntimeMethod* method)
{
	{
		// return (Permission) ( ch - '0' );
		Il2CppChar L_0 = ___ch0;
		return (int32_t)(((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)((int32_t)48))));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_PermissionResult_t6B10043A6076E68C28B3AF4A5D25231B9D35D3C5 (PermissionResult_t6B10043A6076E68C28B3AF4A5D25231B9D35D3C5 * __this, String_t* ___permission0, int32_t ___result1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___permission0' to native representation
	char* ____permission0_marshaled = NULL;
	____permission0_marshaled = il2cpp_codegen_marshal_string(___permission0);

	// Native function invocation
	il2cppPInvokeFunc(____permission0_marshaled, ___result1);

	// Marshaling cleanup of parameter '___permission0' native representation
	il2cpp_codegen_marshal_free(____permission0_marshaled);
	____permission0_marshaled = NULL;

}
// System.Void AndroidRuntimePermissions_PermissionResult::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PermissionResult__ctor_m00953D0D07D62FA440F530249C7D36E088041A57 (PermissionResult_t6B10043A6076E68C28B3AF4A5D25231B9D35D3C5 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void AndroidRuntimePermissions_PermissionResult::Invoke(System.String,AndroidRuntimePermissions_Permission)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PermissionResult_Invoke_m32F0AA474CE06E5032395BE731466AD0CBDB0DBF (PermissionResult_t6B10043A6076E68C28B3AF4A5D25231B9D35D3C5 * __this, String_t* ___permission0, int32_t ___result1, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (String_t*, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___permission0, ___result1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, String_t*, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___permission0, ___result1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< int32_t >::Invoke(targetMethod, ___permission0, ___result1);
					else
						GenericVirtActionInvoker1< int32_t >::Invoke(targetMethod, ___permission0, ___result1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___permission0, ___result1);
					else
						VirtActionInvoker1< int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___permission0, ___result1);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(&___result1) - 1), targetMethod);
				}
				typedef void (*FunctionPointerType) (String_t*, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___permission0, ___result1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< String_t*, int32_t >::Invoke(targetMethod, targetThis, ___permission0, ___result1);
					else
						GenericVirtActionInvoker2< String_t*, int32_t >::Invoke(targetMethod, targetThis, ___permission0, ___result1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< String_t*, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___permission0, ___result1);
					else
						VirtActionInvoker2< String_t*, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___permission0, ___result1);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___permission0) - 1), ___result1, targetMethod);
				}
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (String_t*, int32_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___permission0, ___result1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, String_t*, int32_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___permission0, ___result1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult AndroidRuntimePermissions_PermissionResult::BeginInvoke(System.String,AndroidRuntimePermissions_Permission,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PermissionResult_BeginInvoke_mBD30EDF3AB3D28A043775BAD07C015322B6315EC (PermissionResult_t6B10043A6076E68C28B3AF4A5D25231B9D35D3C5 * __this, String_t* ___permission0, int32_t ___result1, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PermissionResult_BeginInvoke_mBD30EDF3AB3D28A043775BAD07C015322B6315EC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___permission0;
	__d_args[1] = Box(Permission_t472A96823E2CCCEAE2337194F2F9C6D377C72DB5_il2cpp_TypeInfo_var, &___result1);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void AndroidRuntimePermissions_PermissionResult::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PermissionResult_EndInvoke_mB75D0E705715602085F7BBA4C094E0826CCA5B54 (PermissionResult_t6B10043A6076E68C28B3AF4A5D25231B9D35D3C5 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_PermissionResultMultiple_t2CD1F439CAC132F6C973F54FAC4F63B834C4B280 (PermissionResultMultiple_t2CD1F439CAC132F6C973F54FAC4F63B834C4B280 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* ___result1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char**, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___permissions0' to native representation
	char** ____permissions0_marshaled = NULL;
	if (___permissions0 != NULL)
	{
		il2cpp_array_size_t ____permissions0_Length = (___permissions0)->max_length;
		____permissions0_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____permissions0_Length + 1);
		(____permissions0_marshaled)[____permissions0_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____permissions0_Length); i++)
		{
			(____permissions0_marshaled)[i] = il2cpp_codegen_marshal_string((___permissions0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____permissions0_marshaled = NULL;
	}

	// Marshaling of parameter '___result1' to native representation
	int32_t* ____result1_marshaled = NULL;
	if (___result1 != NULL)
	{
		____result1_marshaled = reinterpret_cast<int32_t*>((___result1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	il2cppPInvokeFunc(____permissions0_marshaled, ____result1_marshaled);

	// Marshaling cleanup of parameter '___permissions0' native representation
	if (____permissions0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____permissions0_marshaled_CleanupLoopCount = (___permissions0 != NULL) ? (___permissions0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____permissions0_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____permissions0_marshaled)[i]);
			(____permissions0_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____permissions0_marshaled);
		____permissions0_marshaled = NULL;
	}

}
// System.Void AndroidRuntimePermissions_PermissionResultMultiple::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PermissionResultMultiple__ctor_mB2D086F0E2B4AE39AF2DE99E318B02926E3956E1 (PermissionResultMultiple_t2CD1F439CAC132F6C973F54FAC4F63B834C4B280 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void AndroidRuntimePermissions_PermissionResultMultiple::Invoke(System.String[],AndroidRuntimePermissions_Permission[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PermissionResultMultiple_Invoke_m1D91F914B4496A34B16943BBDC43ADB39624EACC (PermissionResultMultiple_t2CD1F439CAC132F6C973F54FAC4F63B834C4B280 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* ___result1, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___permissions0, ___result1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___permissions0, ___result1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* >::Invoke(targetMethod, ___permissions0, ___result1);
					else
						GenericVirtActionInvoker1< PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* >::Invoke(targetMethod, ___permissions0, ___result1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___permissions0, ___result1);
					else
						VirtActionInvoker1< PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___permissions0, ___result1);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___result1) - 1), targetMethod);
				}
				typedef void (*FunctionPointerType) (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___permissions0, ___result1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* >::Invoke(targetMethod, targetThis, ___permissions0, ___result1);
					else
						GenericVirtActionInvoker2< StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* >::Invoke(targetMethod, targetThis, ___permissions0, ___result1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___permissions0, ___result1);
					else
						VirtActionInvoker2< StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___permissions0, ___result1);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___permissions0) - 1), ___result1, targetMethod);
				}
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___permissions0, ___result1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___permissions0, ___result1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult AndroidRuntimePermissions_PermissionResultMultiple::BeginInvoke(System.String[],AndroidRuntimePermissions_Permission[],System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PermissionResultMultiple_BeginInvoke_m1F0B2F3290CF7D4B1C52808BB301E2E9E8B2775D (PermissionResultMultiple_t2CD1F439CAC132F6C973F54FAC4F63B834C4B280 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___permissions0, PermissionU5BU5D_t09AF5BB71E81A0BAAA9BA494DD14DA9A0BA13B75* ___result1, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___permissions0;
	__d_args[1] = ___result1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void AndroidRuntimePermissions_PermissionResultMultiple::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PermissionResultMultiple_EndInvoke_mEE91A3FFC0DF63641B2795353C5FD8723182F3EE (PermissionResultMultiple_t2CD1F439CAC132F6C973F54FAC4F63B834C4B280 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
