﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E (void);
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 (void);
// 0x00000003 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 (void);
// 0x00000004 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000007 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000008 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000009 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000A System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000B System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000C System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000000E TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000000F System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000010 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000011 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000012 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000013 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000014 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000015 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000016 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000017 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000018 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000019 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000001A System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000001B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000001D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000001E System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000001F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000020 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000021 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000022 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000023 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000024 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000025 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000026 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000027 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000028 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000029 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000002A System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000002B System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000002C System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000002D System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000002E System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000002F System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000030 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000031 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000032 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000033 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000034 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000035 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000036 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000037 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000038 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000039 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000003A System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000003B System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000003C System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000003D System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000003E System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x0000003F System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000040 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000041 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000042 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[66] = 
{
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[66] = 
{
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[16] = 
{
	{ 0x02000004, { 34, 4 } },
	{ 0x02000005, { 38, 9 } },
	{ 0x02000006, { 47, 7 } },
	{ 0x02000007, { 54, 10 } },
	{ 0x02000008, { 64, 1 } },
	{ 0x02000009, { 65, 21 } },
	{ 0x0200000B, { 86, 2 } },
	{ 0x06000004, { 0, 10 } },
	{ 0x06000005, { 10, 5 } },
	{ 0x06000006, { 15, 1 } },
	{ 0x06000007, { 16, 5 } },
	{ 0x06000008, { 21, 4 } },
	{ 0x06000009, { 25, 3 } },
	{ 0x0600000A, { 28, 1 } },
	{ 0x0600000B, { 29, 3 } },
	{ 0x0600000C, { 32, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[88] = 
{
	{ (Il2CppRGCTXDataType)2, 8691 },
	{ (Il2CppRGCTXDataType)3, 6493 },
	{ (Il2CppRGCTXDataType)2, 8692 },
	{ (Il2CppRGCTXDataType)2, 8693 },
	{ (Il2CppRGCTXDataType)3, 6494 },
	{ (Il2CppRGCTXDataType)2, 8694 },
	{ (Il2CppRGCTXDataType)2, 8695 },
	{ (Il2CppRGCTXDataType)3, 6495 },
	{ (Il2CppRGCTXDataType)2, 8696 },
	{ (Il2CppRGCTXDataType)3, 6496 },
	{ (Il2CppRGCTXDataType)2, 8697 },
	{ (Il2CppRGCTXDataType)3, 6497 },
	{ (Il2CppRGCTXDataType)3, 6498 },
	{ (Il2CppRGCTXDataType)2, 5511 },
	{ (Il2CppRGCTXDataType)3, 6499 },
	{ (Il2CppRGCTXDataType)3, 6500 },
	{ (Il2CppRGCTXDataType)3, 6501 },
	{ (Il2CppRGCTXDataType)2, 8698 },
	{ (Il2CppRGCTXDataType)2, 5515 },
	{ (Il2CppRGCTXDataType)2, 8699 },
	{ (Il2CppRGCTXDataType)2, 5517 },
	{ (Il2CppRGCTXDataType)2, 8700 },
	{ (Il2CppRGCTXDataType)2, 8701 },
	{ (Il2CppRGCTXDataType)2, 5518 },
	{ (Il2CppRGCTXDataType)2, 8702 },
	{ (Il2CppRGCTXDataType)2, 5520 },
	{ (Il2CppRGCTXDataType)2, 8703 },
	{ (Il2CppRGCTXDataType)3, 6502 },
	{ (Il2CppRGCTXDataType)2, 5523 },
	{ (Il2CppRGCTXDataType)2, 5525 },
	{ (Il2CppRGCTXDataType)2, 8704 },
	{ (Il2CppRGCTXDataType)3, 6503 },
	{ (Il2CppRGCTXDataType)2, 8705 },
	{ (Il2CppRGCTXDataType)2, 5528 },
	{ (Il2CppRGCTXDataType)3, 6504 },
	{ (Il2CppRGCTXDataType)3, 6505 },
	{ (Il2CppRGCTXDataType)2, 5532 },
	{ (Il2CppRGCTXDataType)3, 6506 },
	{ (Il2CppRGCTXDataType)3, 6507 },
	{ (Il2CppRGCTXDataType)2, 5541 },
	{ (Il2CppRGCTXDataType)2, 8706 },
	{ (Il2CppRGCTXDataType)3, 6508 },
	{ (Il2CppRGCTXDataType)3, 6509 },
	{ (Il2CppRGCTXDataType)2, 5543 },
	{ (Il2CppRGCTXDataType)2, 8618 },
	{ (Il2CppRGCTXDataType)3, 6510 },
	{ (Il2CppRGCTXDataType)3, 6511 },
	{ (Il2CppRGCTXDataType)3, 6512 },
	{ (Il2CppRGCTXDataType)2, 5550 },
	{ (Il2CppRGCTXDataType)2, 8707 },
	{ (Il2CppRGCTXDataType)3, 6513 },
	{ (Il2CppRGCTXDataType)3, 6514 },
	{ (Il2CppRGCTXDataType)3, 6167 },
	{ (Il2CppRGCTXDataType)3, 6515 },
	{ (Il2CppRGCTXDataType)3, 6516 },
	{ (Il2CppRGCTXDataType)2, 5559 },
	{ (Il2CppRGCTXDataType)2, 8708 },
	{ (Il2CppRGCTXDataType)3, 6517 },
	{ (Il2CppRGCTXDataType)3, 6518 },
	{ (Il2CppRGCTXDataType)3, 6519 },
	{ (Il2CppRGCTXDataType)3, 6520 },
	{ (Il2CppRGCTXDataType)3, 6521 },
	{ (Il2CppRGCTXDataType)3, 6173 },
	{ (Il2CppRGCTXDataType)3, 6522 },
	{ (Il2CppRGCTXDataType)3, 6523 },
	{ (Il2CppRGCTXDataType)3, 6524 },
	{ (Il2CppRGCTXDataType)2, 8709 },
	{ (Il2CppRGCTXDataType)3, 6525 },
	{ (Il2CppRGCTXDataType)3, 6526 },
	{ (Il2CppRGCTXDataType)3, 6527 },
	{ (Il2CppRGCTXDataType)2, 5573 },
	{ (Il2CppRGCTXDataType)3, 6528 },
	{ (Il2CppRGCTXDataType)3, 6529 },
	{ (Il2CppRGCTXDataType)2, 5576 },
	{ (Il2CppRGCTXDataType)3, 6530 },
	{ (Il2CppRGCTXDataType)1, 8710 },
	{ (Il2CppRGCTXDataType)2, 5575 },
	{ (Il2CppRGCTXDataType)3, 6531 },
	{ (Il2CppRGCTXDataType)1, 5575 },
	{ (Il2CppRGCTXDataType)1, 5573 },
	{ (Il2CppRGCTXDataType)2, 8711 },
	{ (Il2CppRGCTXDataType)2, 5575 },
	{ (Il2CppRGCTXDataType)3, 6532 },
	{ (Il2CppRGCTXDataType)3, 6533 },
	{ (Il2CppRGCTXDataType)3, 6534 },
	{ (Il2CppRGCTXDataType)2, 5574 },
	{ (Il2CppRGCTXDataType)3, 6535 },
	{ (Il2CppRGCTXDataType)2, 5587 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	66,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	16,
	s_rgctxIndices,
	88,
	s_rgctxValues,
	NULL,
};
